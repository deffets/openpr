function handles = generateToyData(handles)
    path = fullfile(getRegguiPath('examples'), 'data', 'Giraffe');
    
    
    % Proton radiograph
    bImage = handles2BraggImage('GT270', handles);
    
    simuData = bImage.get('data');
    PRSpacing = bImage.get('spacing');
    
    spotsNb = [9 9];
    
    writeMLICData(fullfile(path, 'PR'), 'pr_giraffe', simuData, spotsNb, PRSpacing);
    
    
    % Reference IDD
    referenceName = 'ref270';
    ref = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, referenceName))
            ref = handles.mydata.info{i}.raw ;
        end
    end
    if(isempty(ref))
        error('Reference curve not found in the current list')
    end
    
    
    PRSpacing = [5 5];
    spotsNb = [9 9];
    
    writeMLICData(path, 'referenceIDD', ref, spotsNb, PRSpacing);
    delete(fullfile(path, 'files_order.csv'));
end