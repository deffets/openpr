
% This example shows how to generate a valid .mat file to be loaded in
% openPR.
%
% The data shall be a 3D matrix (m x n x o) where m and n are the number of
% spots along the scanning directions and o is the amount of sampling 
% points of the integral depth dose profiles (IDD).
%
% The data stored in the .mat file shall be directly accessible through importdata().

% In this example, we assume that we irradiate a grid of 9 x 9 spots and 
% that the MLIC has 180 channels.

your_path = ''; % Where you want to store the .mat file. Default = current directory.

 m = 9;
 n = 9;
 o = 180;

 data = zeros(m, n, o);

 % We start with a pristine Bragg curve
 referenceIDD = [28 28 28 28 27 28 28 29 29 29 29 29 30 30 30 30 30 30 30 31 30 30 30 31 30 31 31 31 31 31 31 31 31 31 31 32 31 31 31 32 31 32 31 31 32 31 31 32 32 32 32 32 32 32 33 32 32 32 32 32 32 34 32 33 32 34 32 33 33 33 33 34 33 33 34 34 33 34 34 34 34 34 34 35 34 35 35 35 35 35 35 36 35 36 35 36 36 36 36 37 36 37 37 37 38 37 38 38 38 39 38 39 39 39 40 39 41 40 41 42 41 42 42 43 44 44 44 45 45 47 47 48 49 50 50 52 53 55 55 58 59 63 64 68 74 80 86 98 104 111 97 78 50 25 10 3 1 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0];

 % We assume that the pristine Bragg curve was sampled every 1 mm
 mlicDepth = 0:o-1;

 % We perform some random shifts of the IDDs like if an object with many different thicknesses was present in the beam path
 for i=1 : m
  for j=1 : n
   randomShift = randi(50);

   data(i, j, :) = interp1(mlicDepth, referenceIDD, mlicDepth+randomShift, 'linear', 'extrap');
  end
 end

 % Save the data in the .mat file using the same name for the data and the file so that we can later directly access the data with importdata.
 save(fullfile(your_path, 'data.mat'), 'data');
