
function [handles,test_result] = mainTest(handles)

test_result = true;

    try
        logFile = fullfile(getRegguiPath('userData'), 'openPR_mainTest.log');
        fid = fopen(logFile, 'w');
    catch
        test_result = false;
        error('Could not create log file. Test aborted');
    end
    
    try
        disp('Testing loadPR ...')
        fprintf(fid, 'loadPR\n');
    	handles = loadPR(fullfile(getRegguiPath('examples'), 'data', 'Giraffe', 'PR', 'files_order.csv'), '', [], [5  5], [9  9], 'prGiraffe', handles);
        fprintf(fid, 'Succeeded.\n');
    catch ME
        test_result = false;
        disp('Error during loadPR. See logs.')
        fprintf(fid, getReport(ME));
    end
    
    try
        disp('Testing loadReference ...')
        fprintf(fid, 'loadReference\n');
    	handles = loadReference(fullfile(getRegguiPath('examples'), 'data', 'Giraffe', 'referenceIDD1_1.csv'), '', [], [9  9], 210, 'referenceIDD', handles);
        fprintf(fid, 'Succeeded.\n');
    catch ME
        test_result = false;
        disp('Error during loadReference. See logs.')
        fprintf(fid, getReport(ME));
    end
    
    try
        disp('Testing loadCalibration ...')
        fprintf(fid, 'loadCalibration\n');
    	handles = loadCalibration(fullfile(getRegguiPath('examples'), 'data', 'calibration', 'generic_curve.txt'), fullfile(getRegguiPath('examples'), 'data', 'calibration', 'materials.txt'), 'calib', handles);
        fprintf(fid, 'Succeeded.\n');
    catch ME
        test_result = false;
        disp('Error during loadCalibration. See logs.')
        fprintf(fid, getReport(ME));
    end
    
    try
        disp('Testing saveRegistration ...')
        fprintf(fid, 'saveRegistration\n');
    	handles = saveRegistration('prGiraffe', 'prGiraffe', [0  0  0], 'Reg1', 'Reg2', handles);
        fprin
        test_result = false;
        disp('Error during saveRegistration. See logs.')
        fprintf(fid, getReport(ME));
    end
    
    try
        disp('Testing applyFlipTransform ...')
        fprintf(fid, 'applyFlipTransform\n');
    	handles = applyFlipTransform('prGiraffe', [1   2  90   1  90   2], handles);
        fprintf(fid, 'Succeeded.\n');
    catch ME
        test_result = false;
        disp('Error during applyFlipTransform. See logs.')
        fprintf(fid, getReport(ME));
    end
    
    try
        disp('Testing shiftPRInstr ...')
        fprintf(fid, 'shiftPRInstr\n');
    	handles = shiftPRInstr('prGiraffe', 0, 'prGiraffeshifted', handles);
        fprintf(fid, 'Succeeded.\n');
    catch ME
        test_result = false;
        disp('Error during shiftPRInstr. See logs.')
        fprintf(fid, getReport(ME));
    end
    
%     try
%         disp('Testing MISRInstruction ...')
%         fprintf(fid, 'MISRInstruction\n');
%     	handles = MISRInstruction('prGiraffe', 'referenceIDD', 3, 500, 'WETMISR', handles);
%         fprintf(fid, 'Succeeded.\n');
%     catch ME
%         disp('Error during MISRInstruction. See logs.')
%         fprintf(fid, getReport(ME));
%     end
        
    fclose(fid);
end
