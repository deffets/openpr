
%Proton_Radiography
%
% instructions = Proton_Radiography(handles) launches the GUI of the proton
% radiography plugin.
%
% Input arguments:
%   handles - openREGGUI handles
%
% Authors : S. Deffet
%

function instructions = Proton_Radiography(handles)

% hObject = handles.figure1;
% 
% handles.instructions{length(handles.instructions)+1} = 'handles = progressControlInstruction(handles) ;';
% handles = Execute_reggui_instructions(handles) ;
% 
% guidata(hObject, handles) ;
% 
% PRadioControl(hObject) ;
% 
% guidata(hObject, handles) ;

instructions{1} = 'handles = progressControlInstruction(handles) ;' ;
instructions{2} = 'guidata(handles.regguiC_gui, handles)' ;
instructions{3} = 'PRadioControl(handles.regguiC_gui) ;' ;
