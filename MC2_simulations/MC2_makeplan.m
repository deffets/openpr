function makeplan(outputDir,fieldSizeX, fieldSizeY, spotSpacing, gantryAngle, isocenter,  energy)

if nargin<1
    outputDir = '';
end
MU = 1;
nbSpotsX = fieldSizeX./spotSpacing+1;
nbSpotsY = fieldSizeY./spotSpacing+1;
nbSpots = nbSpotsX*nbSpotsY;
totalMU = MU*nbSpots;

Plan_text = cell(0);
Plan_text{end+1} = '#TREATMENT-PLAN-DESCRIPTION';
Plan_text{end+1} = '#PlanName';
Plan_text{end+1} = 'plan PR auto-generated';
Plan_text{end+1} = '#NumberOfFractions';
Plan_text{end+1} = '1';
Plan_text{end+1} = '##FractionID';
Plan_text{end+1} = '1';
Plan_text{end+1} = '##NumberOfFields';
Plan_text{end+1} = '1';
Plan_text{end+1} = '###FieldsID';
Plan_text{end+1} = '1';
Plan_text{end+1} = '#TotalMetersetWeightOfAllFields';
Plan_text{end+1} = num2str(totalMU);

Plan_text{end+1} = ' ';
Plan_text{end+1} = '#FIELD-DESCRIPTION';
Plan_text{end+1} = '###FieldID';
Plan_text{end+1} = '1';
Plan_text{end+1} = '###FinalCumulativeMeterSetWeight';
Plan_text{end+1} = num2str(totalMU);
Plan_text{end+1} = '###GantryAngle';
Plan_text{end+1} = num2str(gantryAngle);
Plan_text{end+1} = '###PatientSupportAngle';
Plan_text{end+1} = '0';
Plan_text{end+1} = '###IsocenterPosition';
Plan_text{end+1} = num2str(isocenter) ;
Plan_text{end+1} = '###NumberOfControlPoints';
Plan_text{end+1} = '1';
Plan_text{end+1} = ' ';
Plan_text{end+1} = '#SPOTS-DESCRIPTION';


Plan_text{end+1} = '#SPOTS-DESCRIPTION';
Plan_text{end+1} = '####ControlPointIndex';
Plan_text{end+1} = '1';
Plan_text{end+1} = '####SpotTunnedID';
Plan_text{end+1} = '1';
Plan_text{end+1} = '####CumulativeMetersetWeight';
Plan_text{end+1} = num2str(totalMU);
Plan_text{end+1} = '####Energy (MeV)';
Plan_text{end+1} = num2str(energy);
Plan_text{end+1} = '####NbOfScannedSpots';
Plan_text{end+1} = num2str(nbSpots);
Plan_text{end+1} = '####X Y Weight';
for indexX=1:nbSpotsX
    posX = (fieldSizeX/2)-(indexX-1)*spotSpacing;
    for indexY=1:nbSpotsY
        posY = (fieldSizeY/2)-(indexY-1)*spotSpacing;
        Plan_text{end+1} = [num2str(posX),' ',num2str(posY),' ',num2str(MU)];
        
    end
end

fid = fopen(fullfile(outputDir,'PlanPencil.txt'),'w');
for i=1:length(Plan_text)
    fprintf(fid,[Plan_text{i},'\n']);
end
fclose(fid);

end