
function stub(handles)
handles.dataPath = pwd;

[image_name,~] = Image_list(handles,'Select CT',1);

default_name = [handles.dataPath,'/MC2_FP_simulation'];
[filename,simu_dir] = uiputfile({'*.mat', 'Mat file'},'Select output folder and file name to save simulation output',default_name);

SimulatedProtons = 1e4;
max_energy = 180;
energy_step = 2.5;
nb_layers = 32;
fieldSizeX = 400;
fieldSizeY = 300;
spot_spacing = 20;
gantry_angle = 270;
isocenter = '256 256 256';

MC2_FP_simulation(image_name, simu_dir, handles, SimulatedProtons, max_energy, energy_step, nb_layers, fieldSizeX, fieldSizeY, spot_spacing, gantry_angle, isocenter)

end
