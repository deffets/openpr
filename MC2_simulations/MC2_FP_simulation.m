function handles = MC2_simulation(image_name, SimulatedProtons, max_energy, energy_step, nb_layers, fieldSizeX, fieldSizeY, spot_spacing, gantry_angle, isocenter, simu_dir, handles)

current_dir = pwd;
MC2_Interface_path = fullfile(handles.path,'../REGGUI-mc2/functions');
MCsquare_path = fullfile(handles.path,'../REGGUI-mc2/lib');

    DoseToWater = 0;

    BDL_File = fullfile(MCsquare_path, 'BDL/TRENTO_GTR2.txt');

    CT_CalibFiles = fullfile(MCsquare_path, 'Scanners/UCL_Toshiba');

try
    cd(MC2_Interface_path);
catch
    disp('MC2 directory not found')
    cd(current_dir)
    return
end

% find ct image and plan in the lists
ct = [];
for i=1:length(handles.mydata.name)
    if(strcmp(handles.mydata.name{i},image_name))
        ct = handles.mydata.data{i};
        info = handles.mydata.info{i};
    end
end
for i=1:length(handles.images.name)
    if(strcmp(handles.images.name{i},image_name))
        ct = handles.images.data{i};
        info = handles.images.info{i};
    end
end
if(isempty(ct))
    disp('Image not found. Abort.')
    return
end
plan = [];

% create folder
mkdir(simu_dir)

% Export CT image
% Adds 2mm of water to "simulate" the flat-panel
ct(size(ct,1)-2:size(ct,1),:,:)= 0;

ct = flipdim(ct, 1);
ct = flipdim(ct, 2);
disp(['Export MHD CT: ' fullfile(simu_dir, 'CT.mhd')]);
% Write header file (info)
fid = fopen(fullfile(simu_dir, 'CT.mhd'), 'w', 'l');
fprintf(fid,'ObjectType = Image\n');
fprintf(fid,'NDims = 3\n');
fprintf(fid,'DimSize = %d %d %d\n', size(ct));
fprintf(fid,'ElementSpacing = %f %f %f\n', info.Spacing);
fprintf(fid,'ElementType = MET_FLOAT\n');
fprintf(fid,'ElementByteOrderMSB = False\n');
fprintf(fid,'ElementDataFile = CT.raw\n');
fclose(fid);
% Write binary file (data)
fid = fopen(fullfile(simu_dir, 'CT.raw'), 'w', 'l');
fwrite(fid, ct, 'float', 0, 'l');
fclose(fid);


for layer=1:nb_layers
    tic
    disp(['Simulating Layer ' num2str(layer)]);
    energy = max_energy - layer*energy_step;
    
    % (conversion from cm to mm)
    MC2_makeplan(simu_dir, 10*fieldSizeX, 10*fieldSizeY, 10*spot_spacing, gantry_angle, isocenter,  energy);
        
    % Generate MC2 configuration file
    MC2_Config = Generate_MC2_Config(simu_dir, SimulatedProtons, 'CT.mhd', 'PlanPencil.txt', CT_CalibFiles, BDL_File);
    if(DoseToWater == 1)
       %MC2_Config.DoseToWater = 'PostProcessing';
        MC2_Config.DoseToWater = 'OnlineSPR';
    end
    Export_MC2_Config(MC2_Config);
    
    % Run simulation
    try
        MC2_compute(simu_dir, MCsquare_path);
    catch
        fprintf(2,'    ERROR during MCsquare instruction preparation:');
        err = lasterror;
        disp([' ',err.message]);
        disp(err.stack(1));
        cd(current_dir)
        return
    end
    
    
    % Import resulting dose map (image)
   % try
        cd(MC2_Interface_path);
        disp(['Read MHD dose: ' fullfile(simu_dir,'Outputs','Dose.mhd')]);
        Dose_info = mha_read_header(fullfile(simu_dir,'Outputs','Dose.mhd'));
        Dose_data = mha_read_volume(Dose_info);
%     catch
%         disp('Could not open output dose map');
%         err = lasterror;
%         disp(['    ',err.message]);
%         disp(err.stack(1));
%         cd(current_dir)
%         return
%     end
    
    % Convert dose grid for compatibility with MCsquare
    Dose_data = flipdim(Dose_data, 1);
    Dose_data = flipdim(Dose_data, 2);
    
    FP_dose(:,:,layer) = sum(Dose_data(size(ct,1)-2:size(ct,1),:,:),1);
    save(fullfile(simu_dir,['Outputs\FP_Dose.mat']),'FP_dose');
    toc
end


cd(current_dir)
