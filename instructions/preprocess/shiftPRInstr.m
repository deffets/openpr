
%shiftPRInstr
% Shifts all the Bragg curves of a proton radiograph.
%
% handles = shiftPRInstr(PRName, shift_mm, newName, handles) shifts all
% Bragg curves of a proton radiograph.  The resulting data are stored in 
% handles.
%
% Input arguments:
%   PRName - name of the proton radiograph (in openREGGUI handles)
%   shift_mm - shift (same unit as the depth vector of the proton
%              radiograph)
%   newName - name of the resulting proton radiograph
%   handles - openREGGUI handles
%
% Authors : S. Deffet
%

function handles = shiftPRInstr(PRName, shift_mm, newName, handles)
    braggImage = handles2BraggImage(PRName, handles) ;
    
    braggData = braggImage.get('data') ;
    
    braggData = shiftPR(braggData, braggImage.get('depth'), shift_mm*ones(size(braggData, 1), size(braggData, 2))) ;
    
    disp('Adding PR to the list...')
    
    myInfo.Type = 'PR SIMU' ;
    myInfo.spacing = braggImage.get('spacing') ;
    myInfo.depth = braggImage.get('depth') ;
    myInfo.origin = braggImage.get('origin') ;
    
    myImageName = check_existing_names(newName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(braggData) ;
end
