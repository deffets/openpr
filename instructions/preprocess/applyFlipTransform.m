
%applyFlipTransform
% Flip and rotate a proton radiograph (in place)
%
% handles = applyFlipTransform(name, transform, handles) applies a sequence
% of flip and rotation to a proton radiograph and update the proton
% radiography data in handles.
%
% Input arguments:
%   name - Name of the proton radiograph
%   transform - vector representing the sequence of operation. The first
%               element is the first transform to be performed. If
%               transform(i)==1, data are flip according to Matlab built-in
%               flip(data, 1)
%               transform(i)==2, data are flip according to Matlab built-in
%               flip(data, 2)
%               transform(i)==3, data are rotated according to Matlab
%               built-in rot90(data)
%   handles - openREGGUI handles
%
% See also applyTransform, saveRegistration
%
% Authors : S. Deffet
%

function handles = applyFlipTransform(name, transform, handles)
    for i=1 : length(handles.mydata.name)
        if strcmp(handles.mydata.name{i}, name)
            data = handles.mydata.data{i} ;
            
            for j=1 : length(transform)
                switch transform(j)
                    case 1
                        data = flip(data, 1) ;
                    case 2
                        data = flip(data, 2) ;
                    case 90
                        data = rot90(data) ;
                end
            end
            
            handles.mydata.data{i} = data ;
            break ;
        end
    end
end
