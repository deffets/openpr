

function handles = interpolatePRInstruction(PRName, referenceName, newName, handles)
    braggImage = handles2BraggImage(PRName, handles) ;
    
    depth = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, referenceName))
            depth = handles.mydata.info{i}.depth ;
        end
    end
    if(isempty(depth))
        error('Reference curve not found in the current list')
    end
    
    braggData = interpPR(braggImage.get('data'), braggImage.get('depth'), depth);
    
    disp('Adding PR to the list...')
    
    myInfo.Type = 'PR SIMU' ;
    myInfo.spacing = braggImage.get('spacing') ;
    myInfo.depth = depth ;
    myInfo.origin = braggImage.get('origin') ;
    
    myImageName = check_existing_names(newName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(braggData) ;
end

