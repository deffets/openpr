
%MISRInstruction
% WET-map estimation by sparse deconvolution of proton radiography data
%
% handles = MISRInstruction(PRName, referenceName, sigma, lambda, newName, 
% handles) estimates a WET map from a proton radiograph via the sparse 
% deconvolution proposed by Deffet et al 2019.
%
% Input arguments:
%   PRName - name of the proton radiograph (in openREGGUI handles)
%   referenceName - name of the reference Bragg curve (in openREGGUI 
%   handles)
%   sigma - sigma of the beam
%   lambda - hyper-parameter enforcing the sparsity
%   newName - name to give to the estimated WET-map
%   handles - openREGGUI handles
%
% Authors : S. Deffet
%

function handles = MISRInstruction(PRName, referenceName, WEPLName, sigma, lambda, newName, handles)
    outSpacing = [1 1];
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('runningMsg', 'MISR');
        handles.progressControl.set('progressMsg', 'Loading data');
    end
    
    ref = [];
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, referenceName))
            ref = handles.mydata.data{i};
            depth = handles.mydata.info{i}.depth;
        end
    end
    if(isempty(ref))
        error('Reference curve not found in the current list')
    end

    
    % PR
    braggImage = handles2BraggImage(PRName, handles);
    bSpacing = braggImage.get('spacing');
    pr = braggImage.get('data'); 
    
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.2);
        handles.progressControl.set('progressMsg', 'Spatial deconvolution');
    end
    
     
    
    if ~isempty(WEPLName)
        [worldLimit1, worldLimit2] = computeWorldLimits(braggImage.get('XWorldLimits'), ...
                braggImage.get('YWorldLimits'), braggImage.get('XWorldLimits'), ...
                braggImage.get('YWorldLimits'), bSpacing);
            worldLimit2(1) = worldLimit2(1) - bSpacing(1);
            worldLimit2(2) = worldLimit2(2) - bSpacing(2);

        wetIm = handles2Image2D(WEPLName, handles);
        wetInit = wetIm.getData(worldLimit1, worldLimit2, outSpacing);
    else
        wetInit = [];
    end
    

	opt.nIter = 30;
    opt.gamma = 1;
    opt.lambda = lambda;
    opt.rescaleIDD = 1;
    opt.gammaMax = 999999;
    opt.MaxFunEvals = 4;
    opt.bisection = 1;

    
    wet = deconv_pr_compressive(pr, bSpacing, outSpacing, ref, depth, sigma, opt, wetInit);

    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.9);
        handles.progressControl.set('progressMsg', 'Saving');
    end
    
    myInfo.Type = 'WET';
    myInfo.spacing = [1 1];
    myInfo.origin = braggImage.get('origin');
    
    disp('Adding WET-map to the list...')
    myImageName = check_existing_names(newName, handles.mydata.name);
    myImageName = check_existing_names(myImageName, handles.images.name);
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo;
    handles.mydata.data{length(handles.mydata.data)+1} = single(wet);
    
	if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.deleteProgress();
	end
end
