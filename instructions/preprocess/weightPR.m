
function handles = weightPR(PRName, referenceName, newName, handles)
    ref = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, referenceName))
            ref = handles.mydata.data{i} ;
            depth = handles.mydata.info{i}.depth ;
            raw = handles.mydata.info{i}.raw ;
        end
    end
    
    if(isempty(ref))
        error('Calibration curve not found in the current list')
    end
    
    braggImage = handles2BraggImage(PRName, handles) ;
    
    braggData = braggImage.get('data') ;
    
    sR = size(raw) ;
    sB = size(braggData) ;
    sRatio = sB(:)./sR(:) ;
    
    if mod(sRatio(1), 1)~=0 || mod(sRatio(2), 1)~=0
        error('Size of PR not compatible with size of reference')
    end
    
    size(repmat(reshape(ref, 1, 1, length(ref)), size(raw, 1), size(raw, 2), 1))
    size(raw)
    raw(raw==0) = 1 ;
    corr = repmat(reshape(ref, 1, 1, length(ref)), size(raw, 1), size(raw, 2), 1)./raw ;
    braggData = braggData.*repmat(corr, sRatio(1), sRatio(2), 1) ;
    
    disp('Adding PR to the list...')
    
    myInfo.Type = 'PR SIMU' ;
    myInfo.spacing = braggImage.get('spacing') ;
    myInfo.depth = braggImage.get('depth') ;
    myInfo.origin = braggImage.get('origin') ;
    
    myImageName = check_existing_names(newName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(braggData) ;
end
