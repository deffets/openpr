
%loadRSSimulation
% Load a simulation performed by a TPS.
%
% handles = loadRSSimulation(dir, directory_names, spots_nb_h, spots_nb_w, 
% direction1, direction2, spacing, depthOrigin, depthStep, name, handles)
% loads a proton radiograph simulation performed by a TPS and stores the 
% data in handles. Read the wiki for information on the format of the TPS
% simulation.
%
% Input arguments:
%   dir - directory of the simulation
%   directory_names - - File listing the directories containing the simulated 
%                   IDD (in each directory, there must be one file per IDD 
%                   with names like IDD_X=0.0Y=0.0.txt). Here is the syntax 
%                   of this file:
%                   directory_names x y z
%                   relative_path wrt. to this text file/directory1.csv x1 y1 z1
%                   relative_path wrt. to this text file/directory2.csv x2 y2 z2
%                   ...
%   spots_nb_h - number of spots along the first scanning direction
%   spots_nb_w - number of spots along the second scanning direction
%   direction1 - If the IDD files have names like IDD_X=0.0Y=0.0.txt, direction1 should be equal to 'X'
%   direction2 - If the IDD files have names like IDD_X=0.0Y=0.0.txt, direction2 should be equal to 'Y'
%   spacing - spot spacing
%   depthOrigin - first value of the depth axis of the simulated IDDs
%   depthStep - steps used to sample the IDDs
%   name - name of the simulation
%   handles - openREGGUI handles
%
% See also getTPSData
%
% Authors : S. Deffet
%

function handles = loadRSSimulation(dir, files_order, spots_nb_h, spots_nb_w, direction1, direction2, spacing, depthOrigin, depthStep, name, handles)
	res = getTPSData(dir, files_order, spots_nb_h, spots_nb_w, direction1, direction2) ;
%     res = res(:, :, 141:end) ;
    
    myInfo.Type = 'PR' ;
    myInfo.spacing = spacing ;

    depth = 0:1:length(squeeze(res(1, 1, :)))-1 ;
    myInfo.depth = depth*depthStep + depthOrigin;

    myInfo.origin = [0 0 0] ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(name, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(res) ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
end
