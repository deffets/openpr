
function handles = loadMatViva(fileName, energyfile, spacing, spotsNb, name, handles)
    res = load(fileName) ;
    res = res.FP_dose ;
    for i=1 : size(res, 3)
        res2(:, :, i) = rot90(imresize(res(:, :, i), size(res(:, :, i)).*round(1/(0.127*2)*[1 1]))) ;
    end
    res = res2 ;

    energy = importdata(energyfile) ;
%     energy = energy(1:end-2) ;
    
    myInfo.Type = 'Viva' ;
    myInfo.spacing = spacing ;
    myInfo.spotsNb = spotsNb ;
    myInfo.energy = energy ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(name, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(res) ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
end
