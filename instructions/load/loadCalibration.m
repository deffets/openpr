
%loadCalibration
% Add a calibration curve in handles.mydata
%
% handles = loadCalibration(calibrationfile, materialsFile, name, handles)
% loads a calibration curve from the files supplied by RayStation
% (materialsFile). We use the conversion scheme documented in the manual of
% RayStation 4.7. If no RayStation file is provided, a direct converion
% from HU - RSP is assumed to be stored in calibrationfile.
%
% Input arguments:
%   calibrationfile - a conversion from HU to mass density (openReggui
%   standard) and possibly a conversion from HU to RSP (openReggui
%   standard)
%   materialsFile - File supplied by RayStation with the elemental
%   compositions of the (interpolated) tissues used to convert HU to RSP
%   name - name of the resulting calibration curve
%   handles - openREGGUI handles
%
% Authors : S. Deffet
%

function handles = loadCalibration(calibrationfile, materialsFile, name, handles)
    % Add a calibration curve in handles.mydata
    % handles.mydata.data.model = model found in 'calibrationfile'
    % handles.mydata.data.RS_materials = materials found in 'materialsFile'
    % handles.mydata.info.type = 'Calibration'
    
    model = read_reggui_material_file(calibrationfile) ;
    
    if ~isempty(materialsFile)
        RS_materials = read_RS_param(materialsFile) ;
    else
        RS_materials = [] ;
    end
    
    myInfo.Type = 'Calibration' ;
    myData.model = model ;
    myData.RS_materials = RS_materials ;
    
    myDataName = check_existing_names(name, handles.mydata.name) ;
    myDataName = check_existing_names(myDataName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myDataName ;
    handles.mydata.data{length(handles.mydata.data)+1} = myData ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
end
