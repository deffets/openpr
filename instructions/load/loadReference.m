
%loadReference
% Load a reference Bragg curve measured with OmniPro-Incline
%
% handles = loadReference(referenceFile, calibrationFile, logs, spotsNb, 
% energy, name, handles) loads a reference Bragg curve (typically measured 
% without any object though the beam path) measured with OmniPro-Incline 
% and stores the data in handles. Read the wiki for information on the 
% format of the TPS simulation.
%
% Input arguments:
%   referenceFile - OmniPro-Incline .csv file
%   calibrationFile - OmniPro-Incline calibration file. Use an empty array
%                     [] to only rely on referenceFile for the calibration
%                     coefficients
%   logs - NOT USED
%   spotsNb - 2-element vector containing the number of spots along each 
%             scanning direction
%   energy - energy of the beam (NOT USED)
%   name - name to give to this reference curve
%   handles - openREGGUI handles
%
% See also get_reference_data
%
% Authors : S. Deffet
%

function handles = loadReference(referenceFile, calibrationFile, logs, spotsNb, energy, name, handles)
    [data, depth] = get_reference_data(referenceFile, calibrationFile, spotsNb(1), spotsNb(2)) ;
    raw = data ;
    data = mean(reshape(data, size(data, 1)*size(data, 2), size(data, 3)), 1) ;
    
    myInfo.Type = 'PR REF' ;
    myInfo.depth = depth ;
    myInfo.raw = raw ;
    myInfo.energy = energy ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(name, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(data) ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
end
