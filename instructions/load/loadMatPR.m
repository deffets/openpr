
%loadMatPR
% Load a proton radiograph stored in a .mat format
%
% handles = loadMatPR(matFile, spacing, depthSpacing, name, handles)
% loads a proton radiograph stored in a .mat format.
%
% Input arguments:
%   matFile - the .mat file in xhich the PR is stored. Size 1 and 2
%   correspond to the spatial axis and size 3 corresponds to the depth of
%   the Bragg curves.
%   spacing - the spot spacing (in mm)
%   depthSpacing - the discretization in water equivalent mm of the Bragg
%   curves
%   name - name of the resulting proton radiograph
%   handles - openREGGUI handles
%
% Authors : S. Deffet
%


function handles = loadMatPR(matFile, spacing, depthOrigin, depthSpacing, name, handles)
    data = importdata(matFile);
    data = rot90(data);
    data = flip(data, 2);
%     data = flip(data, 1);
    
    origin = [0 0 0];
    
    myInfo.Type = 'PR';
    myInfo.spacing = spacing;
    myInfo.depth = 0 : 1 : (length(squeeze(data(1, 1, :)))-1);
    myInfo.depth = depthSpacing*myInfo.depth + depthOrigin;
    myInfo.origin = origin;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(name, handles.mydata.name);
    myImageName = check_existing_names(myImageName, handles.images.name);
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName;
    handles.mydata.data{length(handles.mydata.data)+1} = single(data);
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo;
end
