
%loadPR
% Load a proton radiograph recoreded with OmniPro-Incline
%
% handles = loadPR(PRFile, calibrationFile, logs, spacing, spotsNb, name, 
% handles) loads a proton radiograph recorded in .csv files by
% OmniPro-Incline. All the .csv raw data from OmniproIncline in a single 
% folder. You must provide a text file listing all the thumbnails and their 
% relative position (defined as the couch movement from the reference 
% thumbnail).
%
% Input arguments:
%   PRFile - a text file listing all the thumbnails and their 
%   relative position (defined as the couch movement from the reference 
%   thumbnail). Here is the syntax of this file:
%     files_names x y z
%     relative_path wrt. to this text file/file1.csv x1 y1 z1
%     relative_path wrt. to this text file/file2.csv x2 y2 z2
%     ...
%   calibrationFile - OmniPro-Incline calibration file (optional)
%   logs - system logs (optional) --> leave empty
%   spacing - spot spacing (in mm)
%   spotsNb - number of spots in a thumbnails (2-element vector)
%   name - name of the resulting proton radiograph
%   handles - openREGGUI handles
%
% Authors : S. Deffet
%

function handles = loadPR(PRFile, calibrationFile, logs, spacing, spotsNb, name, handles)
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('runningMsg', 'Load MLIC PR') ;
        handles.progressControl.set('progressMsg', 'Loading data') ;
    end
    
    [PRFileDir, PRFileName, PRFileExt] = fileparts(PRFile) ;
    PRFileName = [PRFileName PRFileExt] ;
    
    [res, data_coordinates, depth] = get_mlic_data(PRFileDir, PRFileName, calibrationFile, spotsNb(1), spotsNb(2)) ;
    
    origin = [0 0 0] ;
    
    myInfo.Type = 'PR' ;
    myInfo.spacing = spacing ;
    myInfo.depth = depth ;
    myInfo.data_coordinates = data_coordinates ;
    myInfo.origin = origin ;
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('progressMsg', 'Adding PR to the list') ;
    end
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(name, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(res) ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.deleteProgress() ;
	end
end
