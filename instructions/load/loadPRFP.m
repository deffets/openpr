
function handles = loadPRFP(PRFile, PREnergyFile, spacing, spotsNb, name, handles)
    res = readVivaFile(PRFile) ;
    energy = importdata(PREnergyFile) ;
    
%     figure ;
%     A = splitViva(res) ;
%     size(A)
%     for i=1 : size(A, 3)
%         imagesc(A(:, :, i))
%         pause(0.5)
%     end
    
    myInfo.Type = 'Viva' ;
    myInfo.spacing = spacing ;
    myInfo.spotsNb = spotsNb ;
    myInfo.energy = energy ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(name, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(res) ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
end
