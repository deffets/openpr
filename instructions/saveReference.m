
function handles = saveReference(prName, curvePosition, referenceName, handles)

    braggImage = handles2BraggImage(prName, handles);
    
	curve = squeeze(braggImage.getData(curvePosition));
	depth = braggImage.get('depth');
        
    myInfo.Type = 'PR REF' ;
    myInfo.depth = depth ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(referenceName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(curve) ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
end
