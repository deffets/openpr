
function handles = generateMLICMeasurements(CTName, calibrationName, worldLimit1, worldLimit2, spotsNb, PRSpacing, gantry_angle, sigma, sigmaNoise, path, handles)    
    energy = 210;
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('runningMsg', 'Simple PR Simulation');
        handles.progressControl.set('progressMsg', 'Loading data');
    end
    
    hu = [];
    for i=1:length(handles.images.name)
        if(strcmp(handles.images.name{i}, CTName))
            hu = handles.images.data{i};
            huInfo = handles.images.info{i};
        end
    end
    if(isempty(hu))
        error('Input image not found in the current list')
    end
    
    if (huInfo.Spacing(1)~=1 && huInfo.Spacing(2)~=1) && huInfo.Spacing(3)~=1
        error('CT voxel size must be 1')
    end
    
    helpPath = getRegguiPath('help');
    helpFile = fullfile(helpPath, 'ref210MeV.txt');
    ref = importdata(helpFile);
    depth = squeeze(ref(1, :));
    ref = squeeze(ref(2, :));
    
    calibration = [];
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, calibrationName))
            calibration = handles.mydata.data{i};
        end
    end
    if(isempty(calibration))
        error('Calibration curve not found in the current list')
    end
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.1);
        handles.progressControl.set('progressMsg', 'Computing SPR');
	end

    if isfield(calibration.model, 'Stopping_Power') && ~isempty(calibration.model.Stopping_Power)
        spr = hu_to_we(hu, calibration.model);
    else
        density = hu_to_density(hu, calibration.model);
        spr = density2spr_RS(density,  energy, calibration.RS_materials);
    end
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.6);
        handles.progressControl.set('progressMsg', 'Computing WET');
	end
    
    spr = single(spr);
    
    wet = spr2totalWET(spr, huInfo.Spacing, 0, gantry_angle);
    wet = squeeze(wet);
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.7);
        handles.progressControl.set('progressMsg', 'Computing IDD');
	end

    pr = wet2pr(wet, huInfo.Spacing, depth, ref, sigma);
    
    simuImage = BraggImage(pr, depth, [0 0 0], [1 1 1]);
    simuImage = addNoise2PR(simuImage, sigmaNoise, 0);
    
    simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing);
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.9);
        handles.progressControl.set('progressMsg', 'Saving data');
    end
    
    writeMLICData(path, 'pr_simu', simuImageData, spotsNb, PRSpacing)
    
    pr_simu_mat = single(simuImageData);
    save(fullfile(path, 'pr_simu_mat.mat'), 'pr_simu_mat');

    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.deleteProgress();
	end
end
