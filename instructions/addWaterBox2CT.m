
function handles = addWaterBox2CT(CTName, dist, huWater, newName, newFormat, handles)
    ct = [];
    for i=1:length(handles.images.name)
        if(strcmp(handles.images.name{i}, CTName))
            ct = handles.images.data{i};
            info = handles.images.info{i};
        end
    end
    
    if(isempty(ct))
        disp('Image not found. Abort.')
        return
    end
    
    
    ct2 = -1024*ones(size(ct, 1) + dist + 320, size(ct, 2), size(ct, 3));
    
    ct2(1:size(ct, 1), :, :) = ct;
    
    ct2(size(ct, 1)+dist:end, :, :) = huWater;
    
    info.detectorPosition = size(ct, 1) + dist + 1;
         
    disp('Adding CT to the list...')
    myImageName = check_existing_names(newName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    if strcmp(newFormat, 'image')
        handles.images.name{length(handles.images.name)+1} = myImageName ;
        handles.images.data{length(handles.images.data)+1} = single(ct2) ;
        handles.images.info{length(handles.images.info)+1} = info ;

        handles.size(1) = size(ct2, 1);
        handles.size(2) = size(ct2, 2);
        handles.size(3) = size(ct2, 3);
    else
        handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
        handles.mydata.data{length(handles.mydata.data)+1} = single(ct2) ;
        handles.mydata.info{length(handles.mydata.info)+1} = info ;
    end
end
