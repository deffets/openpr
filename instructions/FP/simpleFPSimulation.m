
function handles = simpleFPSimulation(CTName, referenceName, calibrationName, conversionName, sigma, energy, gantry_angle, table_angle, simulationName, handles)
    hu = [];
    for i=1:length(handles.images.name)
        if(strcmp(handles.images.name{i}, CTName))
            hu = handles.images.data{i};
            huInfo = handles.images.info{i};
        end
    end
    if(isempty(hu))
        error('Input image not found in the current list')
    end
    
    calibration = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, calibrationName))
            calibration = handles.mydata.data{i};
        end
    end
    if(isempty(calibration))
        error('Calibration curve not found in the current list')
    end
    
    ref = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, referenceName))
            ref = handles.mydata.data{i} ;
            depth = handles.mydata.info{i}.depth ;
        end
    end
    if(isempty(ref))
        error('Calibration curve not found in the current list')
    end
    
    if isfield(calibration.model, 'Stopping_Power') && ~isempty(calibration.model.Stopping_Power)
        spr = hu_to_we(hu, calibration.model) ;
    else
        density = hu_to_density(hu, calibration.model) ;

        disp('Computing SPR ...')
        switch conversionName
            case 'RayStation'
                spr = density2spr_RS(density,  energy, calibration.RS_materials) ;
            otherwise
                error('Conversion name unknown')
        end
    end
    
    spr = single(spr) ;
    
    wet = spr2totalWET(spr, huInfo.Spacing, table_angle, gantry_angle) ;
    wet = squeeze(wet) ;

    disp('Computing Bragg curves...')
    pr = wet2pr(wet, huInfo.Spacing, depth, ref, sigma) ;
    
    d = importdata('rangeWater.txt') ;
    e = d.data(:, 1) ;
    r = d.data(:, 2)*10 ;
    
    r210 = interp1(e, r, 210) ;
    energies = [1.8000000e+02   1.7750000e+02   1.7500000e+02   1.7250000e+02   1.7000000e+02   1.6750000e+02   1.6500000e+02   1.6250000e+02   1.6000000e+02   1.5750000e+02   1.5500000e+02   1.5250000e+02   1.5000000e+02   1.4750000e+02   1.4500000e+02   1.4250000e+02   1.4000000e+02   1.3750000e+02   1.3500000e+02   1.3250000e+02   1.3000000e+02   1.2750000e+02   1.2500000e+02   1.2250000e+02   1.2000000e+02   1.1750000e+02   1.1500000e+02   1.1250000e+02   1.1000000e+02   1.0750000e+02   1.0500000e+02   1.0250000e+02 ] ;
    ranges = interp1(e, r, energies) ;
    ranges = ranges(:) ;
    ranges = ranges' ;
    
    prFP = zeros(size(pr, 1), size(pr, 2), length(ranges)) ;
    for i=1 : size(pr, 1)
        for j=1 : size(pr, 2)
            prFP(i, j, :) = interp1(depth, squeeze(pr(i, j, :)), depth(1)+r210-ranges) ;
        end
    end
    
    myInfo.Type = 'PR SIMU' ;
    myInfo.prType = 'FP' ;
    myInfo.spacing = huInfo.Spacing ;
    myInfo.depth = energies ;
    myInfo.origin = [0 0 0] ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(simulationName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(prFP) ;
end
