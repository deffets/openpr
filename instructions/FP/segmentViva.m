
function handles = segmentViva(PRName, indx, indy, spacing, newName, handles)
    for i=1 : length(handles.mydata.name)
        if strcmp(handles.mydata.name{i}, PRName)
            data = handles.mydata.data{i} ;
            info = handles.mydata.info{i} ;
        end
    end
    
%     figure

    res = zeros(length(indy)-1, length(indx)-1, size(data, 3)) ;
    for i=2 : length(indy)
        for j=2 : length(indx)
            res(i-1, j-1, :) = squeeze(sum(squeeze(sum(data(indy(i-1):indy(i), indx(j-1):indx(j), :), 1)), 1)) ;
            res(i-1, j-1, :) = res(i-1, j-1, :) - min(res(i-1, j-1, :)) ;

%             imagesc(data(indy(i-1):indy(i), indx(j-1):indx(j), 1))
%             pause(0.1)
        end        
    end

    myInfo.Type = 'PR SIMU' ;
    myInfo.prType = 'FP' ;
    myInfo.spacing = spacing ;
    myInfo.depth = info.energy ;
    myInfo.origin = [0 0 0] ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(newName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(res) ;
end
