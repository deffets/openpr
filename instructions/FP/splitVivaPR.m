
function handles = splitVivaPR(PRName, handles)
    for i=1 : length(handles.mydata.name)
        if strcmp(handles.mydata.name{i}, PRName)
            data = handles.mydata.data{i} ;
            break ;
        end
    end
    
    data2 = splitViva(data) ;

    handles.mydata.data{i} = single(data2) ;
end
