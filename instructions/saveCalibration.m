
%saveCalibration
% Saves a calibration curve in the openREGGUI handles.
%
% handles = saveCalibration(PRName, shift_mm, newName, handles) saves a
% calibration curve in the openREGGUI handles.
%
% Input arguments:
%   curve - A matrix with two rows where the first row contains the HU and
%   the second row contains the RSP.
%   name - name to give to the calibration curve
%   handles - openREGGUI handles
%
% Authors : S. Deffet
%

function handles = saveCalibration(curve, name, handles)
    myImageName = check_existing_names(name, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    myInfo.Type = 'Calibration' ;
    
    calibration.model.Stopping_Power = curve' ;
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.data{length(handles.mydata.data)+1} = calibration ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
end
