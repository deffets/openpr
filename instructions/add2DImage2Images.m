
function handles = add2DImage2Images(imageNameOrImage2D, gantryAngle, newName, handles)
    if gantryAngle~=270
        error('Feature no yet implemented fo gantry angle ~= 270.');
    end
    
    if isa(imageNameOrImage2D, 'Image2D')
        image2D = imageNameOrImage2D;
    else
        image2D = handles2Image2D(imageNameOrImage2D, handles);
    end
    
    origin = image2D.get('origin');
    spacing = image2D.get('spacing') ;
    
    if spacing(1)~=1 && spacing(2)~=1
        error('Spacing should be 1 (assuming that the CT resolution is 1x1x1).');
    end
    
    im = image2D.get('data');
        
    hu = handles.images.data{2};
    
    % Repmat selon la dim 1 après avoir fait flip 1 qui précède un rot90
    % Après on pad
    
    newData = zeros(size(hu));
    
    newDataValue = flip(rot90(im), 2);
    
    newData(:, end-size(newDataValue, 1)-origin(2)+1:end-origin(2), end-size(newDataValue, 2)-origin(1)+1:end-origin(1)) = repmat(reshape(newDataValue, 1, size(newDataValue, 1), size(newDataValue, 2)), size(newData, 1), 1, 1);
    
    handles.images.name{end+1} = newName;
    handles.images.data{end+1} = newData;
    handles.images.info{end+1} = handles.images.info{end};
end
