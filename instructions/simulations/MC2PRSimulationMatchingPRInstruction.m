
function handles = MC2PRSimulationMatchingPRInstruction(CTName, calibrationName, prName, BDLName, energy, FOV, protonsNb, simulationName, handles)
    detectorDist = 50;
    
    warning('Hardcoded detector distance is 50 mm');
    
    if numel(FOV)==1
        FOV = [FOV FOV];
    end
    
    calibration = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, calibrationName))
            calibration = handles.mydata.data{i};
        end
    end
    if(isempty(calibration))
        error('Calibration curve not found in the current list')
    end
    
    if ~isfield(calibration, 'RS_materials')
        error('Only RayStation format is supported')
    end
    
    RS_param = calibration.RS_materials;
    model = calibration.model;
    
    simuPath = getRegguiPath('userData');
    libPath = fullfile(simuPath, 'MC2_lib', 'lib');
    if isdir(libPath)
        rmdir(libPath, 's');
    end
    mkdir(libPath);
    copyfile(fullfile(getRegguiPath('openMCsquare'), 'lib'), libPath);
    
    scannerName = 'scanner';
    scannerPath = fullfile(libPath, 'Scanners', scannerName);
    mkdir(scannerPath);
    materialsPath = fullfile(libPath, 'Materials');
    
    bdlPath = fullfile(libPath, 'BDL');
    copyfile(BDLName, bdlPath);
    [~, name, ext] = fileparts(BDLName);
    BDLName = [name ext];
    BDLPath = fullfile(bdlPath, BDLName);
    
    
    exportRSConversion2MC2(model, RS_param, materialsPath, scannerPath);
    
    hu = [];
    for i=1:length(handles.images.name)
        if(strcmp(handles.images.name{i}, CTName))
            hu = handles.images.data{i};
            huInfo = handles.images.info{i};
        end
    end
    if(isempty(hu))
        error('Input image not found in the current list')
    end    
    
    braggImage = handles2BraggImage(prName, handles);
    spotSpacing = braggImage.get('spacing');
    origin = braggImage.get('origin');
    data = braggImage.get('data');
    
    spotsNb = [0 0];
    spotsNb(1) = FOV(1)./spotSpacing(1)+1;
    spotsNb(2) = FOV(2)./spotSpacing(2)+1;
    
    spotsNb = spotsNb(:);
    spotSpacing = spotSpacing(1:2);
    spotSpacing = spotSpacing(:);
    frameSize = (spotsNb-1).*spotSpacing;
    
    framesNb1 = round(size(data, 1)/spotsNb(1));
    framesNb2 = round(size(data, 2)/spotsNb(2));
    
    for i = 1 : framesNb2
        for j = framesNb1 : -1 : 1
            [x, y] = meshgrid(-frameSize/2:spotSpacing:frameSize/2, -frameSize/2:spotSpacing:frameSize/2);
            x = x(:);
            y = y(:);
            
            xy = [x, y];

            weight = 1;
            isocenter = [size(hu, 1)/2 origin(2)+(i-1)*(frameSize(2)+spotSpacing(2))+frameSize(2)/2-0.5 size(hu, 3)-1-origin(1)-(j-1)*(frameSize(1)+spotSpacing(1))-frameSize(1)/2+0.5];
            isocenter = isocenter(:);
            handles = createPRPlan(xy, weight, energy, isocenter, spotSpacing, spotsNb, 270, 0, [simulationName '_plan_' num2str(i) '_' num2str(j)], handles);
            
            planNames{i, framesNb1-j+1} = handles.plans.name{end};
        end
    end
        
    [density, ind] = unique(calibration.model.Density(:, 2));
    huWater = interp1(density, calibration.model.Density(ind, 1), 1.0, 'linear');
    handles = addWaterBox2CT(CTName, detectorDist, huWater, [CTName '_detector'], 'data', handles);
    
%     hu = handles.mydata.data{end};
%     tform = eye(4);
%     tform(4, 1) = 0;
%     tform(4, 2) = 0;
%     tform(4, 3) = 0;
%     tform = affine3d(tform) ;
%     imR = imref3d(size(hu), [0 size(hu, 2)]-size(hu, 2)/2, [0 size(hu, 1)]-size(hu, 1)/2, [0 size(hu, 3)]-size(hu, 3)/2) ;
%     hu2 = imwarp(hu, imR, tform, 'OutputView', imR, 'FillValues', -1024) ;
%     handles.mydata.data{end} = hu2;
    
    CTName = handles.mydata.name{end};
    
    braggImage = MC2PRSimulation(CTName, planNames, scannerPath, libPath, BDLPath, protonsNb, handles);  
    
    
	SPR_water = density2spr_RS([1 1 1], 100, RS_param);
    depth =  braggImage.get('depth');
    depth = depth*SPR_water(1);
        
    myInfo.Type = 'PR SIMU' ;
    myInfo.spacing = braggImage.get('spacing') ;
    myInfo.depth = depth;
    myInfo.origin = origin ;
    
    data = rot90(single(braggImage.get('data')));
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(simulationName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    handles.mydata.data{length(handles.mydata.data)+1} = data ;    
end

