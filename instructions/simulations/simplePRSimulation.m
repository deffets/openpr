
%simpePRSimulation
% Simulate a proton radiograph based on a CT and a conversion scheme
%
% handles = simplePRSimulation(CTName, referenceName, calibrationName, 
% conversionName, sigma, energy, gantry_angle, table_angle, simulationName, 
% handles) generates a simulated proton radiograph from a 3D CT. The
% resulting data are stored in handles.
%
% Input arguments:
%   CTName - CT name (in openREGGUI handles)
%   referenceName - name of the reference Bragg curve (in openREGGUI
%                   handles)
%   calibrationName - name of the calibration scheme (in openREGGUI handles)
%   conversionName - used if the calibration curve is not a direct
%                    conversion to SPR. If the conversion curve is a
%                    combination of a conversion from HUs to densities and
%                    from densities to materials according to Raystation
%                    manual 4.7 then conversionName should be 'RayStation'
%   sigma - sigma of the beam
%   energy - energy of the protons (in MeV) - only used if conversionName
%            is 'RayStation'
%   gantry_angle - gantry angle (degrees)
%   table_angle - table angle (degrees) - NOT USED
%   simulationName - name of the resulting proton radiograph
%   handles - openREGGUI handles
%
% Authors : S. Deffet
%

function handles = simplePRSimulation(CTName, referenceName, calibrationName, conversionName, sigma, energy, gantry_angle, table_angle, simulationName, handles)
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('runningMsg', 'Simple PR Simulation') ;
        handles.progressControl.set('progressMsg', 'Loading data') ;
    end
    
    hu = [];
    for i=1:length(handles.images.name)
        if(strcmp(handles.images.name{i}, CTName))
            hu = handles.images.data{i};
            huInfo = handles.images.info{i};
        end
    end
    if(isempty(hu))
        error('Input image not found in the current list')
    end
    
    calibration = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, calibrationName))
            calibration = handles.mydata.data{i};
        end
    end
    if(isempty(calibration))
        error('Calibration curve not found in the current list')
    end
    
    ref = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, referenceName))
            ref = handles.mydata.data{i} ;
            depth = handles.mydata.info{i}.depth ;
        end
    end
    if(isempty(ref))
        error('Reference curve not found in the current list')
    end
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.1) ;
        handles.progressControl.set('progressMsg', 'Computing SPR') ;
	end
    
    if isfield(calibration.model, 'Stopping_Power') && ~isempty(calibration.model.Stopping_Power)
%         spr = hu_to_we(hu, calibration.model) ; % Too slow
        spr = interp1(calibration.model.Stopping_Power(:, 1), calibration.model.Stopping_Power(:, 2), hu) ;
    else
        density = hu_to_density(hu, calibration.model) ;

        disp('Computing SPR ...')
        switch conversionName
            case 'RayStation'
                spr = density2spr_RS(density,  energy, calibration.RS_materials) ;
            otherwise
                error('Conversion name unknown')
        end
    end
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.6) ;
        handles.progressControl.set('progressMsg', 'Computing WET') ;
	end
    
    spr = single(spr) ;
    
    wet = spr2totalWET(spr, huInfo.Spacing, table_angle, gantry_angle) ;
    wet = squeeze(wet) ;
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.7) ;
        handles.progressControl.set('progressMsg', 'Computing IDD') ;
	end

    disp('Computing Bragg curves...')
    pr = wet2pr(wet, huInfo.Spacing, depth, ref, sigma) ;
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.9) ;
        handles.progressControl.set('progressMsg', 'Saving data') ;
	end
    
    myInfo.Type = 'PR SIMU' ;
    myInfo.spacing = huInfo.Spacing ;
    myInfo.depth = depth ;
    myInfo.origin = [0 0 0] ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(simulationName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(pr) ;
    
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.deleteProgress() ;
	end
end
