
%simplePRSimulationFromWET
% Simulate a proton radiograph based on a WET map
%
% handles = simplePRSimulationFromWET(wetName, referenceName, sigma, 
% energy, simulationName, handles) generates a simulated proton radiograph
% from a 2D WET map. The resulting data are stored in handles.
%
% Input arguments:
%   wetName - Name of the WET map (in openREGGUI handles)
%   referenceName - name of the reference Bragg curve (in openREGGUI
%                   handles)
%   sigma - sigma of the beam
%   energy - energy of the protons (in MeV) - NOT USED
%   simulationName - name of the resulting WET map
%   handles - openREGGUI handles
%
% Authors : S. Deffet
%

function handles = simplePRSimulationFromWET(wetName, referenceName, sigma, energy, simulationName, handles)
    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('runningMsg', 'Simple PR Simulation From WET') ;
        handles.progressControl.set('progressMsg', 'Loading data') ;
    end

    ref = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, referenceName))
            ref = handles.mydata.data{i} ;
            depth = handles.mydata.info{i}.depth ;
        end
    end
    if(isempty(ref))
        error('Reference curve not found in the current list')
    end
    
    wet = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, wetName))
            wet = handles.mydata.data{i};
            origin = handles.mydata.info{i}.origin;
            wetSpacing = handles.mydata.info{i}.spacing ;
        end
    end
    if(isempty(wet))
        error('WET-map not found in the current list')
    end
    
	if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.5) ;
        handles.progressControl.set('progressMsg', 'Computing IDD') ;
	end

    disp('Computing Bragg curves...')
    pr = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
    
	if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.set('n', 0.9) ;
        handles.progressControl.set('progressMsg', 'Saving data') ;
	end
    
    myInfo.Type = 'PR SIMU' ;
    myInfo.spacing = wetSpacing ;
    myInfo.depth = depth ;
    myInfo.origin = origin ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(simulationName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(pr) ;
    
	if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
        handles.progressControl.deleteProgress() ;
	end
end
