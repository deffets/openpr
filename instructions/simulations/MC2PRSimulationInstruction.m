
function handles = MC2PRSimulationInstruction(CTName, calibrationName, spotsNb, spotSpacing, framesNb1, framesNb2, energy, protonsNb, simulationName, handles)
    BDLName = 'BDL_default_DN.txt';
    detectorDist = 50;
    
    calibration = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, calibrationName))
            calibration = handles.mydata.data{i};
        end
    end
    if(isempty(calibration))
        error('Calibration curve not found in the current list')
    end
    
    if ~isfield(calibration, 'RS_materials')
        error('Only RayStation format is supported')
    end
    
    RS_param = calibration.RS_materials;
    model = calibration.model;
    
    simuPath = getRegguiPath('userData');
    libPath = fullfile(simuPath, 'MC2_lib', 'lib');
    if isdir(libPath)
        rmdir(libPath, 's');
    end
    mkdir(libPath);
    copyfile(fullfile(getRegguiPath('openMCsquare'), 'lib'), libPath);
    
    scannerName = 'scanner';
    scannerPath = fullfile(libPath, 'Scanners', scannerName);
    mkdir(scannerPath);
    materialsPath = fullfile(libPath, 'Materials');
    BDLPath = fullfile(libPath, 'BDL', BDLName);
    
    
    exportRSConversion2MC2(model, RS_param, materialsPath, scannerPath);
    
    
    hu = [];
    for i=1:length(handles.images.name)
        if(strcmp(handles.images.name{i}, CTName))
            hu = handles.images.data{i};
            huInfo = handles.images.info{i};
        end
    end
    if(isempty(hu))
        error('Input image not found in the current list')
    end
    
    size(hu)

    frameSize = (spotsNb-1)*spotSpacing;
    ind1 = 1;
    for i =  -(framesNb1-1)/2 : (framesNb1-1)/2
        ind2 = 1;
        for j = -(framesNb2-1)/2 : (framesNb2-1)/2
            [x, y] = meshgrid(-frameSize/2:spotSpacing:frameSize/2, -frameSize/2:spotSpacing:frameSize/2);
            x = x(:);
            y = y(:);
            
            xy = [x, y];

            weight = 1;
            isocenter = [round(size(hu, 1)/2) size(hu, 2)/2+i*(frameSize+spotSpacing) size(hu, 3)/2+j*(frameSize+spotSpacing)];
            isocenter = isocenter(:);
            handles = createPRPlan(xy, weight, energy, isocenter, spotSpacing, spotsNb, 270, 0, [simulationName '_plan_' num2str(i) '_' num2str(j)], handles);
            
            planNames{ind1, ind2} = handles.plans.name{end};
            ind2 = ind2+1;
        end
        ind1 = ind1+1;
    end
    
    
    huWater = interp1(calibration.model.Density(:, 1), calibration.model.Density(:, 2), 1.0, 'linear');
    handles = addWaterBox2CT(CTName, detectorDist, huWater, [CTName '_detector'], 'data', handles);
    CTName = handles.mydata.name{end};
    
    braggImage = MC2PRSimulation(CTName, planNames, scannerPath, libPath, BDLPath, protonsNb, handles);  
    
    myInfo.Type = 'PR SIMU' ;
    myInfo.spacing = braggImage.get('spacing') ;
    myInfo.depth = braggImage.get('depth') ;
    myInfo.origin = braggImage.get('origin') ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(simulationName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(braggImage.get('data')) ;    
end
