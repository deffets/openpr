
%saveRegistration
% Saves the result of a 2 DOF PR-PR registration
%
% handles = saveRegistration(name1, name2, newOrigin2, newName1, newName2, 
% shandles) applies result of 2 DOF registration represented by the new
% origin of the second proton radiograph (name2). The methods compute the
% geometrical interesection between the two proton radiographs and crop
% them so that their geometrical characteristics are identical. The
% resulting data are stored in handles.
%
% Input arguments:
%   name1 - first proton radiograph
%   name2 - second proton radiograph
%   newOrigin2 - new origin of the second proton radiograph (see BraggImage
%                class for details about geometric parameters of proton 
%                radiograph)
%   newName1 - name for the cropped proton radiograph
%   newName2 - name for the aligned and cropped proton radiograph
%   handles - openREGGUI handles
%
% See also applyTransform
%
% Authors : S. Deffet
%

function handles = saveRegistration(name1, name2, newOrigin2, newName1, newName2, handles)
    braggImage1 = handles2BraggImage(name1, handles) ;
    braggImage2 = handles2BraggImage(name2, handles) ;
            
    braggImage2.set('origin', newOrigin2) ;
    
    spacing2 = braggImage2.get('spacing') ;

    [worldLimit1, worldLimit2] = computeWorldLimits(braggImage1.get('XWorldLimits'), ...
        braggImage1.get('YWorldLimits'), braggImage2.get('XWorldLimits'), ...
        braggImage2.get('YWorldLimits'), spacing2) ;
    worldLimit2(1) = worldLimit2(1) - spacing2(1) ;
    worldLimit2(2) = worldLimit2(2) - spacing2(2) ;

    newImage1 = BraggImage(braggImage1.getData(worldLimit1, worldLimit2, spacing2), braggImage1.get('depth'), worldLimit1, spacing2) ;
    newImage2 = BraggImage(braggImage2.getData(worldLimit1, worldLimit2, spacing2), braggImage2.get('depth'), worldLimit1, spacing2) ;

    % Save image 1
    myInfo.type = 'PR' ;
    myInfo.spacing = newImage1.get('spacing') ;
    myInfo.depth = newImage1.get('depth') ;
    myInfo.origin = newImage1.get('origin') ;
    data = newImage1.get('data') ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(newName1, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(data) ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
    
    % Save image 2
    myInfo.Type = 'PR' ;
    myInfo.spacing = newImage2.get('spacing') ;
    myInfo.depth = newImage2.get('depth') ;
    myInfo.origin = newImage2.get('origin') ;
    data = newImage2.get('data') ;
    
    disp('Adding PR to the list...')
    myImageName = check_existing_names(newName2, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName ;
    handles.mydata.data{length(handles.mydata.data)+1} = single(data) ;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo ;
end