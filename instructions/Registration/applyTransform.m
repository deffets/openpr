
%applyTransform
% Applies affine transform to CT.
%
% handles = applyTransform(CTName, tform, newName, handles) applies an
% affine transform tform to a CT and stores the resulting data in handles.
%
% Input arguments:
%   CTName - CT name
%   tform - transform matrix (see affine3d for matrix requirements)
%   newName - name of the resulting CT
%   handles - openREGGUI handles
%
% See also affine3d, saveRegistration, applyFlipTransform
%
% Authors : S. Deffet
%

function handles = applyTransform(CTName, tform, newName, handles)
    hu = [] ;
    for i=1:length(handles.images.name)
        if(strcmp(handles.images.name{i}, CTName))
            hu = handles.images.data{i};
            huInfo = handles.images.info{i};
        end
    end
    if(isempty(hu))
        error('Input image not found in the current list')
    end

    tform = affine3d(tform) ;
    imR = imref3d(size(hu), [0 size(hu, 2)]-size(hu, 2)/2, [0 size(hu, 1)]-size(hu, 1)/2, [0 size(hu, 3)]-size(hu, 3)/2) ;
    hu2 = imwarp(hu, imR, tform, 'OutputView', imR, 'FillValues', -1024) ;
    
    disp('Adding CT to the list...')
    myImageName = check_existing_names(newName, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    handles.images.name{length(handles.images.name)+1} = myImageName ;
    handles.images.data{length(handles.images.data)+1} = single(hu2) ;
    handles.images.info{length(handles.images.info)+1} = huInfo ;
end

