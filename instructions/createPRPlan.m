
function handles = createPRPlan(spotPosition, weight, energy, isocenter, spotSpacing, spotsNb, gantry_angle, table_angle, planName, handles)
    planData{1}.name = 'B1';
    planData{1}.isocenter = isocenter;
    planData{1}.gantry_angle = gantry_angle;
    planData{1}.table_angle = table_angle;
    planData{1}.final_weight = size(spotPosition, 1)*weight;
    
    planData{1}.spots(1).nb_paintings = 1;
    planData{1}.spots(1).energy = energy;
    
    planData{1}.spots(1).weight = repmat(weight, size(spotPosition, 1), 1);
    planData{1}.spots(1).xy = spotPosition;
    
    planInfo.OriginalHeader.FractionGroupSequence.Item_1.NumberOfFractionsPlanned = 1;
    planInfo.Type = 'pbs_plan';
    planInfo.spotsNb = spotsNb;
    planInfo.spotSpacing = spotSpacing;
    
    
    disp('Adding plan to the list...')
    myPlanName = check_existing_names(planName, handles.plans.name) ;
    myPlanName = check_existing_names(myPlanName, handles.plans.name) ;
    
    handles.plans.name{length(handles.plans.name)+1} = myPlanName ;
    handles.plans.data{length(handles.plans.data)+1} = planData ;
    handles.plans.info{length(handles.plans.info)+1} = planInfo ;
end
