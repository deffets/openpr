
function handles = saveDetector(detectorDimensions, detectorPosition, materials, lengths, actors, repeatNb, name, handles)
    myImageName = check_existing_names(name, handles.mydata.name) ;
    myImageName = check_existing_names(myImageName, handles.images.name) ;
    
    myInfo.Type = 'PRDetector' ;
    
    data.detectorDimensions = detectorDimensions;
    data.detectorPosition = detectorPosition;
    data.materials = materials;
    data.lengths = lengths;
    data.actors = actors;
    data.repeatNb = repeatNb;
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName;
    handles.mydata.data{length(handles.mydata.data)+1} = data;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo;
end
