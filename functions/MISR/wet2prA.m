function pr2 = wet2prA(wet, spacing, depth, ref, sigma, A)
    pr = wet2pr(wet, spacing, depth, ref, sigma) ;
    
    pr2 = zeros(size(pr)) ;
    [xq, yq] = meshgrid(1:1:size(pr, 2), 1:1:size(pr, 1)) ;
    
    Y = repmat(1:size(A, 1), 1, size(A, 2));
    Y = Y(A);
%     [X, ind] = sort(X, 'ascend');
    Y = Y(:);
    X = repmat(1:size(A, 2), size(A, 1), 1);
    X = X(A);
%     Y = Y(ind);
    X = X(:);
    
    
    x2 = X(X>1);
    subSpacing = min(x2(:))-1;
    for sliceNb=1 : size(pr, 3)
        pr2(:, :, sliceNb) = interp2(xq(1:subSpacing:end, 1:subSpacing:end), yq(1:subSpacing:end, 1:subSpacing:end), pr(1:subSpacing:end, 1:subSpacing:end, sliceNb), xq, yq, 'cubic');
    end
    
%     for sliceNb=1 : 1 : size(pr, 3)
%         y = pr(:, :, sliceNb) ;
%         if sliceNb==1
%             F = scatteredInterpolant(X, Y, y(A), 'linear') ;
%         else
%                 F.Values = y(A);
%         end
%         
%             y = F(xq, yq) ;
%             pr2(:, :, sliceNb) = y ;
%     end
    
%     pr2 = pr;%%%%%%%%%%%%%%%
%     pr2(repmat(~A, 1, 1, size(pr, 3))) = 0;%%%%%%%%%%
    
    pr2(isnan(pr2)) = 0 ;
end
