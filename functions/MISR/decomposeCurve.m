
%DECOMPOSECURVE Decomposes an IDD into a set of pristine Bragg curves
%
%   a = decomposeCurve(curves, braggCurve) decomposes a measured Bragg
%   curve into pristine Bragg curves according to Krah et al 2015.
%   braggCurve is the Bragg curve to decompose and curves is the set of
%   allowable pristine Bragg curves (2D matrix).


function a = decomposeCurve(curves, braggCurve)
    a = zeros(1, size(curves, 1)) ;
    
    [m, maxPos] = max(curves, [], 2) ;
    
    braggCurve = reshape(braggCurve, 1, length(braggCurve)) ;
    
    if nargin>2
        [m, ind] = sort(braggCurve, 'ascend') ;
        intervInd = ind(end-n+1:end) ;
    else
        plateau = mean(braggCurve(1:10)) ;
        ind = 1:1:length(braggCurve) ;
        m = mean(braggCurve>plateau) ;
        intervInd = ind(braggCurve>(plateau+(m-plateau)/4)) ;
    end
    
    intervInd = sort(unique([intervInd, max(intervInd):1:length(braggCurve)]), 'ascend') ;
    intervInd = intervInd(braggCurve(intervInd)>0.5) ;
    
    varLength = sum(ismember(maxPos, intervInd)) ;
    
    curvesTemp = curves(ismember(maxPos, intervInd), :) ;

    err = @(aTemp2) double( sum((aTemp2*curvesTemp - repmat(braggCurve, size(aTemp2, 1), 1)).^2, 2) ) ;

    opts = optimoptions(@fmincon,'Algorithm','sqp', 'FiniteDifferenceStepSize', 0.001, 'FunctionTolerance', 0.01) ;
    aTemp2 = fmincon(err, zeros(1, varLength), [], [], [], [], zeros(1, varLength), ones(1, varLength), [], opts) ;
    
    a(ismember(maxPos, intervInd)) = aTemp2 ;
end
