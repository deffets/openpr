
function a = softThreshold(a, direction, gamma, lambda)
    for i=1 : length(direction)
        if iscell(direction{i})
            for j=1 : length(direction{i})
                a{i}{j} = a{i}{j} + gamma*direction{i}{j} ;
%                 a{i}{j} = a{i}{j}.*max(0, 1-gamma./(2*lambda*abs(a{i}{j})));
                a{i}{j} = a{i}{j}.*max(0, 1-lambda./abs(a{i}{j}));
            end
        else
            a{i} = a{i}+ gamma*direction{i} ;
%             a{i} = a{i}.*max(0, 1-gamma./(2*lambda*abs(a{i})));
            a{i} = a{i}.*max(0, 1-lambda./abs(a{i}));
        end
    end
end