%DECONV_PR_COMPRESSIVE WET estimation via sparse deconvolution of proton radiography data
%
%   w = deconv_pr_compressive(pr, prSpacing, outSpacing, ref, depth, sigma, 
%   opt, wetCT) estimates a WET map from a proton radiograph via 
%   the sparse deconvolution proposed by Deffet et al 2019. wetInit is on
%   optional parameter. Use it if you can provide an initial guess of the
%   WET map.
%
% See also decomposeCurve
%
% Authors : S. Deffet
%

function w = deconv_pr_compressive(pr, prSpacing, outSpacing, ref, depth, sigma, opt, wetCT)
    if outSpacing(1)~=1 | outSpacing(2)~=1
        error('outSpacing must be [1 1 1]')
    end
    
    prInit = pr;
    
    pr = pr/sum(ref);
    ref = ref/sum(ref);
    
    n1 = ([size(pr, 1), size(pr, 2)]-1).*prSpacing+1;

    pr2 = zeros(n1(1), n1(2), size(pr, 3));
    pr2(1:prSpacing(1):end, 1:prSpacing(2):end, :) = pr;
    pr = pr2;
    A = pr(:, :, 1)~=0;

    pr = double(pr);
    
    depth10 = 0:0.1:max(depth)+1;
    ref10 = interp1(depth, ref, depth10, 'spline', 'extrap');
    refDeriv = interp1(depth10(1:end-1), (ref10(2:end)-ref10(1:end-1))/(depth10(2)-depth10(1)), depth, 'linear', 'extrap');
    
    nIter = opt.nIter;
    gamma = opt.gamma;
    lambda = opt.lambda;
    rescaleIDD = opt.rescaleIDD;
    gammaMax = opt.gammaMax;
    MaxFunEvals = opt.MaxFunEvals;
    bisection = opt.bisection;
        
    s1 = size(pr, 1);
    s2 = size(pr, 2);
    dimMax = 2^(ceil(log2(max([s1, s2]))));    
    
    addZeros = @(f) padarray(f, [dimMax-s1, dimMax-s2], 'post');
    removeZeros = @(f) f(1:s1, 1:s2);

    opt.nbscales = 5;
    opt.n = dimMax;
    psi = @(a) removeZeros(real(perform_curvelet_transform(a, opt)));
    psiT = @(f) perform_curvelet_transform(addZeros(f), opt);
    
    
    prScaledInterp = zeros(size(pr)); % = approximation to decrease number of iterations
    [xq, yq] = meshgrid(1:1:size(pr, 2), 1:1:size(pr, 1));
    for sliceNb=1 : size(pr, 3)
        prScaledInterp(:, :, sliceNb) = interp2(xq(1:prSpacing(1):end, 1:prSpacing(1):end), yq(1:prSpacing(2):end, 1:prSpacing(2):end), pr(1:prSpacing(1):end, 1:prSpacing(2):end, sliceNb), xq, yq, 'cubic');
    end

%     sumpr = repmat(sum(squeeze(prScaledInterp(:, :, 2:10)), 3), 1, 1, size(pr, 3));
%     prScaledInterp = prScaledInterp./sumpr;
    sumpr = sum(prScaledInterp(:, :, 2:10), 3);
    prScaledInterp = prScaledInterp./mean(sumpr(:));
        
    
    [~, maxInd] = max(ref);
    refMax = depth(maxInd);
    [~, maxInd] = max(prScaledInterp, [], 3);
    roughWET = refMax - depth(maxInd);
    roughWET = interp2(xq(1:prSpacing:end, 1:prSpacing:end), yq(1:prSpacing:end, 1:prSpacing:end), roughWET(1:prSpacing:end, 1:prSpacing:end), xq, yq, 'cubic');
    
    if isempty(wetCT)
        wetCT = zeros(size(roughWET));   
    else
        roughWET = wetCT+randn(size(wetCT));
%         roughWET = deconv_pr_compressive(prInit, prSpacing, outSpacing, ref, depth, sigma, opt, []);
    end
    
    a = psiT(roughWET-wetCT);
    w = psi(a);
% 	a = (zeros(size(roughWET)));
    
    
    
    convFun = @(wet) (wet2prA(wet, outSpacing, depth, ref, sigma, A));
    derivFun = @(wet) (wet2pr(wet, outSpacing, depth, refDeriv, sigma*0));    
    cropBorder = @(wet) wet(16:end-16, 16:end-16);
    
    prInd = true(size(prScaledInterp));
	prInd(1:prSpacing(1):end, 1:prSpacing(2):end, :) = false;
    subsample = @(pr) subsample2(pr, prInd);
    
    
    options = optimset('MaxFunEvals', MaxFunEvals);
        
    wPrev = w;
    int95Prev = inf;
    diverged = false;
    fValPrev = inf;
    
    figure;
    h1 = subplot(2, 2, 1);
    h2 = subplot(2, 2, 2);
    h3 = subplot(2, 2, 3);
%     h4 = subplot(2, 2, 4);
    for it=0 : nIter
        tic
        
        prSim = convFun(wetCT + w);
        derivSim = derivFun(wetCT + w);

        sumprsim = sum(prSim(:, :, 2:10), 3);        
        prSim = prSim./mean(sumprsim(:));
        prSim(isnan(prSim)) = 0;

        convFun2 = @(wet) convFun(wet)./mean(sumprsim(:));
        
        direction_spat = sum(derivSim.*imgaussfilt((prScaledInterp - prSim), sigma), 3);

        if diverged
            disp('Direction spat set to 0')
            direction_spat((wetCT + w)<20) = 0;
        end
        
        direction = psiT(direction_spat);
        
%         if(it==0)
%             gamma = 0.1/mean(direction_spat(:));
%         end
  
        if mod(it, bisection)==0
            gammmaIt = @(gamma) sum(sum(sum((cropBorder((convFun2(wetCT + psi(softThreshold(a, direction, gamma, lambda))))-prScaledInterp).^2), 3)))/numel(w);
            [gamma, fVal(it+1)] = fminbnd(gammmaIt, abs(gamma)/2, abs(gamma)*2, options);
            disp(['It: ' num2str(it) ' - Gamma: ' num2str(gamma) ' - fVal: ' num2str(fVal(it+1))])
        end
        
        if gamma>gammaMax
            gamma = gammaMax;
        end

        
        disp(['It: ' num2str(it) ])
        
        a = softThreshold(a, direction, gamma, lambda);
        
        w = psi(a);
        
        
        w_diff = abs(w(:)-wPrev(:));
        w_diff = sort(w_diff, 'ascend');
        int95 = w_diff(round(0.95*length(w_diff)));
        disp(int95)
        if fVal(it+1)>fValPrev && diverged
            warning('Divergence. Stopped.');
            w = wPrev;
            break
        else
            if fVal(it+1)>fValPrev
                disp('Diverged.');
                diverged = true;
                w = wPrev;
                int95 = int95Prev;
                fVal(it+1) = fValPrev;
            else
                diverged = false;
            end
        end
        if int95<0.01
            disp('Converged.');
            break
        else
            int95Prev = int95;
            wPrev = w;
            fValPrev = fVal(it+1);
        end

        toc
        
        
        subplot(h1);
        imagesc(abs(w))
        axis equal off
        colormap parula
        
        subplot(h2);
        imagesc(wetCT + w)
        axis equal off
        colormap parula
        title('WEPL map')
        
        subplot(h3);
        plot(1:it+1, fVal)
        xlabel('Iteration')
        ylabel('Cost value')
        pause(0.2)
    end
    
    w = wetCT + w;
    
    function pr = subsample2(pr, prInd)
        pr(prInd) = 0;
    end
end
