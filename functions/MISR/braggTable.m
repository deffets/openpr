
%braggTable Generates a set of shifted IDD from a reference one
%
% bTable = braggTable(ref, depth, wetSpacing, n) shifts the reference IDD
% ref n times by a step equal to wetSpacing and returns the n IDDs.
%
% See also decomposeCurve
%
% Authors : S. Deffet
%

function bTable = braggTable(ref, depth, wetSpacing, n)
    depth = reshape(depth, length(depth), 1) ;
    ref = reshape(ref, length(ref), 1) ;
    
    wet = 0 : wetSpacing : wetSpacing*(n-1) ;
    curves = zeros(n, length(ref)) ;
    
    for i=1 : n
        curves(i, :) = interp1([depth ; 400 ; 700], [ref ; 0 ; 0], depth+wetSpacing*(i-1), 'linear', 'extrap') ;
    end
    
    bTable.wet = wet ;
    bTable.curves = curves ;
end
