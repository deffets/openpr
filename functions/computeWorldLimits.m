
%COMPUTEWORLDLIMITS Bounds of overlapping area in world coordinate system
%
%   [worldLimit1, worldLimit2] = computeWorldLimits(XWorldLimit1, 
%   YWorldLimit1, XWorldLimit2, YWorldLimit2, spacing2) computes the bounds
%   of the overlap between (XWorldLimit1, YWorldLimit1) and (XWorldLimit2
%   ,YWorldLimit2) considering that te world is discretized with steps
%   equal to 'spacing'.
%

function [worldLimit1, worldLimit2] = computeWorldLimits(XWorldLimit1, YWorldLimit1, XWorldLimit2, YWorldLimit2, spacing2)
    if XWorldLimit2(1) < XWorldLimit1(1)
        m = mod(XWorldLimit1(1)-XWorldLimit2(1), spacing2(2));
        
        if m
            worldLimit1(2) = XWorldLimit1(1) + spacing2(2) - m;
        else
            worldLimit1(2) = XWorldLimit1(1);
        end
    else
        worldLimit1(2) = max([XWorldLimit1(1), XWorldLimit2(1)]);
    end
    
    if YWorldLimit2(1)<YWorldLimit1(1)
        m = mod(YWorldLimit1(1)-YWorldLimit2(1), spacing2(1));
        
        if m
            worldLimit1(1) = YWorldLimit1(1) + spacing2(2) - m;
        else
            worldLimit1(1) = YWorldLimit1(1);
        end
    else
        worldLimit1(1) = max([YWorldLimit1(1), YWorldLimit2(1)]);
    end
        
    worldLimit2(1) = min([YWorldLimit1(2), YWorldLimit2(2)]);
    worldLimit2(2) = min([XWorldLimit1(2), XWorldLimit2(2)]);
end
