
function A = readVivaFile(fn)
    fp = fopen(fn,'r');
    if (fp == -1)
        error ('Cannot open viv file for reading');
    end

    dd = dir(fn);
    
    xres = 1152 ;
    yres = 1600 ;


    hdr_size = 2048 ;

    nframes = (dd.bytes - hdr_size) / (xres * yres*2) ;

    fseek(fp,hdr_size,'bof');

    [A, count] = fread(fp,xres*yres*nframes,'*uint16');
    A = reshape(A,[xres,yres,nframes]);

    fclose(fp);
end
