
function A = splitViva(data)
    sd = squeeze(sum(squeeze(sum(data, 1)), 1)) ;
    thresh = 2*min(sd) ;
    
    ind = 1 ;
    ind1 = 1 ;
    for i=2 : size(data, 3)
        if sum(squeeze(sum(data(:, :, i), 1)))<thresh && sum(squeeze(sum(data(:, :, i-1), 1)))>=thresh
            A(:, :, ind) = sum(data(:, :, ind1:i), 3) ;
            ind = ind+1 ;
            ind1 = i ;
        end
    end
end
