
function correct_data = correct_mlic_data_v3(mlic_data, spots_number, first_spot)
    if nargin<3
    end

    plateau_thresh = 20 ;
    
%     correct_data = zeros(spots_number, size(mlic_data, 2)) ;

%     plateau = median(mlic_data(:, 1:10), 2) ;
%     
%     plateau_0 = find(plateau==0) ;
%     plateau_0 = sort(plateau_0) ;
%     plateau_to_be_removed = [] ;
%     all_plateau_to_be_removed = [] ;
%     for i=2 : length(plateau_0)
%         if plateau_0(i)==(plateau_0(i-1)+1)
%             plateau_to_be_removed = [plateau_to_be_removed plateau_0(i-1)] ;
%         elseif numel(plateau_to_be_removed)>2
%             plateau_to_be_removed = [plateau_to_be_removed plateau_0(i-1)] ;
%             all_plateau_to_be_removed = [all_plateau_to_be_removed plateau_to_be_removed] ;
%             plateau_to_be_removed = [] ;
%         else
%             plateau_to_be_removed = [] ;
%         end
%     end
%     disp([num2str(numel(all_plateau_to_be_removed)) ' lines removed during pre-processing'])
%     mlic_data(all_plateau_to_be_removed, :) = [] ;  
    
    mlic_data = [zeros(1, size(mlic_data, 2)) ; mlic_data ; zeros(1, size(mlic_data, 2))] ;
    plateau = median(mlic_data(:, 1:10), 2) ;
    
    k = 1 ;
    for i=2 : (size(mlic_data, 1)-1)
        if plateau(i)>120 && plateau(i+1)>120
            disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                disp(i-1+30)
                disp(plateau(i))
                disp(plateau(i+1))
        end
        if (plateau(i-1)<plateau(i) && plateau(i)>plateau(i+1)) && plateau(i)>plateau_thresh
        	correct_data(k, :) = mlic_data(i-1, :) +  mlic_data(i, :) + mlic_data(i+1, :) ;
        	k = k+1 ;
            if median(correct_data(k-1, 1:10), 2)<120
                disp(i-1+30)
                disp(median(correct_data(k-1, 1:10), 2))
            end
        end
    end
    
    disp(spots_number - size(correct_data, 1))
    disp('-----------')
    
%     k = 1 ;
%     correct_data = [zeros(1, size(correct_data, 2)) ; correct_data ; zeros(1, size(correct_data, 2))] ;
%     plateau = median(correct_data(:, 1:10), 2) ;
%     correct_data2 = zeros(spots_number, size(correct_data, 2)) ;
%     for i=2 : (size(correct_data, 1)-1)
%         if plateau(i-1)>plateau(i) && plateau(i)<plateau(i+1)
%             if plateau(i-1)<plateau(i+1) && plateau(i)<median(plateau)*0.7
%                 correct_data2(k-1, :) = correct_data2(k-1, :)+correct_data(i, :) ;
%             elseif plateau(i)<median(plateau)*0.7
%                 correct_data2(k, :) = correct_data2(k, :)+correct_data(i, :) ;
%             end
%         else
%             correct_data2(k, :) = correct_data2(k, :) + correct_data(i, :) ;
%             k = k + 1 ;
%         end
%     end
%     
%     correct_data = correct_data2 ;
    

    if size(correct_data, 1)==spots_number
        return
    end
    correct_data_saved = correct_data ;
    spots_removed = 0 ;
    plateau = median(correct_data(:, 1:10), 2) ;
    [m, i] = min(plateau) ;
    max_iter = 7 ;
    it = 1 ;
    while it<max_iter && sum(plateau<120) %size(correct_data, 1)>spots_number && 
        it = it+1 ;
        
        plateau = median(correct_data(:, 1:10), 2) ;
        
        [m, i] = min(plateau) ;
        
        if i==1 && plateau(i) < plateau(i+1)
            if (plateau(i)+plateau(i+1))<170
                disp([num2str(plateau(i)) ' + ' num2str(plateau(i+1))])
                correct_data(i+1, :) = correct_data(i+1, :) + correct_data(i, :) ;
            else
                continue
            end
        elseif i==size(correct_data, 1) && plateau(i-1) > plateau(i)
            if (plateau(i)+plateau(i-1))<170
                disp([num2str(plateau(i-1)) ' + ' num2str(plateau(i))])
                correct_data(i-1, :) = correct_data(i-1, :) + correct_data(i, :) ;
            else
                continue
            end
        else
            if (plateau(i)+plateau(i-1))<170
                disp([num2str(plateau(i-1)) ' + ' num2str(plateau(i))])
                correct_data(i-1, :) = correct_data(i-1, :) + correct_data(i, :) ;
            elseif (plateau(i)+plateau(i+1))<170
                disp([num2str(plateau(i)) ' + ' num2str(plateau(i+1))])
                correct_data(i+1, :) = correct_data(i+1, :) + correct_data(i, :) ;
            else
                continue
            end
        end
        
        correct_data(i, :) = [] ; %200 ;
        spots_removed = spots_removed + 1 ;
    end
    
    disp([num2str(spots_removed) ' spots removed'])
    
    plateau = median(correct_data(:, 1:10), 2) ;
    disp([num2str(numel(size(correct_data, 1)+1:spots_number)) ' spots missing'])
    if size(correct_data, 1)<spots_number
        disp('Not enough spots')
        correct_data = [zeros(numel(size(correct_data, 1)+1:spots_number), size(correct_data, 2)) ; correct_data] ;
%         correct_data = correct_data./correct_data*200 ;
    elseif size(correct_data, 1)>spots_number
        disp('Too much spots')
        correct_data(plateau==200, :) = [] ;
        %correct_data(spots_number+1:end, :) = [] ;
    end
    
    disp([num2str(numel(size(correct_data, 1)+1:spots_number)) ' spots missing'])
    if size(correct_data, 1)<spots_number
        disp('Not enough spots')
        correct_data = [zeros(numel(size(correct_data, 1)+1:spots_number), size(correct_data, 2)) ; correct_data] ;
    elseif size(correct_data, 1)>spots_number
        disp('Too much spots')
        correct_data(spots_number+1:end, :) = [] ;
    end
    
    
%     
%     figure
%     [m, data_im] = max(correct_data, [], 2) ;
%     % Re-order data
%         for j=0 : 2 : 41 ;
%             data_im(j*41+1 :(j+1)*41) = flipud(data_im(j*41+1 :(j+1)*41)) ;
%         end
%     imagesc(reshape(data_im, 41, 41))
%     colormap(gray)
end
