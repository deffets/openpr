
function writeMLICData(path, name, simuData, spotsNb, PRSpacing)
    spots_nb_h = spotsNb(1);
    spots_nb_w = spotsNb(2);
    
    headers = ['Measurement: /\n' ...
    'Date: /\n' ...
    'MLIC serial number: /\n' ...
    'Comments:\n' ...
    'Calibration info: /\n' ...
    'Background compensation applied: yes\n' ...
    'Uniformity calibration applied: yes\n' ...
    'Snout dimension: /\n' ...
    'Snout distance: 0 [cm]\n'...
    'FieldSize: 10 [cm]\n' ...
    'SSD: 203 [cm]\n' ...
    'Beam delivery mode: PencilBeamScanning\n' ...
    'Sampling time: 200 [ms]\n' ...
    'Number of samples: 159\n' ...
    'Measurement mode: MovieMode\n' ...
    'Measurement time: /\n' ...
    'Build-up thickness: 0 [cm]\n' ...
    'Build-up density: 0 [g/cm^3]\n' ...
    'Bragg Peak type: Unknown\n' ...
    'Channel count (active): 180\n' ...
    'Channel numbers (active): 1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20;21;22;23;24;25;26;27;28;29;30;31;32;33;34;35;36;37;38;39;40;41;42;43;44;45;46;47;48;49;50;51;52;53;54;55;56;57;58;59;60;61;62;63;64;65;66;67;68;69;70;71;72;73;74;75;76;77;78;79;80;81;82;83;84;85;86;87;88;89;90;91;92;93;94;95;96;97;98;99;100;101;102;103;104;105;106;107;108;109;110;111;112;113;114;115;116;117;118;119;120;121;122;123;124;125;126;127;128;129;130;131;132;133;134;135;136;137;138;139;140;141;142;143;144;145;146;147;148;149;150;151;152;153;154;155;156;157;158;159;160;161;162;163;164;165;166;167;168;169;170;171;172;173;174;175;176;177;178;179;180\n' ...
    'Channel numbers (disabled)\n' ...
    '\n' ...
    'Curves: ' num2str(spots_nb_h*spots_nb_w*2+1) '\n' ...
    'Curve depth: [mm]\n' ...
    '1.030;2.893;4.756;6.619;8.483;10.346;12.209;14.072;15.935;17.798;19.661;21.524;23.387;25.250;27.113;28.976;30.839;32.702;34.565;36.428;38.291;40.154;42.017;43.880;45.744;47.607;49.470;51.333;53.196;55.059;56.922;58.785;60.648;62.511;64.374;66.237;68.100;69.963;71.826;73.689;75.552;77.415;79.278;81.141;83.005;84.868;86.731;88.594;90.457;92.320;94.183;96.046;97.909;99.772;101.635;103.498;105.361;107.224;109.087;110.950;112.813;114.676;116.539;118.402;120.266;122.129;123.992;125.855;127.718;129.581;131.444;133.307;135.170;137.033;138.896;140.759;142.622;144.485;146.348;148.211;150.074;151.937;153.800;155.663;157.527;159.390;161.253;163.116;164.979;166.842;168.705;170.568;172.431;174.294;176.157;178.020;179.883;181.746;183.609;185.472;187.335;189.198;191.061;192.924;194.788;196.651;198.514;200.377;202.240;204.103;205.966;207.829;209.692;211.555;213.418;215.281;217.144;219.007;220.870;222.733;224.596;226.459;228.322;230.185;232.049;233.912;235.775;237.638;239.501;241.364;243.227;245.090;246.953;248.816;250.679;252.542;254.405;256.268;258.131;259.994;261.857;263.720;265.583;267.446;269.310;271.173;273.036;274.899;276.762;278.625;280.488;282.351;284.214;286.077;287.940;289.803;291.666;293.529;295.392;297.255;299.118;300.981;302.844;304.707;306.571;308.434;310.297;312.160;314.023;315.886;317.749;319.612;321.475;323.338;325.201;327.064;328.927;330.790;332.653;334.516\n' ...
    'Curve gains: [counts]\n'];

    frameNbI = size(simuData, 1)/spots_nb_h;
    frameNbJ = size(simuData, 2)/spots_nb_w;
    
    simuData = flip(simuData, 2) ;
    simuData = rot90(rot90(rot90(simuData))) ;


    for i=1 : frameNbJ
        for j=1 : frameNbI
            dataTemp = simuData((i-1)*spots_nb_h+1:i*spots_nb_h, (j-1)*spots_nb_w+1:j*spots_nb_w, :);
            dataTemp = reshape(dataTemp, spots_nb_h*spots_nb_w, 180);
            
            for k=0 : 2 : spots_nb_w-1
                dataTemp(k*spots_nb_h+1 :(k+1)*spots_nb_h, :) = flipud(dataTemp(k*spots_nb_h+1 :(k+1)*spots_nb_h, :)) ;
            end
            
            disp(['Writing to ' fullfile(path, [name num2str(i) '_' num2str(j) '.csv'])]);
            
            fid = fopen(fullfile(path, [name num2str(i) '_' num2str(j) '.csv']), 'w');
            
            fprintf(fid, headers);
            
            for l=1 : 180
            	fprintf(fid, '%f;', single(0)) ;
            end
            fprintf(fid, '\n');
            for k=1 : spotsNb(1)*spotsNb(2)
                for l=1 : 180
                    fprintf(fid, '%f;', single(squeeze(dataTemp(k, l)))) ;
                end
                
                fprintf(fid, '\n');
                
                for l=1 : 180
            	fprintf(fid, '%f;', single(0)) ;
            end
            fprintf(fid, '\n');
            end
            
            fclose(fid);
        end
    end
    
    fid = fopen(fullfile(path, 'files_order.csv'), 'w');
    fprintf(fid, 'files_names x y z\n') ;
    for i=1 : frameNbJ
        for j=1 : frameNbI
            fprintf(fid, [name num2str(i) '_' num2str(j) '.csv 0 -%f %f\n'], (j-1)*spots_nb_h*PRSpacing(1)/10, (i-1)*spots_nb_w*PRSpacing(2)/10) ;
        end
    end
    fclose(fid);
end
