function data_res = correct_mlic_data2(data)
 
s = size(data) ;
 
data_res = zeros(41*41, 180) ;
 
ind_res=1;
cond = 0;
i=2;
out = 1 ;
ind_temp=0;
while(cond == 0 )
    if i>size(data, 1)
        break
    end
    if(data(i, 1) > 20)
        if (data(i-1,1) > data(i,1))
            ind_temp = i-1;
        elseif (data (i+1,1) > data (i,1))
            ind_temp = i+1;
        else
            ind_temp = i;
        end
        i = ind_temp+2;
        data_res(ind_res,:)=data(ind_temp-1,:)+data(ind_temp,:)+data(ind_temp+1,:);
        ind_res = ind_res+1 ;
        if (ind_res == 41*41+1)
            cond=1;
            break ;
        end
    end
    i = i+1;
    out = out + 1 ;
    if out>3000
        break;
    end
   
end
size(data_res)
 
end