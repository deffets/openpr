
%GET_MLIC_DATA reads data from several OmniPro-Incline csv files
%
% [res, files_coord, depth] = GET_MLIC_DATA(DIR, FILES_ORDER, 
% calibration_file, spots_nb_h, spots_nb_w) reads the 'samples' data
% from the files listed in FILES_ORDER in directory DIR, retrieve the right
% number of rows, multipliy them by the (gain) coefficients, and put the 
% result in RES. (RES is a 3D matrix - the 1st and 2nd dimensions are the 
% spatial coordinates and the 3rd dimension contains the data 
%
% The content of FILES_ORDER should be as follow:
% files_name x        y        z
% name1      coord1   coord2   coord3
% name2      coord1_2 coord2_2 coord3_3
% ...        ...      ...      ...
% where the first row is just a header row; name1, name2, ... are the names
% of the OmniPro-Incline csv files; and coord1, coord2, ... are the
% coordinates that were intered in Mosaic.
%
% See also get_reference_data, read_mlic_file, read_calibration_file
%
% Authors : S. Deffet
%

function [res, files_coord, depth] = get_mlic_data(dir, files_order, calibration_file, spots_nb_h, spots_nb_w)
    spots_nb = spots_nb_h*spots_nb_w ;

    if ~isempty(calibration_file)
        coeff = read_calibration_file(calibration_file) ;
        coeff = repmat(coeff, spots_nb, 1) ;
    else
        warning('No calibration file') ;
    end

    % Info from file
    files_data = importdata(fullfile(dir, files_order)) ;
    files_names = files_data.textdata(2:end, 1) ;
    files_coord = files_data.data(1:end, 1:3) ;
    
    if size(files_coord, 1)<2
        step = 1;
    else
        step = max(abs(files_coord(2, :)-files_coord(1, :))) ;
    end
    
    files_off = -files_coord/step ;
    files_off(:, 3) = -files_off(:, 3) ;
    files_off = files_off-repmat(min(files_off), size(files_off, 1), 1) ;

    % Parameters
    subplots_nb = length(files_names) ;
    subplots_nb_h = numel(unique(files_off(:, 3))) ;
    subplots_nb_w = numel(unique(files_off(:, 2))) ;
    
    % Initialization
    res = zeros(spots_nb_h*subplots_nb_h, spots_nb_w*subplots_nb_w, 180) ;
    for i=1 : subplots_nb
        disp(['Computing subplot ' num2str(i) '/' num2str(subplots_nb)])
        % Data from MLIC file
        [curve_gains, samples, depth] = read_mlic_file(fullfile(dir, files_names{i})) ;
        
        if ~isempty(calibration_file)
            data = correct_mlic_data(samples, spots_nb, 'pre-process') ;
            data = data.*coeff ;
        else
            data = correct_mlic_data(curve_gains, spots_nb, 'notpre-process') ;
        end

        % Re-order data
        for j=0 : 2 : spots_nb_w
            data(j*spots_nb_h+1 :(j+1)*spots_nb_h, :) = flipud(data(j*spots_nb_h+1 :(j+1)*spots_nb_h, :)) ;
        end

        % Add the frame in the global matrix
        x1 = files_off(i, 3)*spots_nb_h+1 ;
        x2 =(files_off(i, 3)+1)*spots_nb_h;
        y1 = files_off(i, 2)*spots_nb_w+1 ;
        y2 = (files_off(i, 2)+1)*spots_nb_w ;

        res(int16(x1:1:x2), int16(y1:1:y2), :) = reshape(data, spots_nb_h, spots_nb_w, 180) ;
    end
    res = rot90(res) ;
    
    res = flip(res, 2) ;
%     
%     res = rot90(rot90(res)) ;
end
