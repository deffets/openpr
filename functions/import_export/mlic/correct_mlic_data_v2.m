
%CORRECT_MLIC_DATA gives the right amount of rows from the data read in an
%OmniPro-Incline csv file
%
% CORRECT_DATA = CORRECT_MLIC_DATA(MLIC_DATA, SPOTS_NB, FIRST_SPOT) gives
% a number of rows derived from MLIC_DATA equal to SPOTS_NB. If FIRST_SPOT
% is equal to 'pre-process', the first spot is treated in a seperate way. 
% This should be always done since the first spot has higher values than 
% the others.
%
% !!! This function is only suitable for low doses and suppose an
% appropriate sampling time.
%

function correct_data = correct_mlic_data_v2(mlic_data, spots_number, first_spot)
    s = size(mlic_data) ;

    correct_data = zeros(s(1), s(2)) ;
    first_mlic_data = mean(mlic_data(:, 1:10), 2) ;
    first_mlic_data(first_mlic_data==0) = 0.01 ;

    thresh = 4 ;
    
    ind = 1 ;
    flag1 = 1 ;
    flag_init = 1 ;
    for i=1 : s(1)
        if first_mlic_data(i)>thresh
            if flag_init==1
                disp('h')
                correct_data(ind, :) = mlic_data(i, :) ;
                flag_init = 0 ;
            else
                correct_data(ind, :) = correct_data(ind, :) + mlic_data(i, :) ;
            end
            
            flag1 = 0 ;
        elseif flag1==0
            ind = ind+1 ;
            flag1 = 1 ;
        end
    end
    
    ind
    spots_number
end
