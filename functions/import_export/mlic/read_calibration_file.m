
%READ_CALIBRATION_FILE reads OmniPro-Incline calibration file
%
% coeff = read_calibration_file(filename) returns the calibration 
% coefficients of an OmniPro-Incline calibration file.
%
% See also read_mlic_file
%
% Authors : S. Deffet
%

function coeff = read_calibration_file(filename)
    fid = fopen(filename) ;
    
    % Skip first lines
    for i=1 : 12
        fgetl(fid) ;
    end
    
    coeff = str2num(fgetl(fid)) ;
    coeff = reshape(coeff, 1, length(coeff)) ;
    
    fclose(fid) ;
end
