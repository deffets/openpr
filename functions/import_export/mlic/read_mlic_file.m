
%READ_MLIC_FILE reads data from OmniPro-Incline .csv file
%
% CURVE_GAINS = READ_MLIC_FILE(FILENAME) put the data following the line
% 'Curve gains: [counts]' in FILENAME into CURVE_GAINS. Those data are
% already multiplied by the gain coefficients.
%
% [CURVE_GAINS, SAMPLES] = READ_MLIC_FILE(FILENAME) put the data following 
% the line 'Samples: [counts]' in FILENAME into CURVE_GAINS. Those data are
% not multiplied by the gain coefficients.
%
% [CURVE_GAINS, SAMPLES, DEPTH] = READ_MLIC_FILE(FILENAME) put the depth
% coordinates in FILENAME into DEPTH.
%
% Authors : S. Deffet
%

function [curve_gains, samples, depth] = read_mlic_file(filename)
    fid = fopen(filename) ;
    
    % Skip first lines
    for i=1 : 23
        fgetl(fid) ;
    end
    
    curve_info = fgetl(fid) ;
    curve_nb = str2double(curve_info(9:end)) ;
    
    fgetl(fid) ; % Skip 1 line
    
    depth = str2num(fgetl(fid)) ;
    
    fgetl(fid) ; % Skip 1 line
    
    curve_gains = zeros(curve_nb, 180) ;
    for i=1:curve_nb
        line = str2num(fgetl(fid)) ;
        curve_gains(i, :) = line' ;
    end
    
    lTemp = fgetl(fid) ; % Skip 1 line
    
    if ~ischar(lTemp)
        samples = [];
        return;
    end
    
    samples_info = fgetl(fid) ;
    samples_nb = str2double(samples_info(10:end)) ;
    
    samples = zeros(samples_nb, 180) ;
    for i=1:samples_nb
        line = str2num(fgetl(fid)) ;
        samples(i, :) = line' ;
    end
    
    fclose(fid) ;
end
