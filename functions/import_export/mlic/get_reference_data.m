
%GET_REFERENCE_DATA reads reference Bragg curves measured through air only
%
% [data, depth] = get_reference_data(reference_file, calibration_file, 
% spots_nb_h, spots_nb_w) returns a set of reference Bragg curves and 
% associated depth information based on the data stored in an 
% OmniPro-Incline csv file. The calibration file parameter refers to an 
% OmniPro-Incline calibration file. spots_nb_h and spots_nb_w are the 
% number of spots along each scanning direction.
%
% [data, depth] = get_reference_data(reference_file, [],  spots_nb_h, 
% spots_nb_w) Same as above but relying on reference_file only for the
% calibration coefficients.
%
% See also loadReference
%
% Authors : S. Deffet
%

function [data, depth] = get_reference_data(reference_file, calibration_file, spots_nb_h, spots_nb_w)
    spots_nb = spots_nb_h*spots_nb_w ;

    if ~isempty(calibration_file)
        coeff = read_calibration_file(calibration_file) ;
        coeff = repmat(coeff, spots_nb, 1) ;
    else
        warning('No calibration file') ;
    end
    
    spots_nb = spots_nb_h*spots_nb_w ;
    
    [curve_gains, samples, depth] = read_mlic_file(reference_file) ;
    
    if ~isempty(calibration_file)
        data = correct_mlic_data(samples, spots_nb, 'notpre-process') ;
        data = data.*coeff ;
    else
        data = correct_mlic_data(curve_gains, spots_nb, 'notpre-process') ;
    end
    

    % Re-order data
    for j=0 : 2 : spots_nb_w ;
        data(j*spots_nb_h+1 :(j+1)*spots_nb_h, :) = flipud(data(j*spots_nb_h+1 :(j+1)*spots_nb_h, :)) ;
    end
    
    data = reshape(data, spots_nb_h, spots_nb_w, 180) ;

    data = rot90(data) ;
    data = flip(data, 2) ;
end
