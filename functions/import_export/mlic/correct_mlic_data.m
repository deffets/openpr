
%CORRECT_MLIC_DATA gives the rows corresponding to pencil beam shots from the data read in an
%OmniPro-Incline csv file
%
% CORRECT_DATA = CORRECT_MLIC_DATA(MLIC_DATA, SPOTS_NB, FIRST_SPOT) gives
% a number of rows derived from MLIC_DATA equal to SPOTS_NB. If FIRST_SPOT
% is equal to 'pre-process', the first spot is treated in a seperate way. 
% This should be always done since the first spot has higher values than 
% the others.
%
% !!! This function is only suitable for low doses and suppose an
% appropriate sampling time.
%
% Authors : S. Deffet
%

function correct_data = correct_mlic_data(mlic_data, spots_number, first_spot)
    s = size(mlic_data) ;
    
%     correct_data = zeros(spots_number, s(2)) ;
    first_mlic_data = median(mlic_data(:, 1:10), 2) ;
%  
%     % First spot is handled
%     if strcmp(first_spot, 'pre-process')
%         mlic_data_init = mlic_data ;
%         
%         ind = find(first_mlic_data>10) ;
%         correct_data(1, :) = mlic_data(ind(1), :) + mlic_data(ind(1)+1, :) ;
%         
%         mlic_data(ind(1), :) = zeros(size(mlic_data(ind(1), :))) ;
%         mlic_data(ind(1)+1, :) = zeros(size(mlic_data(ind(1), :))) ;
% 
%         first_mlic_data = median(mlic_data(:, 1:10), 2) ; % Recompute for threshold computation
%     end
    
    
    thresh_max = max(first_mlic_data);
    thresh_min = min(first_mlic_data);
    
%     step = min(first_mlic_data)/10; % Maybe should be defined as input parameter for this function
%     
%     % Find the appropriate threshold so that there will be only
%     % spots_number lines remaining
%     thresh_saved = thresh_max ;
%     for thresh = 0 : step : thresh_max
%         new_size1 = numel(first_mlic_data(first_mlic_data>thresh)) ;
%         
%         if strcmp(first_spot, 'pre-process')
%             if new_size1==(spots_number-1)
%                 thresh_saved = thresh ;
%                 break
%             elseif new_size1<(spots_number-1)
%                 thresh_saved = thresh-step ;
%                 break
%             end
%         else
%             if new_size1==spots_number
%                 thresh_saved = thresh ;
%                 break
%             elseif new_size1<spots_number
%                 thresh_saved = thresh-step ;
%                 break
%             end
%         end
%     end
%     thresh = thresh_saved ;
%     
%     if thresh~=spots_number
%         error(['Could not find right amount of IDDs - thresh: ' num2str(thresh) ' - spots <: ' num2str(new_size1)])
%     end
    
%     if strcmp(first_spot, 'pre-process') && new_size1~=(spots_number-1)
% %         warning(['new_size1~=spots_number. new_size1 = ' num2str(new_size1) ' / thresh = ' num2str(thresh)])
%         correct_data = correct_mlic_data(mlic_data_init, spots_number, ' ') ;
%         return
%     end

    opt.Display = 'off';
    opt.TolX = 0.1;
    thresh = fminbnd(@correct_mlic_data_aux2, thresh_min, thresh_max, opt);
    
    fVal = correct_mlic_data_aux2(thresh);
    if fVal~=0
        for ii = thresh*1.5 : -opt.TolX : thresh*0.5
            fVal = correct_mlic_data_aux2(ii);
            
            if fVal==0
                thresh = ii;
                break
            end
        end
    end
    
    fVal = correct_mlic_data_aux2(thresh);
    if fVal~=0
        warning('Could not find the right number of IDDs')
    end
    
    correct_data = correct_mlic_data_aux(thresh);
    
    

    function fVal = correct_mlic_data_aux2(thresh)
        if strcmp(first_spot, 'pre-process')
            ind = 2 ;
        else
            ind = 1 ;
        end
        ind = ind-1;
        
        skip = false;
        for i=1 : s(1)
            if skip
                skip = false;
                continue;
            end
            
            if first_mlic_data(i)>thresh
                ind = ind+1;
                skip = true;
            end
        end
    
        fVal = (spots_number-ind)^2;
    end

    function correct_data = correct_mlic_data_aux(thresh)
        correct_data = zeros(spots_number, s(2)) ;
        
        if strcmp(first_spot, 'pre-process')
            ind = 2 ;
        else
            ind = 1 ;
        end
        
        skip = false;
        for i=1 : s(1)
            if skip
                skip = false;
                continue;
            end
            
            if first_mlic_data(i)>thresh
                correct_data(ind, :) = correct_data(ind, :) + mlic_data(i, :) + mlic_data(i+1, :);
                ind = ind+1;
                skip = true;
            end
        end
	end
end
