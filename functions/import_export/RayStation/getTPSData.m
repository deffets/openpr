
%GETTPSDATA Read a proton radiography simulation performed by RayStation
%
% res = getTPSData(dir, files_order, spots_nb_h, spots_nb_w, direction1, 
% direction2) reads a proton radiography simulation according to a 
% specific format. Read the wiki for more information.
%
% Input arguments
%  dir - Directory of the simulation
%  directory_names - File listing the directories containing the simulated 
%                    IDD (in each directory, there must be one file per IDD 
%                    with names like IDD_X=0.0Y=0.0.txt). Here is the syntax 
%                    of this file:
%                    directory_names x y z
%                    relative_path wrt. to this text file/directory1.csv x1 y1 z1
%                    relative_path wrt. to this text file/directory2.csv x2 y2 z2
%                    ...
%  spots_nb_h - Number of spots per directory along the first scanning direction
%  spots_nb_w - Number of spots per directory along the second scanning direction
%  direction1 - If the IDD files have names like IDD_X=0.0Y=0.0.txt, direction1 should be equal to 'X'
%  direction2 - If the IDD files have names like IDD_X=0.0Y=0.0.txt, direction2 should be equal to 'Y'
%
% See also loadRSSimulation
%
% Authors : S. Deffet
%

function res = getTPSData(dir, files_order, spots_nb_h, spots_nb_w, direction1, direction2)
    % Info from file
    files_data = importdata(fullfile(dir, files_order)) ;
    files_names = files_data.textdata(2:end, 1) ;
    files_coord = files_data.data(1:end, 1:3) ;
    
    if size(files_coord, 1)<2
        step = 1;
    else
        step = max(abs(files_coord(2, :)-files_coord(1, :))) ;
    end
    
    files_off = -files_coord/step ;
    files_off(:, 3) = -files_off(:, 3) ;
    files_off = files_off-repmat(min(files_off), size(files_off, 1), 1) ;

    % Parameters
    subplots_nb = length(files_names) ;
    subplots_nb_h = numel(unique(files_off(:, 3))) ;
    subplots_nb_w = numel(unique(files_off(:, 2))) ;

    % Initialization
    for i=1 : subplots_nb
        % Data from MLIC file
        data = readTPSData(fullfile(dir, files_names(i)), direction1, direction2) ;

        % Add the frame in the global matrix
        x1 = files_off(i, 3)*spots_nb_h+1 ;
        x2 =(files_off(i, 3)+1)*spots_nb_h;
        y1 = files_off(i, 2)*spots_nb_w+1 ;
        y2 = (files_off(i, 2)+1)*spots_nb_w ;

        res(int16(x1:1:x2), int16(y1:1:y2), :) = data ;
    end
    
    res = flip(res, 1) ;
    res = rot90(res) ;
    
%     res = res(:, :, 200:end);
%     for i=1 : size(res, 1)
%         for j=1 : size(res, 2)
%             res(i, j, :) = smooth(squeeze(res(i, j, :)));
%         end
%     end
end
