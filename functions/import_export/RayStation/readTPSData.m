
%READTPSDATA Raystation simulations
%
% frame = readTPSData(directoryName, direction1, direction2) reads a single
% fram of a proton radiography simulation according to a specific format. Read
% the wiki for more information.
%
%
%%Input arguments
% dir - Directory of the simulation
% directory_names - File listing the directories containing the simulated IDD (in each directory, there must be one file per IDD with names like IDD_X=0.0Y=0.0.txt). Here is the syntax of this file:
%                   directory_names x y z
%                   relative_path wrt. to this text file/directory1.csv x1 y1 z1
%                   relative_path wrt. to this text file/directory2.csv x2 y2 z2
%                   ...
% direction1 - If the IDD files have names like IDD_X=0.0Y=0.0.txt, direction1 should be equal to 'X' </td></tr>
% direction2 - If the IDD files have names like IDD_X=0.0Y=0.0.txt, direction2 should be equal to 'Y' </td></tr>
%
%
% See also getTPSData
%
% Authors : S. Deffet
%

function frame = readTPSData(directoryName, direction1, direction2)
    directoryName = char(directoryName) ;
    
    listing = dir(directoryName);
    
    listing(1:2) = [] ;
    dir1 = zeros(length(listing), 1) ;
    dir2 = zeros(length(listing), 1) ;
    
    for i=1 : length(listing)
        disp(listing(i).name)
        
        str = listing(i).name ;
        idd(i, :) = importdata(fullfile(directoryName, str)) ;
        
        [pathstr, str, ext] = fileparts(str) ;
        
        ind = regexp(str, [direction1 '=']) ;
        str(1:ind-1) = [] ;
        ind = regexp(str, [direction2 '=']) ;
        str = [str(1:ind-1) ' ; ' str(ind:end) ' ;'] ;
        eval(str)
        
        eval(['dir1(' num2str(i) ') = ' direction1 ' ;']) ;
        eval(['dir2(' num2str(i) ') = ' direction2 ' ;']) ;
    end
    
    dir1_unique = sort(unique(dir1)) ;
    dir2_unique = sort(unique(dir2)) ;
    
    dir1_min = min(dir1_unique) ;
    dir2_min = min(dir2_unique) ;
    
    if length(dir1_unique)>1
        dir1_step = abs(dir1_unique(2)-dir1_unique(1)) ;
    else
        dir1_step = 1;
    end
    
    if length(dir2_unique)>1
        dir2_step = abs(dir2_unique(2)-dir2_unique(1)) ;
    else
        dir2_step = 1;
    end
    
    dir1 = (dir1 - dir1_min)/dir1_step + 1;
    dir2 = (dir2 - dir2_min)/dir2_step + 1;
    
    for i=1 : length(listing)
        frame(dir1(i), dir2(i), :) = squeeze(idd(i, :)) ;
    end
end
