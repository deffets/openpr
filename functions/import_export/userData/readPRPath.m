
%READPRPATH returns a path based stored by writepath using a pair (key, path)
%
% outPath = readPRPath(key) returns the last path entered by the user
% associated to the key key
%
% See also writePRPath
%
% Authors : S. Deffet
%

function outPath = readPRPath(key)
    outPath = pwd;

    userDataPath = getRegguiPath('userData');
    
    userPaths = fullfile(userDataPath, 'userPaths.txt');
    
    if ~exist(userPaths, 'file')
        return
    end
    
    f = fopen(userPaths, 'r');
    
    lineT = fgetl(f);
    ind = 1;
    while ischar(lineT)
        ind1 = strfind(lineT, ' ');
        
        pathName = lineT(1:ind1-1);
        
        if strcmp(key, pathName)
        	outPath = lineT(ind1+1:end);
            break;
        end
        
        lineT = fgetl(f);
        ind = ind+1;
    end
end
