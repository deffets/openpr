
%WRITEPRPATH stores paths and associated keys (useful to store user
% preferences)
%
% writePRPath(key, path) stores a path with its associated key. If the key
% already exists, the path is updated.
%
% See also readPRPath
%
% Authors : S. Deffet
%

function writePRPath(key, path)
    userDataPath = getRegguiPath('userData');
    
    if ~exist(userDataPath, 'dir')
        mkdir(userDataPath)
    end
    
    userPaths = fullfile(userDataPath, 'userPaths.txt');
    
    pathFound = 0;
    pathNames = [];
    paths = [];
    
    if exist(userPaths, 'file')    
        f = fopen(userPaths, 'r');
        
        lineT = fgetl(f);
        ind = 1;
        while ischar(lineT)
            ind1 = strfind(lineT, ' ');

            pathNames{ind} = lineT(1:ind1-1);
            paths{ind} = lineT(ind1+1:end);

            if strcmp(key, pathNames{ind})
                paths{ind} = path;
                pathFound = 1;
            end

            lineT = fgetl(f);
            ind = ind+1;
        end

        fclose(f);
    
        if ~pathFound
            pathNames{length(pathNames)+1} = key;
            paths{length(paths)+1} = path;
        end
    end
    
    f = fopen(userPaths, 'w');
    for i=1:length(paths)
        fprintf(f, '%s %s\n', pathNames{i}, paths{i});
    end

    fclose(f);
end
