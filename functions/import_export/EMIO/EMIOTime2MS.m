
%read_emio_file Converts time (FPGACounts) format recorded by EMIO
% DIagnostic into milliseconds.
%
% ms = EMIOTime2MS(t) converts time (FPGACounts) format recorded by EMIO
% DIagnostic into milliseconds.
%
% See also read_emio_file, stack_emio_data
%
% Authors : S. Deffet
%

function ms = EMIOTime2MS(t)
    s = strsplit(t, ':');
    ms = s(3)*1000+s(2)*60*1000+s(1)*3600*1000;
    
    s = strsplit(t, '.');
    ms = ms+s(end);
end