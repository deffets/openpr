
%read_emio_file Reads a file recorded by EMIO Diagnostic
%
% [FPGACounts, ports, channels] = read_emio_file(filename) reads 3 columns
% (FPGACounts, ports, channels) from a file recorded by EMIO Diagnostic.
%
% See also EMIOTime2MS, stack_emio_data
%
% Authors : S. Deffet
%

function [FPGACounts, ports, channels] = read_emio_file(filename)
    d = importdata(filename);
    
    FPGACounts = d.data(:, 2);
    ports = d.data(:, 6);
    channels = d.data(:, 7:7+63);
end
