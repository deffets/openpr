
%stack_emio_data Creates a proton radiograph from raw measurements acquired
% with EMIO Diagnostic
%
%
% pr = stack_emio_data(FPGACounts, ports_list, channels, port1, port2) creates a proton radiograph from raw measurements acquired
% by EMIO Diagnostic and read with read_emio_file.
%
% See also EMIOTime2MS, read_emio_file
%
% Authors : S. Deffet
%

function [pr, spotTime] = stack_emio_data(FPGACounts, ports_list, channels, port1, port2)
    ports = unique(ports_list);
    
    if numel(ports)~=2
        error('There should be only two different ports in EMIO data');
    end
    
    if nargin<5
        port1 = ports(1);
        port2 = ports(2);
    end
    
    t = FPGACounts;
    t1_1 = t(ports_list==port1);
    t1_1 = t1_1(1);
    t1_2 = t(ports_list==port2);
    t1_2 = t1_2(1);
    
    t(ports_list==port1) = t(ports_list==port1)-t1_1;
    t(ports_list==port2) = t(ports_list==port2)-t1_2;
    
    t(t<0) = t(t<0) + 2^32;
    t1 = t(ports_list==port1);
    [~, mind] = max(t1);
    typical_step = (t1(mind)-t1(mind-10))/10;
    typical_step = typical_step;
    
%     pr = [];
%     h = waitbar(0, 'Sorting...');
%     for i=2 : length(t)-1
%         if ports_list(i)==port1
%             if ports_list(i+1)==port2 && abs(t(i+1)-t(i))<=typical_step
%                 pr = [pr ; channels(i, :) channels(i+1, :)];
%             else
%                 if ports_list(i-1)==port2 && abs(t(i-1)-t(i))<=typical_step
%                     pr = [pr ; channels(i-1, :) channels(i, :)];
%                 end
%             end
%         end
%         
%         if ~mod(i, 100)
%             waitbar(i / (length(t)-1))
%         end
%     end
    
    c1 = ports_list(1:end-1)==port1;
    c2 = ports_list(2:end)==port2;
    c3 = abs(t(2:end)-t(1:end-1))<=typical_step;
    
    ind1 = c1&c2&c3;
    ind2 = ind1;
    ind2(end, :) = [];
    
    if sum(ind2)<sum(ind1)
        ind2 = [true ; ind2];
    else
        ind2 = [false ; ind2];
    end
    
    ind1_1 = ind1;
    ind2_1 = ind2;
    
    c1 = ports_list(1:end-1)==port2;
    c2 = ports_list(2:end)==port1;
    c3 = abs(t(2:end)-t(1:end-1))<=typical_step;
    
    ind2 = c1&c2&c3;
    ind2 = xor(ind2, ind2_1)&ind2;
    
    ind1 = ind2;
    ind1(end, :) = [];
    
    if sum(ind1)<sum(ind2)
        ind1 = [true ; ind1];
    else
        ind1 = [false ; ind1];
    end    
    
    ind1 = xor(ind1, ind1_1)&ind1;
    
    ind2(1:end-1) = ind1(2:end);
    if sum(ind2)<sum(ind1)
        ind2(end) = true;
    else
        ind2(end) = false;
    end   
    
    pr = [channels(ind1_1|ind1, :), channels(ind2_1|ind2, :)];
    spotTime = t(ind1_1|ind1);
end