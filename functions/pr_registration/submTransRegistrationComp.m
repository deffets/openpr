
function [tMat, out2] = submTransRegistrationComp(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, maxIter, maxTrans, prevTrans, preTransfo, costMethod)
    if nargin<12
        warning('costMethod = squared')
        costMethod = 'squared' ;
    end

    out2 = [] ;
    
    if sprSpacing(1)~=sprSpacing(2) || sprSpacing(2)~=sprSpacing(3)
        error('Spacing should be uniform')
    end
    
    maxTrans = abs(maxTrans) ;
    if maxTrans<0.5
        maxTrans = 0.5 ;
    end
    
    preTransfo(4, 1:3) = prevTrans ;
    tform = affine3d(preTransfo) ;
    
    imR = imref3d(size(spr), [0 size(spr, 2)]-size(spr, 2)/2, [0 size(spr, 1)]-size(spr, 1)/2, [0 size(spr, 3)]-size(spr, 3)/2) ;
	spr = imwarp(spr, imR, tform, 'OutputView', imR, 'FillValues', 0) ;
    
    
%     transVect = submTransRegistration(wetImage270, braggImage270, wetImage0, braggImage0, ref, refDepth, sigma, maxIter, maxTrans, 'false') ;

    
    fun1 = @(trans) submTransRegErrorSPR(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, [trans 0 0], costMethod) ;
    fun2 = @(trans) submTransRegErrorSPR(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, [0 trans 0], costMethod) ;
    fun3 = @(trans) submTransRegErrorSPR(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, [0 0 trans], costMethod) ;

    [trans1, errorVal1] = fminbnd(fun1, double(-maxTrans), double(maxTrans), optimset('MaxFunEvals', maxIter, 'TolX', 0.05, 'TolFun', 10^-6, 'Display', 'off')) ;
    
    if ~isempty(braggImage0)
        [trans2, errorVal2] = fminbnd(fun2, double(-maxTrans), double(maxTrans), optimset('MaxFunEvals', maxIter, 'TolX', 0.25, 'TolFun', 10^-6, 'Display', 'off')) ;
    else
        trans2 = 0 ;
    end
    
    [trans3, errorVal3] = fminbnd(fun3, double(-maxTrans), double(maxTrans), optimset('MaxFunEvals', maxIter, 'TolX', 0.05, 'TolFun', 10^-6, 'Display', 'off')) ;
    
    transVect = [trans1 trans2 trans3] + prevTrans ;
    
    tMat = eye(4) ;
%     tMat(4, 1) = -transVect(2) ;
%     tMat(4, 2) = transVect(3) ;
%     tMat(4, 3) = -transVect(1) ;
    tMat(4, 1) = transVect(1) ;
    tMat(4, 2) = transVect(2) ;
    tMat(4, 3) = transVect(3) ;
end
