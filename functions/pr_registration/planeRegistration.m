
function [angle, tMat] = planeRegistration(wetImage, braggImage)
    spacing1 = wetImage.get('spacing') ;
    spacing2 = braggImage.get('spacing') ;
    
    [worldLimit1, worldLimit2] = computeWorldLimits(wetImage.get('XWorldLimits'), ...
        wetImage.get('YWorldLimits'), braggImage.get('XWorldLimits'), ...
        braggImage.get('YWorldLimits'), spacing2) ;

    worldLimit2(1) = worldLimit2(1) - spacing2(1) ;
    worldLimit2(2) = worldLimit2(2) - spacing2(2) ;

    wetData = wetImage.getData(worldLimit1, worldLimit2, spacing1) ;
    braggData = braggImage.getData(worldLimit1, worldLimit2, spacing2) ;
    
    range = maximumPosition(braggData, braggImage.get('depth')) ;
    range = flip(range, 1) ;
    
    wetR = imref2d(size(wetData), spacing1(2), spacing1(1)) ;
    rangeR = imref2d(size(range), spacing2(2), spacing2(2)) ;
    
%     figure ; imagesc(wetData)  ; axis equal off
%     figure ; imagesc(range)  ; axis equal off

    [optimizer, metric] = imregconfig('multimodal') ;
    [optimizer2, metric2] = imregconfig('monomodal') ;

    tform = imregtform(wetData, wetR, 1./range, rangeR, 'rigid', optimizer2, metric) ;
%     figure ; imagesc(imwarp(wetData, wetR, tform)) ; axis equal off ; figure

    tMat = tform.T ;
    angle = asin(tMat(1, 3)) ;
end
