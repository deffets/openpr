
% output and input angles in deg
function [tMat, rollAngle] = braggRollIterComp(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, maxIter, maxAngle, prevAngle, preTransfo, postTransfo, costMethod)
    if nargin<13
        warning('costMethod set to squared')
        costMethod = 'squared' ;
    end

    initErr = rollErrorComp(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, prevAngle, preTransfo, postTransfo, costMethod) ;
    
    fun = @(angle) rollErrorComp(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, angle+prevAngle, preTransfo, postTransfo, costMethod) ;
    
    maxAngle = abs(maxAngle) ;
    if maxAngle<0.3
        maxAngle = 0.3 ;
    end

    [rollAngle, errorVal] = fminbnd(fun, double(-maxAngle), double(maxAngle), optimset('MaxFunEvals', maxIter, 'TolX', 0.05, 'TolFun', 10^(-6), 'Display', 'off')) ;
    
    if initErr<errorVal
        rollAngle = deg2rad(prevAngle) ;
    else
        rollAngle = deg2rad(rollAngle+prevAngle) ;
    end
    
    tMat =  [cos(rollAngle) -sin(rollAngle) 0 0 ;
             sin(rollAngle) cos(rollAngle) 0 0 ;
             0 0 1 0 ;
             0 0 0 1] ;
         
    rollAngle = rad2deg(rollAngle) ;
end
