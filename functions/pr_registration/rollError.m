
% angle in deg
function err = rollError(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, angle, autoAlign)
    if nargin<9
        disp('autoAlign set to true')
        autoAlign = 'true' ;
    end
    
    rollAngle = deg2rad(angle) ;
    correctionMatrix =  [cos(rollAngle) -sin(rollAngle) 0 0 ;
             sin(rollAngle) cos(rollAngle) 0 0 ;
             0 0 1 0 ;
             0 0 0 1] ;
    tform = affine3d(correctionMatrix) ;
    
	try
        spr2 = imwarp(gpuArray(spr), imref3d(size(spr)), tform, 'OutputView', imref3d(size(spr), sprSpacing(2), sprSpacing(1), sprSpacing(3))) ;
        disp('GPU used')
    catch
        spr2 = imwarp(spr, imref3d(size(spr)), tform, 'OutputView', imref3d(size(spr), sprSpacing(2), sprSpacing(1), sprSpacing(3))) ;
        disp('GPU not used')
    end
    
    [wet270, wet270Spacing] = spr2totalWET(spr2, sprSpacing, 0, 270) ;
    wetImage270 = Image2D(wet270, [0 0 0], wet270Spacing) ;

    [wet0, wet0Spacing] = spr2totalWET(spr2, sprSpacing, 0, 0) ;
    wetImage0 = Image2D(wet0, [0 0 0], wet0Spacing) ;
    
    
    depth = braggImage270.get('depth') ;
    wetSpacing = wetImage270.get('spacing') ;
    PRSpacing = braggImage270.get('spacing') ;

    if sum(~(depth==refDepth))
        error('PR depth and reference depth are not the same')
    end

    if wetSpacing(1)~=wetSpacing(2)
        error('Spacing should be uniform')
    end

    wet = wetImage270.get('data') ;

    simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
    simuImage = BraggImage(simu, depth, wetImage270.get('origin'), wetSpacing) ;

    [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
        simuImage.get('YWorldLimits'), braggImage270.get('XWorldLimits'), ...
        braggImage270.get('YWorldLimits'), PRSpacing) ;
    worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
    worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

    simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
    braggImageData = braggImage270.getData(worldLimit1, worldLimit2, PRSpacing) ;

    e270 = squareError(simuImageData, depth, braggImageData, depth) ;

    
    depth = braggImage0.get('depth') ;
    wetSpacing = wetImage0.get('spacing') ;
    PRSpacing = braggImage0.get('spacing') ;

    if sum(~(depth==refDepth))
        error('PR depth and reference depth are not the same')
    end

    if wetSpacing(1)~=wetSpacing(2)
        error('Spacing should be uniform')
    end

    wet = wetImage0.get('data') ;

    simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
    simuImage = BraggImage(simu, depth, wetImage0.get('origin'), wetSpacing) ;

    [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
        simuImage.get('YWorldLimits'), braggImage0.get('XWorldLimits'), ...
        braggImage0.get('YWorldLimits'), PRSpacing) ;
    worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
    worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

    simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
    braggImageData = braggImage0.getData(worldLimit1, worldLimit2, PRSpacing) ;

    e0 = squareError(simuImageData, depth, braggImageData, depth) ;
    
    
    e = [e270(:) ; e0(:)] ;
    err = double(sum(sum(e))/numel(e)) ;
    
    return
    
    %%

%   
    depth = braggImage270.get('depth') ;
    
    PRSpacing = braggImage270.get('spacing') ;
    
    if sum(~(depth==refDepth))
        error('PR depth and reference depth are not the same')
    end
    
    try
%         error('Do not use GPU')
        spr2 = gather(imrotate(gpuArray(spr), angle, 'bilinear', 'crop')) ;
%         spr2 = gather(imwarp(gpuArray(spr), imref3d(size(spr), sprSpacing(2), sprSpacing(1), sprSpacing(3)), tform, 'OutputView', imref3d(size(spr)))) ;
        disp('GPU used')
    catch e
        spr2 = imrotate(spr, angle, 'bilinear', 'crop') ;
%         spr2 = imwarp(spr, imref3d(size(spr), sprSpacing(2), sprSpacing(1), sprSpacing(3)), tform, 'OutputView', imref3d(size(spr))) ;
        disp('GPU not used')
    end
    
    [wet, wetSpacing] = spr2totalWET(spr2, sprSpacing, 0, 270) ;
    wetImage = Image2D(wet, [0 0 0], wetSpacing) ;
    
%     [worldLimit1, worldLimit2] = computeWorldLimits(wetImage.get('XWorldLimits'), ...
%         wetImage.get('YWorldLimits'), braggImage270.get('XWorldLimits'), ...
%         braggImage270.get('YWorldLimits'), wetSpacing) ;
%     worldLimit2(1) = worldLimit2(1) - wetSpacing(1) ;
%     worldLimit2(2) = worldLimit2(2) - wetSpacing(2) ;
%     
%     wet = wetImage.getData(worldLimit1, worldLimit2, wetSpacing) ;
    
%     wet = wetImage.get('data') ;
    
    
    simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
    simuImage = BraggImage(simu, depth, wetImage.get('origin'), wetSpacing) ;
%     simuImage = BraggImage(simu, depth, worldLimit1, wetSpacing) ;
    
    if strcmp(autoAlign, 'true')
        max_iter = 2 ;       
        newOrigin_tmp = braggImage270.get('origin') ;
        for i=1 : max_iter ;
            newOrigin = registration2DIter(simuImage, braggImage270, simuImage.get('depth'), 5) ;
            braggImage270.set('origin', newOrigin) ;

            if newOrigin_tmp(1)==newOrigin(1) && newOrigin_tmp(2)==newOrigin(2)
                break
            end
            newOrigin_tmp = newOrigin ;
        end   
    end
           
    [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
        simuImage.get('YWorldLimits'), braggImage270.get('XWorldLimits'), ...
        braggImage270.get('YWorldLimits'), PRSpacing) ;
    worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
    worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

    simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
    braggImageData = braggImage270.getData(worldLimit1, worldLimit2, PRSpacing) ;
    
    e = squareError(simuImageData, depth, braggImageData, depth) ;
    err = sum(sum(e))/numel(e) ;

    return

%%
    depth270 = braggImage270.get('depth') ;
    depth0 = braggImage0.get('depth') ;
    
    PRSpacing270 = braggImage270.get('spacing') ;
    PRSpacing0 = braggImage0.get('spacing') ;
    
    if sum(~(depth270==refDepth)) || sum(~(depth0==refDepth))
        error('PR depth and reference depth are not the same')
    end
    
    depth = depth270 ;

    try
        error('Do not use GPU')
        spr2 = gather(imrotate(gpuArray(spr), angle, 'bilinear', 'crop')) ;
%         spr2 = gather(imwarp(gpuArray(spr), imref3d(size(spr), sprSpacing(2), sprSpacing(1), sprSpacing(3)), tform, 'OutputView', imref3d(size(spr)))) ;
        disp('GPU used')
    catch e
        spr2 = imrotate(spr, angle, 'bilinear', 'crop') ;
%         spr2 = imwarp(spr, imref3d(size(spr), sprSpacing(2), sprSpacing(1), sprSpacing(3)), tform, 'OutputView', imref3d(size(spr))) ;
        disp('GPU not used')
    end
    

    
	[wet270, wet270Spacing] = spr2totalWET(spr2, sprSpacing, 0, 270) ;
%     wetImage270 = Image2D(wet270, [0 0 0], wet270Spacing) ;
%     [worldLimit1, worldLimit2] = computeWorldLimits(wetImage270.get('XWorldLimits'), ...
%         wetImage270.get('YWorldLimits'), braggImage270.get('XWorldLimits'), ...
%         braggImage270.get('YWorldLimits'), wet270Spacing) ;
%     worldLimit2(1) = worldLimit2(1) - wet270Spacing(1) ;
%     worldLimit2(2) = worldLimit2(2) - wet270Spacing(2) ;
%     wet270 = wetImage270.getData(worldLimit1, worldLimit2, wet270Spacing) ;
%     worldLimit1_270 = worldLimit1 ;


%     [wet0, wet0Spacing] = spr2totalWET(spr2, sprSpacing, 0, 0) ;
%     wetImage0 = Image2D(wet0, [0 0 0], wet0Spacing) ;
%     [worldLimit1, worldLimit2] = computeWorldLimits(wetImage0.get('XWorldLimits'), ...
%         wetImage0.get('YWorldLimits'), braggImage0.get('XWorldLimits'), ...
%         braggImage0.get('YWorldLimits'), wet0Spacing) ;
%     worldLimit2(1) = worldLimit2(1) - wet0Spacing(1) ;
%     worldLimit2(2) = worldLimit2(2) - wet0Spacing(2) ;
%     wet0 = wetImage0.getData(worldLimit1, worldLimit2, wet0Spacing) ;
%     worldLimit1_0 = worldLimit1 ;
    
    simu270 = wet2pr(wet270, wet270Spacing, depth, ref, sigma) ;
%     simuImage270 = BraggImage(simu270, depth, worldLimit1_270, wet270Spacing) ;
    simuImage270 = BraggImage(simu270, depth, [0 0 0], wet270Spacing) ;
    
%     simu0 = wet2pr(wet0, wet0Spacing, depth, ref, sigma) ;
%     simuImage0 = BraggImage(simu0, depth, worldLimit1_0, wet0Spacing) ;
%     simuImage0 = BraggImage(simu0, depth, [0 0 0], wet0Spacing) ;
    
    max_iter = 4 ;       
%     newOrigin_tmp270 = braggImage270.get('origin') ;
%     for i=1 : max_iter ;
%         newOrigin = registration2DIter(simuImage270, braggImage270, simuImage270.get('depth'), 5) ;
%         braggImage270.set('origin', newOrigin) ;
% 
%         if newOrigin_tmp270(1)==newOrigin(1) && newOrigin_tmp270(2)==newOrigin(2)
%             break
%         end
%         newOrigin_tmp270 = newOrigin ;
%     end
           
%     newOrigin_tmp0 = braggImage0.get('origin') ;
%     for i=1 : max_iter ;
%         newOrigin = registration2DIter(simuImage0, braggImage0, simuImage0.get('depth'), 5) ;
%         braggImage0.set('origin', newOrigin) ;
% 
%         if newOrigin_tmp0(1)==newOrigin(1) && newOrigin_tmp0(2)==newOrigin(2)
%             break
%         end
%         newOrigin_tmp0 = newOrigin ;
%     end
            
           
    [worldLimit1, worldLimit2] = computeWorldLimits(simuImage270.get('XWorldLimits'), ...
        simuImage270.get('YWorldLimits'), braggImage270.get('XWorldLimits'), ...
        braggImage270.get('YWorldLimits'), PRSpacing270) ;
    worldLimit2(1) = worldLimit2(1) - PRSpacing270(1) ;
    worldLimit2(2) = worldLimit2(2) - PRSpacing270(2) ;

    simuImageData = simuImage270.getData(worldLimit1, worldLimit2, PRSpacing270) ;
    braggImageData = braggImage270.getData(worldLimit1, worldLimit2, PRSpacing270) ;
    
        
    r = maximumPosition(simuImageData, depth) ;
    figure ; imagesc(r)
    r = maximumPosition(braggImageData, depth) ;
    figure ; imagesc(r)
    error('stop')
    
    e270 = squareError(simuImageData, depth, braggImageData, depth) ;
    
%     [worldLimit1, worldLimit2] = computeWorldLimits(simuImage0.get('XWorldLimits'), ...
%         simuImage0.get('YWorldLimits'), braggImage0.get('XWorldLimits'), ...
%         braggImage0.get('YWorldLimits'), PRSpacing0) ;
%     worldLimit2(1) = worldLimit2(1) - PRSpacing0(1) ;
%     worldLimit2(2) = worldLimit2(2) - PRSpacing0(2) ;
% 
%     simuImageData = simuImage0.getData(worldLimit1, worldLimit2, PRSpacing0) ;
%     braggImageData = braggImage0.getData(worldLimit1, worldLimit2, PRSpacing0) ;
    
    e0 = [] ; %squareError(simuImageData, depth, braggImageData, depth) ;
    
    
    e = [e270(:) ; e0(:)] ;
    err = sum(e)/numel(e) ;
end
