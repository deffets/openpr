
% angle in deg
function err = rollErrorComp(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, angle, preTransfo, postTransfo, costMethod)
    if nargin<11
        warning('costMethod = squared')
        costMethod = 'squared' ;
    end

	if sprSpacing(1)~=sprSpacing(2) || sprSpacing(2)~=sprSpacing(3)
        error('Spacing should be uniform')
	end
    
    rollAngle = deg2rad(angle) ;
    tMat =  [cos(rollAngle) -sin(rollAngle) 0 0 ;
             sin(rollAngle) cos(rollAngle) 0 0 ;
             0 0 1 0 ;
             0 0 0 1] ;
    
    tform = affine3d(preTransfo*tMat*postTransfo) ;
    
    imR = imref3d(size(spr), [0 size(spr, 2)]-size(spr, 2)/2, [0 size(spr, 1)]-size(spr, 1)/2, [0 size(spr, 3)]-size(spr, 3)/2) ;
    spr2 = imwarp(spr, imR, tform, 'OutputView', imR, 'FillValues', 0) ;

    if ~isempty(braggImage270)
        [wet270, wet270Spacing] = spr2totalWET(spr2, sprSpacing, 0, 270) ;
        wetImage270 = Image2D(wet270, [0 0 0], wet270Spacing) ;

        depth = braggImage270.get('depth') ;
        wetSpacing = wetImage270.get('spacing') ;
        PRSpacing = braggImage270.get('spacing') ;

        if sum(~(depth==refDepth))
            error('PR depth and reference depth are not the same')
        end

        if wetSpacing(1)~=wetSpacing(2)
            error('Spacing should be uniform')
        end

        wet = wetImage270.get('data') ;

        simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
        simuImage = BraggImage(simu, depth, wetImage270.get('origin'), wetSpacing) ;

        [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
            simuImage.get('YWorldLimits'), braggImage270.get('XWorldLimits'), ...
            braggImage270.get('YWorldLimits'), PRSpacing) ;
        worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
        worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

        simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
        braggImageData = braggImage270.getData(worldLimit1, worldLimit2, PRSpacing) ;

        switch costMethod
            case 'squared'
                e270 = squareError(simuImageData, depth, braggImageData, depth) ;
            case 'composed'
                r = rangeError(simuImageData(3:end-3, 3:end-3, :), depth, braggImageData(3:end-3, 3:end-3, :), depth, 0.5, 15) ;
                e270 = squareError(simuImageData(3:end-3, 3:end-3, :), depth, shiftPR(braggImageData(3:end-3, 3:end-3, :), depth, r), depth) ;
        end
    else
        e270 = [] ;
    end
    
    if ~isempty(braggImage0)
        [wet0, wet0Spacing] = spr2totalWET(spr2, sprSpacing, 0, 0) ;
        wetImage0 = Image2D(wet0, [0 0 0], wet0Spacing) ;
    
        depth = braggImage0.get('depth') ;
        wetSpacing = wetImage0.get('spacing') ;
        PRSpacing = braggImage0.get('spacing') ;

        if sum(~(depth==refDepth))
            error('PR depth and reference depth are not the same')
        end

        if wetSpacing(1)~=wetSpacing(2)
            error('Spacing should be uniform')
        end

        wet = wetImage0.get('data') ;

        simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
        simuImage = BraggImage(simu, depth, wetImage0.get('origin'), wetSpacing) ;

        [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
            simuImage.get('YWorldLimits'), braggImage0.get('XWorldLimits'), ...
            braggImage0.get('YWorldLimits'), PRSpacing) ;
        worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
        worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

        simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
        braggImageData = braggImage0.getData(worldLimit1, worldLimit2, PRSpacing) ;

        switch costMethod
            case 'squared'
                e0 = squareError(simuImageData, depth, braggImageData, depth) ;
            case 'composed'
                r = rangeError(simuImageData, depth, braggImageData, depth, 0.5, 15) ;
                e0 = squareError(simuImageData, depth, shiftPR(braggImageData, depth, r), depth) ;
        end
    else
        e0 = [] ;
    end
    
    e = [e270(:) ; e0(:)] ;
    err = double(sum(e)/numel(e)) ;
end
