
function err = angleErrorComp(spr, sprSpacing, braggImage, ref, refDepth, sigma, gantryAngle, angle, preTransfo, postTransfo, registrationAngle, costMethod)
% angleErrorComp  Computes the registration error between a PR and an SPR
% map
%   err = angleErrorComp(spr, sprSpacing, braggImage, ref, refDepth, sigma,
%   gantryAngle, angle, preTransfo, postTransfo) computes the registration
%   error between the PR braggImage and a simulation based on the SPR map
%   spr (whose voxel size is sprSpacing) and the reference curved measured
%   in air ref (refDepth being the depth information of this curve) with a
%   spot size sigma. gantryAngle is the angle of the gantry used for making
%   the PR. The registration angle named angle is asumed to be in the plane
%   perpendicular to the beam. preTransfo and postTranso are
%   transformations to be respectively applied before and after the angle.
%   All angles are in degree.
%   err = angleErrorComp(spr, sprSpacing, braggImage, ref, refDepth, sigma,
%   gantryAngle, angle, preTransfo, postTransfo, registrationAngle) Same as
%   above except that the angle named angle is asumed to be in the plane
%   perpendicular to the beam if it were shoot with the angle named
%   registrationAngle. gantryAngle still corresponds to the acquisition
%   angle of the PR braggImage.
%
%   See also rollErrorComp submTransRegError

    if nargin<12 && nargin>10
        warning('costMethod = squared')
        costMethod = 'squared' ;
    elseif nargin<11
        warning('registrationAngle = gantryAngle')
        registrationAngle = gantryAngle ;
        costMethod = 'composed' ;
    end
    
    depth = braggImage.get('depth') ;
    if sum(~(depth==refDepth))
        error('PR depth and reference depth are not the same')
    end
    
    if sprSpacing(1)~=sprSpacing(2) || sprSpacing(2)~=sprSpacing(3)
        error('Spacing should be uniform')
    end
    
    angle = deg2rad(angle) ;
    
    tMat = eye(4) ;
    switch registrationAngle
        case 270
            tMat(1, 1) = cos(angle) ;
            tMat(1, 3) = sin(angle) ;
            tMat(3, 1) = -sin(angle) ;
            tMat(3, 3) = cos(angle) ;
        case 0
            tMat(2, 2) = cos(angle) ;
            tMat(2, 3) = -sin(angle) ;
            tMat(3, 2) = sin(angle) ;
            tMat(3, 3) =  cos(angle) ;
    end
    
    transfo = preTransfo*tMat*postTransfo ;
    tform = affine3d(transfo) ;
    
    imR = imref3d(size(spr), [0 size(spr, 2)]-size(spr, 2)/2, [0 size(spr, 1)]-size(spr, 1)/2, [0 size(spr, 3)]-size(spr, 3)/2) ;
    spr = imwarp(spr, imR, tform, 'OutputView', imR, 'FillValues', 0) ;
                        
                        
    [wet, wetSpacing] = spr2totalWET(spr, sprSpacing, 0, gantryAngle) ;
    wetImage = Image2D(wet, [0 0 0], wetSpacing) ;
    
    simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
    simuImage = BraggImage(simu, depth, wetImage.get('origin'), wetSpacing) ;
%     simuImage = BraggImage(simu, depth, worldLimit1, wetSpacing) ;
    
    PRSpacing = braggImage.get('spacing') ;
    
    [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
        simuImage.get('YWorldLimits'), braggImage.get('XWorldLimits'), ...
        braggImage.get('YWorldLimits'), PRSpacing) ;
    worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
    worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

    simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
    braggImageData = braggImage.getData(worldLimit1, worldLimit2, PRSpacing) ;

    switch costMethod
        case 'squared'
            e = squareError(simuImageData, depth, braggImageData, depth) ;
        case 'composed'
            r = rangeError(simuImageData(3:end-3, 3:end-3, :), depth, braggImageData(3:end-3, 3:end-3, :), depth, 0.5, 15) ;
            e = squareError(simuImageData(3:end-3, 3:end-3, :), depth, shiftPR(braggImageData(3:end-3, 3:end-3, :), depth, r), depth) ;
    end
    
    err = sum(sum(e))/numel(e) ;
end
