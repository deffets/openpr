
function err = submTransRegError(wetImage270, braggImage270, wetImage0, braggImage0, ref, refDepth, sigma, transVect, autoAlign)
    if nargin<9
        autoAlign = 'true' ;
    end
    
    if transVect(1)==0 && transVect(2)==0
        e270 = [] ;
    else
        if abs(max(transVect))>1
            err = 999 ;
            return
        end

        depth = braggImage270.get('depth') ;
        wetSpacing = wetImage270.get('spacing') ;
        PRSpacing = braggImage270.get('spacing') ;

        if sum(~(depth==refDepth))
            error('PR depth and reference depth are not the same')
        end

        if wetSpacing(1)~=wetSpacing(2)
            error('Spacing should be uniform')
        end

        wet = wetImage270.get('data') ;

        try
    %         error('Do not use GPU')
            wet = gather(imtranslate(gpuArray(wet), [transVect(2) transVect(1)], 'bilinear', 'OutputView', 'same', 'FillValues', 0)) ;
        catch e
            wet = imtranslate(wet, [transVect(2) transVect(1)], 'bilinear', 'OutputView', 'same', 'FillValues', 0) ;
        end

        simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
        simuImage = BraggImage(simu, depth, wetImage270.get('origin'), wetSpacing) ;

        if strcmp(autoAlign, 'true')
            max_iter = 2 ;       
            newOrigin_tmp = braggImage270.get('origin') ;
            for i=1 : max_iter ;
                newOrigin = registration2DIter(simuImage, braggImage270, simuImage.get('depth'), 5) ;
                braggImage270.set('origin', newOrigin) ;

                if newOrigin_tmp(1)==newOrigin(1) && newOrigin_tmp(2)==newOrigin(2)
                    break
                end
                newOrigin_tmp = newOrigin ;
            end
        end

        [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
            simuImage.get('YWorldLimits'), braggImage270.get('XWorldLimits'), ...
            braggImage270.get('YWorldLimits'), PRSpacing) ;
        worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
        worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

        simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
        braggImageData = braggImage270.getData(worldLimit1, worldLimit2, PRSpacing) ;

        e270 = squareError(simuImageData, depth, braggImageData, depth) ;
    end

    if transVect(3)==0 && transVect(1)==0
        e0 = [] ;
    else
        depth = braggImage0.get('depth') ;
        wetSpacing = wetImage0.get('spacing') ;
        PRSpacing = braggImage0.get('spacing') ;

        if sum(~(depth==refDepth))
            error('PR depth and reference depth are not the same')
        end

        if wetSpacing(1)~=wetSpacing(2)
            error('Spacing should be uniform')
        end

        wet = wetImage0.get('data') ;

        try
    %         error('Do not use GPU')
            wet = gather(imtranslate(gpuArray(wet), [transVect(3) transVect(1)], 'bilinear', 'OutputView', 'same', 'FillValues', 0)) ;
            disp('GPU used')
        catch
            wet = imtranslate(wet, [transVect(3) transVect(1)], 'bilinear', 'OutputView', 'same', 'FillValues', 0) ;
            disp('GPU not used')
        end

        simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
        simuImage = BraggImage(simu, depth, wetImage0.get('origin'), wetSpacing) ;

        if strcmp(autoAlign, 'true')
            max_iter = 2 ;       
            newOrigin_tmp = braggImage0.get('origin') ;
            for i=1 : max_iter ;
                newOrigin = registration2DIter(simuImage, braggImage0, simuImage.get('depth'), 5) ;
                braggImage0.set('origin', newOrigin) ;

                if newOrigin_tmp(1)==newOrigin(1) && newOrigin_tmp(2)==newOrigin(2)
                    break
                end
                newOrigin_tmp = newOrigin ;
            end
        end

        [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
            simuImage.get('YWorldLimits'), braggImage0.get('XWorldLimits'), ...
            braggImage0.get('YWorldLimits'), PRSpacing) ;
        worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
        worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

        simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
        braggImageData = braggImage0.getData(worldLimit1, worldLimit2, PRSpacing) ;

        e0 = squareError(simuImageData, depth, braggImageData, depth) ;
    end
        e = [e270(:) ; e0(:)] ;
        err = double(sum(sum(e))/numel(e)) ;
    
    return
    
%%
    depth = braggImage.get('depth') ;
    wetSpacing = wetImage.get('spacing') ;
    PRSpacing = braggImage.get('spacing') ;
    
    if sum(~(depth==refDepth))
        error('PR depth and reference depth are not the same')
    end
    
    if wetSpacing(1)~=wetSpacing(2)
        error('Spacing should be uniform')
    end
    
    wet = wetImage.get('data') ;
    
    try
%         error('Do not use GPU')
        wet = gather(imtranslate(gpuArray(wet), [transVect(2) transVect(1)], 'bilinear', 'OutputView', 'same', 'FillValues', 0)) ;
        disp('GPU used')
    catch
        wet = imtranslate(wet, [transVect(2) transVect(1)], 'bilinear', 'OutputView', 'same', 'FillValues', 0) ;
        disp('GPU not used')
    end
    
    simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
    simuImage = braggImage(simu, depth, wetImage.get('origin'), wetSpacing) ;

    max_iter = 2 ;       
    newOrigin_tmp = braggImage.get('origin') ;
    for i=1 : max_iter ;
        newOrigin = registration2DIter(simuImage, braggImage, simuImage.get('depth'), 5) ;
        braggImage.set('origin', newOrigin) ;

        if newOrigin_tmp(1)==newOrigin(1) && newOrigin_tmp(2)==newOrigin(2)
            break
        end
        newOrigin_tmp = newOrigin ;
    end
           
    [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
        simuImage.get('YWorldLimits'), braggImage.get('XWorldLimits'), ...
        braggImage.get('YWorldLimits'), PRSpacing) ;
    worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
    worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

    simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
    braggImageData = braggImage.getData(worldLimit1, worldLimit2, PRSpacing) ;

    e = squareError(simuImageData, depth, braggImageData, depth) ;
    err = sum(sum(e))/numel(e) ;
end
