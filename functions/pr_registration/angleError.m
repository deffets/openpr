
% angle in deg
function err = angleError(wetImage, braggImage, ref, refDepth, sigma, angle, autoAlign)
    if nargin<7
        autoAlign = 'true' ;
    end
    
    depth = braggImage.get('depth') ;
    wetSpacing = wetImage.get('spacing') ;
    PRSpacing = braggImage.get('spacing') ;
    
    if sum(~(depth==refDepth))
        error('PR depth and reference depth are not the same')
    end
    
    if wetSpacing(1)~=wetSpacing(2)
        error('Spacing should be uniform')
    end
    
    wet = wetImage.get('data') ;    
    
%     wet = imrotateMex(wet, -angle) ;
    try
%         error('Do not use GPU')
        wet = gather(imrotate(gpuArray(wet), -angle, 'bilinear', 'crop')) ;
        disp('GPU used')
    catch
        wet = imrotate(wet, angle, 'bilinear', 'crop') ;
        disp('GPU not used')
    end
    
    simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
    simuImage = BraggImage(simu, depth, wetImage.get('origin'), wetSpacing) ;
%     simuImage = BraggImage(simu, depth, worldLimit1, wetSpacing) ;
    
    if strcmp(autoAlign, 'true')
        max_iter = 2 ;       
        newOrigin_tmp = braggImage.get('origin') ;
        for i=1 : max_iter ;
            newOrigin = registration2DIter(simuImage, braggImage, simuImage.get('depth'), 5) ;
            braggImage.set('origin', newOrigin) ;

            if newOrigin_tmp(1)==newOrigin(1) && newOrigin_tmp(2)==newOrigin(2)
                break
            end
            newOrigin_tmp = newOrigin ;
        end
    end
           
    [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
        simuImage.get('YWorldLimits'), braggImage.get('XWorldLimits'), ...
        braggImage.get('YWorldLimits'), PRSpacing) ;
    worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
    worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

    simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
    braggImageData = braggImage.getData(worldLimit1, worldLimit2, PRSpacing) ;

    e = squareError(simuImageData, depth, braggImageData, depth) ;
    err = sum(sum(e))/numel(e) ;
end
