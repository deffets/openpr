
function tMat = wetRegIter(spr, sprSpacing, braggImage270, braggImage0)
    [wet270, wet270Spacing] = spr2totalWET(spr, sprSpacing, 0, 270) ;
    wet270Image = Image2D(wet270, [0 0 0], wet270Spacing) ;
    [angle270, tMat270] = planeRegistration(wet270Image, braggImage270) ;

    [wet0, wet0Spacing] = spr2totalWET(spr, sprSpacing, 0, 0) ;
    wet0Image = Image2D(wet0, [0 0 0], wet0Spacing) ;
    [angle0, tMat0] = planeRegistration(wet0Image, braggImage0) ;

    tMat_tmp = eye(4) ;
    tMat_tmp(1, 1) = tMat270(1, 1) ;
    tMat_tmp(1, 3) = -tMat270(1, 2) ;
    tMat_tmp(3, 1) = -tMat270(2, 1) ;
    tMat_tmp(3, 3) = tMat270(2, 2) ;
    tMat270 = tMat_tmp ;

    tMat_tmp = eye(4) ;
    tMat_tmp(2, 2) = tMat0(1, 1) ;
    tMat_tmp(2, 3) = tMat0(1, 2) ;
    tMat_tmp(3, 2) = tMat0(2, 1) ;
    tMat_tmp(3, 3) = tMat0(2, 2) ;
    tMat0 = tMat_tmp ;

    tMat = tMat270*tMat0 ;
end
