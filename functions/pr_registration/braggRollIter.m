
% maxAngle in rad
% rollAngle in rad
function [tMat, rollAngle] = braggRollIter(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, maxIter, maxAngle, autoAlign)
    if nargin<6
        maxIter = 10 ;
        maxAngle = 1 ;
    end
    
    fun = @(angle) rollError(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, angle, autoAlign) ;
    
    if rad2deg(maxAngle)<0.3
        maxAngle = deg2rad(0.3) ;
    end

    [rollAngle, errorVal] = fminbnd(fun, double(-rad2deg(maxAngle)), double(rad2deg(maxAngle)), optimset('MaxFunEvals', maxIter, 'TolX', 0.1, 'TolFun', 0.1, 'Display', 'off')) ;
    
    rollAngle = deg2rad(rollAngle) ;
    tMat =  [cos(rollAngle) -sin(rollAngle) 0 0 ;
             sin(rollAngle) cos(rollAngle) 0 0 ;
             0 0 1 0 ;
             0 0 0 1] ;
end
