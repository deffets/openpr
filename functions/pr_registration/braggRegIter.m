
function [tMat, angle270, angle0] = braggRegIter(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, maxIter, maxAngle270, maxAngle0, autoAlign)
    tic
    [wet270, wet270Spacing] = spr2totalWET(spr, sprSpacing, 0, 270) ;
    wetImage270 = Image2D(wet270, [0 0 0], wet270Spacing) ;
    [angle270, errorVal270] = braggRegistration(wetImage270, braggImage270, ref, refDepth, sigma, maxIter, maxAngle270, autoAlign) ;
%     angle270 = braggRegistrationMex(wetImage270, braggImage270, ref, refDepth, sigma, maxIter, maxAngle270) ;
    toc
    tic
    [wet0, wet0Spacing] = spr2totalWET(spr, sprSpacing, 0, 0) ;
    wetImage0 = Image2D(wet0, [0 0 0], wet0Spacing) ;
    [angle0, errorVal0] = braggRegistration(wetImage0, braggImage0, ref, refDepth, sigma, maxIter, maxAngle0, autoAlign) ;
    toc

    tMat270 = eye(4) ;
    tMat270(1, 1) = cos(angle270) ;
    tMat270(1, 3) = sin(angle270) ;
    tMat270(3, 1) = -sin(angle270) ;
    tMat270(3, 3) = cos(angle270) ;
    
    tMat0 = eye(4) ;
    tMat0(2, 2) = cos(angle0) ;
    tMat0(2, 3) = -sin(angle0) ;
    tMat0(3, 2) = sin(angle0) ;
    tMat0(3, 3) =  cos(angle0) ;
    
    tMat = tMat270*tMat0 ;
end
