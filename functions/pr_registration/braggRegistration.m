
function [angle, errorVal] = braggRegistration(wetImage, braggImage, ref, refDepth, sigma, maxIter, maxAngle, autoAlign)
    if nargin<6
        maxIter = 10 ;
        maxAngle = 1 ;
    end
    
    depth = braggImage.get('depth') ;
    wetSpacing = wetImage.get('spacing') ;
    
    if sum(~(depth==refDepth))
        error('PR depth and reference depth are not the same')
    end
    
    if wetSpacing(1)~=wetSpacing(2)
        error('Spacing should be uniform')
    end
    
    fun = @(angle) angleError(wetImage, braggImage, ref, refDepth, sigma, angle, autoAlign) ;

    if rad2deg(maxAngle)<0.3
        maxAngle = 0.3 ;
    end

    [angle, errorVal] = fminbnd(fun, double(-rad2deg(maxAngle)), double(rad2deg(maxAngle)), optimset('MaxFunEvals', maxIter, 'TolX', 0.1, 'TolFun', 0.1, 'Display', 'iter')) ;
    
    angle = deg2rad(angle) ;
end
