
function [transVect, errorVal] = submTransRegistration(wetImage270, braggImage270, wetImage0, braggImage0, ref, refDepth, sigma, maxIter, maxTrans, autoAlign)
    if nargin<6
        maxIter = 10 ;
        maxTrans = 1 ;
    end
    
    if maxTrans<1
        maxTrans = 1 ;
    end
    
%     fun = @(trans) submTransRegError(wetImage270, braggImage270, wetImage0, braggImage0, ref, refDepth, sigma, trans) ;
% 
%     
%     transVect = fminunc(fun, [0 0 0], optimoptions('fminunc', 'Algorithm', ...
%         'quasi-newton', 'FunctionTolerance', 0.1, 'StepTolerance', 0.1, 'MaxFunctionEvaluations', maxIter, 'FiniteDifferenceStepSize', 0.1, 'Display', 'iter')) ;
%     errorVal = 0 ;
%     
%     return
%     
    
    fun1 = @(trans) submTransRegError(wetImage270, braggImage270, wetImage0, braggImage0, ref, refDepth, sigma, [trans 0 0], autoAlign) ;
    fun2 = @(trans) submTransRegError(wetImage270, braggImage270, wetImage0, braggImage0, ref, refDepth, sigma, [0 trans 0], autoAlign) ;
    fun3 = @(trans) submTransRegError(wetImage270, braggImage270, wetImage0, braggImage0, ref, refDepth, sigma, [0 0 trans], autoAlign) ;

    [trans1, errorVal1] = fminbnd(fun1, double(-maxTrans), double(maxTrans), optimset('MaxFunEvals', maxIter, 'TolX', 0.1, 'TolFun', 0.1, 'Display', 'off')) ;
    [trans2, errorVal2] = fminbnd(fun2, double(-maxTrans), double(maxTrans), optimset('MaxFunEvals', maxIter, 'TolX', 0.1, 'TolFun', 0.1, 'Display', 'off')) ;
    [trans3, errorVal3] = fminbnd(fun3, double(-maxTrans), double(maxTrans), optimset('MaxFunEvals', maxIter, 'TolX', 0.1, 'TolFun', 0.1, 'Display', 'off')) ;
    
    errorVal = errorVal1+errorVal2+errorVal3 ;
    transVect = [trans1 trans2 trans3] ;
end
