
function newOrigin = registration2DIter(braggImage1, braggImage2, step, nbIter)
    origin = braggImage2.get('origin') ;
    
    errors = zeros(5*nbIter, 1) ;
    origins = zeros(5*nbIter, 2) ;
    for i=1 : 5*nbIter
        origin2 = origin ;
        
        switch mod(i, 5)
            case 1
                origin2 = origin ;
            case 2
                origin2(1) = origin2(1) - (floor((i-1)/5)+1)*step(1) ;
            case 3
                origin2(1) = origin2(1) + (floor((i-1)/5)+1)*step(1) ;
            case 4
                origin2(2) = origin2(2) - (floor((i-1)/5)+1)*step(2) ;
            case 0
                origin2(2) = origin2(2) + (floor((i-1)/5)+1)*step(2) ;
        end

        braggImage2.set('origin', origin2) ;
        spacing2 = braggImage2.get('spacing') ;

        [worldLimit1, worldLimit2] = computeWorldLimits(braggImage1.get('XWorldLimits'), ...
            braggImage1.get('YWorldLimits'), braggImage2.get('XWorldLimits'), ...
            braggImage2.get('YWorldLimits'), spacing2) ;
        worldLimit2(1) = worldLimit2(1) - spacing2(1) ;
        worldLimit2(2) = worldLimit2(2) - spacing2(2) ;

        data1 = braggImage1.getData(worldLimit1, worldLimit2, spacing2) ;
        data2 = braggImage2.getData(worldLimit1, worldLimit2, spacing2) ;

        e = squareError(data1, braggImage1.get('depth'), data2, braggImage2.get('depth')) ;
        errors(i) = sum(sum(e))/numel(e) ;
        origins(i, 1) = origin2(1) ;
        origins(i, 2) = origin2(2) ;
    end
    
    [m, ind] = min(errors) ;
    newOrigin(1) = origins(ind, 1) ;
    newOrigin(2) = origins(ind, 2) ;
    braggImage2.set('origin', newOrigin) ;
end
