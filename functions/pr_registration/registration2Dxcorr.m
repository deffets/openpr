
function newOrigin = registration2Dxcorr(braggImage1, braggImage2)
    depth1 = braggImage1.get('depth');
    depth2 = braggImage2.get('depth');
    
    if length(depth1) ~= length(depth2) || (depth1(1)~=depth2(1) || depth1(2)-depth1(1)~=depth2(2)-depth2(1))
        error('Depths are not the same');
    end
     
    spacing1 = braggImage1.get('spacing') ;
    spacing2 = braggImage2.get('spacing') ;

    if sum(mod(spacing1(:), 1)~=0) || sum(mod(spacing2(:), 1)~=0)
        error('Spacing must be an integer value')
    end
    
    data1 = braggImage1.get('data') ;
    data2 = braggImage2.get('data') ;
    
    l = length(1:2:size(data1, 3)) ;
    
    data1Res = zeros(size(data1, 1)*spacing1(1), size(data1, 2)*spacing1(2), l) ;
    data2Res = zeros(size(data2, 1)*spacing2(1), size(data2, 2)*spacing2(2), l) ;
    
    data1Res(1:spacing1(1):size(data1Res, 1), 1:spacing1(2):size(data1Res, 2), :) = data1(:, :, 1:2:size(data1, 3)) ;
    data2Res(1:spacing2(1):size(data2Res, 1), 1:spacing2(2):size(data2Res, 2), :) = data2(:, :, 1:2:size(data2, 3)) ;
    
    corrVal = zeros(size(data1Res, 1)+size(data2Res, 1)-1, size(data1Res, 2)+size(data2Res, 2)-1, l) ;
    for i=1:l
        corrVal(:, :, i) = xcorr2(data1Res(:, :, i), data2Res(:, :, i)) ;
    end
    
    corrValSum = sum(corrVal, 3) ;

    [~, maxInd] = max(corrValSum(:)) ;
    
    [ij, ji] = ind2sub(size(corrValSum), maxInd) ;
    
    o = braggImage1.get('origin') ;
    newOrigin(1) = o(1) + ij /spacing1(1) - size(data2, 1)*spacing2(1) ;
    newOrigin(2) = o(2) + ji /spacing1(2) - size(data2, 2)*spacing2(2) ;
end
