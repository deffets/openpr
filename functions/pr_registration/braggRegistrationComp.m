
function [tMat, angle, errorVal] = braggRegistrationComp(spr, sprSpacing, braggImage, ref, refDepth, sigma, gantryAngle, maxIter, maxAngle, prevAngle, preTransfo, postTransfo, registrationAngle, costMethod)
% braggRegistrationComp  Performs a 1 DOF (yaw or pitch) registration 
% between a PR and an SPR map
%   [tMat, angle, errorVal] = braggRegistrationComp(spr, sprSpacing, 
%   braggImage, ref, refDepth, sigma, gantryAngle, maxIter, maxAngle, 
%   prevAngle, preTransfo, postTransfo, registrationAngle) performs a 
%   registration between the PR braggImage and a simulation based on the SPR map
%   spr (whose voxel size is sprSpacing) and the reference curved measured
%   in air ref (refDepth being the depth information of this curve) with a
%   spot size sigma. gantryAngle is the angle of the gantry used for making
%   the PR. The initial guess named prevAngle is asumed to be in the plane
%   perpendicular to the beam. preTransfo and postTranso are
%   transformations to be respectively applied before and after the angle.
%   All angles are in degree.
%   [tMat, angle, errorVal] = braggRegistrationComp(spr, sprSpacing, 
%   braggImage, ref, refDepth, sigma, gantryAngle, maxIter, maxAngle, 
%   prevAngle, preTransfo, postTransfo) Same as
%   above except that the registration angle is asumed to be in the plane
%   perpendicular to the beam if it were shoot with the angle named
%   registrationAngle. gantryAngle still corresponds to the acquisition
%   angle of the PR braggImage.
%
%   See also BraggRollIterComp submTransRegistrationComp

    if nargin<14 && nargin>12
        warning('costMethod = squared')
        costMethod = 'squared' ;
    elseif nargin<13
        warning('registrationAngle = gantryAngle')
        registrationAngle =  gantryAngle ;
        costMethod = 'composed' ;
    end

	initErr = angleErrorComp(spr, sprSpacing, braggImage, ref, refDepth, sigma, gantryAngle, prevAngle, preTransfo, postTransfo, registrationAngle, costMethod) ;
    
    fun = @(angle) angleErrorComp(spr, sprSpacing, braggImage, ref, refDepth, sigma, gantryAngle, angle+prevAngle, preTransfo, postTransfo, registrationAngle, costMethod) ;

    if abs(maxAngle)<0.3
        maxAngle = 0.3 ;
    elseif abs(maxAngle)>2
        maxAngle = 2 ;
    end

    [angle, errorVal] = fminbnd(fun, double(-abs(maxAngle)), double(abs(maxAngle)), optimset('MaxFunEvals', maxIter, 'TolX', 0.05, 'TolFun', 10^(-6), 'Display', 'off')) ;
%     [angle, errorVal] = goldenSection(fun, -abs(maxAngle), abs(maxAngle), maxIter, 0.1) ;
    
    if initErr<errorVal
        angle = deg2rad(prevAngle) ;
    else
        angle = deg2rad(angle+prevAngle) ;
    end
    
    tMat = eye(4) ;
    switch registrationAngle
        case 270
            tMat(1, 1) = cos(angle) ;
            tMat(1, 3) = sin(angle) ;
            tMat(3, 1) = -sin(angle) ;
            tMat(3, 3) = cos(angle) ;
        case 0
            tMat(2, 2) = cos(angle) ;
            tMat(2, 3) = -sin(angle) ;
            tMat(3, 2) = sin(angle) ;
            tMat(3, 3) =  cos(angle) ;
    end
    
    angle = rad2deg(angle) ;
end
