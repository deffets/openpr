function [tMat, trans270, trans0] = submTransRegIter(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, maxIter, maxTrans, autoAlign)

    [wet270, wet270Spacing] = spr2totalWET(spr, sprSpacing, 0, 270) ;
    wetImage270 = Image2D(wet270, [0 0 0], wet270Spacing) ;
    [wet0, wet0Spacing] = spr2totalWET(spr, sprSpacing, 0, 0) ;
    wetImage0 = Image2D(wet0, [0 0 0], wet0Spacing) ;
    
    transVect = submTransRegistration(wetImage270, braggImage270, wetImage0, braggImage0, ref, refDepth, sigma, maxIter, maxTrans, autoAlign) ;
    
    tMat = eye(4) ;
    tMat(4, 1) = -transVect(2) ;
    tMat(4, 2) = transVect(3) ;
    tMat(4, 3) = -transVect(1) ;
    
    return
    
    
    if maxTrans<1
        maxTrans = 1 ;
    end

    fun1 = @(trans) submTransRegErrorSPR(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, [trans 0 0]) ;
    fun2 = @(trans) submTransRegErrorSPR(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, [0 trans 0]) ;
    fun3 = @(trans) submTransRegErrorSPR(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, [0 0 trans]) ;

    [trans1, errorVal1] = fminbnd(fun1, double(-maxTrans), double(maxTrans), optimset('MaxFunEvals', maxIter, 'TolX', 0.1, 'TolFun', 0.1, 'Display', 'off')) ;
    [trans2, errorVal2] = fminbnd(fun2, double(-maxTrans), double(maxTrans), optimset('MaxFunEvals', maxIter, 'TolX', 0.1, 'TolFun', 0.1, 'Display', 'off')) ;
    [trans3, errorVal3] = fminbnd(fun3, double(-maxTrans), double(maxTrans), optimset('MaxFunEvals', maxIter, 'TolX', 0.1, 'TolFun', 0.1, 'Display', 'off')) ;
    
    transVect = [trans1 trans2 trans3] ;
	
    tMat = eye(4) ;
    tMat(4, 1) = transVect(1) ;
    tMat(4, 2) = transVect(2) ;
    tMat(4, 3) = transVect(3) ;
    
    return
    
    
    %%%
    [wet270, wet270Spacing] = spr2totalWET(spr, sprSpacing, 0, 270) ;
    wetImage270 = Image2D(wet270, [0 0 0], wet270Spacing) ;
    [wet0, wet0Spacing] = spr2totalWET(spr, sprSpacing, 0, 0) ;
    wetImage0 = Image2D(wet0, [0 0 0], wet0Spacing) ;
    
    transVect = submTransRegistration(wetImage270, braggImage270, wetImage0, braggImage0, ref, refDepth, sigma, maxIter, maxTrans, autoAlign) ;
    
    tMat = eye(4) ;
    tMat(4, 1) = -transVect(2) ;
    tMat(4, 2) = transVect(3) ;
    tMat(4, 3) = -transVect(1) ;
    
    return    

    
    %%
    
    %%
    
    [wet270, wet270Spacing] = spr2totalWET(spr, sprSpacing, 0, 270) ;
    wetImage270 = Image2D(wet270, [0 0 0], wet270Spacing) ;
    [trans270, errorVal270] = submTransRegistration(wetImage270, braggImage270, ref, refDepth, sigma, maxIter, maxTrans270) ;

    [wet0, wet0Spacing] = spr2totalWET(spr, sprSpacing, 0, 0) ;
    wetImage0 = Image2D(wet0, [0 0 0], wet0Spacing) ;
    [trans0, errorVal0] = submTransRegistration(wetImage0, braggImage0, ref, refDepth, sigma, maxIter, maxTrans0) ;

    tMat = eye(4) ;
    tMat(4, 1) = -trans270(2) ;
    tMat(4, 2) = trans0(2) ;
    tMat(4, 3) = mean([trans270(1) trans0(1)]) ;
end
