
function [x, y] = gateTPSPR(planName, destFolder, energy, spacing, spotsNb, offset, isocenter)
    x = (0:1:(spotsNb(1)-1))*spacing(1)-(spotsNb(1)-1)*spacing(1)/2 + offset(1);
    y = (0:1:(spotsNb(2)-1))*spacing(2)-(spotsNb(2)-1)*spacing(2)/2 + offset(2);
    
    [x, y] = meshgrid(x, y);
    x = x(:);
    y = y(:);
    
    weight = 0.1;
    
    for i=1:length(x)
        X = x(i);
        Y = y(i);
        
        fid = fopen(fullfile(destFolder, [planName '_X=' num2str(X) 'Y=' num2str(Y) '.txt']), 'w');
        
        str = '#TREATMENT-PLAN-DESCRIPTION\n#PlanName\nplan\n#NumberOfFractions\n1\n##FractionID\n1\n##NumberOfFields\n1\n###FieldsID\n1\n#TotalMetersetWeightOfAllFields\n1.62\n\n#FIELD-DESCRIPTION\n###FieldID\n1\n###FinalCumulativeMeterSetWeight\n1.62\n###GantryAngle\n270\n###PatientSupportAngle\n0\n###IsocenterPosition\n';
        fprintf(fid, str);
        fprintf(fid, '%d %d %d\n', uint32(isocenter(1)), uint32(isocenter(2)), uint32(isocenter(3)));
        
        str = '###NumberOfControlPoints\n1\n\n#SPOTS-DESCRIPTION\n####ControlPointIndex\n1\n####SpotTunnedID\n1\n####CumulativeMetersetWeight\n1.62\n####Energy (MeV)\n';
        fprintf(fid, str);
        fprintf(fid, '%d\n', uint32(energy));
        
        str = '####NbOfScannedSpots\n1\n####X Y Weight\n';
        fprintf(fid, str);
        
%         fprintf(fid, '%6.2f %6.2f %6.2f', X, Y, weight);
        fprintf(fid, [num2str(X) ' ' num2str(Y) ' ' num2str(weight)]);
        fprintf(fid, '\n \n');
    
        fclose(fid);
    end
end
