
function simpleGateSimu(user, pass, ip, port, CTName, detectorName, spotsNb, spacing, spotsOrigin, resolution, energy, simuName, progressFunction, handles)
    guesFolder = '/home/gate/reggui';
    guestFolderMac = [guesFolder '/mac'];
    guestFolderData = [guesFolder '/data'];
    guestFolderOutput = [guesFolder '/output'];
    reggui_pr = getRegguiPath('openREGGUI-pr');
    hostFolderMac = fullfile(reggui_pr, 'functions', 'Gate', 'example1', 'mac');
    hostFolderData = fullfile(reggui_pr, 'functions', 'Gate', 'example1', 'data');
    hostFolderOutput = fullfile(reggui_pr, 'userData', 'Gate', 'output');
    tempFolderMac = fullfile(reggui_pr, 'userData', 'Gate', 'mac');
    tempFolderData = fullfile(reggui_pr, 'userData', 'Gate', 'data');
    
    delete(fullfile(tempFolderMac, '*'));
    copyfile(fullfile(hostFolderMac, '*'), tempFolderMac);
     
    detector = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, detectorName))
            detector = handles.mydata.data{i};
        end
    end
    if(isempty(detector))
        error('Detector not found in the current list')
    end

    detectorDimensions = detector.detectorDimensions;
    position = detector.detectorPosition;
    materials = detector.materials;
    lengths = detector.lengths;
    actors = detector.actors;
    repeatNb = detector.repeatNb;

    detectorNames = gateDetectorGeometry(detectorDimensions, position, materials, lengths, actors, repeatNb, fullfile(tempFolderMac, 'geometry.mac'));


    delete(fullfile(tempFolderData, '*'));
    copyfile(fullfile(hostFolderData, '*'), tempFolderData);

    planName = 'test1';
    [X, Y] = gateTPSPR(planName, tempFolderData, energy, spacing, spotsNb, [0 0], [0 0 0]);
    
    spotsDone = false(length(X), 1);
    progressFunction([X(:), Y(:)], spotsDone, [], [], []);

    sshModule = SSHModule(user, ip, port, pass);
    msg = ['rm -r ' guesFolder];
    sshModule.write(msg);
    a = sshModule.read();
    disp(a)
    sshModule.exit();

    sshModule = SSHModule(user, ip, port, pass);
    msg = ['mkdir ' guesFolder ' && mkdir ' guestFolderMac ' && mkdir ' guestFolderData ' && mkdir ' guestFolderOutput];
    sshModule.write(msg);
    a = sshModule.read();
    disp(a)
    sshModule.exit();

    zipFile = 'zipFile.zip';
    zip(fullfile(tempFolderData, zipFile), fullfile(tempFolderData, '**'));
    
%     dataFiles = dir(tempFolderData);
    scpModule = SCPModule(user, ip, port, pass);
%     for j=1 : length(dataFiles)
%         if exist(fullfile(tempFolderData, dataFiles(j).name), 'file') == 2
%             scpModule.copyToRemote(fullfile(tempFolderData, dataFiles(j).name), [guestFolderData '/' dataFiles(j).name]);
%         end
%     end
    scpModule.copyToRemote(fullfile(tempFolderData, zipFile), [guestFolderData '/' zipFile]);
    sshModule = SSHModule(user, ip, port, pass);
    msg = ['cd ' guestFolderData ' && unzip ' zipFile ' -d .'] ;
    sshModule.write(msg);
    a = sshModule.read();
    disp(a)
        
    if isdir(fullfile(hostFolderOutput, simuName))
        delete(fullfile(hostFolderOutput, simuName, '*'));
        rmdir(fullfile(hostFolderOutput, simuName));
    end

    prevTime = 0;
    
    for i=1 : length(X)
        tic
        
        sshModule = SSHModule(user, ip, port, pass);
        msg = ['rm -r ' guestFolderOutput];
        sshModule.write(msg);
        a = sshModule.read();
        disp(a)
        sshModule.exit();
        
        
        sshModule = SSHModule(user, ip, port, pass);
        msg = ['mkdir ' guestFolderOutput];
        sshModule.write(msg);
        a = sshModule.read();
        disp(a)
        sshModule.exit();
        
        gateExport(planName, X(i), Y(i), detectorNames, resolution, tempFolderMac)
        gateSource(planName, X(i), Y(i), hostFolderMac, tempFolderMac)

        sshModule = SSHModule(user, ip, port, pass);
        msg = ['cd ' guestFolderMac ' && rm *.*'] ;
        sshModule.write(msg);
        a = sshModule.read();
        disp(a)
        
        zip(fullfile(tempFolderMac, zipFile), fullfile(tempFolderMac, '**'));
        scpModule.copyToRemote(fullfile(tempFolderMac, zipFile), [guestFolderMac '/' zipFile]);
        sshModule = SSHModule(user, ip, port, pass);
        msg = ['cd ' guestFolderMac ' && unzip ' zipFile ' -d .'] ;
        sshModule.write(msg);
        a = sshModule.read();
        disp(a)
    
%         macFiles = dir(tempFolderMac); 
% 
%         for j=1 : length(macFiles)
%             if exist(fullfile(tempFolderMac, macFiles(j).name), 'file') == 2
%                 scpModule.copyToRemote(fullfile(tempFolderMac, macFiles(j).name), [guestFolderMac '/' macFiles(j).name]);
%             end
%         end

        sshModule = SSHModule(user, ip, port, pass);
        msg = ['cd ' guestFolderMac ' && . start.sh'];
        sshModule.write(msg);
        a = sshModule.read();
        disp(a)

        sshModule.exit();
        
        if ~isdir(hostFolderOutput)
            mkdir(hostFolderOutput)
        end

%         sshModule = SSHModule(user, ip, port, pass);
%         msg = ['echo REGGUI_LS && ls ' guestFolderOutput];
%         sshModule.write(msg);
%         a = sshModule.read();
%         disp(a)
%         outputfiles = strsplit(a, 'REGGUI_LS');
%         outputfiles = strsplit(outputfiles{2}, '\n');
%         outputfiles = outputfiles(~strcmp(outputfiles, ''));
%         sshModule.exit();

        outFilesFolder = ['C_' num2str(X(i)) '_' num2str(Y(i))];

        if ~isdir(fullfile(hostFolderOutput, outFilesFolder))
            mkdir(fullfile(hostFolderOutput, outFilesFolder));
        else
            delete(fullfile(hostFolderOutput, outFilesFolder, '*'));
        end

        zipFile = 'zipFile.zip';
        
        sshModule = SSHModule(user, ip, port, pass);
        msg = ['cd ' guestFolderOutput ' && zip ' zipFile ' *.txt'] ;
        sshModule.write(msg);
        a = sshModule.read();
        disp(a)
        
%         for j=1 : length(outputfiles)    
            scpModule.copyToLocal([guestFolderOutput '/' zipFile], fullfile(hostFolderOutput, outFilesFolder, zipFile));
%         end
        
        unzip(fullfile(hostFolderOutput, outFilesFolder, zipFile), fullfile(hostFolderOutput, outFilesFolder));
        delete(fullfile(hostFolderOutput, outFilesFolder, zipFile));

        if ~isdir(fullfile(hostFolderOutput, simuName))
            mkdir(fullfile(hostFolderOutput, simuName))
        end

        removeGateHeaders(fullfile(hostFolderOutput, outFilesFolder), detectorNames, fullfile(hostFolderOutput, simuName), simuName, X(i), Y(i));
        
        if isdir(fullfile(hostFolderOutput, outFilesFolder))
            delete(fullfile(hostFolderOutput, outFilesFolder, '*'));
            rmdir(fullfile(hostFolderOutput, outFilesFolder));
        end
        
        spotsDone(i) = true;
        bCurve = importdata(fullfile(hostFolderOutput, simuName, [simuName '_X=' num2str(X(i)) 'Y=' num2str(Y(i)) '.txt']));
        newTime = toc;
        newTime = (prevTime*(i-1)+newTime)/i;
        remTime = newTime*(length(X)-i);
        prevTime = newTime;
        
        progressFunction([X(:), Y(:)], spotsDone, newTime, remTime, bCurve);
    end
end
