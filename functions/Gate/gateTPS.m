
function [x, y] = gateTPS(planName, destFolder, handles)
    for i=1 : length(handles.plans.name)
        if strcmp(handles.plans.name{i}, planName)
            plan = handles.plans.data{i}{1};
            break
        end
    end
    
    pos = plan.spots.xy;
    energy = 180; % plan.spots.energy;
    iso = plan.isocenter;
    weight = plan.spots.weight;
    
    x = pos(:, 1);
    y = pos(:, 2);
    for i=1:size(pos, 1)
        X = x(i);
        Y = y(i);
        
        fid = fopen(fullfile(destFolder, [planName '_X=' num2str(X) 'Y=' num2str(Y) '.txt']), 'w');
        
        str = '#TREATMENT-PLAN-DESCRIPTION\n#PlanName\nplan\n#NumberOfFractions\n1\n##FractionID\n1\n##NumberOfFields\n1\n###FieldsID\n1\n#TotalMetersetWeightOfAllFields\n1.62\n\n#FIELD-DESCRIPTION\n###FieldID\n1\n###FinalCumulativeMeterSetWeight\n1.62\n###GantryAngle\n270\n###PatientSupportAngle\n0\n###IsocenterPosition\n';
        fprintf(fid, str);
        fprintf(fid, '%d %d %d\n', uint32(iso(1)), uint32(iso(2)), uint32(iso(3)));
        
        str = '###NumberOfControlPoints\n1\n\n#SPOTS-DESCRIPTION\n####ControlPointIndex\n1\n####SpotTunnedID\n1\n####CumulativeMetersetWeight\n1.62\n####Energy (MeV)\n';
        fprintf(fid, str);
        fprintf(fid, '%d\n', uint32(energy));
        
        str = '####NbOfScannedSpots\n1\n####X Y Weight\n';
        fprintf(fid, str);
        
        fprintf(fid, '%6.2f %6.2f %6.2f', X, Y, weight(i));
        fprintf(fid, '\n \n');
    
        fclose(fid);
    end
end
