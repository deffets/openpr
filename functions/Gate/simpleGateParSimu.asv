
function simpleGateParSimu(user, pass, ip, port, CTName, detectorName, spotsNb, spacing, spotsOrigin, resolution, energy, simuName, progressFunction, handles)
    tic
    reggui_pr = getPath('openREGGUI-pr');
    hostFolderMac = fullfile(reggui_pr, 'functions', 'Gate', 'example1', 'mac');
    hostFolderData = fullfile(reggui_pr, 'functions', 'Gate', 'example1', 'data');
    
    baseTempFolder = fullfile(reggui_pr, 'userData', 'Gate', 'reggui');
    tempPlanFolder = fullfile(reggui_pr, 'userData', 'Gate', 'temp');
    tempFolderCT = fullfile(baseTempFolder, 'CT');
    simuOutputFolder = fullfile(reggui_pr, 'userData', 'Gate', simuName);
    
    guestBaseFolder = '/home/gate/reggui';
    
    sshModule = SSHModule(user, ip, port, pass);
	msg = ['rm -r ' guestBaseFolder];
	sshModule.write(msg);
	a = sshModule.read();
	disp(a)
	sshModule.exit();
    
    disp('Creating temporary folders...')
    try
        if isdir(baseTempFolder)
            rmdir(baseTempFolder, 's');
        end
        mkdir(baseTempFolder);
    catch
    end
    
    try
        if isdir(tempPlanFolder)
            rmdir(tempPlanFolder, 's');
        end
        mkdir(tempPlanFolder);
    catch
    end
    
    try
        if isdir(simuOutputFolder)
            rmdir(simuOutputFolder, 's');
        end
        mkdir(simuOutputFolder);
    catch
    end
    
    try
        if isdir(tempFolderCT)
            rmdir(tempFolderCT, 's');
        end
        mkdir(tempFolderCT);
    end
    
    Export_image(CTName, fullfile(tempFolderCT, 'ct.mhd'), 3, handles);
    
    detector = [] ;
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, detectorName))
            detector = handles.mydata.data{i};
        end
    end
    if(isempty(detector))
        error('Detector not found in the current list')
    end

    detectorDimensions = detector.detectorDimensions;
    position = detector.detectorPosition;
    materials = detector.materials;
    lengths = detector.lengths;
    actors = detector.actors;
    repeatNb = detector.repeatNb;

    [X, Y] = gateTPSPR(simuName, tempPlanFolder, energy, spacing, spotsNb, [0 0], [0 0 0]);
    
    spotsDone = false(length(X), 1);
    progressFunction([X(:), Y(:)], spotsDone, [], [], []);
    
    
    disp('Copying files...')
    for i=1 : length(X)
        guestFolder{i} = [guestBaseFolder '/reggui' num2str(i)];
        guestFolderMac{i} = [guestFolder{i} '/mac'];
        guestFolderData{i} = [guestFolder{i} '/data'];
        guestFolderOutput{i} = [guestFolder{i} '/output'];
        
        tempFolder{i} = fullfile(baseTempFolder, ['reggui' num2str(i)]);
        tempFolderMac{i} = fullfile(tempFolder{i}, 'mac');
        tempFolderData{i} = fullfile(tempFolder{i}, 'data');
        tempFolderOutput{i} = fullfile(tempFolder{i}, 'output');
        
        mkdir(tempFolderOutput{i});
        
        copyfile(fullfile(hostFolderMac, '*'), tempFolderMac{i});
        copyfile(fullfile(hostFolderData, '*'), tempFolderData{i});
        copyfile(fullfile(tempPlanFolder, '*'), tempFolderData{i});
        
        detectorNames = gateDetectorGeometry(detectorDimensions, position, materials, lengths, actors, repeatNb, fullfile(tempFolderMac{i}, 'geometry.mac'));
        gateExport(simuName, X(i), Y(i), detectorNames, resolution, tempFolderMac{i});
        gateSource(simuName, X(i), Y(i), hostFolderMac, tempFolderMac{i});
        gateStart(tempFolderMac{i}, guestFolderMac{i});
    end
    
    zipFile = 'zipFile.zip';
    if exist(fullfile(baseTempFolder, zipFile))
        delete(fullfile(baseTempFolder, zipFile));
    end
	zip(fullfile(baseTempFolder, zipFile), baseTempFolder);
        
    scpModule = SCPModule(user, ip, port, pass);
    scpModule.copyToRemote(fullfile(baseTempFolder, zipFile), ['./' zipFile]);
    
    sshModule = SSHModule(user, ip, port, pass);
    msg = ['unzip ' zipFile ' -d . && echo Done'] ;
    sshModule.write(msg);
    a = sshModule.read();
    disp(a)
    sshModule.exit();

    gateSlurm(baseTempFolder, 'jobs.txt', guestFolderMac, 'start.sh');
    scpModule.copyToRemote(fullfile(baseTempFolder, 'jobs.txt'), [guestBaseFolder '/jobs.txt']);

    msg = 'parallel Gate ';
    for i=1 : length(X)
        msg = [msg 'reggui/reggui' num2str(i) '/main.m
    end
    
    msg = ['parallel --load 80% --noswap < ' guestBaseFolder '/jobs.txt && echo Done'];
    sshModule = SSHModule(user, ip, port, pass);
    sshModule.write(msg);
    a = sshModule.read();
    disp(a)
    sshModule.exit();

    prevTime = 0;
    for i=1 : length(X)
        tic
        zipFile = 'zipFile.zip';
        
        sshModule = SSHModule(user, ip, port, pass);
        msg = ['cd ' guestFolderOutput{i} ' && zip ' zipFile ' *.txt && echo Done'] ;
        sshModule.write(msg);
        a = sshModule.read();
        disp(a)
        
        scpModule.copyToLocal([guestFolderOutput{i} '/' zipFile], fullfile(tempFolderOutput{i}, zipFile));
        
        unzip(fullfile(tempFolderOutput{i}, zipFile), tempFolderOutput{i});
        delete(fullfile(tempFolderOutput{i}, zipFile));

        removeGateHeaders(tempFolderOutput{i}, detectorNames, simuOutputFolder, simuName, X(i), Y(i));
        
        spotsDone(i) = true;
        bCurve = importdata(fullfile(simuOutputFolder, [simuName '_X=' num2str(X(i)) 'Y=' num2str(Y(i)) '.txt']));
        newTime = toc;
        newTime = (prevTime*(i-1)+newTime)/i;
        prevTime = newTime;
        
        progressFunction([X(:), Y(:)], spotsDone, newTime, [], bCurve);
    end
end
