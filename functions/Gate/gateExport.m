
function gateExport(planName, X, Y, detectorNames, resolution, destFolder)
    destFID = fopen(fullfile(destFolder, 'export.mac'), 'w');
	
	pathSplit = strsplit(destFolder, filesep);
    
    for i=1 : length(detectorNames)
        actorName = ['actor_' detectorNames{i}];
        
        l = ['/gate/actor/addActor DoseActor ' actorName '\n'];
        fprintf(destFID, l);
        
        l = ['/gate/actor/' actorName '/save reggui/' pathSplit{end-1} '/output/dose_' detectorNames{i} '_' planName '_X=' num2str(X) 'Y=' num2str(Y) '.txt\n'];
        fprintf(destFID, l);
        
        l = ['/gate/actor/' actorName '/attachTo ' detectorNames{i} '\n'];
        fprintf(destFID, l);
        
        l = ['/gate/actor/' actorName '/setPosition 0 0 0 mm\n' ...
            '/gate/actor/' actorName '/setResolution ' num2str(resolution(1)) ' ' num2str(resolution(2)) ' ' num2str(resolution(3)) '\n' ...
            '/gate/actor/' actorName '/saveEveryNEvents 1000\n' ...
            '/gate/actor/' actorName '/enableEdep false\n' ...
            '/gate/actor/' actorName '/enableUncertaintyEdep false\n' ...
            '/gate/actor/' actorName '/enableDose true\n' ...
            '/gate/actor/' actorName '/enableUncertaintyDose false\n' ...
            '/gate/actor/' actorName '/enableNumberOfHits false\n' ...
            '/gate/actor/' actorName '/enableDoseToWater false\n' ...
            '/gate/actor/' actorName '/normaliseDoseToWater false\n'];
        fprintf(destFID, l);

        fprintf(destFID, '\n \n');
    end
    
    fclose(destFID);
end
