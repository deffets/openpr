
function gateMain(tempFolder, fileNb) %, maxTime)
    reggui_pr = getRegguiPath('openREGGUI-pr');
    fid = fopen(fullfile(reggui_pr, 'functions', 'Gate', 'example1', 'mac', 'main.mac'), 'r');
    mainData = fread(fid);
    fclose(fid);
    
    mainData = reshape(char(mainData), 1, length(mainData));
    mainData = regexprep(mainData, 'reggui1', ['reggui' num2str(fileNb)]);
    
    fid = fopen(fullfile(tempFolder, 'main.mac'), 'w');
    
    fwrite(fid, mainData);
    
    fclose(fid);
end
