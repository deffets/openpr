
function gateStart(hostFolder, guestFolder)
    fid = fopen(fullfile(hostFolder, 'start.sh'), 'w');
    
    msg = ['cd ' guestFolder ' \n'...
        'DISPLAY=:0.0 Gate main.mac \n'];
    
    fprintf(fid, msg);

    fclose(fid);
end
