
function removeGateHeaders(folder, detectorNames, outFolder, simuName, X, Y)
    dataFiles = dir(folder);
    
    while length(dataFiles)>0
        fName = fullfile(folder, dataFiles(1).name);
        fNameShort = dataFiles(1).name;
        
        if exist(fName, 'file') == 2 && strcmp(fName(end-2:end), 'txt')
            disp(['Processing ' fNameShort])
            fullfile(outFolder, [simuName '_X=' num2str(X) 'Y=' num2str(Y) '.txt'])
            fid = fopen(fullfile(outFolder, [simuName '_X=' num2str(X) 'Y=' num2str(Y) '.txt']), 'w');
            
            for i=1 : length(detectorNames)
                cmpRes1 = strncmp(['dose_' detectorNames{i}], {dataFiles.name}, length(['dose_' detectorNames{i}]));
                
                cmpRes2Temp = strfind({dataFiles.name}, fNameShort(length(['dose_' detectorNames{i}])+3:end)); %% +3 because layer1_1 and layer1_180 must lead to same str

                compres2 = false(size(cmpRes1));
                for k=1 : length(cmpRes2Temp)
                    if ~isempty(cmpRes2Temp{k})
                        compres2(k) = true;
                    end
                end
                
                compRes = cmpRes1 & compres2;

                for k=1 : length(compRes)
                    if compRes(k) && strcmp(dataFiles(k).name(length(['dose_' detectorNames{i}])+1) , '_');
                        disp(dataFiles(k).name)
                        
                        fName = fullfile(folder, dataFiles(k).name);

                        fidr = fopen(fName);
                        for l = 1 : 6
                            fgetl(fidr);
                        end

                        buffer = fread(fidr, Inf);
                        fclose(fidr);

                        delete(fName);
                        
                        fwrite(fid, buffer);

                        break;
                    end
                end
                
                dataFiles(k) = [];
            end
            
            fclose(fid);
        else
            dataFiles(1) = [];
        end
    end
end
