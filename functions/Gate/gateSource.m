
function gateSource(planName, X, Y, sourceFolder, destFolder)
    sourceFID = fopen(fullfile(sourceFolder, 'sourcePBS.mac'));
    destFID = fopen(fullfile(destFolder, 'sourcePBS.mac'), 'w');
	
	pathSplit = strsplit(destFolder, filesep);
    
    l = '# SOURCE PBS\n /gate/source/addSource PBS TPSPencilBeam\n';
    fprintf(destFID, l);
    
    l = ['/gate/source/PBS/setPlan reggui/regguiShared/' planName '_X=' num2str(X) 'Y=' num2str(Y) '.txt\n'];
    fprintf(destFID, l);
    
    l = fgetl(sourceFID);
    while ischar(l)
        fprintf(destFID, '%s\n', l);
        
        l = fgetl(sourceFID);
    end

    fprintf(destFID, '\n \n');
    
    fclose(sourceFID);
    fclose(destFID);
end
