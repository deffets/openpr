
%HU2WEIGHT_ULTRAFAST total length of HU crossed by the protons
%
%   weights = hu2weight_ultrafast(hu, huRef) computes the total length  
%   crossed by the protons in each bin of Hounsfield Units delimeted by  
%   huRef, according to Collins-Fekete et al 2017.
%
% See also hu2weight, hu2weight_fast
%
% Authors : S. Deffet
%

function weights = hu2weight_ultrafast(hu, huRef)
    weights = zeros(size(hu, 1), length(huRef)) ;
    
	weights(:, 1) = sum(hu<=min(huRef), 2);
	weights(:, end) = sum(hu>=max(huRef), 2);
        
	hu(hu<=min(huRef)) = nan;
	hu(hu>=max(huRef)) = nan;
        
	diff_hu = repmat(hu, 1, 1, length(huRef)) - repmat(reshape(huRef, 1, 1, length(huRef)), size(hu, 1), size(hu, 2), 1);
	diff_hu(diff_hu<0) = nan ;
        
    [~, I] = nanmin(diff_hu, [], 3);
    I = reshape(I, size(hu));
    
	hu1 = huRef(I);    
	hu2 = huRef(I+1);
        
	SPR2_coeff = (hu - hu1)./(hu2-hu1);
	SPR1_coeff = 1+(hu1-hu)./(hu2-hu1);
        
	SPR1_ind = repmat(I, 1, 1, length(huRef));
	ind_linear = repmat(reshape(1:length(huRef), 1, 1, length(huRef)), size(SPR2_coeff, 1), size(SPR2_coeff, 2), 1);
        
	SPR2_coeff = repmat(SPR2_coeff, 1, 1, length(huRef));
	SPR2_coeff(SPR1_ind+1~=ind_linear)=0;
	SPR2_coeff = squeeze(nansum(SPR2_coeff, 2));
        
	SPR1_coeff = repmat(SPR1_coeff, 1, 1, length(huRef));
	SPR1_coeff(SPR1_ind~=ind_linear)=0;
	SPR1_coeff = squeeze(nansum(SPR1_coeff, 2));
        
    SPR1_coeff = reshape(SPR1_coeff, size(weights));
    SPR2_coeff = reshape(SPR2_coeff, size(weights));
    
    weights = weights+SPR2_coeff+SPR1_coeff;
end
