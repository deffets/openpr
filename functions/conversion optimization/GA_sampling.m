
function [eliteIndiv, eliteFitness] = GA_sampling(vec, nb, costFunc, opt)
    vecNb = length(vec);
    popSize = opt.popSize;               % The size of the population (must be an even number)
    
    maxGens = opt.maxGens;                % The maximum number of generations allowed in a run
    probCrossover = opt.probCrossover;           % The probability of crossing over. 
    probMutation = opt.probMutation;        % The mutation probability (per bit)
    sigmaScalingFlag = opt.sigmaScalingFlag;        % Sigma Scaling is described on pg 168 of M. Mitchell's
                               % GA book. It often improves GA performance.
    sigmaScalingCoeff = opt.sigmaScalingCoeff;       % Higher values => less fitness pressure 

    SUSFlag = opt.SUSFlag;                 % 1 => Use Stochastic Universal Sampling (pg 168 of 
                               %      M. Mitchell's GA book)
                               % 0 => Do not use Stochastic Universal Sampling
                               %      Stochastic Universal Sampling almost always
                               %      improves performance

    useMaskRepositoriesFlag = opt.useMaskRepositoriesFlag; % 1 => draw uniform crossover and mutation masks from 
                               %      a pregenerated repository of randomly generated bits. 
                               %      Significantly improves the speed of the code with
                               %      no apparent changes in the behavior of
                               %      the SGA
                               % 0 => generate uniform crossover and mutation
                               %      masks on the fly. Slower.
                               
	% pre-generate two ?repositories? of random binary digits from which the  
    % the masks used in mutation and uniform crossover will be picked. 
    % maskReposFactor determines the size of these repositories.

    maskReposFactor = 5;
    mutmaskRepos = rand(popSize, nb*maskReposFactor)<probMutation;

    % preallocate vectors for recording the average and maximum fitness in each
    % generation
    medFitnessHist = zeros(1, maxGens);
    maxFitnessHist = zeros(1, maxGens);
    maxCondHist = zeros(1, maxGens);
    maxResHist = zeros(1, maxGens);
    
    eliteFitness = -Inf;

    % Population initialization
    pop = zeros(popSize, nb);
    for i=1:popSize
        pop(i, :) = sort(randperm(vecNb, nb));
    end
    
    % Optimization
    for gen=1 : maxGens
        % Fitness values
        [uniquePop, ~, uniquePopInd] = unique(pop, 'rows');
        uniquePopSize = size(uniquePop, 1);
        
        uniqueFitnesses = zeros(nb-1, 1);
        c = zeros(nb-1, 1);
        res = zeros(nb-1, 1);
        for j = 1:uniquePopSize %%%% parfor
            [uniqueFitnesses(j), ~, c(j), res(j), boundsOpti(:, j), sprOpti(:, j)] = costFunc(uniquePop(j, :));
        end
        
        fitnessVals = uniqueFitnesses(uniquePopInd);
        
        % Elite
        [maxFitnessHist(gen), maxIndex] = max(fitnessVals);
        medFitnessHist(gen) = median(fitnessVals);
        maxCondHist(gen) = c(uniquePopInd(maxIndex));
        maxResHist(gen) = res(uniquePopInd(maxIndex));
        boundsOpti = boundsOpti(:, uniquePopInd(maxIndex));
        sprOpti = sprOpti(:, uniquePopInd(maxIndex));
        
        if eliteFitness<maxFitnessHist(gen)
            eliteFitness = maxFitnessHist(gen);
            eliteIndiv = pop(maxIndex, :);
        end
        
        if gen>1 && maxFitnessHist(gen)<maxFitnessHist(gen-1)+0.1 && medFitnessHist(gen)<medFitnessHist(gen-1)+0.1
            break
        end        
        
        % Display
%         figure(1)
%         subplot(2, 2, 1)
%         set (gcf, 'color', 'w');
%         histogram(pop, 1:vecNb, 'Normalization', 'countdensity');
%         hold on;
%         plot(eliteIndiv+0.5, 0*eliteIndiv+popSize, '.', 'Markersize',25);
%         axis([0 vecNb 0 popSize]);
%         title(['Generation = ' num2str(gen) ', Median Fitness = ' sprintf('%0.3f', medFitnessHist(1, gen))]);
%         ylabel('Frequency of measure in t');
%         hold off
%         set(gca,'XTickLabel', vec(round(get(gca,'XTick'))+1)+vec(2)-vec(1))
%         
%         subplot(2, 2, 2)
%         hold on;
%         plot(gen, -medFitnessHist(gen), '*b')
%         plot(gen, -maxFitnessHist(gen), '*r')
%         plot(gen, -maxCondHist(gen), '*g')
%         plot(gen, -maxResHist(gen), '*c')
%         hold off
%         legend('Median', 'Best', 'Condition', 'Residual')
%         ylim([0 1000])
%         
%         subplot(2, 2, 3)
%         plot(boundsOpti, sprOpti)
        
%         drawnow;
        
        
        % Conditionally perform sigma scaling 
        if sigmaScalingFlag
            sigma = std(fitnessVals);
            if sigma~=0
                fitnessVals = 1+(fitnessVals-mean(fitnessVals))/(sigmaScalingCoeff*sigma);
                fitnessVals(fitnessVals<=0)=0;
            else
                fitnessVals = ones(1, popSize);
            end
        end
        
        fitnessVals(isnan(fitnessVals)) = 99999999;
        
        % Use fitness proportional selection with Stochastic Universal or Roulette
        % Wheel Samplforing to determine the indices of the parents 
        % of all crossover operations 
        cumNormFitnessVals = cumsum(fitnessVals/sum(fitnessVals));

        if SUSFlag
            markers = (1:popSize)/popSize + rand;
            markers(markers>1) = markers(markers>1)-1;
        else
            markers = rand(1, popSize);
        end

        markers = sort(markers, 'ascend');
        
        [~, parentIndices] = histc(markers, [0 cumNormFitnessVals(:)']);
        parentIndices = parentIndices(randperm(popSize)); % shuffle  

        % deterimine the first parents of each mating pair
        firstParents = pop(parentIndices(1:popSize/2),:);
        % determine the second parents of each mating pair
        secondParents = pop(parentIndices(popSize/2+1:end),:);


        % CROSSOVER: COUNT PRESERVING CROSSOVER
        % determine which parents will contribute to crossvecNbover
        crossoverIndices = rand(popSize/2, 1)<probCrossover;
        coupleList = 1:popSize/2;
        coupleList = coupleList(crossoverIndices);

        firstKids = firstParents;
        secondKids = secondParents;
        for i = coupleList %%%% parfor
            [firstKid, secondKid] = slimCrossOver(firstParents(i,:),secondParents(i,:), firstKids(i,:),secondKids(i,:));
            firstKids(i,:) = firstKid;
            secondKids(i,:) = secondKid;
        end
        pop = sort([firstKids; secondKids], 2);

        % implement mutations
        if useMaskRepositoriesFlag
            temp = floor(rand*nb*(maskReposFactor-1));
            masks = mutmaskRepos(:, temp+1:temp+nb);
        else
            masks = rand(popSize, nb)<probMutation;
        end
        % masks(i,j)==1 iff pop(i,j) has to be mutated (0 elsewhere)

        randInd = randi(vecNb, popSize, nb) - 1;
        pop = sort(~masks.*pop + masks.*randInd, 2);
        pop(pop==0) = 1;
        
        % Replace duplicates measurements and sort
        for i = 1:popSize %%%%
            line = pop(i,:);
            duplicatesIndices = logical([0 ~diff(line)]);
            yetIn = line(~duplicatesIndices);
            addable = setdiff(randperm(nb), yetIn, 'stable');
            pop(i, duplicatesIndices) = addable(1:sum(duplicatesIndices));
        end
        
        pop = sort(pop, 2);
    end
end
