
%HU2WEIGHT total length of HU crossed by the protons
%
%   weights = hu2weight(hu, huRef) computes the total length crossed by the
%   protons in each bin of Hounsfield Units delimeted by huRef, according
%   to Collins-Fekete et al 2017. See hu2weight_ultrafast for a faster implementation.
%
% See also hu2weight_fast, hu2weight_ultrafast
%
% Authors : S. Deffet
%

function weights = hu2weight(hu, huRef)
    warning('A faster method exists: see hu2weight_ultrafast');
    
    weights = zeros(size(hu, 1), length(huRef));
    
    for i=1 : size(hu, 1)
        for j=1 : size(hu, 2)
            if hu(i, j)<=min(huRef)
                weights(i, 1) = weights(i, 1) + 1;
                continue
            elseif hu(i, j)>= max(huRef)
                weights(i, end) = weights(i, end) + 1;
                continue
            end
            
            diff_hu = hu(i, j)-huRef;
            diff_hu(diff_hu<0) = 999999;
            [~, I] = min(diff_hu);
            
            if I==length(huRef)
                weights(i, I) = weights(i, I) + 1;
                continue
            end
            
            hu1 = huRef(I);
            hu2 = huRef(I+1);

            SPR2_coeff = (hu(i, j) - hu1)/(hu2-hu1);
            SPR1_coeff = 1+(hu1-hu(i,j))/(hu2-hu1);

            weights(i, I) = weights(i, I) + SPR1_coeff;
            weights(i, I+1) = weights(i, I+1) + SPR2_coeff;
        end
    end
end
