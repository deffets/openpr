
% rng(0)
% bounds = 1:5:100;
% boundsInd = randperm(length(bounds));
% boundsInd = sort(boundsInd(1:5), 'ascend')
% boundsInd = [1 ; boundsInd(:) ; length(boundsInd)];
% boundsInd = unique(boundsInd);
% bounds2 = bounds(boundsInd);
% a = randi(100, 5, 20);
% wInit = hu2weight_ultrafast(a, bounds);
% w2True = hu2weight_ultrafast(a, bounds2)
% [~, w2Tested] = GA_cc_fcn(wInit, bounds, boundsInd, [], [], [], [])

% !!!!!!! Only works if lv and uv non empty
function [fVal, w, c, res2, boundsOpti, sprOpti] = optimalSamplingCF(wInit, boundsInit, boundsInd, lv, uv, huRef, sprRef, wet, lambda)
    if isempty(lv) || isempty(uv)
        error('lv and uv cannot be empty')
    end
    
    if ~islogical(boundsInd)
    	boundsInd = ismember(1:length(boundsInit), boundsInd);
    end
    
    boundsInd = [true(length(lv), 1) ; boundsInd(:) ; true(length(uv), 1)];
    boundsInit = [lv(:) ; boundsInit(:) ; uv(:)];
    
    boundsIndNum = 1:length(boundsInd);
    boundsIndNum = boundsIndNum(boundsInd);
    
    len = sum(boundsInd);
    
	bF = boundsInit(boundsInd);
    bV = boundsInit(~boundsInd);
    
    bF = bF(:);
    bV = bV(:);
    
    d = repmat(bF', length(bV), 1)-repmat(bV, 1, length(bF));
    d(d<0) = Inf;
    [~, mInd] = min(d, [], 2);
    lb = mInd-1; % Bounds in boundsInd
	lb(lb==0) = 1;
    ub = mInd; % Bounds in boundsInd
    ub(ub==1) = len;
    
    lbNum = lb;
    ubNum = ub;

    lb = boundsInit(boundsIndNum(lbNum));
    ub = boundsInit(boundsIndNum(ubNum));
    
    
    coeff1 = (ub(:)-bV(:)) ./ (ub(:)-lb(:));
    coeff2 = 1-coeff1;
    
    coeff1 = repmat(coeff1, 1, len);
    coeff2 = repmat(coeff2, 1, len);
    
    ind = 1:len;
    ind = repmat(ind, length(lbNum), 1);
    lbInd = ind == repmat(lbNum(:), 1, len);
    ubInd = ind == repmat(ubNum(:), 1, len);
    
    coeff1(~lbInd) = 0;
    coeff2(~ubInd) = 0;
    
    w = wInit(:, boundsInd) +  wInit(:, ~boundsInd)*coeff1 + wInit(:, ~boundsInd)*coeff2;
    
    boundsOpti = boundsInit(boundsInd);
    sprRefBounds = interp1(huRef(:), sprRef(:), boundsOpti);
    wl = w(:, ismember(boundsOpti, lv))*sprRefBounds(ismember(boundsOpti, lv));
    wu = w(:, ismember(boundsOpti, uv))*sprRefBounds(ismember(boundsOpti, uv));
    wet = wet(:);
    wet(:) = wet(:) - wl(:) - wu(:);
    sprOpti = linsolve(double(w(:, boundsOpti>lv(end) & boundsOpti<uv(1))), wet);
    sprOpti = [sprRefBounds(ismember(boundsOpti, lv)) ; sprOpti ; sprRefBounds(ismember(boundsOpti, uv))];
    
%     res = -lambda*sqrt(sum((wet(:) - w*sprOpti).^2))/numel(wet);
    res = (wet(:) - w*sprOpti);
    res = sort(abs(res), 'ascend');
    
    res2 = res(floor(0.95*length(res)));
    res = -max(lambda*res2, lambda*1);
    res2 = -res2; %-max(res2, 1);
    
    c = -cond(w(:, boundsInit(boundsInd)>lv(end) & boundsInit(boundsInd)<uv(1)));
%     c = 0;
    
    fVal = c + res;
    
    if isnan(fVal)
        fVal = 999999;
    end
end
