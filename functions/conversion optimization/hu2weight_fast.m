
%HU2WEIGHT_FAST total length of HU crossed by the protons
%
%   weights = hu2weight_fast(hu, huRef) computes the total length crossed 
%   by the protons in each bin of Hounsfield Units delimeted by huRef, 
%   according to Collins-Fekete et al 2017. See hu2weight_ultrafast for a faster implementation.
%
% See also hu2weight, hu2weight_ultrafast
%
% Authors : S. Deffet
%

function weights = hu2weight_fast(hu, huRef)
    warning('A faster method exists: see hu2weight_ultrafast');
    
    weights = zeros(size(hu, 1), length(huRef)) ;
    
    for i=1 : size(hu, 1)
        huLine = squeeze(hu(i, :));
        
        weights(i, 1) = sum(huLine<=min(huRef));
        weights(i, end) = sum(huLine>=max(huRef));
        
        huLine(huLine<=min(huRef)) = nan;
        huLine(huLine>=max(huRef)) = nan;
        
        diff_hu = repmat(huLine(:), 1, length(huRef)) - repmat(huRef(:)', length(huLine), 1);
        diff_hu(diff_hu<0) = nan ;
        
        [~, I] = nanmin(diff_hu, [], 2);
        
        hu1 = huRef(I);
        hu2 = huRef(I+1);
        
        SPR2_coeff = (huLine(:) - hu1(:))./(hu2(:)-hu1(:));
        SPR1_coeff = 1+(hu1(:)-huLine(:))./(hu2(:)-hu1(:));
        
        SPR1_ind = repmat(I(:), 1, length(huRef));
        ind_linear = repmat(1:length(huRef), length(SPR2_coeff), 1);
        
        SPR2_coeff = repmat(SPR2_coeff(:), 1, length(huRef));
        SPR2_coeff(SPR1_ind+1~=ind_linear)=0;
        SPR2_coeff = squeeze(nansum(SPR2_coeff, 1));
        
        SPR1_coeff = repmat(SPR1_coeff(:), 1, length(huRef));
        SPR1_coeff(SPR1_ind~=ind_linear)=0;
        SPR1_coeff = squeeze(nansum(SPR1_coeff, 1));
        
        weights(i, :) = weights(i, :)+SPR2_coeff+SPR1_coeff;
        
    end
end
