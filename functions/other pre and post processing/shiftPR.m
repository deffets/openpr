
%SHIFTPR Shifts all the IDDs of a proton radiograph
%
%   res = shiftPR(data, depth, wet) shifts a proton radiograph by a
%   2D WET-map.
%
%   The WET map and the proton radiograph are assumed to have similar
%   spatial characteristics (spatial resolution, size, etc.). data contains
%   the Bragg curves of the proton radiograph and depth is a vector  of
%   which the length is equal to the third dimension of data an which 
%   contains the water equivalent depth at the sampling points of
%   the proton radigraph.
%
% See also braggTable
%
% Authors : S. Deffet
%

function res = shiftPR(data, depth, wet)
    data(isnan(data)) = 0 ;
    
    res = zeros(size(data)) ;
    
    for i=1 : size(wet, 1)
        for j=1 : size(wet, 2)
            curve_ij = squeeze(data(i, j, :)) ;
            new_curve_ij = interp1([reshape(depth, 1, length(depth)), depth(end)+50, depth(end)+100], [reshape(curve_ij, 1, length(curve_ij)), 0, 0]', depth + wet(i, j)) ;

            curve_ij_tmp = new_curve_ij ;
            curve_ij_tmp(isnan(curve_ij_tmp)) = [] ;

            new_curve_ij(isnan(new_curve_ij)) = curve_ij_tmp(1) ; % Replace NaN by first value
            res(i, j, :) = new_curve_ij ;
        end
    end
end
