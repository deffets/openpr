
%ADDNOISE2PR Add noise to a proton radiograph
%
%   newBraggImage = addNoise2PR(braggImage, sigma, rndSeed) add a random
%   noise to the proton radiograph such that IDD = IDD*(1+r)*sigma where r
%   is normally distributed (variance 1).
%
% Authors : S. Deffet
%

function newBraggImage = addNoise2PR(braggImage, sigma, rndSeed)
    if nargin>2
        rng(rndSeed) ;
    end
    
    data = braggImage.get('data') ;
    depth = braggImage.get('depth') ;
    origin = braggImage.get('origin') ;
    spacing = braggImage.get('spacing') ;
    
        
    data = data + data .* randn(size(data, 1) , size(data, 2), size(data, 3)) * sigma ;
    
    newBraggImage = BraggImage(data, depth, origin, spacing) ;
end
