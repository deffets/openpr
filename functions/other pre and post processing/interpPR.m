
%INTERPPR Bragg curves (proton radiography) resampling (re-interpolation)
%
%   res = interpPR(data, depth, depth2) interpolates the Bragg curves
%   contained in data at the new water equivalent depth contained in the
%   vector depth2. depth is the previous water equivalent depth vector.
%   Its length must correspond the the third dimension of data.
%
% Authors : S. Deffet
%

function res = interpPR(data, depth, depth2)
    res = zeros(size(data, 1), size(data, 2), length(depth2));
    
    depth2 = depth2(:);
    depth2 = depth2';
    
    for i=1 : size(data, 1)
        for j=1 : size(data, 2)
            curve_ij = squeeze(data(i, j, :)) ;
            new_curve_ij = interp1([reshape(depth, 1, length(depth)), depth(end)+50, depth(end)+100], [reshape(curve_ij, 1, length(curve_ij)), 0, 0]', depth2) ;

            curve_ij_tmp = new_curve_ij ;
            curve_ij_tmp(isnan(curve_ij_tmp)) = [] ;

            new_curve_ij(isnan(new_curve_ij)) = curve_ij_tmp(1) ; % Replace NaN by first value
            res(i, j, :) = new_curve_ij ;
        end
    end
end
