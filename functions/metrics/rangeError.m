
%RANGEERROR Range difference between two proton radiographs
%
%   error_map = rangeError(data1, depth1, data2, depth2) 
%   computes the shift to apply to the Bragg curves data2 so as to match
%   data1.
%
%   data1 and data2 are the Bragg curves. depth1 and depth2 are vectors
%   containing the water equivalent depth at the sampling points of the
%   Bragg curves.
%
%   error_map = rangeError(data1, depth1, data2, depth2, step, max_diff) 
%   computes the shift to apply to the Bragg curves data2 so as to match
%   data1 using a specific step and assuming a maximum range difference of
%   max_diff.
%
%   If step and max_diff are not specified, step = 0.05 and max_diff = 20.
%
% See also squareError, stdError
%
% Authors : S. Deffet
%

function error_map = rangeError(data1, depth1, data2, depth2, step, max_diff)
    depth1 = depth1(:) ;
    depth2 = depth2(:) ;
    
    if size(data1, 1) ~= size(data2, 1) && size(data1, 2) ~= size(data2, 2)
        error('Data must be the same size')
    end
    
    data2Temp = 0*data1;
    if length(depth1) ~= length(depth2) || (depth1(1)~=depth2(1) || depth1(2)-depth1(1)~=depth2(2)-depth2(1))
        warning('Depth vectors are not the same. Consider equal depth vectors for speed.')
        
        for i=1 : size(data2, 1)
            data2Temp(i, :, :) = interp1(depth2, squeeze(data2(i, :, :))', depth1)';
        end
        
        data2 = data2Temp;
    end

    
    data1(isnan(data1)) = 0 ;
    data2(isnan(data2)) = 0 ;
    
    depth = depth1 ;
    
    if nargin<5
        step = 0.05 ;
        max_diff = 20 ;
    end
    
    offsets = -max_diff : step : max_diff;

    error_map = zeros(size(data1, 1), size(data1, 2)) ;
    for i=1 : size(error_map, 1)
        for j=1 : size(error_map, 2)
            curve_ij = squeeze(data2(i, j, :))/mean(squeeze(data2(i, j, :))) ;
            ref_curve_ij = squeeze(data1(i, j, :))/mean(squeeze(data1(i, j, :))) ;

            depth_tmp = repmat(reshape(depth, 1, length(depth)), length(offsets), 1) + repmat(reshape(offsets, length(offsets), 1), 1, length(depth)) ;

            data_tmp = interp1([reshape(depth, 1, length(depth)), depth(end)+1, depth(end)+2], [reshape(curve_ij, 1, length(curve_ij)), 0, 0]', depth_tmp, 'linear', 'extrap') ;
            errors = sum((data_tmp - repmat(reshape(ref_curve_ij, 1, length(ref_curve_ij)), size(data_tmp, 1), 1)).^2, 2) ;

            [val, ind] = min(errors);

            error_map(i, j) = offsets(ind) ;
            
        end
    end
end
