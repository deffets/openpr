
%SQUAREERROR Squared difference between two proton radiographs
%
%   error_map = squareError(data1, depth1, data2, depth2) computes
%   the standard deviation of the difference between two proton radiographs
%   after some normalization of the data.
%
%   data1 and data2 are the Bragg curves. depth1 and depth2 are vectors
%   containing the water equivalent depth at the sampling points of the
%   Bragg curves.
%
% Authors : S. Deffet
%

function error_map = squareError(data1, depth1, data2, depth2)
    depth1 = depth1(:) ;
    depth2 = depth2(:) ;

    if size(data1, 1) ~= size(data2, 1) && size(data1, 2) ~= size(data2, 2)
        error('Data must be the same size')
    end
    
    data2Temp = 0*data1;
    if length(depth1) ~= length(depth2) || (depth1(1)~=depth2(1) || depth1(2)-depth1(1)~=depth2(2)-depth2(1))
        warning('Depth vectors are not the same. Consider equal depth vectors for speed.')
        
        for i=1 : size(data2, 1)
            data2Temp(i, :, :) = interp1(depth2, squeeze(data2(i, :, :))', depth1)';
        end
        
        data2 = data2Temp;
    end
    
    data1(isnan(data1)) = 0 ;
    data2(isnan(data2)) = 0 ;
    
    data1 = data1./repmat(mean(data1, 3), 1, 1, size(data1, 3)) ;
    data2 = data2./repmat(mean(data2, 3), 1, 1, size(data2, 3)) ;
    
    data1(isnan(data1)) = 0 ;
    data2(isnan(data2)) = 0 ;
    
    error_map = sum((data1-data2).^2, 3) ;
end
