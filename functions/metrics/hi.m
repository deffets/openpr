
%HI Heterogeneity index defined by Pflugfelder et al 2007
%
%   res = hi(wetMap, sigma) computes the heterogeneity index proposed by
%   Pflugfelder et al 2007.
%
% Authors : S. Deffet
%

function res = hi(wetMap, sigma)
    res = sqrt(imgaussfilt(wetMap.^2, sigma) + wetMap.^2 - 2*imgaussfilt(wetMap, sigma).*wetMap );
    res = real(res);
end
