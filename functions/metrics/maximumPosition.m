
%MAXIMUMPOSITION Approximate range map from a proton radiograph
%
%   pos = maximumPosition(data, depth) returns the approximate range of
%   the Bragg curves in data using the depth information contained in the
%   vector depth.
%
%   data must be a 3D matrix. The third dimension must be equal to the
%   length of the depth vector.
%
% Authors : S. Deffet
%

function pos = maximumPosition(data, depth)
    [~, i] = max(data, [], 3) ;
    
    pos = depth(i) ;
    
    pos = reshape(pos, size(data, 1), size(data, 2));
end
