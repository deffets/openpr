
function newSPR = optimizeSPR4(curve, huToOptimize, braggImage270, braggImage0, ctImage, spots270, spots0, depth, ref, sigma, varargin)
    huRef = curve(1, :) ;
    sprRef = curve(2, :) ;
    
    huRefInit = huRef ;
    
    huRef = min(huRef) : 50 : max(huRef) ;
    sprRef = interp1(huRefInit, sprRef, huRef) ;


    % 270
    [worldLimit1, worldLimit2] = computeWorldLimits(ctImage.get('XWorldLimits', 270), ctImage.get('YWorldLimits', 270), braggImage270.get('XWorldLimits'), braggImage270.get('YWorldLimits'), braggImage270.get('spacing')) ;

    wetImage = pr2wet(braggImage270, ctImage, curve, depth, ref, sigma, 270) ;
    wet = wetImage.getData(worldLimit1, worldLimit2-braggImage270.get('spacing'), braggImage270.get('spacing')) ;
    wet270 = wet(:) ;

    voxels = ctImage.getData(worldLimit1, worldLimit2-braggImage270.get('spacing'), braggImage270.get('spacing'), 270) ;
    voxels = reshape(voxels, size(voxels, 1)*size(voxels, 2), size(voxels, 3)) ;
    voxels270 = voxels ;
    
    A270 = hu2weight(voxels, huRef) ;
    
    % 0
    [worldLimit1, worldLimit2] = computeWorldLimits(ctImage.get('XWorldLimits', 0), ctImage.get('YWorldLimits', 0), braggImage0.get('XWorldLimits'), braggImage0.get('YWorldLimits'), braggImage0.get('spacing')) ;

    wetImage = pr2wet(braggImage0, ctImage, curve, depth, ref, sigma, 0) ;
    wet = wetImage.getData(worldLimit1, worldLimit2-braggImage0.get('spacing'), braggImage0.get('spacing')) ;
    wet0 = wet(:) ;

    voxels = ctImage.getData(worldLimit1, worldLimit2-braggImage0.get('spacing'), braggImage0.get('spacing'), 0) ;
    voxels = reshape(voxels, size(voxels, 1)*size(voxels, 2), size(voxels, 3)) ;
    voxels0 = voxels ;
    
%     A0 = [] ;
%     wet0 = [] ;
    A0 = hu2weight(voxels, huRef) ;
    
    A270 = [A270 ; A0] ;
    wet270 = [wet270(:) ; wet0(:)] ;
    
    
    %
    
    s = max(A270, [], 1) ;
    s = s(:)>20 | (s(:).*sprRef(:))>3 ;
    huRef = huRef(s) ;
    sprRef = sprRef(s) ;
    A270 = hu2weight(voxels270, huRef) ;
    A0 = hu2weight(voxels0, huRef) ;
    A270 = [A270 ; A0] ;
    
    disp('Removing correlated variables')
    while 1
        col = [] ;
        A270 = hu2weight(voxels270, huRef) ;
        A0 = hu2weight(voxels0, huRef) ;
        A270 = [A270 ; A0] ;

        cMat = corr(A270) ;
        cMat = cMat - eye(size(cMat, 1)) ;
        cMat(huRef==3100, :) = 0 ;
        cMat(:, huRef==3100) = 0 ;

        if max(max(cMat))>0.7
            while max(max(cMat))>0.7
                [i, j] = find(cMat == max(max(cMat))) ;

                meani = mean(squeeze(A270(:, i))) ;
                meanj = mean(squeeze(A270(:, j))) ;

                if meani>=meanj
                    col = [col j(1)] ;
                else
                    col = [col i(1)] ;
                end

                cMat([i j], :) = 0 ;
                cMat(:, [i j]) = 0 ;
            end
            
            disp(['Removing HU: ' num2str(huRef(col))])
            huRef(col) = [] ;
            sprRef(col) = [] ;
        else
            break ;
        end
    end

    
    disp('Solving')
    %
%     a = lsqnonneg(double([A270 ; A0]), double([wet270 ; wet0])) ;
    a = linsolve(double(A270), double(wet270)) ;
    
    a(a<0) = 0 ;

    sprRef = sprRef(:) ;
    a = a(:) ;
    ind = sprRef>0.1 & a==0 ;
    
    newSPR(1, :) = huRef(~ind) ;
    newSPR(2, :) =  a(~ind) ;

    huRef = newSPR(1, :) ;
    sprRef = newSPR(2, :) ;
end
