

function pr = spr2pr_sad(spr, spacing, depth, ref, spotsNb, spacingOut, sigma, sad1, sad2, isocenter)
    if numel(sigma)>1 && sigma(1)~=sigma(2)
        error('sigma(1) must be equal to sigma(2)')
    end
    
    kSize = 11 ;
    
    pr = zeros(spotsNb(1), spotsNb(2), length(ref)) ;

    gauss = fspecial('gaussian', kSize*2+1, sigma(1)) ;
    gauss = repmat(gauss, 1, 1, length(ref)) ;
    
    i = 1 ;
    for pos1 = -floor(spotsNb(1)/2)*spacingOut(1) : spacingOut(1) : ceil(spotsNb(1)/2)*spacingOut(1)
        j = 1 ;
        
        for pos2 = -floor(spotsNb(2)/2)*spacingOut(2) : spacingOut(2) : ceil(spotsNb(2)/2)*spacingOut(2)
            m1 = (pos1+isocenter(1) - isocenter(1))/(isocenter(3) - sad1(3)) ;
            m2 = (pos2+isocenter(2) - isocenter(2))/(isocenter(3) - sad2(3)) ;
    
            qe3 = 0 ;
            qs3 = size(spr, 3)*spacing(3) ;
            
            e1 = pos1+isocenter(1) - m1*isocenter(3) ;
            s1 = m1*qs3 + e1 ;
            
            e2 = pos2+isocenter(2) - m2*isocenter(3) ;
            s2 = m2*qs3 + e2 ;
            
            rayLength = sqrt((s1 - e1)^2 + (s2 - e2)^2 + (qs3 - qe3)^2) ;
            
            q3 = qe3:(qs3-qe3)/rayLength:qs3 ;
            
            p1 = m1*q3 + e1 ;
            p2 = m2*q3 + e2 ;
            
            [q1Temp, q2Temp] = meshgrid((-kSize:1:kSize), (-kSize:1:kSize)) ;
            
            q1 = repmat(q1Temp, 1, 1, length(q3)) + repmat(reshape(p1, 1, 1, length(p1)), kSize*2+1, kSize*2+1, 1) ;
            q2 = repmat(q2Temp, 1, 1, length(q3)) + repmat(reshape(p2, 1, 1, length(p2)), kSize*2+1, kSize*2+1, 1) ;
            q3 = repmat(reshape(q3, 1, 1, length(q3)), kSize*2+1, kSize*2+1, 1) ;
            
            sprq = interp3(0:spacing(2):(size(spr, 2)-1)*spacing(2), 0:spacing(1):(size(spr, 1)-1)*spacing(1), 0:spacing(3):(size(spr, 3)-1)*spacing(3), spr, q2, q1, q3, 'linear', 0) ;
            
            wet = sum(sprq, 3)*rayLength/(size(sprq, 3)*spacing(3)) ;
            
            prTemp = wet2pr(wet, spacing, depth, ref, [0 0]) ;
            prTemp = prTemp.*gauss ;
            
            pr(i, j, :) = squeeze(sum(squeeze(sum(prTemp, 1)), 1)) ;
            j = j+1 ;
        end
        i = i+1 ;
    end
end
