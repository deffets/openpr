
function newSPR = optimizeSPR(curve, huToOptimize, braggImage270, braggImage0, ctImage, spots270, spots0, depth, ref, sigma)
    huRef = curve(1, :) ;
    sprRef = curve(2, :) ;
    huToOptimize = huToOptimize(1, :) ;
    huToOptimize = sort(huToOptimize, 'ascend') ;
    
    % View 270
    spacing = braggImage270.get('spacing') ;
    o = braggImage270.get('origin') ;

    wetImage = pr2wet(braggImage270, ctImage, curve, depth, ref, sigma, 270) ;
    wet = zeros(size(spots270, 1)+size(spots0, 1), 1) ;
    
    for i=1:size(spots270, 1)
        pos1 = spots270(i, 1) ;
        pos2 = spots270(i, 2) ;
        pos1 = round((pos1-o(1))/spacing(1))*spacing(1)+o(1) ;
        pos2 = round((pos2-o(2))/spacing(2))*spacing(2)+o(2) ;

        voxels(i, :) = ctImage.getData([pos1 pos2], [pos1 pos2], ctImage.get('spacing'), 270) ;
        wet(i) = wetImage.getData([pos1 pos2], [pos1 pos2], braggImage270.get('spacing')) ;
    end
    
    if ~isempty(spots270)
        A270 = hu2weight(voxels, huRef) ;
    else
        A270 = [] ;
    end
    
    % View 0
    spacing = braggImage0.get('spacing') ;
    o = braggImage0.get('origin') ;

    wetImage = pr2wet(braggImage0, ctImage, curve, depth, ref, sigma, 0) ;
    
    for i=1:size(spots0, 1)
        pos1 = spots0(i, 1) ;
        pos2 = spots0(i, 2) ;
        pos1 = round((pos1-o(1))/spacing(1))*spacing(1)+o(1) ;
        pos2 = round((pos2-o(2))/spacing(2))*spacing(2)+o(2) ;

        voxels0(i, :) = ctImage.getData([pos1 pos2], [pos1 pos2], ctImage.get('spacing'), 0) ;
        wet(i+size(spots270, 1)) = wetImage.getData([pos1 pos2], [pos1 pos2], braggImage270.get('spacing')) ;
    end
    
    if ~isempty(spots0)
        A0 = hu2weight(voxels0, huRef) ;
    else
        A0 = [] ;
    end
    
    A = [A270 ; A0] ;

    ind = ~ismember(huRef, huToOptimize) ;
    wet = reshape(wet, length(wet), 1) ;
    wet = wet-A(:, ind)*reshape(sprRef(ind), length(sprRef(ind)), 1) ;
    A(:, ind) = [] ;
    A = squeeze(A) ;
    
    if numel(huToOptimize)==1
        newSPR(1) = huToOptimize ;
        newSPR(2) = lsqnonneg(A, wet) ;
    else
        newSPR(1, :) = huToOptimize ;
        newSPR(2, :) = lsqnonneg(A, wet) ;
    end
end
