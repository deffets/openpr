
function meanError = meanWetErrorPerc(spr, spr2, sprSpacing, braggImage270, braggImage0, thresh)
    if ~isempty(braggImage270)
        PRSpacing = braggImage270.get('spacing') ;

        [wet270, wet270Spacing] = spr2totalWET(spr, sprSpacing, 0, 270) ;
        [wet0, wet0Spacing] = spr2totalWET(spr, sprSpacing, 0, 0) ;
        wetImage270 = Image2D(wet270, [0 0 0], wet270Spacing) ;
        wetImage0 = Image2D(wet0, [0 0 0], wet0Spacing) ;

        [wet270_2, wet270Spacing] = spr2totalWET(spr2, sprSpacing, 0, 270) ;
        [wet0_2, wet0Spacing] = spr2totalWET(spr2, sprSpacing, 0, 0) ;
        wetImage270_2 = Image2D(wet270_2, [0 0 0], wet270Spacing) ;
        wetImage0_2 = Image2D(wet0_2, [0 0 0], wet0Spacing) ;

        [worldLimit1, worldLimit2] = computeWorldLimits(braggImage270.get('XWorldLimits'), ...
            braggImage270.get('YWorldLimits'), braggImage270.get('XWorldLimits'), ...
            braggImage270.get('YWorldLimits'), PRSpacing) ;

        worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
        worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

        wet270 = wetImage270.getData(worldLimit1, worldLimit2, PRSpacing) ;
        wet270_2 = wetImage270_2.getData(worldLimit1, worldLimit2, PRSpacing) ;
        
        wet270(wet270<=thresh) = NaN ;
        wet270_2(wet270<=thresh) = NaN ;
        
        e270 = abs(wet270-wet270_2)./wet270 ;
    else
        e270 = [] ;
    end
    
    if ~isempty(braggImage0)
        PRSpacing = braggImage0.get('spacing') ;
        
        [worldLimit1, worldLimit2] = computeWorldLimits(braggImage0.get('XWorldLimits'), ...
            braggImage0.get('YWorldLimits'), braggImage0.get('XWorldLimits'), ...
            braggImage0.get('YWorldLimits'), PRSpacing) ;

        worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
        worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

        wet0 = wetImage0.getData(worldLimit1, worldLimit2, PRSpacing) ;
        wet0_2 = wetImage0_2.getData(worldLimit1, worldLimit2, PRSpacing) ;
        
        wet0(wet0<=thresh) = NaN ;
        wet0_2(wet0<=thresh) = NaN ;

        e0 = abs(wet0-wet0_2)./wet0 ;
    else
        e0 = [] ;
    end

    e = [e270(:) ; e0(:)] ;
    e(isnan(e)) = [];
    
    meanError = mean(abs(e)) ;
end
