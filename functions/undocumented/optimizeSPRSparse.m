
function newSPR = optimizeSPRSparse(curve, huToOptimize, braggImage270, braggImage0, ctImage, spots270, spots0, depth, ref, sigma, varargin)
    huRef = curve(1, :) ;
    sprRef = curve(2, :) ;

%     b0Spacing = braggImage270.get('spacing');
%     b0Origin = braggImage270.get('origin');
%     ctOrigin = ctImage.get('origin');
    
    if ~isempty(huToOptimize)
        huToOptimize = huToOptimize(1, :);
        indToOptimize = ismember(huRef, huToOptimize);
    else
        indToOptimize = true(size(huRef));
    end
    
    b270Spacing = braggImage270.get('spacing');
    b270Origin = braggImage270.get('origin');
    bData = braggImage270.get('data');
    
    ctOrigin = ctImage.get('origin');
    ctSpacing = ctImage.get('spacing');
    ctData = ctImage.get('data');
    sprData = interp1([-2000 ; -1500 ; huRef(:)], [0 ; 0 ; sprRef(:)], ctData, 'linear', 'extrap');
    proj1 = spr2totalWET(sprData, ctSpacing, 0, 270);
	simu = wet2pr(proj1, ctSpacing, depth, ref, sigma);
    simuImage = BraggImage(simu, depth, ctOrigin, ctSpacing);
    projIm = Image2D(proj1, ctOrigin, ctSpacing);
    
    XWorldLimits270 = braggImage270.get('XWorldLimits');
    YWorldLimits270 = braggImage270.get('YWorldLimits');
    simuData = simuImage.getData([YWorldLimits270(1) XWorldLimits270(1)], [YWorldLimits270(2) XWorldLimits270(2)]-1, b270Spacing);
    
    re = rangeError(simuData, depth, bData, braggImage270.get('depth'), 0.05, 50);
    wet = projIm.getData([YWorldLimits270(1) XWorldLimits270(1)], [YWorldLimits270(2) XWorldLimits270(2)]-1, b270Spacing) - re;
    wetImage = Image2D(wet, b270Origin, b270Spacing);
    
    for i=1:size(spots270, 1)
        pos1 = spots270(i, 1);
        pos2 = spots270(i, 2);
        pos1 = round((pos1-b270Origin(1))/b270Spacing(1))*b270Spacing(1)+b270Origin(1) ;
        pos2 = round((pos2-b270Origin(2))/b270Spacing(2))*b270Spacing(2)+b270Origin(2) ;

        voxels270(i, :) = ctImage.getData([pos1 pos2], [pos1 pos2], ctSpacing, 270) ;
        wet270(i) = wetImage.getData([pos1 pos2], [pos1 pos2], b270Spacing) ;
    end

    w = hu2weight(voxels270, huRef);
    A = w(:, indToOptimize);
    
    sprRefFixed = sprRef(~indToOptimize);
    wet270 = wet270(:)-w(:, ~indToOptimize)*sprRefFixed(:);
    
    X = lsqnonneg(double(A), double(wet270(:)));
    
    newSPR(1, :) = huRef;
    newSPR(2, :) = sprRef;
    newSPR(2, indToOptimize) = X;
end
% 
% function newSPR = optimizeSPRSparse(curve, huToOptimize, braggImage270, braggImage0, ctImage, spots270, spots0, depth, ref, sigma, varargin)
%     huRef = curve(1, :) ;
%     sprRef = curve(2, :) ;
%     
%     b270Spacing = braggImage270.get('spacing');
%     b270Origin = braggImage270.get('origin');
%     b0Spacing = braggImage270.get('spacing');
%     b0Origin = braggImage270.get('origin');
%     ctSpacing = ctImage.get('spacing');
%     ctOrigin = ctImage.get('origin');
%     
%     if ~isempty(huToOptimize)
%         huToOptimize = huToOptimize(1, :);
%         indToOptimize = ismember(huRef, huToOptimize);
%     else
%         indToOptimize = true(size(huRef));
%     end
%     
% 
%     % 270
%     [worldLimit1_270, worldLimit2_270] = computeWorldLimits(ctImage.get('XWorldLimits', 270), ctImage.get('YWorldLimits', 270), braggImage270.get('XWorldLimits'), braggImage270.get('YWorldLimits'), b270Spacing) ;
%     if ~isempty(spots270)
%         for i=1:size(spots270, 1)
%             pos1 = spots270(i, 1);
%             pos2 = spots270(i, 2);
%             pos1 = round((pos1-b270Origin(1))/b270Spacing(1))*b270Spacing(1)+b270Origin(1) ;
%             pos2 = round((pos2-b270Origin(2))/b270Spacing(2))*b270Spacing(2)+b270Origin(2) ;
% 
%             voxels270(i, :) = ctImage.getData([pos1 pos2], [pos1 pos2], ctSpacing, 270) ;
%             pr270(i, :) = braggImage270.getData([pos1 pos2], [pos1 pos2], b270Spacing) ;
%         end
% 
%         A270 = hu2weight(voxels270, huRef) ;
%     else
%         if isempty(spots0)
%             hu270 = ctImage.getData(worldLimit1_270, worldLimit2_270-b270Spacing, b270Spacing, 270) ;
%             hu270 = permute(hu270,[1,3,2]);
%             hu270 = flip(hu270, 3) ;
%             hu270 = permute(hu270, [2 3 1]);
%             hu270 = flip(hu270, 1);
%     
%             s270 = size(hu270);
%             
%             hu270 = reshape(hu270, s270(1)*s270(2), s270(3)) ;
%             
%             A270 = hu2weight(hu270, huRef);
%             
%             pr270 = braggImage270.getData(worldLimit1_270, worldLimit2_270-b270Spacing, b270Spacing) ;
%             pr270 = reshape(pr270, size(pr270, 1)*size(pr270, 2), size(pr270, 3)) ;
%         else
%             A270 = [];
%             pr270 = [];
%         end
%     end
%     
% %     figure;
% %     imagesc(reshape(A270*sprRef(:), s270(1), s270(2)))
% %     return
%     
% 
%     % 0
%     [worldLimit1_0, worldLimit2_0] = computeWorldLimits(ctImage.get('XWorldLimits', 0), ctImage.get('YWorldLimits', 0), braggImage0.get('XWorldLimits'), braggImage0.get('YWorldLimits'), braggImage0.get('spacing')) ;
%     hu0 = ctImage.getData(worldLimit1_0, worldLimit2_0-braggImage0.get('spacing'), braggImage0.get('spacing'), 0) ;
%     hu0 = reshape(hu0, size(hu0, 1)*size(hu0, 2), size(hu0, 3)) ;
% %     A0 = hu2weight(hu0, huRef);
%     A0 = [];
%     pr0 = [];
%     
%     
%     % All
%     pr = [pr270 ; pr0];
%     prInit = pr;
%     A = [A270 ; A0];
%     
%     huFull = ctImage.get('data') ;   
%     
%     
%     % Functions
%     depth10 = 0:0.1:max(depth)+1;
%     ref10 = interp1(depth, ref, depth10, 'spline', 'extrap');
%     refDeriv = interp1(depth10(1:end-1), (ref10(2:end)-ref10(1:end-1))/(depth10(2)-depth10(1)), depth, 'linear', 'extrap');
%     
%     convFun = @(wet) wet2pr(wet, ctImage.get('spacing'), depth, ref, sigma) ;
%     derivFun = @(wet) wet2pr(wet, ctImage.get('spacing'), depth, refDeriv, sigma) ;
%     
%     
%     % Initial variables and parameters
%     s = sprRef;
% %     s(end) = s(end) + 0.5;
% %     s = s + randn(size(s))*0.1.*s;
% %     s(s<0) = 0;
%     sInit = s;
% 
%     
%     [wet270Full, wet270Spacing] = spr2totalWET(interp1(huRef, s, huFull, 'linear', 'extrap'), ctSpacing, 0, 270);
%     
%     
%     
%     
%     gamma = 0.000000001;
%     lambdaTikhonov = 0.0001;
%     
%         
%         
%     % Optimization
%     f1 = figure('MenuBar', 'none');
%     for it=1 : 30
%         % Simu 270
%         prSim270 = convFun(wet270Full);
%         derivSim270 = derivFun(wet270Full);
%         
%         braggImage270Sim = BraggImage(prSim270, depth, ctOrigin, wet270Spacing);
%         braggImage270DerivSim = BraggImage(derivSim270, depth, ctOrigin, wet270Spacing);
%         
%         clear('prSim270');
%         clear('derivSim270');
%         if ~isempty(spots270)
%             for i=1:size(spots270, 1)
%                 pos1 = spots270(i, 1);
%                 pos2 = spots270(i, 2);
%                 pos1 = round((pos1-b270Origin(1))/b270Spacing(1))*b270Spacing(1)+b270Origin(1) ;
%                 pos2 = round((pos2-b270Origin(2))/b270Spacing(2))*b270Spacing(2)+b270Origin(2) ;
%                 
%                 prSim270(i, :) = braggImage270Sim.getData([pos1 pos2], [pos1 pos2], b270Spacing);
%                 derivSim270(i, :) = braggImage270DerivSim.getData([pos1 pos2], [pos1 pos2], b270Spacing);
%             end
%         else
%             if isempty(spots0)
%                 prSim270 = braggImage270Sim.getData(worldLimit1_270, worldLimit2_270-b270Spacing, b270Spacing);
%                 prSim270 = reshape(prSim270, size(prSim270, 1)*size(prSim270, 2), size(prSim270, 3));
%                 
%                 derivSim270 = braggImage270DerivSim.getData(worldLimit1_270, worldLimit2_270-b270Spacing, b270Spacing);
%                 derivSim270 = reshape(derivSim270, size(derivSim270, 1)*size(derivSim270, 2), size(derivSim270, 3));
%             else
%                 prSim270 = [];
%             end
%         end
%         
%         % Simu 0
%         prSIm0 = [];
%         derivSim0 = [];
%         
%         prSim = [prSim270 ; prSIm0];
%         derivSim = [derivSim270 ; derivSim0];
% 
%         sumpr = repmat(sum(squeeze(prInit(:, 2:10)), 2), 1, size(prInit, 2));
%         sumprsim = repmat(sum(squeeze(prSim(:, 2:10)), 2), 1, size(prInit, 2));
%         
%         pr = prInit./sumpr.*sumprsim;
% 
% 
%         errorFcn = @(gamma) optimizeSPRSparse_s(gamma, huRef, s, sInit, A, convFun, derivSim, pr, prSim, braggImage270, braggImage0, ctImage, spots270, spots0, indToOptimize);
%         
%         options = optimset('MaxFunEvals', 5, 'TolX', abs(gamma)/100, 'Display', 'iter');
%         [gamma, fVal] = fminbnd(errorFcn, abs(gamma)/5, abs(gamma)*5, options);
%         
%         if it>1
%             if fVal > fVal2(it-1)*5
%                 break
%             end
%             
%             if it>5
%                 if abs(fVal - fVal2(it-1)) <= 0.1*abs(fVal2(it-1)-fVal2(it-2))
%                     break
%                 end
%             end
%         end
%         fVal2(it) = fVal;
%         
%     
%         s2 = s;
%         s2(indToOptimize) = s(indToOptimize) + (gamma*A(:, indToOptimize)'*sum(derivSim.*(pr - prSim), 2)/size(prSim, 1))'; % + gamma*lambdaTikhonov*(s-sInit);
%         s2(s2<0) = 0;
%         
%         s = s2;
%         [wet270Full, wet270Spacing] = spr2totalWET(interp1(huRef, s2, huFull, 'linear', 'extrap'), ctSpacing, 0, 270);
%         
%         subplot(1, 2, 1)
%         plot(1:it, fVal2)
%         xlabel('Iteration')
%         ylabel('Cost value')
%         subplot(1, 2, 2)
%         plot(huRef, s, 'b')
%         hold on
%         plot(huRef, sprRef, 'r')
%         hold off
%         legend('Optimized curve', 'Original curve')
%         xlabel('HU')
%         ylabel('RSP')
%         pause(0.2)
%     end
%     
%     newSPR(1, :) = huRef;
%     newSPR(2, :) = s;
%     
%     close(f1);
% end
% 
