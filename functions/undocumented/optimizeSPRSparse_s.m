
function err = optimizeSPRSparse_s(gamma, huRef, s, sInit, A, convFun, derivSim, pr, prSim, braggImage270, braggImage0, ctImage, spots270, spots0, indToOptimize)
    b270Spacing = braggImage270.get('spacing');
    b270Origin = braggImage270.get('origin');
    b0Spacing = braggImage270.get('spacing');
    b0Origin = braggImage270.get('origin');
    ctSpacing = ctImage.get('spacing');
    ctOrigin = ctImage.get('origin');
    
    huFull = ctImage.get('data');
    depth = braggImage270.get('depth');
    
    s2 = s;

    s2(indToOptimize) = s(indToOptimize) + (gamma*A(:, indToOptimize)'*sum(derivSim.*(pr - prSim), 2)/size(prSim, 1))'; % + gamma*lambdaTikhonov*(s-sInit);
    
    [worldLimit1_270, worldLimit2_270] = computeWorldLimits(ctImage.get('XWorldLimits', 270), ctImage.get('YWorldLimits', 270), braggImage270.get('XWorldLimits'), braggImage270.get('YWorldLimits'), b270Spacing) ;
 
    if ~isempty(spots270)
        [wet270, wet270Spacing] = spr2totalWET(interp1(huRef, s2, huFull, 'linear', 'extrap'), ctSpacing, 0, 270);
        pr270 = convFun(wet270);
        braggImage270New = BraggImage(pr270, depth, ctOrigin, wet270Spacing);
    
        clear('pr270');
        for i=1:size(spots270, 1)
            pos1 = spots270(i, 1);
            pos2 = spots270(i, 2);
            pos1 = round((pos1-b270Origin(1))/b270Spacing(1))*b270Spacing(1)+b270Origin(1) ;
            pos2 = round((pos2-b270Origin(2))/b270Spacing(2))*b270Spacing(2)+b270Origin(2) ;
        
            pr270(i, :) = braggImage270New.getData([pos1 pos2], [pos1 pos2], b270Spacing);
        end
    else
        if isempty(spots0)
            [wet270, wet270Spacing] = spr2totalWET(interp1(huRef, s2, huFull, 'linear', 'extrap'), ctSpacing, 0, 270);
            pr270 = convFun(wet270);
            braggImage270New = BraggImage(pr270, depth, ctOrigin, wet270Spacing);
            pr270 = braggImage270New.getData(worldLimit1_270, worldLimit2_270-b270Spacing, b270Spacing);
            pr270 = reshape(pr270, size(pr270, 1)*size(pr270, 2), size(pr270, 3));
        else
            pr270 = [];
        end
    end
    
    pr0 = [];
    
    pr2 = [pr270 ; pr0];
    
    err = double(sum(sum((pr2 - pr).^2))/size(pr2, 1));
end
