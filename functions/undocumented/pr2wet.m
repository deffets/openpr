
function wetImage = pr2wet(braggImage, ctImage, curve, depth, ref, sigma, view)
    XWorldLimit = ctImage.get('XWorldLimits', view) ;
    YWorldLimit = ctImage.get('YWorldLimits', view) ;
    worldUpperLeft = [YWorldLimit(1) XWorldLimit(1)] ;
    worldLowerRight = [YWorldLimit(2) XWorldLimit(2)] - 1 ;
  
    hu = ctImage.getData(worldUpperLeft, worldLowerRight, ctImage.get('spacing'), view) ;
    spr = interp1(curve(1, :), curve(2, :), hu) ;
    
    if view==270
        spr = flip(spr, 2) ;
        spr = flip(spr, 1) ;
        spr = permute(spr, [3, 2, 1]) ;
    else
        spr = flip(spr, 1) ;
        spr = permute(spr, [2, 3, 1]) ;
    end

    wet = spr2totalWET(spr, ctImage.get('spacing'), 0, view) ;
    wet = squeeze(wet) ;
%     figure ;
%     imagesc(wet)
    
    
    pr = wet2pr(wet, ctImage.get('spacing'), depth, ref, sigma) ;
    
    simu = BraggImage(pr, depth, ctImage.get('origin'), ctImage.get('spacing')) ;
    
    wetImage = Image2D(wet, ctImage.get('origin'), ctImage.get('spacing')) ;
    
    [worldLimit1, worldLimit2] = computeWorldLimits(simu.get('XWorldLimits'), simu.get('YWorldLimits'), braggImage.get('XWorldLimits'), braggImage.get('YWorldLimits'), braggImage.get('spacing')) ;
    
    simuData = simu.getData(worldLimit1, worldLimit2-braggImage.get('spacing'), braggImage.get('spacing')) ;
    braggData = braggImage.getData(worldLimit1, worldLimit2-braggImage.get('spacing'), braggImage.get('spacing')) ;
    wetData = wetImage.getData(worldLimit1, worldLimit2-braggImage.get('spacing'), braggImage.get('spacing')) ;
    
    errorMap = rangeError(braggData, braggImage.get('depth'), simuData, depth) ;
    
%     figure ;
%     subplot(1, 2, 1)
%     [m, ind] = max(simuData, [], 3) ;
%     imagesc(ind)
%     axis equal off
    
    wet = wetData + errorMap ;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
%     subplot(1, 2, 2)
%     [m, ind] = max(braggData, [], 3) ;
%     imagesc(ind)
%     axis equal off
%     
%     figure ;
  
    wetImage = Image2D(wet, worldLimit1, braggImage.get('spacing')) ;
end
