
% transfo = [angleRoll, angle270, angle0, trans1, trans2, trans3]

function err = transfoRegError(spr, sprSpacing, braggImage270, braggImage0, ref, refDepth, sigma, transfo)
    anglez = transfo(1) ;
    angley = transfo(2) ;
    anglex = transfo(3) ;
    trans1 = transfo(4) ;
    trans2 = transfo(5) ;
    trans3 = transfo(6) ;
    
    if max(abs(transfo(4:6)))>1
        err = max(abs(transfo(4:6)))*999 ;
        return
    end
    
    matz =  [cos(deg2rad(anglez)) -sin(deg2rad(anglez)) 0 0 ;
             sin(deg2rad(anglez)) cos(deg2rad(anglez)) 0 0 ;
             0 0 1 0 ;
             0 0 0 1] ;

    maty = [cos(deg2rad(angley)) 0 sin(deg2rad(angley)) 0 ;
            0 1 0 0 ;
            -sin(deg2rad(angley)) 0 cos(deg2rad(angley)) 0 ;
            0 0 0 1] ;

    matx = [1 0 0 0
            0 cos(deg2rad(anglex)) -sin(deg2rad(anglex)) 0 ;
            0 sin(deg2rad(anglex)) cos(deg2rad(anglex)) 0 ;
            0 0 0 1] ;
    
    tMat = matz*maty*matx ;
    tMat(4, 1:3) = [trans1 trans2 trans3] ;
    
    tform = affine3d(tMat) ;
    
    try
        spr = imwarp(gpuArray(spr), imref3d(size(spr)), tform, 'OutputView', imref3d(size(spr))) ;
        disp('GPU used')
    catch
        spr = imwarp(spr, imref3d(size(spr)), tform, 'OutputView', imref3d(size(spr))) ;
        disp('GPU not used')
    end
    
    [wet270, wet270Spacing] = spr2totalWET(spr, sprSpacing, 0, 270) ;
    wetImage270 = Image2D(wet270, [0 0 0], wet270Spacing) ;
    
    [wet0, wet0Spacing] = spr2totalWET(spr, sprSpacing, 0, 0) ;
    wetImage0 = Image2D(wet0, [0 0 0], wet0Spacing) ;
    

    depth = braggImage270.get('depth') ;
    wetSpacing = wetImage270.get('spacing') ;
    PRSpacing = braggImage270.get('spacing') ;

    if sum(~(depth==refDepth))
        error('PR depth and reference depth are not the same')
    end

    if wetSpacing(1)~=wetSpacing(2)
        error('Spacing should be uniform')
    end

    wet = wetImage270.get('data') ;

    simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
    simuImage = BraggImage(simu, depth, wetImage270.get('origin'), wetSpacing) ;

    max_iter = 4 ;       
    newOrigin_tmp = braggImage270.get('origin') ;
    for i=1 : max_iter ;
        newOrigin = registration2DIter(simuImage, braggImage270, simuImage.get('depth'), 5) ;
        braggImage270.set('origin', newOrigin) ;

        if newOrigin_tmp(1)==newOrigin(1) && newOrigin_tmp(2)==newOrigin(2)
            break
        end
        newOrigin_tmp = newOrigin ;
    end

    [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
        simuImage.get('YWorldLimits'), braggImage270.get('XWorldLimits'), ...
        braggImage270.get('YWorldLimits'), PRSpacing) ;
    worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
    worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

    simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
    braggImageData = braggImage270.getData(worldLimit1, worldLimit2, PRSpacing) ;

    e270 = squareError(simuImageData, depth, braggImageData, depth) ;


    depth = braggImage0.get('depth') ;
    wetSpacing = wetImage0.get('spacing') ;
    PRSpacing = braggImage0.get('spacing') ;

    if sum(~(depth==refDepth))
        error('PR depth and reference depth are not the same')
    end

    if wetSpacing(1)~=wetSpacing(2)
        error('Spacing should be uniform')
    end

    wet = wetImage0.get('data') ;

    simu = wet2pr(wet, wetSpacing, depth, ref, sigma) ;
    simuImage = BraggImage(simu, depth, wetImage0.get('origin'), wetSpacing) ;

    max_iter = 2 ;       
    newOrigin_tmp = braggImage0.get('origin') ;
    for i=1 : max_iter ;
        newOrigin = registration2DIter(simuImage, braggImage0, simuImage.get('depth'), 5) ;
        braggImage0.set('origin', newOrigin) ;

        if newOrigin_tmp(1)==newOrigin(1) && newOrigin_tmp(2)==newOrigin(2)
            break
        end
        newOrigin_tmp = newOrigin ;
    end

    [worldLimit1, worldLimit2] = computeWorldLimits(simuImage.get('XWorldLimits'), ...
        simuImage.get('YWorldLimits'), braggImage0.get('XWorldLimits'), ...
        braggImage0.get('YWorldLimits'), PRSpacing) ;
    worldLimit2(1) = worldLimit2(1) - PRSpacing(1) ;
    worldLimit2(2) = worldLimit2(2) - PRSpacing(2) ;

    simuImageData = simuImage.getData(worldLimit1, worldLimit2, PRSpacing) ;
    braggImageData = braggImage0.getData(worldLimit1, worldLimit2, PRSpacing) ;

    e0 = squareError(simuImageData, depth, braggImageData, depth) ;

    e = [e270(:) ; e0(:)] ;
    err = double(sum(sum(e))/numel(e)) ;
end
