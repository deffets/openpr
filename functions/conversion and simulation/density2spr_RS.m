
%density2spr_RS Conversion from mass densities to relative proton stopping
% powers according to RayStation 4.7
%
%   SPR = density2spr_RS(density, energy, param_RS) convert a 3D mass 
%   density map to a SPR map using the same conversion scheme as RayStation
%   version 4.7. param_RS is a structure obtained by a call to 
%   read_RS_param(filename). The parameter energy is expressed in MeV.
%
% See also read_RS_param
%
% Authors : S. Deffet
%

function [SPR, S_MeVcm2g] = density2spr_RS(density, energy, param_RS, S_water_Jm2g)
    if numel(unique(size(density)))==2 && min(unique(size(density)))==1
        density = reshape(density, length(density), 1) ;
    end
    
    eV = 1.602176634*10^-19 ; % J
    MeV = 10^6*eV ;
    c = 299792458; % m/s
    m_e = 0.51998910*MeV/(c^2) ; % electron mass
    m_p = 938.272046*MeV/(c^2) ; % proton mass
    u = 931.49410242*MeV/(c^2) ; % atomic mass unit
    e = 1.602176634*10^-19; % Elementary charge
    I_water = 75*eV;
    E = energy*MeV ; % energy of protons
    NA = 6.02214076*10^23;
    re = 2.8179403227*10^-15; %m
    
    % Parameters
    E = energy*MeV ; % energy of protons
    Z_water = [1 8] ;
    A_water = [1.01 16] ;
    w_water = [2.02/18.02 16/18.02] ;
    density_water = 1 ;
    
    
    % Eventually, the parameters for each voxel of the CT are:
    I_RS = param_RS.I ;
    Z_RS = param_RS.Z ;
    A_RS = param_RS.A ;
    w_RS = param_RS.w ;

    I = I_RS*eV ; % Ionization potential
    Z = Z_RS ;
    A = A_RS ;
    w = w_RS ;

    % Now we determine the stopping power relative to water by using the
    % Bethe-Bloch formula:
    A(A==0) = 1 ; % To avoid dividing by 0
    A = A*10^-3;
    
    % Stopping power:
    a2 = log(2*m_e*c^2./I) ;
    a1 = squeeze(sum(w.*Z./A, 2)) ;
    a1 = reshape(a1, length(a1), 1) ;
    a2 = reshape(a2, length(a2), 1) ;
    beta_2 = 1 - (1 + E/(m_p*c^2))^(-2) ;
    K = 4*pi*NA*re^2*m_e*c^2*a1.*(a2-log(1/beta_2 - 1) - beta_2)/beta_2;

    % Assign to each density the properties of the closest element in the
    % table 'InterpolatedMaterialsForTrento'
    
%   index_RS = indexRSMex(density, param_RS.Density) ;
    % First, we have to find the position of the closest element
    index_RS = zeros(size(density)) ;
    % We will do it slice after slice:
    
    for i=1 : size(density, 3)
        slice_density = squeeze(density(:, :, i)) ; % Select the densities of a single CT slice
        
        % For each element of the slice, copy the densities present in the
        % parameters fileMeV (in order to further find the closest element in
        % the parameters file)
        density_tmp = repmat(slice_density, 1, 1, size(w, 1)) ;
        density_RS_tmp = param_RS.Density ;
        density_RS_tmp = repmat(density_RS_tmp, size(slice_density, 1), 1) ;
        density_RS_tmp = repmat(density_RS_tmp, 1, 1, size(slice_density, 2)) ;
        density_RS_tmp = permute(density_RS_tmp, [1 3 2]) ;
        
        % For each element in the slice, find the closest element in the parameter file and store its position
        [~, ind] = min(abs(density_tmp-density_RS_tmp), [], 3) ;
        index_RS(:, :, i) = ind ;
    end
    
    S = density.*K(index_RS) ;
    
    % Stopping power relative to water:
    a2 = log(2*m_e*c^2./I_water) ;
    S_water_Jm2g_RS = 4*pi*NA*re^2*m_e*c^2*squeeze(sum(w_water.*Z_water./(A_water*10^-3)))*(a2-log(1/beta_2 - 1) - beta_2)/beta_2;
    SPR = single(S/S_water_Jm2g_RS) ;
    
    if nargin <4
        S_MeVcm2g = S./density;
    else
        S_MeVcm2g = S_water_Jm2g*SPR./density;
    end
end




% 
% %density2spr_RS Conversion from mass densities to relative proton stopping
% % powers according to RayStation 4.7
% %
% %   SPR = density2spr_RS(density, energy, param_RS) convert a 3D mass 
% %   density map to a SPR map using the same conversion scheme as RayStation
% %   version 4.7. param_RS is a structure obtained by a call to 
% %   read_RS_param(filename). The parameter energy is expressed in MeV.
% %
% % See also read_RS_param
% %
% % Authors : S. Deffet
% %
% 
% function [SPR, S_MeVcm2g] = density2spr_RS(density, energy, param_RS, S_water_Jm2g)
%     if numel(unique(size(density)))==2 && min(unique(size(density)))==1
%         density = reshape(density, length(density), 1) ;
%     end
% 
%     % Physical constants (natural units)
% %     c = 1 ; % speed of light
% %     eV = 1 ;
% %     MeV = 10^6*eV ;
% %     m_e = 0.51998910*MeV/(c^2) ; % electron mass
% %     m_p = 938.272046*MeV/(c^2) ; % proton mass
% %     u = 931.49410242*MeV/(c^2) ; % atomic mass unit
% %     reduced_planck = 6.582119569509*10^-16 *eV; %*s
% %     e = sqrt(1/137.035999); % Elementary charge
%     
%     eV = 1.602176634*10^-19 ; % J
%     MeV = 10^6*eV ;
%     c = 299792458; % m/s
%     m_e = 0.51998910*MeV/(c^2) ; % electron mass
%     m_p = 938.272046*MeV/(c^2) ; % proton mass
%     u = 931.49410242*MeV/(c^2) ; % atomic mass unit
%     e = 1.602176634*10^-19; % Elementary charge
%     density_water = 1000 ; % kg/m^3
%     I_water = 75*eV;
%     E = energy*MeV ; % energy of protons
%     NA = 6.02214076*10^23;
%     re = 2.8179403227*10^-15; %m    
%     
%     % Parameters
%     E = energy*MeV ; % energy of protons
%     I_water = 75.0*eV ; % 80.8*eV ;% apparently ICRU Report 49 => 75 eV and ICRU Report 73 => 80.8 eV => I am not sure of the value we should use! It seems to work well with 80.8 eV
%     Z_water = [1 8] ;
%     A_water = [1.01 16] ;
%     w_water = [2.02/18.02 16/18.02] ;
%     density_water = 1 ;
%     
%     
%     % Eventually, the parameters for each voxel of the CT are:
%     I_RS = param_RS.I ;
%     Z_RS = param_RS.Z ;
%     A_RS = param_RS.A ;
%     w_RS = param_RS.w ;
% 
%     I = I_RS*eV ; % Ionization potential
%     Z = Z_RS ;
%     A = A_RS ;
%     w = w_RS ;
% 
%     % Now we determine the stopping power relative to water by using the
%     % Bethe-Bloch formula:
%     A(A==0) = 1 ; % To avoid dividing by 0
% 
%     % Stopping power:
%     a2 = log(2*m_e*c^2./I) ;
%     a1 = sum(w.*Z./A, 2) ;
%     a1 = reshape(a1, length(a1), 1) ;
%     a2 = reshape(a2, length(a2), 1) ;
%     beta_2 = 1 - (1 + E/(m_p*c^2))^(-2) ;
% %     K = a1.*(a2-log(1/beta_2 - 1) - beta_2)/beta_2 ;
%     K = 4*pi*NA*re^2*m_e*c^2*a1.*(a2-log(1/beta_2 - 1) - beta_2)/beta_2; %density_water*
% 
% 
%     % Assign to each density the properties of the closest element in the
%     % table 'InterpolatedMaterialsForTrento'
%     
% %   index_RS = indexRSMex(density, param_RS.Density) ;
%     % First, we have to find the position of the closest element
%     index_RS = zeros(size(density)) ;
%     % We will do it slice after slice:
%     
%     for i=1 : size(density, 3)
%         slice_density = squeeze(density(:, :, i)) ; % Select the densities of a single CT slice
%         
%         % For each element of the slice, copy the densities present in the
%         % parameters fileMeV (in order to further find the closest element in
%         % the parameters file)
%         density_tmp = repmat(slice_density, 1, 1, size(w, 1)) ;
%         density_RS_tmp = param_RS.Density ;
%         density_RS_tmp = repmat(density_RS_tmp, size(slice_density, 1), 1) ;
%         density_RS_tmp = repmat(density_RS_tmp, 1, 1, size(slice_density, 2)) ;
%         density_RS_tmp = permute(density_RS_tmp, [1 3 2]) ;
%         
%         % For each element in the slice, find the closest element in the parameter file and store its position
%         [~, ind] = min(abs(density_tmp-density_RS_tmp), [], 3) ;
%         index_RS(:, :, i) = ind ;
%     end
%     
% %     index_RS_init = index_RS ;
% %     S = zeros(size(index_RS_init)) ;
% 
%     S = density.*K(index_RS) ;
% %     for i=1 : size(index_RS_init, 3)
% %         index_RS = index_RS_init(:, :, i) ;
% % 
% %         size(density(:, :, i))
% %         size(K(index_RS))
% %         S(:, :, i) = density(:, :, i).*K(index_RS) ;
% %     end
%     
%     % Stopping power of water:
%     a2 = log(2*m_e*c^2./I_water) ;
%     a1 = squeeze(sum(w_water.*Z_water./A_water)) ;
% %     S_water = density_water.*a1.*(a2-log(1/beta_2 - 1) - beta_2)/beta_2 ;
%     S_water = 4*pi*NA*re^2*m_e*c^2*a1*(a2-log(1/beta_2 - 1) - beta_2)/beta_2; %density_water*
%     
%     % Stopping power relative to water:
%     SPR = single(S/S_water) ;
%     
%     
%     eV = 1.602176634*10^-19 ; % J
%     MeV = 10^6*eV ;
%     c = 299792458; % m/s
%     m_e = 0.51998910*MeV/(c^2) ; % electron mass
%     m_p = 938.272046*MeV/(c^2) ; % proton mass
%     u = 931.49410242*MeV/(c^2) ; % atomic mass unit
%     e = 1.602176634*10^-19; % Elementary charge
%     density_water = 1000 ; % kg/m^3
%     I_water = 75*eV;
%     E = energy*MeV ; % energy of protons
%     NA = 6.02214076*10^23;
%     re = 2.8179403227*10^-15; %m
%     
%     MeVcm2g = MeV*10^-4/10^-3; % J*m^2/kg
%     
%     beta_2 = 1 - (1 + E/(m_p*c^2))^(-2);
%     a2 = log(2*m_e*c^2./I_water) ;
%     
%     if nargin <4
%         S_water_Jm2g = 4*pi*NA*re^2*m_e*c^2*squeeze(sum(w_water.*Z_water./(A_water*10^-3)))*(a2-log(1/beta_2 - 1) - beta_2)/beta_2; %density_water*
%         S_MeVcm2g = S_water_Jm2g*SPR./density/MeVcm2g;
%     else
%         S_MeVcm2g = S_water_Jm2g*SPR./density;
%     end
% end
