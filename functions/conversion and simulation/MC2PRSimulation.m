
function braggImage = MC2PRSimulation(CTName, planName, scannerName, MC2_lib_path, BDLName, protonsNb, handles)
    if ~iscell(planName)
        planName{1, 1} = planName;
    end
    
    pr = [];
    
    figure;
    h = subplot(1, 1, 1);
    for i=1 : size(planName, 1)
        for j=1 : size(planName, 2)
            [pr2, spotSpacing] = MC2PRCompute(CTName, planName{i, j}, scannerName, MC2_lib_path, BDLName, protonsNb, handles);
            pr((i-1)*size(pr2, 1)+1:i*size(pr2, 1), (j-1)*size(pr2, 2)+1:j*size(pr2, 2), :) = pr2;
            
            subplot(h)
            imagesc(maximumPosition(pr, 1:size(pr, 3)), [1 size(pr, 3)]);
            axis equal
            pause(0.2)
        end
    end
    
    depth = 1:1:size(pr, 3);
    
    origin = [0 0 0]; % TODO: set origin according to plans
    
    braggImage = BraggImage(pr, depth, origin, [spotSpacing spotSpacing]);
end
