
%SPR2TOTALWET WET Map from SPR map
%
%   [wet, wetSpacing] = spr2totalWET(spr, spacing, table_angle,
%   gantry_angle) computes a 2D WET map based on a 3D SPR map. Divergence
%   of the beam is not accounted for, ie. proton beamlets are assumed to be
%   parallel. spacing is a 3-element vector referring to the spacing of 
%   the SPR map.
%

function [wet, wetSpacing] = spr2totalWET(spr, spacing, table_angle, gantry_angle)
    non_orthogonal_computation = 0 ;
    spacing = single(spacing) ;
    
    % conversion from DICOM to FRS
    spr = permute(spr,[1,3,2]);
    spr = flip(spr, 3) ;

    switch table_angle
        case 0
            switch gantry_angle
                case 0
%                     spr = flip(spr, 3);
                    wet = sum(spr,3)*spacing(2);% spacing(2) because dicom (-y) to frs (z)
                    wet = flip(rot90(wet), 1) ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    wetSpacing = [spacing(1), spacing(3)] ;
                case 90
%                     spr = flip(spr, 1) ;
                    wet = sum(spr,1)*spacing(1);
                    wetSpacing = [spacing(2), spacing(3)] ;
                    wet = flip(squeeze(wet), 2) ;
%                 case 180
%                     wet = sum(spr,3)*spacing(2);
                case 270
                    wet = sum(spr,1)*spacing(1);
                    wetSpacing = [spacing(3), spacing(2)] ;
                otherwise
                    if spacing(1)~=spacing(3)
                        error('spacing(1)~=spacing(3)')
                    end
                    spr2 = flip(spr, 3) ;
                    spr2 = permute(spr2, [1, 3, 2]);
                    spr2 = imrotate(spr2, -gantry_angle, 'crop');
                    spr2 = permute(spr2, [1, 3, 2]);
                    spr2 = flip(spr2, 3) ;
                    wet = sum(spr2, 3)*spacing(2);% spacing(2) because dicom (-y) to frs (z)
                    wet = flip(rot90(wet), 1) ; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    wetSpacing = [spacing(1), spacing(3)] ;
            end
        case 180
            switch gantry_angle
                case 0
%                     spr = flip(spr, 3) ;
                    wet = sum(spr,3)*spacing(2);
                    wetSpacing = [spacing(1), spacing(3)] ;
                case 90
                    wet = sum(spr,1)*spacing(1);
%                 case 180
%                     wet = sum(spr,3)*spacing(2);
                case 270
%                     spr = flip(spr, 1) ;
                    wet = sum(spr,1)*spacing(1);
                    wetSpacing = [spacing(3), spacing(2)] ;
                otherwise
                    non_orthogonal_computation = 1;
            end
        otherwise
            non_orthogonal_computation = 1;
    end
    
    if non_orthogonal_computation
        error('Not yet implemented')
    end
    
    wet = squeeze(wet) ;
    wet = flip(wet, 1) ;
end
