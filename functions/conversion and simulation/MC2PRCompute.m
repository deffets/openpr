
function [pr, spotSpacing] = MC2PRCompute(CTName, planName, CT_CalibFiles, MC2_lib_path, BDL_File, protonsNb, handles)    
    MC2Path = fullfile(getRegguiPath('userData'), 'MC2');
    if isdir(MC2Path)
        rmdir(MC2Path, 's');
    end
    mkdir(MC2Path);
    
    Simu_dir = fullfile(MC2Path, 'simu');
    mkdir(Simu_dir);
            
    ct = [];
    for i=1:length(handles.images.name)
        if(strcmp(handles.images.name{i},CTName))
            ct = handles.images.data{i};
            info = handles.images.info{i};
        end
    end
    for i=1:length(handles.mydata.name)
        if(strcmp(handles.mydata.name{i}, CTName))
            ct = handles.mydata.data{i};
            info = handles.mydata.info{i};
            break;
        end
    end
    if(isempty(ct))
        disp('Image not found. Abort.')
        return
    end
    
    plan = [];
    planName
    for i=1:length(handles.plans.name)
        if(strcmp(handles.plans.name{i}, planName))
            plan = handles.plans.data{i};
            plan_info = handles.plans.info{i};
        end
    end
    if(isempty(plan))
        disp('Plan not found. Abort.')
        return
    end

    detectorPos = info.detectorPosition; %% temp

    % Export CT image
    ct = flipdim(ct, 1);
    ct = flipdim(ct, 2);
    disp(['Export MHD CT: ' fullfile(Simu_dir, 'CT.mhd')]);
    % Write header file (info)
    fid = fopen(fullfile(Simu_dir, 'CT.mhd'), 'w', 'l');
    fprintf(fid,'ObjectType = Image\n');
    fprintf(fid,'NDims = 3\n');
    fprintf(fid,'DimSize = %d %d %d\n', size(ct));
    fprintf(fid,'ElementSpacing = %f %f %f\n', info.Spacing);
    fprintf(fid,'ElementType = MET_FLOAT\n');
    fprintf(fid,'ElementByteOrderMSB = False\n');
    fprintf(fid,'ElementDataFile = CT.raw\n');
    fclose(fid);
    % Write binary file (data)
    fid = fopen(fullfile(Simu_dir, 'CT.raw'), 'w', 'l');
    fwrite(fid, ct, 'float', 0, 'l');
    fclose(fid);
    
    % Export PBS Plan
    disp(['Export PBS Plan: ' fullfile(Simu_dir, 'PlanPencil.txt')]);
    Save_MC2_Plan(plan, plan_info, fullfile(Simu_dir, 'PlanPencil.txt'), 'gate');

    % Generate MC2 configuration file
%     CT_CalibFiles = fullfile(MC2_lib_path, 'Scanners' , scannerName);
%     BDL_File = fullfile(MC2_lib_path, 'BDL', BDLName);

    CT_CalibFiles
    MC2_Config = Generate_MC2_Config(Simu_dir, protonsNb, 'CT.mhd', 'PlanPencil.txt', CT_CalibFiles, BDL_File);
    MC2_Config.Beamlet_Mode = 1;
    MC2_Config.Out_Dose_MHD = 0;
    MC2_Config.Out_Dose_Sparse = 1;
    MC2_Config.Dose_Sparse_Threshold = 0;

    Export_MC2_Config(MC2_Config);

    % Run simulation
    try
        MC2_compute(Simu_dir, MC2_lib_path);
    catch
        fprintf(2,'    ERROR during MCsquare instruction preparation:');
        err = lasterror;
        disp([' ',err.message]);
        disp(err.stack(1));
        return
    end

    % Import dose (sparse format)
    try
        disp(['Read sparse beamlets: ' fullfile(Simu_dir, 'Outputs', 'Sparse_Dose.txt')]);
        Sparse_info = Sparse_read_header(fullfile(Simu_dir, 'Outputs', 'Sparse_Dose.txt'));
        if(sum(strcmp(Sparse_info.SimulationMode, 'Beamlet')) < 1)
            error(['Not a beamlet file'])
        end
        BinFile = fullfile(Simu_dir, 'Outputs', Sparse_info.BinaryFile);
        disp(['Read binary file: ' BinFile]);
        Sparse_beamlets = mexSparseBeamletsReader(BinFile, Sparse_info.ImageSize, Sparse_info.NbrSpots);
    catch
        disp('Could not open output dose map');
        err = lasterror;
        disp(['    ',err.message]);
        disp(err.stack(1));
        return
    end
    
    frameSize = (plan_info.spotsNb-1).*plan_info.spotSpacing;
    spotSpacing = plan_info.spotSpacing;
    
    xy = plan{1}.spots(1).xy;
    x = xy(:, 1);
    x = (x+frameSize(1)/2)/spotSpacing(1)+1;
    y = xy(:, 2);
    y = (y+frameSize(2)/2)/spotSpacing(2)+1;
%     pr = zeros(max(x), max(y), size(ct, 1)-detectorPos+1);
    pr = zeros(max(x), max(y), size(ct, 1)-detectorPos+1);
    
    for i=1 : length(x)
%         try
%             disp(['Read MHD dose: ' fullfile(Simu_dir, 'Outputs', ['Dose_Beamlet_0_0_' num2str(i-1) '.mhd'])]);
%             Dose_info = mha_read_header(fullfile(Simu_dir, 'Outputs', ['Dose_Beamlet_0_0_' num2str(i-1) '.mhd']));
%             Dose_data = mha_read_volume(Dose_info);
%         catch
%             disp('Could not open output dose map');
%             err = lasterror;
%             disp(['    ',err.message]);
%             disp(err.stack(1));
%             return
%         end
%     
%         Dose_data = sum(sum(Dose_data, 3), 2);
%         Dose_data = flip(Dose_data(:));
%         Dose_data = Dose_data(detectorPos:end);
        
        Dose_data = squeeze(Sparse_beamlets(:, i));
        Dose_data = reshape(full(Dose_data), Sparse_info.ImageSize);
        Dose_data = sum(squeeze(sum(Dose_data, 3)), 2);
        Dose_data = Dose_data(detectorPos:end);
        
%         Dose_data = sumVectDim(Sparse_beamlets(:, i), Sparse_info.ImageSize, 2);
%         Dose_data = sumVectDim(Dose_data, [Sparse_info.ImageSize(1), Sparse_info.ImageSize(3)], 2);

        pr(x(i), y(i), :) = Dose_data(:);
    end
end
