
%WET2PR Proton radiography simulation
%
%   pr = wet2pr(wet, spacing, depth, ref, sigma) simulates a proton
%   radiograph based on a WET map.
%
%   spacing is a vector referring to the spacing of the wet map. ref is the
%   Bragg curve that will be shifted by the wet. Hence, ref is assumed to
%   represent the Bragg curve that would be measured by the detector
%   without any material through the beam path. depth is a vector
%   containing the water equivalent depth at the sampling points of the
%   reference Bragg curve. sigma is the sigma of the beam. It can be
%   scalar or a 2-element vector.
%

function pr = wet2pr(wet, spacing, depth, ref, sigma)
%     pr = wet2prMex(wet, spacing, depth, ref, sigma) ;
%     return
    if spacing(1)~=spacing(2)
        waring('Spacing is not uniform')
    end
    
    depth = reshape(depth, length(depth), 1) ;
    ref = reshape(ref, length(ref), 1) ;
    
    pr = zeros(size(wet, 1), size(wet, 2), length(depth)) ;
    
    for i=1 : size(wet, 1)
        depth_mat = repmat(depth, 1, numel(wet(i, :))) + repmat(reshape(wet(i, :), 1, numel(wet(i, :))), length(depth), 1) ;
        bragg_data_tmp = interp1([depth', 335, 700], [ref', 0, 0]', depth_mat) ;
        
        for j=1:size(wet, 2)
            curve_ij_tmp = squeeze(bragg_data_tmp(:, j)) ;
            curve_ij = curve_ij_tmp ;
            curve_ij_tmp(isnan(curve_ij_tmp)) = [] ;

            if isempty(curve_ij_tmp)
                curve_ij_tmp = 0 ;
            end           
 
            curve_ij(isnan(curve_ij)) = curve_ij_tmp(1) ;
            
            pr(i, j, :) = curve_ij ;
        end
    end
    
    
    if numel(sigma)==1
        sigma = [sigma sigma] ;
    end
    
    if sigma(1)~=0 &&  sigma(2)~=0
        pr = imgaussfilt(pr, [sigma(1)/spacing(1) sigma(2)/spacing(2)], 'FilterDomain', 'auto', 'FilterSize', [25 25]) ;
    end
end
