
% Input: everything must be in world coordinates
% Output:
%  limit1: upper-left limit
%  limti2: lower-right limit

function [limit1, limit2] = crossWorldLimits(origin1, size1, origin2, size2)
    limit1(1) = max(origin1(1), origin2(1)) ;
    limit1(2) = max(origin1(2), origin2(2)) ;
    
    limit2(1) = min(origin1(1)+size1(1), origin2(1)+size2(1)) ;
    limit2(2) = min(origin1(2)+size1(2), origin2(2)+size2(2)) ;
    
    if limit1(1)<limit2(1) || limit1(2)<limit2(2)
        limit1 = [] ;
        limit2 = [] ;
    end
end