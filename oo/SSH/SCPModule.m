
classdef SCPModule<handle
    properties (Access = private)
        sshConnection;
        channel;
        timer1;
        user;
        pass;
        ip;
        port;
    end
    
    methods (Access = public)
        function this = SCPModule(user, ip, port, pass)
            import com.jcraft.jsch.*;
            
            this.user = user;
            this.pass = pass;
            this.ip = ip;
            this.port = port;
            
            return
            jschSSHChannel = JSch();
            this.sshConnection = jschSSHChannel.getSession(user, ip, port);
            this.sshConnection.setPassword(pass);
            this.sshConnection.setConfig('StrictHostKeyChecking', 'no');
            this.sshConnection.connect();
            
            this.channel = SSHChannel(this.sshConnection);
        end
        
        function copyToRemote(this, source, dest)
            reggui_pr = getRegguiPath('openREGGUI-pr');
            ScpPath = fullfile(reggui_pr, 'oo', 'SSH');
            
            disp(['cd ' ScpPath ' && java -cp ".:' fullfile(matlabroot, 'jarext', 'java', 'jsch.jar') '" ScpTo ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' source ' ' dest])
            
%             status = system(['export CLASSPATH=/usr/local/matlab2016/java/jarext/jsch.jar:' ScpPath ' ; java ScpTo ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' source ' ' dest]);
            if isunix
                [status, cmdout] = system(['cd ' ScpPath ' && java -cp ".:' fullfile(matlabroot, 'java', 'jarext', 'jsch.jar') '" ScpTo ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' source ' ' dest]);
            else
                [status, cmdout] = system(['cd ' ScpPath ' && java -cp ".;' fullfile(matlabroot, 'java', 'jarext', 'jsch.jar') '" ScpTo ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' source ' ' dest]);
            end
            
            for i=1 : 10
                if status || strcmp(cmdout, 'com.jcraft.jsch.JSchException: verify: false')
%                     status = system(['export CLASSPATH=/usr/local/matlab2016/java/jarext/jsch.jar:' ScpPath ' ; java ScpTo ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' source ' ' dest]);
                    if isunix
                        [status, cmdout] = system(['cd ' ScpPath ' && java -cp ".:' fullfile(matlabroot, 'java', 'jarext', 'jsch.jar') '" ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' source ' ' dest]);
                    else
                        [status, cmdout] = system(['cd ' ScpPath ' && java -cp ".;' fullfile(matlabroot, 'java', 'jarext', 'jsch.jar') '" ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' source ' ' dest]);
                    end
                else
                    break
                end
            end
            
            if status
                warning('SCP failed');
            end

            return
            import java.awt.*;
            import javax.swing.*;
            import java.io.*;
            
            command = ['scp -t ' this.user '@' this.ip ':' dest];
            disp(command)
            this.write(command);
            
            lfile = File(source);

            % send 'C0644 filesize filename'
            fileSize = lfile.length();
            
            li = strfind(source, filesep);
            li = li(end);
            if ~isempty(li)
                s2 = source(li+1:end);
            else
                s2 = source;
            end
            command = sprintf('C0644 %d %s\n', fileSize, s2);
            disp(command)
            this.channel.writeOutputStream(command);
            
            s = uint32(this.read(1));
            if s
                return;
            end

            fis =  FileInputStream(source);
            
            % send a content of lfile
            buf = [];
            readByte = char(fis.read());
            while double(readByte)
                buf = [buf readByte];
                readByte = char(fis.read());
            end
            fis.close();
            
            disp(buf)
            this.channel.writeOutputStream(buf, 1);
            
            % send '\0'
            buf = char(0);
            this.channel.writeOutputStream(buf);
        end
        
        function copyToLocal(this, source, dest)
            reggui_pr = getRegguiPath('openREGGUI-pr');
            ScpPath = fullfile(reggui_pr, 'oo', 'SSH');
            
            disp(['cd ' ScpPath ' && java -cp ".:' fullfile(matlabroot, 'java', 'jarext', 'jsch.jar') '" ScpFrom ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' dest ' ' source])
            
%             status = system(['export CLASSPATH=/usr/local/matlab2016/java/jarext/jsch.jar:' ScpPath ' ; java ScpFrom ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' dest ' ' source]);
            if isunix
                [status, cmdout] = system(['cd ' ScpPath ' && java -cp ".:' fullfile(matlabroot, 'jarext', 'jsch.jar') '" ScpFrom ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' dest ' ' source]);
            else
                [status, cmdout] = system(['cd ' ScpPath ' && java -cp ".;' fullfile(matlabroot, 'jarext', 'jsch.jar') '" ScpFrom ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' dest ' ' source]);
            end 
            
            for i=1 : 10
                if status || strcmp(cmdout, 'com.jcraft.jsch.JSchException: verify: false')
%                     status = system(['export CLASSPATH=/usr/local/matlab2016/java/jarext/jsch.jar:' ScpPath ' ; java ScpFrom ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' dest ' ' source]);
                    if isunix
                        [status, cmdout] = system(['cd ' ScpPath ' && java -cp ".:' fullfile(matlabroot, 'java', 'jarext', 'jsch.jar') '" ScpFrom ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' dest ' ' source]);
                    else
                        [status, cmdout] = system(['cd ' ScpPath ' && java -cp ".;' fullfile(matlabroot, 'java', 'jarext', 'jsch.jar') '" ScpFrom ' this.user ' ' this.pass ' ' this.ip ' ' num2str(this.port) ' ' dest ' ' source]);
                    end
                else
                    break;
                end
            end
            
            if status
                warning('SCP failed');
            end
            
            return
            import java.awt.*;
            import javax.swing.*;
            import java.io.*;
            
            command = ['scp -t ' dest];
            this.write(command);
            
            % send '\0'
            buf(1) = 0;
            this.channel.writeOutputStream(buf);

            while 1
                c = this.read(1);
                if ~strcmp(c, 'C')
                    break;
                end
            end

            % Read '0644 '
            buf = this.read(5);

            fileSize = 0;
            while 1
                buf = this.read(1);
                if strcmp(buf, ' ')
                    break;
                end

                fileSize = fileSize*10 + uint32(buf) - uint32('0');
            end
            
            i = 1;
            while 1
                buf = [buf this.read(1)];
                if uint32(buf(i))==10 % New line
                    file = buf;
                    break;
                end
                
                i = i+1;
            end
            
            % send '\0'
            buf(1) = 0;
            this.channel.writeOutputStream(buf, 0 , 1);
            
            
            % read a content of lfile
            fos = FileOutputStream(lfile);
            while 1
                if length(buf)<filesize
                    foo = length(buf);
                else foo = uint32(filesize);
                end
                
                buf = in.read(foo);
            
                fos.writeOutputStream(buf, 0, foo);
                filesize = filesize-foo;
                if ~filesize
                    break;
                end
            end
                
            fos.close();

            % send '\0'
            buf = 0;
            this.channel.writeOutputStream(buf);
        end
        
        function write(this, msg)
            this.channel.write(msg);
        end
        
        function out = read(this, varargin)
            out = this.channel.read(varargin{:});
        end
        
        function exit(this)
            this.channel.exit();
            this.sshConnection.disconnect();
        end
    end
end
