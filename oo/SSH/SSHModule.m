
classdef SSHModule<handle
    properties (SetObservable)
        outBuffer;
    end
    
    properties (Access = private)
        sshConnection;
        channel;
        timer1;
    end
    
    methods (Access = public)
        function this = SSHModule(user, ip, port, pass)
            import com.jcraft.jsch.*;
            
            jschSSHChannel = JSch();
            this.sshConnection = jschSSHChannel.getSession(user, ip, port);
            this.sshConnection.setPassword(pass);
            this.sshConnection.setConfig('StrictHostKeyChecking', 'no');
            this.sshConnection.connect();
            
            this.channel = SSHChannel(this.sshConnection);
            
%             this.timer1 = timer('Name', 'Timer1', 'Tag', 'Timer1', 'Period', 2, 'ExecutionMode', 'fixedRate', 'TimerFcn', @this.read);
%             start(this.timer1);
        end
        
        function write(this, msg)
            this.channel.write(msg);
        end
        
        function writeOutputStream(this, msg, varargin)
            this.channel.writeOutputStream(msg, varargin{:});
        end
        
        function exit(this)
%             stop(this.timer1);
            this.channel.exit();
            this.sshConnection.disconnect();
        end
%     end
% 
% 	methods (Access = private)
        function out = read(this, source, event)
            this.outBuffer = this.channel.read();
            out = this.outBuffer;
        end
    end
end