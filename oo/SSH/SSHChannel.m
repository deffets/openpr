
classdef SSHChannel<handle
    properties (Access = private)
        channel;
        inputStream;
        outputStream;
    end
    
    methods (Access = public)
        function this = SSHChannel(sshConnection)
            this.channel = sshConnection.openChannel('exec');
            this.inputStream = this.channel.getInputStream();
            this.outputStream = this.channel.getOutputStream();
        end
        
        function write(this, msg)
            disp(msg)
            this.channel.setCommand(msg);
        end
        
        function writeOutputStream(this, msg, off, len, notFlush)
            try
                this.channel.connect();
                
                if nargin<3
                    this.outputStream.write(msg, 0, length(msg));
                else
                    this.outputStream.write(msg, off, len);
                end
                
                if nargin<5 || (nargin>5 && ~notFlush)
                    this.outputStream.flush();
                end
                
                this.channel.disconnect();
            catch
            end
        end
        
        function exit(this)
            this.channel.disconnect();
        end

        function outBuffer = read(this, n)
            outBuffer = [];
            
            try
                this.channel.connect();

                if nargin<2
                    readByte = char(this.inputStream.read());
                    while(double(readByte))
                        outBuffer = [outBuffer readByte];
                        readByte = char(this.inputStream.read());
                    end
                else
                    for i=1 : n
                        outBuffer = [outBuffer char(this.inputStream.read())];
                    end
                end
            catch
            end
        end
    end
end
