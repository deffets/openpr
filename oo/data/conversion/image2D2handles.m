
function handles = image2D2handles(image2D, type, name, handles, inPlace)
    if nargin<5
        inPlace = false;
    end
        
    data = image2D.get('data');
    spacing = image2D.get('spacing');
    origin = image2D.get('origin');
    
    myInfo.Type = type;
    myInfo.spacing = spacing;
    myInfo.origin = origin;
    
    if ~inPlace
        disp('Adding new data to the list')
        myImageName = check_existing_names(name, handles.mydata.name);
        myImageName = check_existing_names(myImageName, handles.images.name);

        handles.mydata.name{length(handles.mydata.name)+1} = myImageName;
        handles.mydata.info{length(handles.mydata.info)+1} = myInfo;
        handles.mydata.data{length(handles.mydata.data)+1} = single(data);
    else
        found = false;
        
        for i=1 : length(handles.mydata.name)
            if strcmp(handles.mydata.name{i}, name)
                handles.mydata.info{i} = myInfo;
                handles.mydata.data{i} = single(data);
                
                found = true;
                break;
            end
        end
        
        if ~found
            warning(['No match found for ' name '. Creating new data instead']);
            handles = image2D2handles(image2D, type, name, handles, false);
        end
    end
end

