
%HANDLES2BRAGGIMAGE convert proton radiograph from openREGGUI handles
%
% braggImage = handles2BraggImage(name, handles) returns a BraggImage
% object corresponding to a proton radiograph stored in an openREGGUI
% handles.
%
% See also handles2Image2D, BraggImage
%

function braggImage = handles2BraggImage(name, handles)
    data = [] ;
    depth = [] ;
    spacing = [] ;
    origin = [] ;
    
    for i=1 : length(handles.mydata.name)
        if strcmp(handles.mydata.name{i}, name)
            data = handles.mydata.data{i} ;
            depth = handles.mydata.info{i}.depth ;
            spacing = handles.mydata.info{i}.spacing ;
            origin = handles.mydata.info{i}.origin ;
            break ;
        end
    end
    
    braggImage = BraggImage(data, depth, origin, spacing) ;
end
