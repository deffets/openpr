
function handles = braggImage2handles(braggImage, type, name, handles)
    data = braggImage.get('data');
    spacing = braggImage.get('spacing');
    origin = braggImage.get('origin');
    depth = braggImage.get('depth');
    
    myInfo.Type = type;
    myInfo.spacing = spacing;
    myInfo.origin = origin;
    myInfo.depth = depth;
    
    myImageName = check_existing_names(name, handles.mydata.name);
    myImageName = check_existing_names(myImageName, handles.images.name);
    
    handles.mydata.name{length(handles.mydata.name)+1} = myImageName;
    handles.mydata.info{length(handles.mydata.info)+1} = myInfo;
    handles.mydata.data{length(handles.mydata.data)+1} = single(data);
end

