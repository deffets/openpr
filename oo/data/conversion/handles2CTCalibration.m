
function ctCalib = handles2CTCalibration(name, handles)
    
    for i=1 : length(handles.mydata.name)
        if strcmp(handles.mydata.name{i}, name)
            calibration = handles.mydata.data{i} ;
            break ;
        end
    end
    
    if isfield(calibration, 'RS_materials')
        ctCalib = GenericObject('density', calibration.model.Density, 'stopping power', calibration.model.Stopping_Power, 'RayStation materials', calibration.RS_materials);
    else
        ctCalib = GenericObject('density', calibration.model.Density, 'stopping power', calibration.model.Stopping_Power);
    end
end
