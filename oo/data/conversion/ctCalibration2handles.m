
function handles = ctCalibration2handles(genericObject, name, handles, inPlace)
    if nargin<5
        inPlace = false;
    end
    
    propertyNames = genericObject.get('propertyNames');
    
    densityInd = strcmp(propertyNames, 'density');
    if ~isempty(propertyNames{densityInd})
        data.model.Density = genericObject.get(propertyNames{densityInd});
    end
    
    densityInd = strcmp(propertyNames, 'stopping power');
    if ~isempty(propertyNames{densityInd})
        data.model.Stopping_Power = genericObject.get(propertyNames{densityInd});
    end
    
    densityInd = strcmp(propertyNames, 'RayStation materials');
    if ~isempty(propertyNames{densityInd})
        data.RS_materials = genericObject.get(propertyNames{densityInd});
    end
    
    myInfo.Type = 'Calibration';
    
    if ~inPlace
        disp('Adding new data to the list')
        myImageName = check_existing_names(name, handles.mydata.name);
        myImageName = check_existing_names(myImageName, handles.images.name);

        handles.mydata.name{length(handles.mydata.name)+1} = myImageName;
        handles.mydata.info{length(handles.mydata.info)+1} = myInfo;
        handles.mydata.data{length(handles.mydata.data)+1} = data;
    else
        found = false;
        
        for i=1 : length(handles.mydata.name)
            if strcmp(handles.mydata.name{i}, name)
                handles.mydata.info{i} = myInfo;
                handles.mydata.data{i} = single(data);
                
                found = true;
                break;
            end
        end
        
        if ~found
            warning(['No match found for ' name '. Creating new data instead']);
            handles = ctCalibration2handles(genericObject, name, handles, false);
        end
    end
end