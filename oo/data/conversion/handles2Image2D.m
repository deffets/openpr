
%HANDLES2IMAGE2D convert image from openREGGUI handles
%
% braggImage = handles2BraggImage(name, handles) returns a Image2D
% object corresponding to an image stored in an openREGGUI handles.
%
% See also handles2BraggImage, Image2D
%

function im = handles2Image2D(name, handles)
    data = [] ;
    spacing = [] ;
    origin = [] ;
    
    for i=1 : length(handles.mydata.name)
        if strcmp(handles.mydata.name{i}, name)
            data = handles.mydata.data{i} ;
            spacing = handles.mydata.info{i}.spacing ;
            origin = handles.mydata.info{i}.origin ;
            break ;
        end
    end
    
    im = Image2D(data, origin, spacing) ;
end
