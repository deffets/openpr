%IMAGE2D 2D data
%
% Imag2D is a class to store and handle 3D data
%
% Imag2D Methods:
% 	object = Image2D(data) Constructs and initializes an Image3D object
% 	with some data.
%
%	value = get(this, property, view) returns a property of the object.
%	View refers to the angle from which the object is seen.
%   property     | view     | description
%   data         | x        | underlying 3D data
%   origin       | x        | geometrical origin of the 3D data (3-element vector)
%   spacing      | x        | spacing (3-element vector)
%   XWorldLimits | 0 or 270 | bounds of the obj. in world coord. syst. (x)
%   YWorldLimits | O or 270 | bounds of the obj. in world coord. syst. (y)
%
%   set(this, property, value) sets object properties. Properties that can
%   be set are data, origin, spacing.
%
%   data = getData(this, worldUpperLeft, worldLowerRight, worldStep, view)
%   get data present between corner worldUpperLeft (2D vector) and corner
%   worldLowerRight with a step worldStep looking from a certain angle
%   (view can be 0 or 270). Query coordinates must be expressed in the
%   world coordinate system. Data are not interpolated.
%
% See also BraggImage, Image3D, handles2Image2D
%

classdef Image2D < handle
    properties (Access = private)
        data;
        origin;
        spacing;
    end
    
    methods (Access = public)
        function this = Image2D(data, origin, spacing)
            this.data = data;
            this.origin = origin;
            this.spacing = spacing;
        end
        
        function value = get(this, property)
            switch property
                case 'data'
                    value = this.data;
                case 'origin'
                    value = this.origin;
                case 'spacing'
                    value = this.spacing;
                case 'XWorldLimits'
                    value = [this.origin(2), this.origin(2) + size(this.data, 2)*this.spacing(2)];
                case 'YWorldLimits'
                    value = [this.origin(1), this.origin(1) + size(this.data, 1)*this.spacing(1)];
                otherwise
                    error(['Unknown property ' property 'for class Image2D']);
            end
        end
        
        function set(this, property, value)
            switch property
                case 'data'
                    this.data = value;
                case 'origin'
                    this.origin = value;
                case 'spacing'
                    this.spacing = value;
                otherwise
                    error(['Unknown property ' property 'for class Image2D']);
            end
        end
        
        function data = getData(this, worldUpperLeft, worldLowerRight, worldStep)
            if nargin < 4
                worldLowerRight = worldUpperLeft;
                worldStep = [1 1];
            end
            
            c1 = worldUpperLeft(1) : worldStep(1) : worldLowerRight(1);
            c2 = worldUpperLeft(2) : worldStep(2) : worldLowerRight(2);
            [index1, index2] = world2index(this.origin, this.spacing, c1, c2);
            
            data = this.data(index1, index2);
        end
    end
end
