
%WORLD2INDEX world coordinate system to linear index
%
% [index1, index2] = world2index(origin, spacing, pos1, pos2) pos1 and pos2
% are respect. vectors of x and y coordinates in the world coordinate
% system. origin is the geometrical origin in the world coordinate system
% of the 2D data and spacing is the spacing of the data.
%
% TODO: this method should be a private method in ImageXD class
%
function [index1, index2] = world2index(origin, spacing, pos1, pos2)
    index1 = round((pos1-origin(1))/spacing(1)) + 1 ;
    index2 = round((pos2-origin(2))/spacing(2)) + 1 ;
end
