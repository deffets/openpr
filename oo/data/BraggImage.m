
%BRAGGIMAGE Proton radiograph
%
% BraggImage is a class to store and handle proton radiography data
%
% BraggImage Methods:
% 	object = BraggImage(data, depth, origin, spacing) Constructs and 
%   initializes an BraggImage object. The depth vector refers to the depth
%   sampling of the Bragg curves. Its dimension must be equal to the third
%   dimension of the data. origin is the geometrical origin of the proton
%   radiograph in the world coordinate system. spacing is a 2-element
%   vector containing the spacing between spots.
%
%	value = get(this, property) returns a property of the object.
%   property     | description
%   depth        | depth vector
%   data         | underlying data (Bragg curves)
%   origin       | geometrical origin of the data (3-element vector)
%   spacing      | spacing (2-element vector)
%   XWorldLimits | bounds of the obj. in world coord. syst. (x)
%   YWorldLimits | bounds of the obj. in world coord. syst. (y)
%
%   set(this, property, value) sets object properties. Properties that can
%   be set are data, origin, spacing.
%
%   data = getData(this, worldUpperLeft, worldLowerRight, worldStep) get
%   data present between corner worldUpperLeft (2D vector) and corner
%   worldLowerRight with a step worldStep. Query coordinates must be 
%   expressed in the world coordinate system. Data are not interpolated.
%
% See also Image2D, Image3D, handles2BraggImage
%


classdef BraggImage < handle
    properties (Access = private)
        data ;
        depth ;
        origin ;
        spacing ;
    end
    
    methods (Access = public)
        function this = BraggImage(data, depth, origin, spacing)
            this.data = data ;
            this.depth = depth ;
            this.origin = origin ;
            this.spacing = spacing ;
        end
        
        function value = get(this, property)
            switch property
                case 'data'
                    value = this.data ;
                case 'depth'
                    value = this.depth ;
                case 'origin'
                    value = this.origin ;
                case 'spacing'
                    value = this.spacing ;
                case 'XWorldLimits'
                    value = [this.origin(2), this.origin(2) + size(this.data, 2)*this.spacing(2)] ;
                case 'YWorldLimits'
                    value = [this.origin(1), this.origin(1) + size(this.data, 1)*this.spacing(1)] ;
                otherwise
                    error(['Unknown property ' property 'for class Image2D']);
            end
        end
        
        function set(this, property, value)
            switch property
                case 'data'
                    this.data = value ;
                case 'depth'
                    this.depth = value ;
                case 'origin'
                    this.origin = value ;
                case 'spacing'
                    this.spacing = value ;
                otherwise
                    error(['Unknown property ' property 'for class Image2D']);
            end
        end
        
        function data = getData(this, worldUpperLeft, worldLowerRight, worldStep)
            if nargin < 4
                worldLowerRight = worldUpperLeft ;
                worldStep = [1 1] ;
            end
            
            c1 = worldUpperLeft(1) : worldStep(1) : worldLowerRight(1) ;
            c2 = worldUpperLeft(2) : worldStep(2) : worldLowerRight(2) ;
            [index1, index2] = world2index(this.origin, this.spacing, c1, c2) ;
            
            
            data = this.data(index1, index2, :) ;
        end
    end
end
