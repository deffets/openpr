%IMAGE3D 3D data
%
% Imag3D is a class to store and handle 3D data
%
% Imag3D Methods:
% 	object = Image3D(data) Constructs and initializes an Image3D object
% 	with some data.
%
%	value = get(this, property, view) returns a property of the object.
%	View refers to the angle from which the object is seen.
%   property     | view     | description
%   data         | x        | underlying 3D data
%   origin       | x        | geometrical origin of the 3D data (3-element vector)
%   spacing      | x        | spacing (3-element vector)
%   XWorldLimits | 0 or 270 | bounds of the obj. in world coord. syst. (x)
%   YWorldLimits | O or 270 | bounds of the obj. in world coord. syst. (y)
%
%   set(this, property, value) sets object properties. Properties that can
%   be set are data, origin, spacing.
%
%   data = getData(this, worldUpperLeft, worldLowerRight, worldStep, view)
%   get data present between corner worldUpperLeft (2D vector) and corner
%   worldLowerRight with a step worldStep looking from a certain angle
%   (view can be 0 or 270). Query coordinates must be expressed in the
%   world coordinate system. Data are not interpolated.
%
% See also BraggImage, Image2D
%

classdef Image3D < handle
    properties (Access = private)
        data ;
        origin ;
        spacing ;
    end
    
    methods (Access = public)
        function this = Image3D(data)
            this.data = data ;
            this.origin = [0 0 0] ;
            this.spacing = [1 1 1] ;
        end
        
        function value = get(this, property, view)
            switch property
                case 'data'
                    value = this.data ;
                case 'origin' ;
                    value = this.origin ;
                case 'spacing'
                    value = this.spacing ;
                case 'XWorldLimits'
                    if (this.origin(1)~=0 || this.origin(2)~=0 || this.origin(3)~=0)
                        error('Not yet implemented for origin other than [0 0 0]');
                    end
                    
                    if (this.spacing(1)~=1 || this.spacing(2)~=1 || this.spacing(3)~=1)
                        error('Not yet implemented for spacing other than [1 1 1]');
                    end
                    
                    switch view
                        case 270
                            data = permute(this.data, [3, 2, 1]) ;
                            data = flip(data, 1) ;
                            data = flip(data, 2) ;
                            value = [0, 0 + size(data, 2)] ;
                        case 0
                            data = permute(this.data, [3, 1, 2]) ;
                            data = flip(data, 1) ;
                            value = [0, 0 + size(data, 2)] ;
                    end
                case 'YWorldLimits'
                    switch view
                        case 270
                            data = permute(this.data, [3, 2, 1]) ;
                            data = flip(data, 1) ;
                            data = flip(data, 2) ;
                            value = [0, 0 + size(data, 1)] ;
                        case 0
                            data = permute(this.data, [3, 1, 2]) ;
                            data = flip(data, 1) ;
                            value = [0, 0 + size(data, 1)] ;
                    end
            end
        end
        
        function set(this, property, value)
            switch property
                case 'data'
                    this.data = value ;
                case 'origin' ;
                    this.origin = value ;
                case 'spacing'
                    this.spacing = value ;
            end
        end
        
        function data = getData(this, worldUpperLeft, worldLowerRight, worldStep, view)
            if nargin < 4
                worldLowerRight = worldUpperLeft ;
                worldStep = [1 1] ;
            end
            
            c1 = worldUpperLeft(1) : worldStep(1) : worldLowerRight(1) ;
            c2 = worldUpperLeft(2) : worldStep(2) : worldLowerRight(2) ;
            

            switch view
                case 270
                    [index1, index2] = world2index(this.origin([3 2]), this.spacing([3 2]), c1, c2) ;
                    
                    data = permute(this.data,[1,3,2]);
                    data = flip(data, 2) ;
                    data = flip(data, 3) ;
                    data = squeeze(data(:, index1, index2)) ;
                    data = flip(data, 3) ;
                    data = flip(data, 2) ;
                    data = permute(data,[1,3,2]);
                case 0
                    [index1, index2] = world2index(this.origin([1 3]), this.spacing([1 3]), c1, c2) ;
                    
                    data = permute(this.data,[1,3,2]);
                    data = flip(data, 3) ;
                    data = rot90(data);
                	data = squeeze(data(index1, index2, :)) ;
                    data = rot90(data, 3);
                    data = flip(data, 3) ;
                    data = permute(data,[1,3,2]);
            end
        end
    end
end
