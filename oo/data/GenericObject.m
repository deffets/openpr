
classdef GenericObject < handle
    properties (Access = private)
        propertyNames;
        propertyValues;
    end
    
    methods (Access = public)
        function this = GenericObject(varargin)
            for i=1 : 2 : nargin-1
                this.propertyNames{i} = varargin{i};
                this.propertyValues{i} = varargin{i+1};
            end
        end
        
        function value = get(this, propertyName)
            if strcmp(propertyName, 'propertyNames')
                value = this.propertyNames;
            else
                ind = strcmp(this.propertyNames, propertyName);
                value = this.propertyValues{ind};
            end
        end
        
        function set(this, propertyName, value)
            ind = strcmp(this.propertyNames, propertyName);
            this.propertyValues{ind} = value;
        end
    end
end
