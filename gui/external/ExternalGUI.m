
classdef ExternalGUI < handle
    properties (Access = private)
        f;
        checkbutton;
        goButton;
        libraries;
    end
    
    methods (Access = public)
        function this = ExternalGUI()
            this.f = figure('NumberTitle', 'off', 'Name', 'Installation wizard', 'Toolbar', 'none', 'MenuBar', 'none', 'Position', [1 200 600 500], 'Resize', 'off');
            
            fid = fopen('externalPaths.txt');
            l = char(fgetl(fid));
            i = 1;
            while l
                this.libraries{i} = l;
                i = i+1;
                l = char(fgetl(fid));
            end
            fclose(fid);
            
            fid = fopen('externalInstallationText.txt');
            s = fread(fid, '*char');
            s = reshape(s, 1, length(s));
            fclose(fid);
            
            uicontrol('Parent', this.f, 'Style', 'text', 'String', s, 'Position', [25 400 550 100]);
            
            uicontrol('Parent', this.f, 'Style', 'listbox', 'String', this.libraries, 'Position', [25 300 550 100]);
            
            
            fid = fopen('externalAgreementText.txt');
            s = fread(fid, '*char');
            s = reshape(s, 1, length(s));
            fclose(fid);
            
            this.checkbutton = uicontrol('Parent', this.f, 'Style', 'checkbox', 'Position', [25 100+75 25 25], 'Callback', @this.enableCallback);
            uicontrol('Parent', this.f, 'Style', 'text', 'String', s, 'Position', [60 100 400 100], 'Callback', @this.enableCallback);
            
            this.goButton = uicontrol('Parent', this.f, 'Style', 'pushbutton', 'String', 'Install', 'Position', [400 25 100 50], 'Enable', 'off', 'Callback', @this.installCallback);
            
            if ~isdir(getRegguiPath('external'))
                mkdir(getRegguiPath('external'));
            end
        end
    end
    
    methods (Access = private)
        function enableCallback(this, s, e)
            if this.checkbutton.get('Value')
                this.goButton.set('Enable', 'on');
            else
                this.goButton.set('Enable', 'off');
            end
        end
        
        function installCallback(this, s, e)
            this.checkbutton.set('Enable', 'off');
            this.goButton.set('Enable', 'off', 'String', 'Installing...',  'Callback', @this.closeCallback);
            
            pause(0.5);
            
            for i=1 : length(this.libraries)
                p = strsplit(this.libraries{i}, '/');
                websave([fullfile(getRegguiPath('external'), p{end}), '.zip'], [this.libraries{i} '/archive/master.zip']);
            end
            
            list = dir(getRegguiPath('external'));
            
            for i=1 : length(list)
                [~,  ~, ext] = fileparts(list(i).name);
                if strcmp(ext, '.zip')
                    unzip(fullfile(getRegguiPath('external'), list(i).name), getRegguiPath('external'));
                    delete(fullfile(getRegguiPath('external'), list(i).name));
                end
            end
            
            addpath(genpath(getRegguiPath('external')));
            
            uicontrol('Parent', this.f, 'Style', 'text', 'String', 'Installation done.', 'Position', [60 40 200 25], 'Callback', @this.enableCallback);
            this.goButton.set('Enable', 'on', 'String', 'Close');
        end
        
        function closeCallback(this, s, e)
            close(this.f);
        end
    end
end
