
classdef GenericModel < handle
    % Generic Model for PR GUI
    %   A model that can be passed to any object inherited from GenericControl
    %   See also GenericControl
    
    properties (Access = protected)
        regguiHObject ; % Object handle of the Reggui interface
    end
    
    methods (Access = public)
        function obj = GenericModel(regguiHObject)
            % GenericModel Creates a model that can be passed to any object
            % inherited from GenericControl
            %   GenericModel(regguiHObject) where regguiHObject is the object handle of the Reggui interface
            
            obj.regguiHObject = regguiHObject ;
        end
        
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'path'
                    value = readPRPath(varargin{2});
                case 'help'
                    value = readLink(varargin{2});
                case 'regguiHObject'
                    value = this.regguiHObject;
                case 'WETNames'
                    value = this.getWETNames();
                case 'PRNames'
                    value = this.getPRNames();
                case 'CTNames'
                    value = this.getCTNames();
                case 'planNames'
                    value = this.getPlanNames();
                case 'detectorNames'
                    value = this.getDetectorNames();
                case 'maskNames'
                    value = this.getMaskNames();
                case 'curveNames'
                    value = this.getCurvesNames();
                otherwise
                    error(['Data property unknown: ' property]);
            end
        end
        
        function set(this, varargin)
            property = varargin{1};
            
            switch property
                case 'path'
                    writePRPath(varargin{2}, varargin{3});
                otherwise
                    error('Argument 2 is invalid');
            end
        end
        
        function CTNames = getCTNames(this)
            % getCTNames  Get names of images loaded in Reggui
            
            handles = guidata(this.regguiHObject) ;
            CTNames = handles.images.name ;
            
            if isempty(CTNames)
                CTNames = 'None' ;
            end
        end
        
        function planNames = getPlanNames(this)
            handles = guidata(this.regguiHObject) ;
            planNames = handles.plans.name ;
            
            if isempty(planNames)
                planNames = 'None' ;
            end
        end
        
        function PRNames = getPRNames(this)
            % getPRNames  Get names of proton radiographs loaded in Reggui
            
            handles = guidata(this.regguiHObject) ;
            
            PRNames = {} ;
            
            a = 1 ;
            for i=1 : length(handles.mydata.name)
                if (isfield(handles.mydata.info{i}, 'type') && (strcmp(handles.mydata.info{i}.type, 'PR') || strcmp(handles.mydata.info{i}.type, 'PR SIMU'))) ...
                        || (isfield(handles.mydata.info{i}, 'Type') && (strcmp(handles.mydata.info{i}.Type, 'PR') || strcmp(handles.mydata.info{i}.Type, 'PR SIMU')))
                    PRNames{a} = handles.mydata.name{i} ;
                    a = a+1 ;
                end
            end
            
            if isempty(PRNames)
                PRNames = 'None' ;
            end
        end
        
        function maskNames = getMaskNames(this)
            handles = guidata(this.regguiHObject) ;
            
            maskNames = {} ;
            
            a = 1 ;
            for i=1 : length(handles.mydata.name)
                if (isfield(handles.mydata.info{i}, 'type') && (strcmp(handles.mydata.info{i}.type, 'mask'))) ...
                        || (isfield(handles.mydata.info{i}, 'Type') && (strcmp(handles.mydata.info{i}.Type, 'mask')))
                    maskNames{a} = handles.mydata.name{i} ;
                    a = a+1 ;
                end
            end
            
            if isempty(maskNames)
                maskNames = 'None' ;
            end
        end
        
        function PRNames = getVivaNames(this)
            % getVivaNames  Get names of Viva proton radiographs loaded in Reggui
            
            handles = guidata(this.regguiHObject) ;
            
            PRNames = {} ;
            
            a = 1 ;
            for i=1 : length(handles.mydata.name)
                if (isfield(handles.mydata.info{i}, 'type') && strcmp(handles.mydata.info{i}.type, 'Viva')) ...
                        || (isfield(handles.mydata.info{i}, 'Type') && strcmp(handles.mydata.info{i}.Type, 'Viva'))
                    PRNames{a} = handles.mydata.name{i} ;
                    a = a+1 ;
                end
            end
            
            if isempty(PRNames)
                PRNames = 'None' ;
            end
        end
        
        function curvesNames = getCurvesNames(this)
            % getCurvesNames  Get names of calibration curves loaded in Reggui
            
            handles = guidata(this.regguiHObject) ;
            
            curvesNames = {} ;
            
            a = 1 ;
            for i=1 : length(handles.mydata.name)
                if (isfield(handles.mydata.info{i}, 'type') && strcmp(handles.mydata.info{i}.type, 'Calibration')) ...
                    || (isfield(handles.mydata.info{i}, 'Type') && strcmp(handles.mydata.info{i}.Type, 'Calibration'))
                    curvesNames{a} = handles.mydata.name{i} ;
                    a = a+1 ;
                end
            end
            
            if isempty(curvesNames)
                curvesNames = 'None' ;
            end
        end
        
        function referenceNames = getReferenceNames(this)
            % getReferenceNames  Get names of reference Bragg curves loaded in Reggui
            
            handles = guidata(this.regguiHObject) ;
            
            referenceNames = {} ;
            
            a = 1 ;
            for i=1 : length(handles.mydata.name)
                if (isfield(handles.mydata.info{i}, 'type') && strcmp(handles.mydata.info{i}.type, 'PR REF')) ...
                        || (isfield(handles.mydata.info{i}, 'Type') && strcmp(handles.mydata.info{i}.Type, 'PR REF')) 
                    referenceNames{a} = handles.mydata.name{i} ;
                    a = a+1 ;
                end
            end
            
            if isempty(referenceNames)
                referenceNames = 'None' ;
            end
        end
        
        function wetNames = getWETNames(this)
            % getWETNames  Get names of WET-maps loaded in Reggui
            
            handles = guidata(this.regguiHObject) ;
            
            wetNames = {} ;
            
            a = 1 ;
            for i=1 : length(handles.mydata.name)
                if (isfield(handles.mydata.info{i}, 'type') && strcmp(handles.mydata.info{i}.type, 'WET')) ...
                        || (isfield(handles.mydata.info{i}, 'Type') && strcmp(handles.mydata.info{i}.Type, 'WET'))
                    wetNames{a} = handles.mydata.name{i} ;
                    a = a+1 ;
                end
            end
            
            if isempty(wetNames)
                wetNames = 'None' ;
            end
        end
        
        function detectorNames = getDetectorNames(this)
            handles = guidata(this.regguiHObject) ;
            
            detectorNames = {} ;
            
            a = 1 ;
            for i=1 : length(handles.mydata.name)
                if (isfield(handles.mydata.info{i}, 'type') && strcmp(handles.mydata.info{i}.type, 'PRDetector')) ...
                        || ((isfield(handles.mydata.info{i}, 'Type') && strcmp(handles.mydata.info{i}.Type, 'PRDetector')))
                    detectorNames{a} = handles.mydata.name{i} ;
                    a = a+1 ;
                end
            end
            
            if isempty(detectorNames)
                detectorNames = 'None' ;
            end
        end
        
        function phantomNames = getPhantomNames(this)
            handles = guidata(this.regguiHObject) ;
            
            phantomNames = {} ;
            
            a = 1 ;
            for i=1 : length(handles.mydata.name)
                if (isfield(handles.mydata.info{i}, 'type') && strcmp(handles.mydata.info{i}.type, 'CT phantom')) ...
                    || (isfield(handles.mydata.info{i}, 'Type') && strcmp(handles.mydata.info{i}.Type, 'CT phantom'))
                    phantomNames{a} = handles.mydata.name{i} ;
                    a = a+1 ;
                end
            end
            
            if isempty(phantomNames)
                phantomNames = 'None' ;
            end
        end
        
        function spectrumNames = getSpectrumNames(this)
            handles = guidata(this.regguiHObject) ;
            
            spectrumNames = {} ;
            
            a = 1 ;
            for i=1 : length(handles.mydata.name)
                if (isfield(handles.mydata.info{i}, 'type') && strcmp(handles.mydata.info{i}.type, 'CT spectrum')) ...
                        || (isfield(handles.mydata.info{i}, 'Type') && strcmp(handles.mydata.info{i}.Type, 'CT spectrum'))
                    spectrumNames{a} = handles.mydata.name{i} ;
                    a = a+1 ;
                end
            end
            
            if isempty(spectrumNames)
                spectrumNames = 'None' ;
            end
        end
        
        function regguiHObject = getRegguiHObject(this)
            % getRegguiHObject  Get object handle of the Reggui interface
            
            regguiHObject = this.regguiHObject ;
        end
    end
end
