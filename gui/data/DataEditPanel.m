
classdef DataEditPanel < MainPanel
    properties (Access = private)
        control;
        dataList;
        dataTypePopup;
        rightPanel1;
        rightPanel2;
        rightPanelContent;
        propertyList;
        newNameEdit;
    end
    
    methods (Access = public)
        function this = DataEditPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            leftPanel = uipanel('Parent', this.mainPanel, 'Title', 'Show data', 'Position', [0 0 0.1 1]);
            this.rightPanel1 = uipanel('Parent', this.mainPanel, 'Position', [0.15 0.15 0.7 0.85]);
            this.rightPanel2 = uipanel('Parent', this.mainPanel, 'Title', 'Data properties', 'Position', [0.9 0.15 0.1 0.85]);
            bottomPanel = uipanel('Parent', this.mainPanel, 'Title', 'Save', 'Position', [0.15 0.0 0.85 0.1]);
            
            this.newNameEdit = uicontrol('Parent', bottomPanel, 'Style', 'edit', 'String', 'New name', 'Units', 'normalized', 'Position', [0.1, 0.1, 0.4, 0.8]);
            uicontrol('Parent', bottomPanel, 'Style', 'pushbutton', 'String', 'Save', 'Units', 'normalized', 'Position', [0.6, 0.1, 0.2, 0.8], 'Callback', @this.saveCallback);
            
            this.dataList = uicontrol('Parent', leftPanel, 'Style', 'listbox', 'Max', 50, 'Units', 'normalized', 'Position', [0, 0, 1, 0.8], 'Callback', @this.dataSelected);
            this.dataTypePopup = uicontrol('Parent', leftPanel, 'Style', 'popup', 'String', this.control.get('dataTypes'), 'Units', 'normalized', 'Position', [0, 0.85, 1, 0.1], 'Callback', @this.dataTypeSelected);
            
            this.rightPanelContent = [];
            
            this.propertyList = uicontrol('Parent', this.rightPanel2, 'Style', 'listbox', 'Max', 10, 'Units', 'normalized', 'Position', [0, 0, 1, 1], 'Callback', @this.propertySelected);
            
            this.updateList();
        end
        
        function setVisible(this, visible)
            this.updateList();
            
            setVisible@MainPanel(this, visible)
        end
        
        function updateList(this)
        	names = this.control.get('dataNames');
            this.dataList.set('String', names);
            
            propertyNames = this.control.get('dataEditablePropertyNames');
            this.propertyList.set('String', propertyNames);
        end
        
        function showData(this)
            if ~isempty(this.rightPanelContent)
                delete(this.rightPanelContent);
            end
            
            items = get(this.propertyList, 'String');
            index_selected = get(this.propertyList, 'Value');
            propertyName = items{index_selected};
            
            data = this.control.get('dataEditableProperty', propertyName);
            
            if numel(size(data))>2
                error('3D data visualization not yet implemented');
            end
            
            this.rightPanelContent =  uitable('Parent', this.rightPanel1, 'Data', data, 'Units', 'Normalized', 'Position', [0, 0, 1, 1], 'ColumnEditable', true, 'CellEditCallback', @this.saveModifications);
        end
    end
    
    methods (Access = private)
        function dataSelected(this, source, event)
            items = get(this.dataList, 'String');
            index_selected = get(this.dataList, 'Value');
            dataName = items{index_selected};          
            this.control.set('selectedDataName', dataName);
            
            this.showData();
        end
        
        function dataTypeSelected(this, source, event)
            items = get(this.dataTypePopup, 'String');
            index_selected = get(this.dataTypePopup, 'Value');
            type = items{index_selected};
            this.control.set('selectedDataType', type);
            
            this.updateList();
            
            items = get(this.dataList, 'String');
            if ~isempty(items)
            	this.dataList.set('Value', 1);
                this.propertyList.set('Value', 1);
                this.dataSelected([], []);
            end
        end
        
        function propertySelected(this, source, event)
            this.showData();
        end
        
        function saveModifications(this, source, event)
            try
                items = get(this.propertyList, 'String');
                index_selected = get(this.propertyList, 'Value');
                propertyName = items{index_selected};

                data = this.rightPanelContent.get('Data');
                this.control.set('dataEditableProperty', propertyName, data);
            catch
            end
        end
        
        function saveCallback(this, source, event)
            this.saveModifications();
            this.control.saveData(this.newNameEdit.get('String'));
            
            this.updateList();
        end
    end
end
