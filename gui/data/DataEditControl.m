
classdef DataEditControl < GenericControl
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function this = DataEditControl(model)
            this@GenericControl(model);
        end
        
        function set(this, varargin)
            this.model.set(varargin{:});
        end
        
        function value = get(this, varargin)
        	value = this.model.get(varargin{:});
        end
        
        function saveData(this, name)
            this.model.saveData(name);
        end
    end
end
