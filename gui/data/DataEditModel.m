
classdef DataEditModel < GenericModel
    properties (Access = private)
        selectedDataName;
        dataTypes;
        selectedDataType;
        data;
    end
    
    methods (Access = public)
        function this = DataEditModel(regguiHObject)
            this@GenericModel(regguiHObject);
            
            this.selectedDataName = '';
            this.selectedDataType = 'WET';
            this.dataTypes = {'WET', 'PR', 'CT calibration'};
            this.data = [];
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'selectedDataName'
                    value = this.selectedDataName;
                case 'selectedDataType'
                    value = this.selectedDataType;
                case 'dataTypes'
                    value = this.dataTypes ;
                case 'dataNames'
                    switch this.selectedDataType
                        case 'WET'
                            value = get@GenericModel(this, 'WETNames');
                        case 'PR'
                            value = get@GenericModel(this, 'PRNames');
                        case 'CT calibration'
                            value = get@GenericModel(this, 'curveNames');
                    end
                case 'dataEditablePropertyNames'
                    switch this.selectedDataType
                        case 'WET'
                            value = {'data', 'spacing', 'origin'};
                        case 'PR'
                            value = {'data', 'spacing', 'origin', 'depth'};
                        case 'CT calibration'
                            value = {'density', 'stopping power'};
                    end
                case 'dataEditableProperty'
                    if ~isempty(this.data)
                        value = this.data.get(varargin{2});
                    else
                        value = [];
                    end
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end
        
        function set(this, varargin)
            property = varargin{1};
            value = varargin{2};
            
            switch property                 
                case 'selectedDataName'
                    this.selectedDataName = value;
                    
                    handles = guidata(this.regguiHObject);
                    switch this.selectedDataType
                        case 'PR'
                            this.data = handles2BraggImage(this.selectedDataName, handles);
                        case 'WET'
                            this.data = handles2Image2D(this.selectedDataName, handles);
                        case 'CT calibration'
                            this.data = handles2CTCalibration(this.selectedDataName, handles);
                    end
                case 'selectedDataType'
                    this.selectedDataType = value;
                    this.selectedDataName = '';
                    this.data = [];
                case 'dataEditableProperty'
                    propertyName = varargin{2};
                    propertyValue = varargin{3};
                    disp('Setting data')
                    this.data.set(propertyName, propertyValue);
                otherwise
                    set@GenericModel(this, varargin{:});
            end
        end
        
        function saveData(this, name)
            handles = guidata(this.regguiHObject);
            
            switch this.selectedDataType
                case 'PR'
                    handles = braggImage2handles(this.data, 'PR', name, handles);
                case 'WET'
                    handles = image2D2handles(this.data, 'WET', name, handles, false);
                case 'CT calibration'
                    handles = ctCalibration2handles(this.data, name, handles, false);
                otherwise
                    error('Data class not supported');
            end
            
            guidata(this.regguiHObject, handles);
        end
    end
    
    methods (Access = private)
        
    end
end
