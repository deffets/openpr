
classdef SimulationModel < GenericModel
    properties (SetObservable)
    end
    
    methods (Access = public)
        function obj = SimulationModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
        end
        
        function stub(this)
            stub(guidata(this.regguiHObject)) ;
        end
    end
end
