
classdef MC2SimulationPanel < MainPanel
    properties (Access = private)
        CTPopup ;

        control ;
        
        maxEnergyEdit ;
        energyStepEdit;
        nbLayersEdit;
        fieldSizeXEdit;
        fieldSizeYEdit;
        spotSpacingEdit;
        gantryAngleEdit;
        isocenterEdit;
        simulatedProtonsEdit;
        
        goButton ;
        
    end
    
    methods (Access = public)
        function obj = MC2SimulationPanel(f, control)
            obj@MainPanel(f) ;
            obj.control = control ;
            
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'CT', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]) ;
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'GTR Angle', 'Units', 'normalized', 'Position', [0.14, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Max Energy', 'Units', 'normalized', 'Position', [0.23, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Energy Step', 'Units', 'normalized', 'Position', [0.32, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', '# Layers', 'Units', 'normalized', 'Position', [0.41, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Size X', 'Units', 'normalized', 'Position', [0.5, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Size Y', 'Units', 'normalized', 'Position', [0.59, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Spacing', 'Units', 'normalized', 'Position', [0.68, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'ISO', 'Units', 'normalized', 'Position', [0.75, 0.55, 0.08, 0.3]) ;
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', '# p+', 'Units', 'normalized', 'Position', [0.85, 0.55, 0.05, 0.3]) ;
            
            obj.CTPopup = uicontrol('Parent', obj.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]) ;
            obj.gantryAngleEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('gantryAngle')), 'Units', 'normalized', 'Position', [0.14, 0.15, 0.05, 0.3]) ;
            obj.maxEnergyEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('maxEnergy')), 'Units', 'normalized', 'Position', [0.23, 0.15, 0.05, 0.3]) ;
            obj.energyStepEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('energyStep')), 'Units', 'normalized', 'Position', [0.32, 0.15, 0.05, 0.3]) ;
            obj.nbLayersEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('nbLayers')), 'Units', 'normalized', 'Position', [0.41, 0.15, 0.05, 0.3]) ;
            obj.fieldSizeXEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('fieldSizeX')), 'Units', 'normalized', 'Position', [0.5, 0.15, 0.05, 0.3]) ;
            obj.fieldSizeYEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('fieldSizeY')), 'Units', 'normalized', 'Position', [0.59, 0.15, 0.05, 0.3]) ;
            obj.spotSpacingEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('spotSpacing')), 'Units', 'normalized', 'Position', [0.68, 0.15, 0.05, 0.3]) ;
            obj.isocenterEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('isocenter')), 'Units', 'normalized', 'Position', [0.75, 0.15, 0.08, 0.3]) ;
            obj.simulatedProtonsEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('simulatedProtons')), 'Units', 'normalized', 'Position', [0.85, 0.15, 0.05, 0.3]) ;

            obj.goButton = uicontrol('Parent', obj.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.93, 0.3, 0.05, 0.5], 'Callback', @obj.goCallback) ;
            
            obj.updatePopups() ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
	methods (Access = private)
        function updatePopups(this)
            names = this.control.getCTNames() ;
            CTName = this.control.get('CTName') ;
            if ~isempty(CTName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, CTName)) ;
            else
                val = 1 ;
            end
            
            if isempty(val)
                val = 1 ;
            end
            
            set(this.CTPopup, 'String', names) ;
            set(this.CTPopup, 'Value', val) ;

        end
        
        function goCallback(this, source, event)
            items = get(this.CTPopup, 'String') ;
            index_selected = get(this.CTPopup, 'Value') ;
            CTName = items{index_selected} ;
            this.control.set('CTName', CTName) ;

            this.control.set('maxEnergy', str2double(get(this.maxEnergyEdit, 'String'))) ;
            this.control.set('energyStep', str2double(get(this.energyStepEdit, 'String'))) ;
            this.control.set('nbLayers', str2double(get(this.nbLayersEdit, 'String'))) ;
            this.control.set('fieldSizeX', str2double(get(this.fieldSizeXEdit, 'String'))) ;
            this.control.set('fieldSizeY', str2double(get(this.fieldSizeYEdit, 'String'))) ;
            this.control.set('spotSpacing', str2double(get(this.spotSpacingEdit, 'String'))) ;
            this.control.set('gantryAngle', str2double(get(this.gantryAngleEdit, 'String'))) ;
            this.control.set('isocenter', str2num(get(this.isocenterEdit, 'String'))) ;
            this.control.set('simulatedProtons', str2double(get(this.simulatedProtonsEdit, 'String'))) ;
            
            simuDir = uigetdir(this.control.get('simuDir'),'Select output folder and file name to save simulation output');
            this.control.set('simuDir', simuDir) ;
            this.control.mc2Simulation() ;
        end
    end
    
end
