
classdef MC2SimulationModel < GenericModel
    properties (SetObservable)
        CTName ;
        
        maxEnergy ;
        energyStep;
        nbLayers;
        fieldSizeX;
        fieldSizeY;
        spotSpacing;
        gantryAngle;
        isocenter;
        simulatedProtons;
        simuDir;

    end
    
    methods (Access = public)
        function obj = MC2SimulationModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
            
            % update obj list
            obj.CTName = [] ;
            obj.maxEnergy = 230;
            obj.energyStep = 10;
            obj.nbLayers = 13;
            obj.fieldSizeX = 40;
            obj.fieldSizeY = 30;
            obj.spotSpacing = 2;
            obj.gantryAngle = 270;
            obj.isocenter = [256 256 256];
            obj.simulatedProtons = 1e6;
            handles = guidata(regguiHObject) ;
            obj.simuDir = [handles.dataPath];
        end
        
        function set(this, property, value)
            switch property
                case 'CTName'
                    this.CTName = value ;
                case 'maxEnergy'
                    this.maxEnergy = value ;
                case 'energyStep'
                    this.energyStep = value ;
                case 'nbLayers'
                    this.nbLayers = value ;
                case 'fieldSizeX'
                    this.fieldSizeX = value ;
                case 'fieldSizeY'
                    this.fieldSizeY = value ;
                case 'spotSpacing'
                    this.spotSpacing = value ;
                case 'gantryAngle'
                    this.gantryAngle = value ;
                case 'isocenter'
                    this.isocenter = value ;
                case 'simulatedProtons'
                    this.simulatedProtons = value ;
                case 'simuDir'
                    this.simuDir = value ;
            end
        end
        
        
        function value = get(this, property)
            switch property
                case 'CTName'
                    value = this.CTName ;
                case 'maxEnergy'
                    value = this.maxEnergy ;
                case 'energyStep'
                    value = this.energyStep ;
                case 'nbLayers'
                    value = this.nbLayers ;                    
                case 'fieldSizeX'
                    value = this.fieldSizeX ;
                case 'fieldSizeY'
                    value = this.fieldSizeY ;
                case 'spotSpacing'
                    value = this.spotSpacing ;
                case 'gantryAngle'
                    value = this.gantryAngle ;
                case 'isocenter'
                    value = this.isocenter ;
                case 'simulatedProtons'
                    value = this.simulatedProtons ;
                case 'simuDir'
                    value = this.simuDir ;
            end
        end
        
        function mc2Simulation(this)
            warning('Some parameters are hard-coded! Look at file MC2SimulationModel');
            handles = guidata(this.regguiHObject) ;
            
            instruction = ['handles = MC2_FP_simulation(' '''' this.CTName '''' ', ' ...
                num2str(this.simulatedProtons) ', ' ...
                num2str(this.maxEnergy) ', ' ...
                num2str(this.energyStep) ', ' ...
                num2str(this.nbLayers) ', ' ...
                num2str(this.fieldSizeX) ', ' ...
                num2str(this.fieldSizeY) ', ' ...
                num2str(this.spotSpacing) ', ' ...
                num2str(this.gantryAngle) ', ' ...
                '[' num2str(this.isocenter) ']' ', ' ...
                '''' this.simuDir '''' ', handles) ;'] ;
            
            handles.instructions{length(handles.instructions)+1} = instruction ;
            
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
    
       
    methods (Access = private)
        
    end
end
