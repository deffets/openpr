
classdef MC2SimulationControl < GenericControl
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function obj = MC2SimulationControl(model)
            obj@GenericControl(model) ;
        end
        
        function set(this, property, value)
            this.model.set(property, value) ;
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
        
        function mc2Simulation(this)
            this.model.mc2Simulation() ;
        end
    end
    
    methods (Access = private)
        
    end
end
