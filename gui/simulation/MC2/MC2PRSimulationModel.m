
classdef MC2PRSimulationModel < GenericModel
    properties (SetObservable)
        CTName;
        calibrationName;
        
        energy;
        spotSpacing;
        spotsNb;
        gantryAngle;
        protonsNb;
        framesNb1;
        framesNb2;
        
        simulationName;
    end
    
    methods (Access = public)
        function obj = MC2PRSimulationModel(regguiHObject)
            obj@GenericModel(regguiHObject);
            
            % update obj list
            obj.CTName = [];
            obj.calibrationName = [];
            obj.energy = 210;
            obj.spotSpacing = 5;
            obj.spotsNb = 9;
            obj.framesNb1 = 2;
            obj.framesNb2 = 2;
            obj.gantryAngle = 270;
            obj.protonsNb = 10^4;
        end
        
        function set(this, property, value)
            switch property
                case 'CTName'
                    this.CTName = value;
                case 'energy'
                    this.energy = value;
                case 'spotSpacing'
                    this.spotSpacing = value;
                case 'spotsNb'
                    this.spotsNb = value;
                case 'gantryAngle'
                    this.gantryAngle = value;
                case 'protonsNb'
                    this.protonsNb = value;
                case 'simulationName'
                    this.simulationName = value;
                case 'calibrationName'
                    this.calibrationName = value;
                case 'framesNb1'
                    this.framesNb1 = value;
                case 'framesNb2'
                    this.framesNb2 = value;
            end
        end
        
        
        function value = get(this, property)
            switch property
                case 'CTName'
                    value = this.CTName;
                case 'energy'
                    value = this.energy;
                case 'spotSpacing'
                    value = this.spotSpacing;
                case 'spotsNb'
                    value = this.spotsNb;                    
                case 'gantryAngle'
                    value = this.gantryAngle;
                case 'protonsNb'
                    value = this.protonsNb;
                case 'simulationName'
                    value = this.simulationName;
                case 'calibrationName'
                    value = this.calibrationName;
                case 'framesNb1'
                    value = this.framesNb1;
                case 'framesNb2'
                    value = this.framesNb2;
            end
        end
        
        function mc2Simulation(this)
            handles = guidata(this.regguiHObject);
                       
            instruction = ['handles = MC2PRSimulationInstruction(' '''' this.CTName '''' ', ' '''' this.calibrationName '''' ', ' ...
                num2str(this.spotsNb) ', ' ...
                num2str(this.spotSpacing) ', ' ...
                num2str(this.framesNb1) ', ' ...
                num2str(this.framesNb2) ', ' ...
                num2str(this.energy) ', ' ...
                num2str(this.protonsNb) ', ' ...
                '''' this.simulationName '''' ', handles);'];
            
            handles.instructions{length(handles.instructions)+1} = instruction;
            
            handles = Execute_reggui_instructions(handles);
            guidata(this.regguiHObject, handles);
        end
    end
    
       
    methods (Access = private)
        
    end
end
