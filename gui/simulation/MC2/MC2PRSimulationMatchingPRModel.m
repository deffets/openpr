
classdef MC2PRSimulationMatchingPRModel < GenericModel
    properties (SetObservable)
        CTName;
        calibrationName;
        PRName;
        BDLName;
        
        FOV;
        gantryAngle;
        energy;
        spotSpacing;
        protonsNb;
        
        simulationName;
    end
    
    methods (Access = public)
        function obj = MC2PRSimulationMatchingPRModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
            
            obj.CTName = [] ;
            obj.PRName = [];
            obj.BDLName = [];
            obj.energy = 230;
            obj.gantryAngle = 270;
            obj.protonsNb = 1e4;
            obj.FOV = 40;
        end
        
        function set(this, varargin)
            property = varargin{1};
            value = varargin{2};
            
            switch property
                case 'CTName'
                    this.CTName = value;
                case 'PRName'
                    this.PRName = value;
                case 'energy'
                    this.energy = value;
                case 'gantryAngle'
                    this.gantryAngle = value;
                case 'protonsNb'
                    this.protonsNb = value;
                case 'simulationName'
                    this.simulationName = value;
                case 'calibrationName'
                    this.calibrationName = value;
                case 'FOV'
                    this.FOV = value;
                case 'BDLName'
                    this.BDLName = value;
                otherwise
                    set@GenericModel(this, varargin{:});
            end
        end
        
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'CTName'
                    value = this.CTName;
                case 'PRName'
                    value = this.PRName;
                case 'energy'
                    value = this.energy;            
                case 'gantryAngle'
                    value = this.gantryAngle;
                case 'protonsNb'
                    value = this.protonsNb;
                case 'simulationName'
                    value = this.simulationName;
                case 'calibrationName'
                    value = this.calibrationName;
                case 'BDLName'
                    value = this.BDLName;
                case 'FOV'
                    value = this.FOV;
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end
        
        function mc2Simulation(this)
            handles = guidata(this.regguiHObject);
                       
            instruction = ['handles = MC2PRSimulationMatchingPRInstruction(' '''' this.CTName '''' ', ' '''' this.calibrationName '''' ', ' '''' this.PRName '''' ', ' '''' this.BDLName '''' ', '...
                num2str(this.energy) ', ' ...
                num2str(this.FOV) ', ' ...
                num2str(this.protonsNb) ', ' ...
                '''' this.simulationName '''' ', handles);'];
            
            handles.instructions{length(handles.instructions)+1} = instruction;
            
            handles = Execute_reggui_instructions(handles);
            guidata(this.regguiHObject, handles);
        end
    end
    
       
    methods (Access = private)
        
    end
end
