
classdef MC2PRSimulationControl < GenericControl
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function obj = MC2PRSimulationControl(model)
            obj@GenericControl(model) ;
        end
        
        function set(this, varargin)
            this.model.set(varargin{:}) ;
        end
        
        function value = get(this, varargin)
            value = this.model.get(varargin{:}) ;
        end
        
        function mc2Simulation(this)
            this.model.mc2Simulation() ;
        end
    end
    
    methods (Access = private)
        
    end
end
