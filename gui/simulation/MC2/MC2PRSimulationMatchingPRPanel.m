
classdef MC2PRSimulationMatchingPRPanel < MainPanel
    properties (Access = private)
        CTPopup;
        calibrationPopup;
        PRPopup;
        
        BDLButton;
        
        control;
        
        fovEdit;
        energyEdit;
        BDLEdit;
        gantryAngleEdit;
        simulatedProtonsEdit;
        
        nameEdit;
        
        goButton;
        
    end
    
    methods (Access = public)
        function obj = MC2PRSimulationMatchingPRPanel(f, control)
            obj@MainPanel(f);
            obj.control = control;
            
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'CT', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]);
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Calibration', 'Units', 'normalized', 'Position', [0.12, 0.55, 0.1, 0.3]);
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'PR', 'Units', 'normalized', 'Position', [0.24, 0.55, 0.1, 0.3]);
            
            obj.BDLButton = uicontrol('Parent', obj.mainPanel, 'Style', 'pushbutton', 'String', 'BDL', 'Units', 'normalized', 'Position', [0.35, 0.55, 0.1, 0.3], 'Callback', @obj.BDLCallback) ;
            
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'FOV', 'Units', 'normalized', 'Position', [0.5, 0.55, 0.05, 0.3]);
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Energy', 'Units', 'normalized', 'Position', [0.55, 0.55, 0.05, 0.3]);
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', '# protons', 'Units', 'normalized', 'Position', [0.6, 0.55, 0.05, 0.3]);
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.7, 0.55, 0.1, 0.3]);
            
            obj.CTPopup = uicontrol('Parent', obj.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]);
            obj.calibrationPopup = uicontrol('Parent', obj.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.12, 0.15, 0.1, 0.3]);     
            obj.PRPopup = uicontrol('Parent', obj.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.24, 0.16, 0.1, 0.3]);
            
%             obj.gantryAngleEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('gantryAngle')), 'Units', 'normalized', 'Position', [0.5, 0.15, 0.05, 0.3]);
            obj.fovEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('FOV')), 'Units', 'normalized', 'Position', [0.5, 0.15, 0.05, 0.3]);
            obj.energyEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('energy')), 'Units', 'normalized', 'Position', [0.55, 0.15, 0.05, 0.3]);
            obj.simulatedProtonsEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('protonsNb')), 'Units', 'normalized', 'Position', [0.6, 0.15, 0.05, 0.3]);
            obj.nameEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', obj.control.get('simulationName'), 'Units', 'normalized', 'Position', [0.7, 0.15, 0.1, 0.3]);
            obj.BDLEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', obj.control.get('BDLName'), 'Units', 'normalized', 'Position', [0.35, 0.15, 0.1, 0.3]);
            
            obj.goButton = uicontrol('Parent', obj.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @obj.goCallback);
            
            obj.updatePopups();
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups();
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
	methods (Access = private)
        function updatePopups(this)
            names = this.control.getCTNames();
            CTName = this.control.get('CTName');
            if ~isempty(CTName)
                ind = 1 : length(names);
                val = ind(strcmp(names, CTName));
            else
                val = 1;
            end
            
            if isempty(val)
                val = 1;
            end
            
            set(this.CTPopup, 'String', names);
            set(this.CTPopup, 'Value', val);
            
            
            
            names = this.control.getCurvesNames();
            calibrationName = this.control.get('calibrationName');
            if ~isempty(calibrationName)
                ind = 1 : length(names);
                val = ind(strcmp(names, calibrationName));
            else
                val = 1;
            end
            
            if isempty(val)
                val = 1;
            end
            
            set(this.calibrationPopup, 'String', names);
            set(this.calibrationPopup, 'Value', val);
            
            names = this.control.getPRNames();
            prName = this.control.get('PRName');
            if ~isempty(prName)
                ind = 1 : length(names);
                val = ind(strcmp(names, prName));
            else
                val = 1;
            end
            
            if isempty(val)
                val = 1;
            end
            
            set(this.PRPopup, 'String', names);
            set(this.PRPopup, 'Value', val);
        end
        
        function goCallback(this, source, event)
            items = get(this.CTPopup, 'String');
            index_selected = get(this.CTPopup, 'Value');
            CTName = items{index_selected};
            this.control.set('CTName', CTName);
            
            items = get(this.calibrationPopup, 'String');
            index_selected = get(this.calibrationPopup, 'Value');
            calibrationName = items{index_selected};
            this.control.set('calibrationName', calibrationName);
            
            items = get(this.PRPopup, 'String');
            index_selected = get(this.PRPopup, 'Value');
            PRName = items{index_selected};
            this.control.set('PRName', PRName);

            
%             this.control.set('gantryAngle', str2double(get(this.gantryAngleEdit, 'String')));
            this.control.set('FOV', str2double(get(this.fovEdit, 'String')));
            this.control.set('energy', str2double(get(this.energyEdit, 'String')));
            this.control.set('protonsNb', str2double(get(this.simulatedProtonsEdit, 'String')));
            this.control.set('simulationName', get(this.nameEdit, 'String'));

            this.control.mc2Simulation();
        end
        
        function BDLCallback(this, source, event)
            path = this.control.get('path', 'BDLFile');
            [fileName, pathName] = uigetfile(fullfile(path, '*.*'));
            
            if fileName~=0
                this.control.set('path', 'BDLFile', pathName);
            
                this.control.set('BDLName', fullfile(pathName, fileName)) ;
                
                BDLFile = this.control.get('BDLName') ;
                set(this.BDLEdit, 'String', BDLFile) ;
            end
        end
    end
    
end
