
classdef SimpleFPSimulationControl < GenericControl
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function obj = SimpleFPSimulationControl(model)
            obj@GenericControl(model) ;
        end
        
        function set(this, property, value)
            this.model.set(property, value) ;
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
        
        function simpleSimulation(this)
            this.model.simpleSimulation() ;
        end
    end
    
    methods (Access = private)
        
    end
end
