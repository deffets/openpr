
classdef SimpleFPSimulationPanel < MainPanel
    properties (Access = private)
        CTPopup ;
        referencePopup ;
        curvePopup ;
        control ;
        
        CTText ;
        angleText ;
        referenceText ;
        calibrationText ;
        nameText ;
        sigmaText ;
        
        angleEdit ;
        nameEdit ;
        sigmaEdit ;
        
        goButton ;
    end
    
    methods (Access = public)
        function obj = SimpleFPSimulationPanel(f, control)
            obj@MainPanel(f) ;
            obj.control = control ;
            
            obj.CTText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'CT', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]) ;
            obj.angleText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Angle', 'Units', 'normalized', 'Position', [0.15, 0.55, 0.05, 0.3]) ;
            obj.referenceText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Reference', 'Units', 'normalized', 'Position', [0.25, 0.55, 0.1, 0.3]) ;
            obj.calibrationText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Calibration curve', 'Units', 'normalized', 'Position', [0.4, 0.55, 0.1, 0.3]) ;
            obj.nameText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.55, 0.55, 0.1, 0.3]) ;
            obj.sigmaText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Sigma', 'Units', 'normalized', 'Position', [0.7, 0.55, 0.05, 0.3]) ;
            
            obj.angleEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('angle')), 'Units', 'normalized', 'Position', [0.15, 0.15, 0.05, 0.3]) ;
            obj.nameEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', obj.control.get('name'), 'Units', 'normalized', 'Position', [0.55, 0.15, 0.1, 0.3]) ;
            obj.sigmaEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', obj.control.get('sigma'), 'Units', 'normalized', 'Position', [0.7, 0.15, 0.05, 0.3]) ;            
            
            obj.CTPopup = uicontrol('Parent', obj.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]) ;
            obj.referencePopup = uicontrol('Parent', obj.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.25, 0.15, 0.1, 0.3]) ;
            obj.curvePopup = uicontrol('Parent', obj.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.4, 0.15, 0.1, 0.3]) ;
            
            obj.goButton = uicontrol('Parent', obj.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @obj.goCallback) ;
            
            obj.updatePopups() ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
	methods (Access = private)
        function updatePopups(this)
            names = this.control.getCTNames() ;
            CTName = this.control.get('CTName') ;
            if ~isempty(CTName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, CTName)) ;
            else
                val = 1 ;
            end
            
            if isempty(val)
                val = 1 ;
            end
            
            set(this.CTPopup, 'String', names) ;
            set(this.CTPopup, 'Value', val) ;
        
            
            names = this.control.getReferenceNames() ;
            referenceName = this.control.get('referenceName') ;
            if ~isempty(referenceName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, referenceName)) ;
            else
                val = 1 ;
            end
            
            if isempty(val)
                val = 1 ;
            end
            
            set(this.referencePopup, 'String', names) ;
            set(this.referencePopup, 'Value', val) ;
       
            names = this.control.getCurvesNames() ;
            curveName = this.control.get('curveName') ;
            if ~isempty(curveName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, curveName)) ;
            else
                val = 1 ;
            end
            
            if isempty(val)
                val = 1 ;
            end
            set(this.curvePopup, 'String', names) ;
            set(this.curvePopup, 'Value', val) ;
        end
        
        function goCallback(this, source, event)
            this.control.set('angle', str2double(get(this.angleEdit, 'String'))) ;
            this.control.set('name', get(this.nameEdit, 'String')) ;
            this.control.set('sigma', str2double(get(this.sigmaEdit, 'String'))) ;
            
            items = get(this.CTPopup, 'String') ;
            index_selected = get(this.CTPopup, 'Value') ;
            CTName = items{index_selected} ;
            this.control.set('CTName', CTName) ;
            
            items = get(this.referencePopup, 'String') ;
            index_selected = get(this.referencePopup, 'Value') ;
            referenceName = items{index_selected} ;
            this.control.set('referenceName', referenceName) ;
            
            items = get(this.curvePopup, 'String') ;
            index_selected = get(this.curvePopup, 'Value') ;
            curveName = items{index_selected} ;
            this.control.set('curveName', curveName) ;
            
            this.control.simpleSimulation() ;
        end
    end
    
end
