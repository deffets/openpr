
classdef SimpleFPSimulationModel < GenericModel
    properties (SetObservable)
        CTName ;
        angle ;
        referenceName ;
        name ;
        curveName ;
        sigma ;
    end
    
    methods (Access = public)
        function obj = SimpleFPSimulationModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
            
            obj.CTName = [] ;
            obj.angle = 0 ;
            obj.referenceName = [] ;
            obj.name = [] ;
            obj.curveName = [] ;
            obj.sigma = 3 ;
        end
        
        function set(this, property, value)
            switch property
                case 'angle'
                    this.angle = value ;
                case 'name'
                    this.name = value ;
                case 'CTName'
                    this.CTName = value ;
                case 'curveName'
                    this.curveName = value ;
                case 'referenceName'
                    this.referenceName = value ;
                case 'sigma'
                    this.sigma = value ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'angle'
                    value = this.angle ;
                case 'name'
                    value = this.name ;
                case 'CTName'
                    value = this.CTName ;
                case 'curveName'
                    value = this.curveName ;
                case 'referenceName'
                    value = this.referenceName ;
                case 'sigma'
                    value = this.sigma ;
            end
        end

        function simpleSimulation(this)
            handles = guidata(this.regguiHObject) ;
            
            instruction = ['handles = simpleFPSimulation(' '''' this.CTName '''' ', ' ...
                '''' this.referenceName ''''  ', ' '''' this.curveName '''' ', ' ...
                '''RayStation''' ', '  ...
                num2str(this.sigma) ', ' num2str(210) ', ' num2str(this.angle) ', ' ...
                num2str(0) ', ' '''' this.name '''' ', handles) ;'] ;
            
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
    
    methods (Access = private)
        
    end
end
