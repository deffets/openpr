
classdef SimpleSimulationWETPanel < MainPanel
    properties (Access = protected)
        control ;
        
        wetPopup ;
        referencePopup ;
        
        wetText ;
        referenceText ;
        nameText ;
        sigmaText ;
        
        nameEdit ;
        sigmaEdit ;
        
        goButton ;
    end
    
    methods (Access = public)
        function obj = SimpleSimulationWETPanel(f, control)
            obj@MainPanel(f) ;
            obj.control = control ;
            
            obj.wetText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'WET-map', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]) ;
            obj.referenceText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Reference', 'Units', 'normalized', 'Position', [0.25, 0.55, 0.1, 0.3]) ;
            obj.nameText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.55, 0.55, 0.1, 0.3]) ;
            obj.sigmaText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Sigma', 'Units', 'normalized', 'Position', [0.7, 0.55, 0.05, 0.3]) ;
            
            obj.nameEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', obj.control.get('name'), 'Units', 'normalized', 'Position', [0.55, 0.15, 0.1, 0.3]) ;
            obj.sigmaEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', obj.control.get('sigma'), 'Units', 'normalized', 'Position', [0.7, 0.15, 0.05, 0.3]) ;            
            
            obj.wetPopup = uicontrol('Parent', obj.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]) ;
            obj.referencePopup = uicontrol('Parent', obj.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.25, 0.15, 0.1, 0.3]) ;
            
            obj.goButton = uicontrol('Parent', obj.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @obj.goCallback) ;
            
            obj.updatePopups() ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
    
        function updatePopups(this)
            names = this.control.getWETNames() ;
            wetName = this.control.get('wetName') ;
            if ~isempty(wetName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, wetName)) ;
            else
                val = 1 ;
            end
            
            if isempty(val)
                val = 1 ;
            end
            
            set(this.wetPopup, 'String', names) ;
            set(this.wetPopup, 'Value', val) ;
        
            
            names = this.control.getReferenceNames() ;
            referenceName = this.control.get('referenceName') ;
            if ~isempty(referenceName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, referenceName)) ;
            else
                val = 1 ;
            end
            
            if isempty(val)
                val = 1 ;
            end
            
            set(this.referencePopup, 'String', names) ;
            set(this.referencePopup, 'Value', val) ;
        end
    end
    
	methods (Access = protected)
        function goCallback(this, source, event)
            this.control.set('name', get(this.nameEdit, 'String')) ;
            this.control.set('sigma', str2double(get(this.sigmaEdit, 'String'))) ;
            
            items = get(this.wetPopup, 'String') ;
            index_selected = get(this.wetPopup, 'Value') ;
            wetName = items{index_selected} ;
            this.control.set('wetName', wetName) ;
            
            items = get(this.referencePopup, 'String') ;
            index_selected = get(this.referencePopup, 'Value') ;
            referenceName = items{index_selected} ;
            this.control.set('referenceName', referenceName) ;
            
            this.control.simpleSimulation() ;
        end
    end
    
end
