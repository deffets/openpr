
classdef SimpleSimulationWETModel < GenericModel
    properties (SetObservable)
        wetName ;
        referenceName ;
        name ;
        sigma ;
        
        progressControl ;
    end
    
    methods (Access = public)
        function obj = SimpleSimulationWETModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
            
            obj.wetName = [] ;
            obj.referenceName = [] ;
            obj.name = [] ;
            obj.sigma = 3 ;
        end
        
        function set(this, property, value)
            switch property
                case 'name'
                    this.name = value ;
                case 'wetName'
                    this.wetName = value ;
                case 'referenceName'
                    this.referenceName = value ;
                case 'sigma'
                    this.sigma = value ;
                case 'progressControl' ;
                    this.progressControl = value ;
                otherwise
                    error('Set property not found')
            end
        end
        
        function value = get(this, property)
            switch property
                case 'name'
                    value = this.name ;
                case 'wetName'
                    value = this.wetName ;
                case 'referenceName'
                    value = this.referenceName ;
                case 'sigma'
                    value = this.sigma ;
                otherwise
                    error('Get property not found')
            end
        end

        function simpleSimulation(this)
            handles = guidata(this.regguiHObject) ;
            
            instruction = ['handles = simplePRSimulationFromWET(' '''' this.wetName '''' ', ' ...
                '''' this.referenceName ''''  ', ' ...
                num2str(this.sigma) ', ' num2str(210) ', ' ...
                '''' this.name '''' ', handles) ;'] ;
            
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
end
