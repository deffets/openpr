
classdef SimpleSimulationWETControl < GenericControl
    properties (Access = private)
        progressControl ;
    end
    
    methods (Access = public)
        function obj = SimpleSimulationWETControl(model)
            obj@GenericControl(model) ;
            obj.progressControl = [] ;
        end
        
        function set(this, property, value)
            switch property
                case 'progressControl'
                    this.progressControl = value ;
                    this.model.set('progressControl', this.progressControl) ;
                otherwise
                    this.model.set(property, value) ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'progressControl'
                    value = this.progressControl ;
                otherwise
                    value = this.model.get(property) ;
            end
        end
        
        function simpleSimulation(this)
            this.model.simpleSimulation() ;
        end
    end
end
