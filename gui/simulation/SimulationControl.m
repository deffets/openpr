
classdef SimulationControl < GenericControl
    properties (Access = private)
        simpleSimulationWETControl ;
        simpleSimulationControl ;
        wetSimulationControl;
        gateSimulationControl;
        mc2PRSimulationControl;
        mc2PRSimulationMatchingPRControl;
    end
    
    methods (Access = public)
        function obj = SimulationControl(model)
            obj@GenericControl(model) ;
            
            simpleSimulationWETModel = SimpleSimulationWETModel(obj.model.getRegguiHObject()) ;
            obj.simpleSimulationWETControl = SimpleSimulationWETControl(simpleSimulationWETModel) ;
            
            simpleSimulationModel = SimpleSimulationModel(obj.model.getRegguiHObject()) ;
            obj.simpleSimulationControl = SimpleSimulationControl(simpleSimulationModel) ;
            
            mc2PRSimulationModel = MC2PRSimulationModel(obj.model.getRegguiHObject()) ;
            obj.mc2PRSimulationControl = MC2PRSimulationControl(mc2PRSimulationModel) ;
            
            mc2PRSimulationModel = MC2PRSimulationMatchingPRModel(obj.model.getRegguiHObject()) ;
            obj.mc2PRSimulationMatchingPRControl = MC2PRSimulationControl(mc2PRSimulationModel);

            wetSimulationModel = WETSimulationModel(obj.model.getRegguiHObject()) ;
            obj.wetSimulationControl = WETSimulationControl(wetSimulationModel);
            
            gateSimulationModel = GateSimulationModel(obj.model.getRegguiHObject()) ;
            obj.gateSimulationControl = GateSimulationControl(gateSimulationModel) ;
        end
        
        function control = getMC2PRSimulationControl(this)
            control = this.mc2PRSimulationControl;
        end
        
        function control = getMC2PRSimulationMatchingPRControl(this)
            control = this.mc2PRSimulationMatchingPRControl;
        end
        
        function control = getSimpleSimulationWETControl(this)
            control = this.simpleSimulationWETControl ;
        end
        
        function control = getSimpleSimulationControl(this)
            control = this.simpleSimulationControl ;
        end
        
        
        function control = getWETSimulationControl(this)
            control = this.wetSimulationControl;
        end
        
        function control = getGateSimulationControl(this)
            control = this.gateSimulationControl ;
        end
    end
end
