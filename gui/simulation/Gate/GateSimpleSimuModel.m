
classdef GateSimpleSimuModel < GateMainSimulationModel
    properties (SetObservable)
        CTName;
        detectorName;
        simuName;
        
        energy;
        
        spotsNb;
        spotsSpacing;
        spotsOrigin;
        
        spotsPos;
        spotsDone;
        bCurve;
        ct;
        avrgTime;
        remTime;
    end
    
    methods (Access = public)
        function this = GateSimpleSimuModel(regguiHObject, user, pass, ip, port)
            this@GateMainSimulationModel(regguiHObject, user, pass, ip, port);
            
            this.CTName = [];
            this.detectorName = [];
            this.simuName = [];
            
            this.energy = 180;
            this.spotsNb = [10 10];
            this.spotsSpacing = [5 5];
            this.spotsOrigin = [0 0];
            
            this.spotsPos = [];
            this.spotsDone = [];
            this.bCurve = [];
            this.ct = [];
        end
        
        function set(this, varargin)
            property = varargin{1};
            value = varargin{2};
            
            switch property
                case 'CTName'
                    this.CTName = value;
                case 'detectorName'
                    this.detectorName = value;
                case 'simuName'
                    this.simuName = value;
                case 'energy'
                    this.energy = value;
                case 'spotsNb'
                    this.spotsNb = value;
                case 'spotsSpacing'
                    this.spotsSpacing = value;
                case 'spotsOrigin'
                    this.spotsOrigin = value;
                otherwise
                    set@GateMainSimulationModel(this, varargin{:});
            end
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'CTName'
                    value = this.CTName;
                case 'detectorName'
                    value = this.detectorName;
                case 'simuName'
                    value = this.simuName;
                case 'energy'
                    value = this.energy;
                case 'spotsNb'
                    value = this.spotsNb;
                case 'spotsSpacing'
                    value = this.spotsSpacing;
                case 'spotsOrigin'
                    value = this.spotsOrigin;
                case 'spotsPos'
                    value = this.spotsPos;
                case 'spotsDone'
                    value = this.spotsDone;
                case 'bCurve'
                    value = this.bCurve;
                case 'ct'
                    value = this.ct;
                case 'avrgTime'
                    value = this.avrgTime;
                case 'remTime'
                    value = this.remTime;
                otherwise
                    value = get@GateMainSimulationModel(this, varargin{:});
            end
        end
        
        function progressFun(this, spotsPos, spotsDone, avrgTime, remTime, bCurve)
            this.spotsPos = spotsPos + repmat([size(this.ct, 2) size(this.ct, 1)]/2, size(spotsPos, 1), 1);
            this.spotsDone = spotsDone;
            this.avrgTime = avrgTime;
            this.remTime = remTime;
            this.bCurve = bCurve;
        end
        
        function simpleSimulation(this)
            handles = guidata(this.get('regguiHObject'));
            
            for i=1:length(handles.images.name)
                if(strcmp(handles.images.name{i}, this.CTName))
                    this.ct = spr2totalWET(handles.images.data{i}, [1 1 1], 0, 270);
                    break;
                end
            end
            
            simpleGateParSimu(this.get('user'), this.get('pass'), this.get('ip'), this.get('port'), ...
                this.CTName, this.detectorName, this.spotsNb, this.spotsSpacing, this.spotsOrigin, [1 1 1], this.energy, this.simuName, @this.progressFun, handles);
        end
    end
    
    methods (Access = private)
        
    end
end
