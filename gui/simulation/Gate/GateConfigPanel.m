
classdef GateConfigPanel < MainPanel
    properties (Access = private)       
        control;
        
    end
    
    methods (Access = public)
        function this = GateConfigPanel(f, control)
            this@MainPanel(f);
            this.control = control;  

            tabGroup = uitabgroup(this.mainPanel, 'Position', [0 0 1 1]);
            hostTab = uitab(tabGroup, 'Title', 'Host', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'host');
            guestTab = uitab(tabGroup, 'Title', 'Guest', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'guest');
            
            uicontrol('Parent', hostTab, 'Style', 'pushbutton', 'String', 'Output folder', 'Units', 'normalized', 'Position', [0.01, 0.8, 0.1, 0.05], 'Callback', @this.hostFolderCallback);
            uicontrol('Parent', hostTab, 'Style', 'edit', 'String', this.control.get('hostSimuFolder'), 'Units', 'normalized', 'Position', [0.15, 0.8, 0.1, 0.05]);
            uicontrol('Parent', hostTab, 'Style', 'pushbutton', 'String', 'Save', 'Units', 'normalized', 'Position', [0.81, 0.1, 0.1, 0.05], 'Callback', @this.saveHostCallback);
            
            uicontrol('Parent', guestTab, 'Style', 'text', 'String', 'Output folder', 'Units', 'normalized', 'Position', [0.01, 0.8, 0.1, 0.05]);
            uicontrol('Parent', guestTab, 'Style', 'edit', 'String', this.control.get('guestSimuFolder'), 'Units', 'normalized', 'Position', [0.15, 0.8, 0.1, 0.05]);
            uicontrol('Parent', guestTab, 'Style', 'pushbutton', 'String', 'Save', 'Units', 'normalized', 'Position', [0.81, 0.1, 0.1, 0.05], 'Callback', @this.saveGuestCallback);
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private)
        function hostFolderCallback(this, source, event)
            
        end
        
        function saveHostCallback(this, source, event)
            
        end
        
        function saveGuestCallback(this, source, event)
            
        end
        
        function panelCallback(this, source, event)
            
        end
    end
end
