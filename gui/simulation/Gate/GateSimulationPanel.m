
classdef GateSimulationPanel < MainPanel
    properties (Access = private)
        control ;

        userEdit ;
        passEdit ;
        ipEdit;
        portEdit ;
        
        goButton ;
    end
    
    methods (Access = public)
        function obj = GateSimulationPanel(f, control)
            obj@MainPanel(f) ;
            obj.control = control ;
            
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Username', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]);
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Password', 'Units', 'normalized', 'Position', [0.15, 0.55, 0.1, 0.3]);
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'IP', 'Units', 'normalized', 'Position', [0.30, 0.55, 0.1, 0.3]);
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Port', 'Units', 'normalized', 'Position', [0.45, 0.55, 0.1, 0.3]);
            
            obj.userEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', obj.control.get('user'), 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]);
            obj.passEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', obj.control.get('pass'), 'Units', 'normalized', 'Position', [0.15, 0.15, 0.1, 0.3]);
            obj.ipEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', obj.control.get('ip'), 'Units', 'normalized', 'Position', [0.30, 0.15, 0.1, 0.3]);
            obj.portEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'String', num2str(obj.control.get('port')), 'Units', 'normalized', 'Position', [0.45, 0.15, 0.1, 0.3]);
            
            obj.goButton = uicontrol('Parent', obj.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @obj.goCallback);
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
        end
    end
    
	methods (Access = private)
        function goCallback(this, source, event)
            this.control.set('user', get(this.userEdit, 'String')) ;
            this.control.set('pass', get(this.passEdit, 'String')) ;
            this.control.set('ip', get(this.ipEdit, 'String')) ;
            this.control.set('port', str2double(get(this.portEdit, 'String'))) ;
            
            this.control.startGate() ;
        end
    end
    
end
