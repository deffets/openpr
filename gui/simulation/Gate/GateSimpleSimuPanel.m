
classdef GateSimpleSimuPanel < MainPanel
    properties (Access = private)
        control;
        
        CTPopup;
        detectorPopup;
        
        xNb;
        yNb;
        xSpacing;
        ySpacing;
        xOrigin;
        yOrigin;
        
        energyEdit;
        nameEdit ;
        sigmaEdit ;
        
        goButton ;
        
        imAx;
        braggAx;
        spotsH;
        
        progressText;
    end
    
    methods (Access = public)
        function this = GateSimpleSimuPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'CT', 'Units', 'normalized', 'Position', [0.01, 0.85, 0.05, 0.025]);
            this.CTPopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'String', this.control.get('CTNames'), 'Units', 'normalized', 'Position', [0.1, 0.85, 0.1, 0.025]) ;
            
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Detector', 'Units', 'normalized', 'Position', [0.01, 0.8, 0.05, 0.025]);
            this.detectorPopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'String', this.control.get('detectorNames'), 'Units', 'normalized', 'Position', [0.1, 0.8, 0.1, 0.025]) ;
            
            spotsNb = this.control.get('spotsNb');
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Spots #', 'Units', 'normalized', 'Position', [0.01, 0.725, 0.05, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'X', 'Units', 'normalized', 'Position', [0.1, 0.75, 0.05, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Y', 'Units', 'normalized', 'Position', [0.2, 0.75, 0.05, 0.025]);
            this.xNb = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(spotsNb(1)), 'Units', 'normalized', 'Position', [0.1, 0.725, 0.05, 0.025]);
            this.yNb = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(spotsNb(2)), 'Units', 'normalized', 'Position', [0.2, 0.725, 0.05, 0.025]);
            
            spotsSpacing = this.control.get('spotsSpacing');
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Spots spacing', 'Units', 'normalized', 'Position', [0.01, 0.65, 0.05, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'X', 'Units', 'normalized', 'Position', [0.1, 0.675, 0.05, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Y', 'Units', 'normalized', 'Position', [0.2, 0.675, 0.05, 0.025]);
            this.xSpacing = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(spotsSpacing(1)), 'Units', 'normalized', 'Position', [0.1, 0.65, 0.05, 0.025]);
            this.ySpacing = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(spotsSpacing(2)), 'Units', 'normalized', 'Position', [0.2, 0.65, 0.05, 0.025]);
            
            spotsOrigin = this.control.get('spotsOrigin');
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Spots origin', 'Units', 'normalized', 'Position', [0.01, 0.575, 0.05, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'X', 'Units', 'normalized', 'Position', [0.1, 0.6, 0.05, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Y', 'Units', 'normalized', 'Position', [0.2, 0.6, 0.05, 0.025]);
            this.xOrigin = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(spotsOrigin(1)), 'Units', 'normalized', 'Position', [0.1, 0.575, 0.05, 0.025]);
            this.yOrigin = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(spotsOrigin(2)), 'Units', 'normalized', 'Position', [0.2, 0.575, 0.05, 0.025]);
            
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Energy', 'Units', 'normalized', 'Position', [0.01, 0.525, 0.05, 0.025]);
            this.energyEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(this.control.get('energy')), 'Units', 'normalized', 'Position', [0.1, 0.525, 0.05, 0.025]);
            
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.01, 0.4, 0.05, 0.025]);
            this.nameEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', this.control.get('simuName'), 'Units', 'normalized', 'Position', [0.1, 0.4, 0.1, 0.025]) ;
            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.25, 0.4, 0.05, 0.025], 'Callback', @this.goCallback) ;
            
            this.progressText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'Units', 'normalized', 'Position', [0.5, 0.35, 0.45, 0.025]);
            
            this.imAx = axes('Parent', this.mainPanel, 'Position', [0.5 0.5 0.45 0.45]);
            axis equal off
            this.braggAx = axes('Parent', this.mainPanel, 'Position', [0.5 0.1 0.45 0.2]);
            axis off
            
            this.spotsH = [];
            
            this.control.set('simuPanel', this);
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups();
            end
            
            setVisible@MainPanel(this, visible);
        end
        
        function update(this, property)
            switch property
                case 'popups'
                    this.updatePopups();
                case 'ct'
                    ct = this.control.get('ct');
                    subplot(this.imAx)
                    imagesc(ct)
                    colormap gray
                    axis equal off
                    pause(0.2)
                case 'progress'
                    if ~isempty(this.spotsH)
                        for i=1:length(this.spotsH)
                            delete(this.spotsH{i});
                        end
                    end
                    
                    spotsPos = this.control.get('spotsPos');
                    spotsDone = this.control.get('spotsDone');
                    
                    subplot(this.imAx)
                    hold on
                    
                    for i=1:length(spotsDone)
                        if ~spotsDone(i)
                            this.spotsH{i} = plot(spotsPos(i, 1), spotsPos(i, 2), 'ob', 'MarkerSize', 5, 'LineWidth', 2);
                        else
                            this.spotsH{i} = plot(spotsPos(i, 1), spotsPos(i, 2), 'og', 'MarkerSize', 5, 'LineWidth', 2);
                        end
                    end
                    
                    hold off
                    
                    bCurve = this.control.get('bCurve');

                    if ~isempty(bCurve)
                        subplot(this.braggAx)
                        plot(bCurve, 'LineWidth', 2)
                    end
                    
                    avrgTime = this.control.get('avrgTime');
                    remTime = this.control.get('remTime');
                    if ~isempty(avrgTime)
                        avrgTime = datetime(2017, 1, 1, 0, 0, avrgTime);
                        remTime = datetime(2017, 1, 1, 0, 0, remTime);

                        this.progressText.set('String', ['Spot: ' num2str(sum(spotsDone)) '/' num2str(length(spotsDone)) ...
                            '     Time/spot: ' num2str(day(avrgTime)-1) ' d ' num2str(hour(avrgTime)) ':' num2str(minute(avrgTime)) ':' num2str(second(avrgTime)) ...
                            '     Remaining time: ' num2str(day(remTime)-1) ' d ' num2str(hour(remTime)) ':' num2str(minute(remTime)) ':' num2str(second(remTime))]);
                    end
                    
                    pause(0.2)
            end
        end
        
        function updatePopups(this)
            names = this.control.get('CTNames') ;
            CTName = this.control.get('CTName') ;
            if ~isempty(CTName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, CTName)) ;
            else
                val = 1 ;
            end
            
            if isempty(val)
                val = 1 ;
            end
            
            set(this.CTPopup, 'String', names) ;
            set(this.CTPopup, 'Value', val) ;
            
            
            names = this.control.get('detectorNames') ;
            detectorName = this.control.get('detectorName') ;
            if ~isempty(detectorName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, detectorName)) ;
            else
                val = 1 ;
            end
            
            if isempty(val)
                val = 1 ;
            end
            
            set(this.detectorPopup, 'String', names) ;
            set(this.detectorPopup, 'Value', val) ;
        end
    end
    
    methods (Access = private)
        function goCallback(this, source, event)
            this.control.set('simuName', get(this.nameEdit, 'String')) ;
            
            items = get(this.CTPopup, 'String') ;
            index_selected = get(this.CTPopup, 'Value') ;
            CTName = items{index_selected} ;
            this.control.set('CTName', CTName) ;
            
            items = get(this.detectorPopup, 'String') ;
            index_selected = get(this.detectorPopup, 'Value') ;
            detectorName = items{index_selected} ;
            this.control.set('detectorName', detectorName) ;
            
            spotsNb = [str2double(this.xNb.get('String')) str2double(this.yNb.get('String'))];
            this.control.set('spotsNb', spotsNb);
            
            spotsSpacing = [str2double(this.xSpacing.get('String')) str2double(this.ySpacing.get('String'))];
            this.control.set('spotsSpacing', spotsSpacing);
            
            spotsOrigin = [str2double(this.xOrigin.get('String')) str2double(this.yOrigin.get('String'))];
            this.control.set('spotsOrigin', spotsOrigin);
        
            energy = str2double(this.energyEdit.get('String'));
            this.control.set('energy', energy);

            this.control.simpleSimulation() ;
        end
    end
end