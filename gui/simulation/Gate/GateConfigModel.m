
classdef GateConfigModel < GenericModel
    properties (SetObservable)
    end
    
    properties (Access = private)
        hostSimuFolder;
        guestSimuFolder;
    end
    
    methods (Access = public)
        function this = GateConfigModel(regguiHthisect)
            this@GenericModel(regguiHthisect) ;
            
            this.hostSimuFolder = [] ;
            this.guestSimuFolder = [] ;
        end
        
        function set(this, property, value)
            switch property
                case 'hostSimuFolder'
                    this.hostSimuFolder = value ;
                case 'guestSimuFolder'
                    this.guestSimuFolder = value ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'hostSimuFolder'
                    value = this.hostSimuFolder ;
                case 'guestSimuFolder'
                    value = this.guestSimuFolder ;
            end
        end
    end
    
    methods (Access = private)

    end
end
