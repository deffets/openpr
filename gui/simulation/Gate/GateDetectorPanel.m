
classdef GateDetectorPanel < MainPanel
    properties (Access = private)
        control;

        xDimension;
        yDimension;
        zDimension;
        
        xPosition;
        yPosition;
        zPosition;
        
        dimTab;
        
        nameEdit;
        
        repeatNb;
        
        updateButton;
        saveButton;
        
        ax1;
        
        detectorLines;
        detectorLayers;
    end
    
    methods (Access = public)
        function this = GateDetectorPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            dim = this.control.get('detectorDimensions');
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Detector dimensions (mm)', 'Units', 'normalized', 'Position', [0.01, 0.875, 0.1, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'X', 'Units', 'normalized', 'Position', [0.15, 0.9, 0.05, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Y', 'Units', 'normalized', 'Position', [0.25, 0.9, 0.05, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Z', 'Units', 'normalized', 'Position', [0.35, 0.9, 0.05, 0.025]);
            this.xDimension = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(dim(1)), 'Units', 'normalized', 'Position', [0.15, 0.875, 0.05, 0.025]);
            this.yDimension = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(dim(2)), 'Units', 'normalized', 'Position', [0.25, 0.875, 0.05, 0.025]);
            this.zDimension = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(dim(3)), 'Units', 'normalized', 'Position', [0.35, 0.875, 0.05, 0.025]);
            
            pos = this.control.get('detectorPosition');
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Detector position (mm)', 'Units', 'normalized', 'Position', [0.01, 0.8, 0.1, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'X', 'Units', 'normalized', 'Position', [0.15, 0.825, 0.05, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Y', 'Units', 'normalized', 'Position', [0.25, 0.825, 0.05, 0.025]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Z', 'Units', 'normalized', 'Position', [0.35, 0.825, 0.05, 0.025]);
            this.xPosition = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(pos(1)), 'Units', 'normalized', 'Position', [0.15, 0.8, 0.05, 0.025]);
            this.yPosition = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(pos(2)), 'Units', 'normalized', 'Position', [0.25, 0.8, 0.05, 0.025]);
            this.zPosition = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(pos(3)), 'Units', 'normalized', 'Position', [0.35, 0.8, 0.05, 0.025]);
            
            cNames = {'Show', 'Material', 'Width (mm)', 'Actor', 'Remove', 'Add'};
            shows = this.control.get('shows');
            materials = this.control.get('materials');
            widths = this.control.get('lengths');
            actors = this.control.get('actors');
            if isempty(materials)
                materials = {'None'};
                widths = {0};
                actors = {false};
                shows = {false};
            else
                if ~iscell(materials)
                    materials = {materials};
                end
                
                widths = num2cell(widths);
                actors = num2cell(actors);
                shows = num2cell(shows);
            end
            
            addCell = num2cell(logical(zeros(length(materials), 1)));
            removeCell = num2cell(logical(zeros(length(materials), 1)));

            tableData = {shows{:} ; materials{:} ; widths{:} ; actors{:} ; removeCell{:} ; addCell{:}}';
                
            rNames = num2cell(1:length(actors))';
            this.dimTab = uitable('Parent', this.mainPanel, 'Data', tableData, 'ColumnName', ...
                cNames, 'RowName', rNames, 'Units', 'Normalized', 'Position', [0.01, 0.3, 0.4, 0.475], 'ColumnEditable', [true true true true true true], 'CellEditCallback', @this.tableCallback);
%             this.dimTab.Position(3) = this.dimTab.Extent(3)+0.01;

            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Repeat', 'Units', 'normalized', 'Position', [0.01, 0.25, 0.08, 0.025]);
            this.repeatNb = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(this.control.get('repeatNb')), 'Units', 'normalized', 'Position', [0.1, 0.25, 0.05, 0.025]);
            
            this.updateButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Update', 'Units', 'normalized', 'Position', [0.6875, 0.9, 0.1, 0.025], 'Callback', @this.updateCallback);
            
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.01, 0.1, 0.05, 0.025]);
            this.nameEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.1, 0.1, 0.1, 0.025]);
            this.saveButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Save', 'Units', 'normalized', 'Position', [0.25, 0.1, 0.1, 0.025], 'Callback', @this.goCallback);
            
            this.ax1 = axes('Parent', this.mainPanel, 'Position', [0.5 0.3 0.475 0.475]);
            [x, y, z] = sphere;
            surf(x*10, y*10, z*10, zeros(size(z)))
            text(0-10, 0-10, 0-10, 'Isocenter')
            
            hold on
            p1 = [-100 0 0];
            p2 = [-10 0 0];
            dp = p2 - p1;
            quiver3(p1(1), p1(2), p1(3), dp(1), dp(2), dp(3), 'color', 'black', 'LineWidth', 2)
            text(p1(1)-10, p1(2)-10, p1(3)-10, 'Beam')
            hold off
            
            xlabel('X')
            ylabel('Y')
            zlabel('Z')
            axis equal
            
            this.detectorLines = [];
            this.detectorLayers = [];
            
            this.update('detector');
            
            this.control.set('detectorPanel', this);
        end
        
        function update(this, property)
            switch property
                case 'detector'
                    if ~isempty(this.detectorLines)
                        for i=1 : length(this.detectorLines)
                            delete(this.detectorLines{i});
                        end
                        
                        this.detectorLines = [];
                    end
                    
                    % Replace with edit.get('String');
                    dim = this.control.get('detectorDimensions');
                    pos = this.control.get('detectorPosition');
                    
                    subplot(this.ax1);
                    
                    hold on
                    
                    this.detectorLines{1} = line([pos(1) pos(1)]-dim(1)/2, [pos(2) pos(2)]-dim(2)/2, [pos(3) pos(3)+dim(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    this.detectorLines{2} = line([pos(1) pos(1)]-dim(1)/2, [pos(2) pos(2)+dim(2)]-dim(2)/2, [pos(3) pos(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    this.detectorLines{3} = line([pos(1) pos(1)]-dim(1)/2, [pos(2) pos(2)+dim(2)]-dim(2)/2, [pos(3)+dim(3) pos(3)+dim(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    this.detectorLines{4} = line([pos(1) pos(1)]-dim(1)/2, [pos(2)+dim(2) pos(2)+dim(2)]-dim(2)/2, [pos(3) pos(3)+dim(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2); 
                    
                    this.detectorLines{5} = line([pos(1) pos(1)+dim(1)]-dim(1)/2, [pos(2) pos(2)]-dim(2)/2, [pos(3) pos(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    this.detectorLines{6} = line([pos(1) pos(1)+dim(1)]-dim(1)/2, [pos(2) pos(2)]-dim(2)/2, [pos(3)+dim(3) pos(3)+dim(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    this.detectorLines{7} = line([pos(1) pos(1)+dim(1)]-dim(1)/2, [pos(2)+dim(2) pos(2)+dim(2)]-dim(2)/2, [pos(3) pos(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    this.detectorLines{8} = line([pos(1) pos(1)+dim(1)]-dim(1)/2, [pos(2)+dim(2) pos(2)+dim(2)]-dim(2)/2, [pos(3)+dim(3) pos(3)+dim(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    
                    this.detectorLines{9} = line([pos(1)+dim(1) pos(1)+dim(1)]-dim(1)/2, [pos(2) pos(2)]-dim(2)/2, [pos(3) pos(3)+dim(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    this.detectorLines{10} = line([pos(1)+dim(1) pos(1)+dim(1)]-dim(1)/2, [pos(2) pos(2)+dim(2)]-dim(2)/2, [pos(3) pos(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    this.detectorLines{11} = line([pos(1)+dim(1) pos(1)+dim(1)]-dim(1)/2, [pos(2) pos(2)+dim(2)]-dim(2)/2, [pos(3)+dim(3) pos(3)+dim(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    this.detectorLines{12} = line([pos(1)+dim(1) pos(1)+dim(1)]-dim(1)/2, [pos(2)+dim(2) pos(2)+dim(2)]-dim(2)/2, [pos(3) pos(3)+dim(3)]-dim(3)/2, 'LineStyle', '--', 'color', 'blue', 'LineWidth', 2);
                    
                    if ~isempty(this.detectorLayers)
                        for i=1 : length(this.detectorLayers)
                            delete(this.detectorLayers{i});
                        end
                        
                        this.detectorLayers = [];
                    end
                    
                    shows = this.control.get('shows');
                    materials = this.control.get('materials');
                    widths = this.control.get('lengths');
                    
                    repeatVector = sum(widths);

                    if ~iscell(materials)
                        materials = {materials};
                    end
                    
                    rN = this.control.get('repeatNb');
                    
                    detectorLayersInd = 1;
                    for i=1 : length(shows)
                        col = rand(3, 1);
                        if shows(i)
                            for j=1 : rN
                                if i>1
                                    trans = sum(widths(1:i-1)) + (j-1)*repeatVector;
                                else
                                    trans = (j-1)*repeatVector;
                                end
                                
                                x = [trans ; trans ;
                                    trans ; trans ;
                                    trans ; trans ;
                                    trans ; trans ;
                                    trans ; trans+widths(i) ;
                                    trans ; trans+widths(i) ;
                                    trans ; trans+widths(i) ;
                                    trans ; trans+widths(i) ;
                                    trans+widths(i) ; trans+widths(i) ;
                                    trans+widths(i) ; trans+widths(i) ;
                                    trans+widths(i) ; trans+widths(i) ;
                                    trans+widths(i) ; trans+widths(i)] + pos(1) - dim(1)/2;
                                
                                y = [pos(2) ; pos(2) ;
                                    pos(2) ; pos(2)+dim(2) ;
                                    pos(2) ; pos(2)+dim(2) ;
                                    pos(2)+dim(2) ; pos(2)+dim(2) ;
                                    pos(2) ; pos(2) ;
                                    pos(2) ; pos(2) ;
                                    pos(2)+dim(2) ; pos(2)+dim(2) ;
                                    pos(2)+dim(2) ; pos(2)+dim(2) ;
                                    pos(2) ; pos(2) ;
                                    pos(2) ; pos(2)+dim(2) ;
                                    pos(2) ; pos(2)+dim(2) ;
                                    pos(2)+dim(2) ; pos(2)+dim(2)] - dim(2)/2;
                                
                                z = zeros(length(x), length(y));
                                
                                z2 = [pos(3) ; pos(3)+dim(3) ;
                                    pos(3) ; pos(3) ;
                                    pos(3)+dim(3) ; pos(3)+dim(3) ;
                                    pos(3) ; pos(3)+dim(3) ;
                                    pos(3) ; pos(3) ;
                                    pos(3)+dim(3) ; pos(3)+dim(3) ;
                                    pos(3) ; pos(3) ;
                                    pos(3)+dim(3) ; pos(3)+dim(3) ;
                                    pos(3) ; pos(3)+dim(3) ;
                                    pos(3) ; pos(3) ;
                                    pos(3)+dim(3) ; pos(3)+dim(3) ;
                                    pos(3) ; pos(3)+dim(3)] - dim(3)/2;
                                
                                z(logical(eye(size(z, 1)))) = z2;
                                z(logical(triu(ones(size(z, 1)), +1))) = max(z2);
                                z(logical(tril(ones(size(z, 1)), +1))) = min(z2);

                                this.detectorLayers{detectorLayersInd} = surf(x, y, z, 'EdgeColor', 'black', 'FaceColor', col);
                                detectorLayersInd = detectorLayersInd + 1;
                            end
                        end
                    end
                    
                    hold off
            end
        end
    end
    
    methods (Access = private)
        function tableCallback(this, source, event)
            r = event.Indices(1);
            c = event.Indices(2);
            if c<5
                return
            end
            
            tableData = this.dimTab.get('Data');
            shows = {tableData{1:end, 1}};
            materials = {tableData{1:end, 2}};
            widths = {tableData{1:end, 3}};
            actors = {tableData{1:end, 4}};
            
            if c==5 && length(widths)>1
                tableData(r, :) = [];
            end
            
            if c==6
                shows = {shows{1:r} false shows{r+1:end}};
                materials = {materials{1:r} 'None' materials{r+1:end}};
                widths = {widths{1:r} 0  widths{r+1:end}};
                actors = {actors{1:r} false actors{r+1:end}};
                    
                addCell = num2cell(logical(zeros(length(materials), 1)));
                removeCell = num2cell(logical(zeros(length(materials), 1)));
                
                tableData = {shows{:} ; materials{:} ; widths{:} ; actors{:} ; removeCell{:} ; addCell{:}}';
            end

            rNames = num2cell(1:size(tableData, 1))';
            this.dimTab.set('RowName', rNames, 'Data', tableData, 'ColumnEditable', [true true true true true true], 'CellEditCallback', @this.tableCallback);
        end
        
        function updateCallback(this, source, event)
            dim(1) = str2double(this.xDimension.get('String'));
            dim(2) = str2double(this.yDimension.get('String'));
            dim(3) = str2double(this.zDimension.get('String'));
            this.control.set('detectorDimensions', dim);
            
            pos(1) = str2double(this.xPosition.get('String'));
            pos(2) = str2double(this.yPosition.get('String'));
            pos(3) = str2double(this.zPosition.get('String'));
            this.control.set('detectorPosition', pos);
            
            rN = str2double(this.repeatNb.get('String'));
            this.control.set('repeatNb', rN);
            
            tableData = this.dimTab.get('Data');
            shows = {tableData{1:end, 1}};
            materials = {tableData{1:end, 2}};
            widths = {tableData{1:end, 3}};
            actors = {tableData{1:end, 4}};
            
            widths = cell2mat(widths);
            actors = cell2mat(actors);
            shows = cell2mat(shows);
            
            this.control.set('materials', materials);
            this.control.set('lengths', widths);
            this.control.set('actors', actors);
            this.control.set('shows', shows);
            
            this.update('detector');
        end
    
        function goCallback(this, source, event)
            this.updateCallback([], []);
            
            this.control.set('name', this.nameEdit.get('String'));

            this.control.save() ;
        end
    end
end