
classdef GateSimulationControl < GenericControl
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function obj = GateSimulationControl(model)
            obj@GenericControl(model) ;
        end
        
        function set(this, property, value)
            this.model.set(property, value) ;
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
        
        function startGate(this)
            this.model.startGate() ;
        end
    end
end
