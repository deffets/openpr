
classdef GateMainSimulationPanel < handle   
    properties (Access = private)
        control ;
        
        f ;
        mainPanel;

        detectorTab;
        simpleSimuTab;
        configTab ;
                
        detectorPanel;
        simpleSimuPanel;
        configPanel;
    end
    
    methods (Access = public)
        function this = GateMainSimulationPanel(control)
            this.control = control ;
            
            this.f = figure('Name', 'Gate Interface', 'MenuBar', 'None', 'NumberTitle', 'off', 'units', 'normalized', 'outerposition', [0 0 1 1]);
            
            tabGroup = uitabgroup(this.f, 'Position', [0 0 1 1]);
            this.detectorTab = uitab(tabGroup, 'Title', 'Detector', 'ButtonDownFcn', @this.detectorCallback);
            this.simpleSimuTab = uitab(tabGroup, 'Title', 'Simulation', 'ButtonDownFcn', @this.simpleSimuCallback);
            this.configTab = uitab(tabGroup, 'Title', 'Configuration', 'ButtonDownFcn', @this.configCallback);
            
            this.detectorPanel = GateDetectorPanel(this.detectorTab, this.control.get('detectorControl'));
            this.detectorPanel.setVisible('on');
            
            this.simpleSimuPanel = GateSimpleSimuPanel(this.simpleSimuTab, this.control.get('simpleSimuControl'));
            this.simpleSimuPanel.setVisible('on');
            
            this.configPanel = GateConfigPanel(this.configTab, this.control.get('configControl'));
            this.configPanel.setVisible('on');
        end
        
        function clearDisplay(this)
            % clearDisplay Set visibility of all panels to 'off'
            
            this.detectorPanel.setVisible('off');
            this.simpleSimuPanel.setVisible('off');
            this.configPanel.setVisible('off');
        end
        
        function unclearDisplay(this)
            % unclearDisplay Set visibility of all panels to 'on'
            
            this.detectorPanel.setVisible('on');
            this.simpleSimuPanel.setVisible('on');
            this.configPanel.setVisible('on');
        end
    end
    
    methods (Access = private)
        function detectorCallback(this, source, event)
            this.clearDisplay();
            this.detectorPanel.setVisible('on');
        end
        
        function simpleSimuCallback(this, source, event)
            this.clearDisplay();
            this.simpleSimuPanel.setVisible('on');
        end
        
        function configCallback(this, source, event)
            this.clearDisplay();
            this.configPanel.setVisible('on');
        end
    end
end
