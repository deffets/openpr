
classdef GateMainSimulationControl < handle
    properties (Access = private)
        view;
        model;

        detectorControl;
        simpleSimuControl;
        configControl;
        
        progressControl;
    end
    
    methods (Access = public)
        function this = GateMainSimulationControl(model)
            this.model = model;

            detectorModel = GateDetectorModel(this.model.get('regguiHObject'));
            this.detectorControl = GateDetectorControl(detectorModel);
            
            simpleSimuModel = GateSimpleSimuModel(this.model.get('regguiHObject'), this.model.get('user'), this.model.get('pass'), this.model.get('ip'), this.model.get('port'));
            this.simpleSimuControl = GateSimpleSimuControl(simpleSimuModel);
            
            configModel = GateConfigModel(this.model.get('regguiHObject'));
            this.configControl = GateConfigControl(configModel);
        end
        
        function value = get(this, varargin)
            % get Get the value of a specific property
            %   get(property) Get the value of a specific property. See
            %   PRadioModel for all properties.
            %   See also PRadioModel
            
            property = varargin{1};

            switch property
                case 'configControl'
                    value = this.configControl;
                case 'simpleSimuControl'
                    value = this.simpleSimuControl;
                case 'detectorControl'
                    value = this.detectorControl;
                otherwise
                    value = this.model.get(varargin{:});
            end
        end
    end
end
