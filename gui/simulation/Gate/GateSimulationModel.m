
classdef GateSimulationModel < GenericModel
    properties (SetObservable)
        user ;
        pass ;
        ip ;
        port ;
    end
    
    methods (Access = public)
        function this = GateSimulationModel(regguiHObject)
            this@GenericModel(regguiHObject) ;
            
            this.user = 'gate' ;
            this.pass = 'virtual' ;
            this.ip = '192.168.56.101' ;
            this.port = 22 ;
        end
        
        function set(this, property, value)
            switch property
                case 'user'
                    this.user = value ;
                case 'pass'
                    this.pass = value ;
                case 'ip'
                    this.ip = value ;
                case 'port'
                    this.port = value ;
                otherwise
                    set@GenericModel(this, property, value);
            end
        end
        
        function value = get(this, property)
            switch property
                case 'user'
                    value = this.user ;
                case 'pass'
                    value = this.pass ;
                case 'ip'
                    value = this.ip ;
                case 'port'
                    value = this.port ;
                otherwise
                    value = get@GenericModel(this, property);
            end
        end

        function startGate(this)
            GateMainSimulationPanel(GateMainSimulationControl(GateMainSimulationModel(this.regguiHObject, this.user, this.pass, this.ip, this.port)));
        end
    end
    
    methods (Access = private)
        
    end
end
