
classdef GateConfigControl < GenericControl
    properties(Access = private)
    end

    methods (Access = public)
        function this = GateConfigControl(model)
            this@GenericControl(model) ;
        end
        
        function set(this, varargin)
            switch varargin{1}
                otherwise
                    this.model.set(varargin{:}) ;
            end
        end
        
        function value = get(this, varargin)
            value = this.model.get(varargin{:}) ;
        end
    end
    
    methods (Access = private)
    end
end
