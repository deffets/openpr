
classdef GateDetectorControl < GenericControl
    properties (Access = private)
        detectorPanel;
    end
    
    methods (Access = public)
        function this = GateDetectorControl(model)
            this@GenericControl(model) ;
            
%             addlistener(this.model, 'shows', 'PostSet', @this.showCallback);
        end
        
        function set(this, property, value)
            switch property
                case 'detectorPanel'
                    this.detectorPanel = value;
                otherwise
                    this.model.set(property, value);
            end
        end
        
        function value = get(this, property)
            value = this.model.get(property);
        end
        
        function save(this)
            this.model.save();
        end
    end
    
    methods (Access = private)
        function showCallback(this, source, event)
            this.detectorPanel('detector');
        end
    end
end
