
classdef GateSimpleSimuControl < GenericControl
    properties (Access = private)
        simuPanel;
    end
    
    methods (Access = public)
        function this = GateSimpleSimuControl(model)
            this@GenericControl(model);
            
            addlistener(this.model, 'bCurve', 'PostSet', @this.updateProgress);
            addlistener(this.model, 'ct', 'PostSet', @this.updateCT);
        end
        
        function set(this, property, value)
            switch property
                case 'simuPanel'
                    this.simuPanel = value;
                otherwise
                    this.model.set(property, value);
            end
        end
        
        function value = get(this, property)
            value = this.model.get(property);
        end
        
        function simpleSimulation(this)
            this.model.simpleSimulation();
        end
    end
    
    methods (Access = private)
        function updateProgress(this, source, event)
            this.simuPanel.update('progress');
        end
        
        function updateCT(this, source, event)
            this.simuPanel.update('ct');
        end
    end
end
