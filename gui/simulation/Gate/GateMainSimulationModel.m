
classdef GateMainSimulationModel < GateSimulationModel   
    properties (Access = private)
    end
    
    methods (Access = public)
        function this = GateMainSimulationModel(regguiHObject, user, pass, ip, port)
            this@GateSimulationModel(regguiHObject);
            
            this.set('user', user);
            this.set('pass', pass);
            this.set('ip', ip);
            this.set('port', port);
        end
        
        function set(this, varargin)
            property = varargin{1};
            switch property
                otherwise
                    set@GateSimulationModel(this, varargin{:});
            end
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            switch property
                otherwise
                    value = get@GateSimulationModel(this, varargin{:});
            end
        end
    end
    
    methods (Access = private)
    end
end
