
classdef GateDetectorModel < GenericModel
    properties (SetObservable)
        shows;
    end
    
    properties (SetObservable)
        detectorDimensions;
        detectorPosition;
        materials;
        lengths;
        actors;
        repeatNb;
        name;
    end
    
    methods (Access = public)
        function this = GateDetectorModel(regguiHObject)
            this@GenericModel(regguiHObject);
            
            this.detectorDimensions = [1000 300 200];
            this.detectorPosition = [1000 0 0];
            this.materials = {};
            this.lengths = [];
            this.actors = [];
            this.shows = [];
            this.repeatNb = 1;
        end
        
        function set(this, varargin)
            property = varargin{1};
            value = varargin{2};
            
            switch property
                case 'detectorDimensions'
                    this.detectorDimensions = value;
                case 'detectorPosition'
                    this.detectorPosition = value;
                case 'materials'
                    this.materials = value;
                case 'lengths'
                    this.lengths = value ;
                case 'actors'
                    this.actors = value;
                case 'shows'
                    this.shows = value;
                case 'repeatNb'
                    this.repeatNb = value;
                case 'name'
                    this.name = value;
                otherwise
                    set@GenericModel(this, varargin{:});
            end
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'detectorDimensions'
                    value = this.detectorDimensions;
                case 'detectorPosition'
                    value = this.detectorPosition;
                case 'materials'
                    value = this.materials;
                case 'lengths'
                    value = this.lengths;
                case 'actors'
                    value = this.actors;
                case 'shows'
                    value = this.shows;
                case 'repeatNb'
                    value = this.repeatNb;
                case 'name'
                    value = this.name;
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end
        
        function save(this)
            handles = guidata(this.regguiHObject) ;
            
            handles = saveDetector(this.detectorDimensions, this.detectorPosition, this.materials, this.lengths, this.actors, this.repeatNb, this.name, handles);
            
            guidata(this.regguiHObject, handles) ;
        end
    end
end
