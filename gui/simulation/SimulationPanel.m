
classdef SimulationPanel < MainPanel
    properties (Access = private)
        simpleSimuPanel ;
        simpleSimuWETPanel ;
        mc2PRSimuPanel;
        mc2PRSimuMatchingPRPanel;
        gateSimuPanel;
        wetSimuPanel;
        
        control ;
    end
    
    methods (Access = public)
        function this = SimulationPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            p = uipanel('Parent', this.mainPanel, 'Title', 'Simple simulation - MLIC', 'Position', [0 0.85 1 0.12]) ;
            this.simpleSimuPanel = SimpleSimulationPanel(p, this.control.getSimpleSimulationControl()) ;
            
            p1 = uipanel('Parent', this.mainPanel, 'Title', 'Simple simulation From WET-map - MLIC', 'Position', [0 0.7 1 0.12]) ;
            this.simpleSimuWETPanel = SimpleSimulationWETPanel(p1, this.control.getSimpleSimulationWETControl()) ;
            
            p2 = uipanel('Parent', this.mainPanel, 'Title', 'Monte Carlo - MC2', 'Position', [0 0.55 1 0.12]) ;
            this.mc2PRSimuPanel = MC2PRSimulationPanel(p2, this.control.getMC2PRSimulationControl()) ;
            
            p3 = uipanel('Parent', this.mainPanel, 'Title', 'Monte Carlo - MC2', 'Position', [0 0.4 1 0.12]) ;
            this.mc2PRSimuMatchingPRPanel = MC2PRSimulationMatchingPRPanel(p3, this.control.getMC2PRSimulationMatchingPRControl()) ;
            
            p4 = uipanel('Parent', this.mainPanel, 'Title', 'Monte Carlo - Gate', 'Position', [0 0.25 1 0.12]) ;
            this.gateSimuPanel = GateSimulationPanel(p4, this.control.getGateSimulationControl()) ;
            
            p5 = uipanel('Parent', this.mainPanel, 'Title', 'WET', 'Position', [0 0.1 1 0.12]) ;
            this.wetSimuPanel = WETSimulationPanel(p5, this.control.getWETSimulationControl()) ;
        end
        
        function setVisible(this, visible)
            this.simpleSimuPanel.setVisible(visible) ;
            this.simpleSimuWETPanel.setVisible(visible) ;
            this.mc2PRSimuPanel.setVisible(visible);
            this.mc2PRSimuMatchingPRPanel.setVisible(visible);
            this.gateSimuPanel.setVisible(visible);
            this.wetSimuPanel.setVisible(visible);
            setVisible@MainPanel(this, visible)
        end
    end
    
end
