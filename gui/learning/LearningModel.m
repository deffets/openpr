
classdef LearningModel < handle   
    properties (Access = private)
        regguiHObject ;
        progressControl ;
    end
    
    methods (Access = public)
        function this = LearningModel(regguiHObject)
            this.regguiHObject = regguiHObject ;
            this.progressControl = [] ;
            
            this.startProgressControl() ;
        end
        
        function set(this, varargin)
            property = varargin{1};
            
            switch property
                case 'progressControl'
                    this.progressControl = varargin{2} ;
            end
        end
        
        function value = get(this, varargin)
            property = varargin{1};

            switch property
                case 'progressControl'
                    value = this.progressControl ;
            end
        end
    end
    
    methods (Access = private)
        function startProgressControl(this)
            
            handles = guidata(this.regguiHObject) ;

            if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                this.set('progressControl', handles.progressControl) ;
            end
            
        end
    end
end
