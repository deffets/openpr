
classdef LearningControl < handle
    properties (Access = private)
        regguiHObject ;
        view ;
        model ;

        learningMLICDataModel ;
        learningMLICDataControl ;
        
        progressControl;
    end
    
    methods (Access = public)
        function this = LearningControl(regguiHObject)
            this.regguiHObject = regguiHObject ;
          
            this.model = LearningModel(this.regguiHObject) ;

            this.learningMLICDataModel = LearningMLICDataModel(this.regguiHObject) ;
            this.learningMLICDataControl = LearningMLICDataControl(this.learningMLICDataModel) ;
        end
        
        function value = get(this, varargin)
            % get Get the value of a specific property
            %   get(property) Get the value of a specific property. See
            %   PRadioModel for all properties.
            %   See also PRadioModel
            
            property = varargin{1};

            switch property
                case 'learningMLICDataControl'
                    value = this.learningMLICDataControl;
                otherwise
                    value = this.model.get(varargin{:}) ;
            end
        end
        
        function set(this, varargin)
            switch varargin{1}
                case 'n'
                    this.progressControl.set(n, varargin{2}) ;
            end            
        end
        
        function deleteProgress(this)
            this.progressControl.deleteProgress() ;
        end
    end
end
