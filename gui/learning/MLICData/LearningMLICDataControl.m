
classdef LearningMLICDataControl < GenericControl
    properties (Access = private)
        progressControl ;
        view;
    end
    
    methods (Access = public)
        function this = LearningMLICDataControl(model)
            this@GenericControl(model) ;
            
            this.progressControl = [] ;
            this.view = [];
            
            addlistener(this.model, 'CTProjection', 'PostSet', @this.CTProjectionCallback) ;
            addlistener(this.model, 'worldLimit2', 'PostSet', @this.worldLimitCallback) ;
        end
        
        function set(this, varargin)
            switch varargin{1}
                case 'progressControl'
                    this.progressControl = varargin{2} ;
                    this.model.set('progressControl', this.progressControl) ;
                case 'view'
                    this.view = varargin{2} ;
                otherwise
                    this.model.set(varargin{:}) ;
            end
        end
        
        function value = get(this, varargin)
            switch varargin{1}
                case 'progressControl'
                    value = this.progressControl ;
                otherwise
                    value = this.model.get(varargin{:}) ;
            end
        end
        
        function simpleSimulation(this)
            this.model.simpleSimulation() ;
        end
        
        function updateIm(this)
            this.model.updateIm();
        end
    end
    
    methods (Access = private)
        function CTProjectionCallback(this, source, event)
            if ~isempty(this.view)
                this.view.update('CTProjection');
            end
        end
        
        function worldLimitCallback(this, source, event)
            if ~isempty(this.view)
                this.view.update('lines');
            end
        end
    end
end
