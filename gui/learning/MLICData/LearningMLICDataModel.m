
classdef LearningMLICDataModel < GenericModel
    properties (SetObservable)
        CTName;
        angle;
        name;
        sigma;
        spacing;
        spotsNb;
        energy;
        curveName;
        folder;
        
        sigmaNoise;
        worldLimit1;
        
        progressControl ;
    end
    
    properties (SetObservable)
        worldLimit2;
        CTProjection;
    end
    
    methods (Access = public)
        function this = LearningMLICDataModel(regguiHObject)
            this@GenericModel(regguiHObject);
            
            this.CTName = [];
            this.angle = 0;
            this.name = [];
            this.sigma = 3;
            this.angle = 270;
            this.spacing = 5;
            this.spotsNb = 9;
            this.energy = 210;
            this.curveName = [];
            this.sigmaNoise = 0;
            
            this.worldLimit1 = [];
            this.worldLimit2 = [];
            
            this.CTProjection = [];
        end
        
        function set(this, varargin)
            property = varargin{1};
            value = varargin{2};
            
            switch property
                case 'angle'
                    this.angle = value ;
                case 'name'
                    this.name = value ;
                case 'CTName'
                    this.CTName = value ;
                case 'sigma'
                    this.sigma = value ;
                case 'progressControl' ;
                    this.progressControl = value ;
                case 'spotsNb'
                    this.spotsNb = value;
                case 'energy'
                    this.energy = value;
                case 'curveName'
                    this.curveName = value;
                case 'folder'
                    this.folder = value;
                case 'spacing'
                    this.spacing = value;
                case 'worldLimit1'
                    this.worldLimit1 = value;
                case 'worldLimit2'
                    this.worldLimit2 = value;
                    this.checkWorldLimits();
                case 'sigmaNoise'
                    this.sigmaNoise = value;
                otherwise
                    set@GenericModel(this, varargin{:});
            end
        end
        
        function value = get(this, varargin)
            switch varargin{1}
                case 'angle'
                    value = this.angle ;
                case 'name'
                    value = this.name ;
                case 'CTName'
                    value = this.CTName ;
                case 'sigma'
                    value = this.sigma ;
                case 'spacing'
                    value = this.spacing;
                case 'spotsNb'
                    value = this.spotsNb;
                case 'energy'
                    value = this.energy;
                case 'curveName'
                    value = this.curveName;
                case 'folder'
                    value = this.folder;
                case 'CTProjection'
                    value = this.CTProjection;
                case 'worldLimit1'
                    value = this.worldLimit1;
                case 'worldLimit2'
                    value = this.worldLimit2;
                case 'sigmaNoise'
                    value = this.sigmaNoise;
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end

        function simpleSimulation(this)
            handles = guidata(this.regguiHObject) ;

            instruction = ['handles = generateMLICMeasurements(' '''' this.CTName '''' ', ' ...
                '''' this.curveName '''' ...
                ', [' num2str(this.worldLimit1(1)) ', ' num2str(this.worldLimit1(2)) '], [' num2str(this.worldLimit2(1)) ', ' num2str(this.worldLimit2(2)) '], ' ...
                '[' num2str(this.spotsNb) ', ' num2str(this.spotsNb) '], ' ...
                '[' num2str(this.spacing) ', ' num2str(this.spacing) '], ' num2str(this.angle) ', ' num2str(this.sigma) ', ' ...
                num2str(this.sigmaNoise) ', ' '''' this.folder '''' ', handles) ;'] ;
            
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
        
        function updateIm(this)
            handles = guidata(this.regguiHObject) ;
            
            hu = [];
            for i=1:length(handles.images.name)
                if(strcmp(handles.images.name{i}, this.CTName))
                    hu = handles.images.data{i};
                    huInfo = handles.images.info{i};
                end
            end
            if(isempty(hu))
                error('Input image not found in the current list')
            end
            
            [this.CTProjection, ~] = spr2totalWET(hu, huInfo.Spacing, 0, this.angle);
        end
    end
    
    methods (Access = private)
        function checkWorldLimits(this)
            this.worldLimit1 = round(this.worldLimit1);
            this.worldLimit2 = this.worldLimit1 + ((this.spotsNb-1)*this.spacing)*ceil((this.worldLimit2 - this.worldLimit1)/(this.spotsNb*this.spacing)) + this.spacing;
        end
    end
end
