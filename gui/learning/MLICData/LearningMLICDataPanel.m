
classdef LearningMLICDataPanel < MainPanel
    properties (Access = private)
        control ;
        
        CTPopup ;
        curvePopup ;
        
        angleEdit ;
        nameEdit ;
        sigmaEdit ;
        spotsNbEdit ;
        spacingEdit ;
        energyEdit ;
        folderEdit;
        noiseEdit;
        
        goButton ;
        
        imPanel;
        mainPlot;
        imh;
        linesh;
    end
    
    methods (Access = public)
        function this = LearningMLICDataPanel(f, control)
            this@MainPanel(f) ;
            
            this.control = control;
            
            parametersPanel = uipanel('Parent', this.mainPanel, 'Title', 'Parameters', 'Position', [0 0.825 1 0.15]) ;
            
            uicontrol('Parent', parametersPanel, 'Style', 'text', 'String', 'CT', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]) ;
            uicontrol('Parent', parametersPanel, 'Style', 'text', 'String', 'Calibration curve', 'Units', 'normalized', 'Position', [0.15, 0.55, 0.1, 0.3]) ;
            
            uicontrol('Parent', parametersPanel, 'Style', 'text', 'String', 'Angle', 'Units', 'normalized', 'Position', [0.3, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', parametersPanel, 'Style', 'text', 'String', '# spots', 'Units', 'normalized', 'Position', [0.37, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', parametersPanel, 'Style', 'text', 'String', 'Spacing', 'Units', 'normalized', 'Position', [0.44, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', parametersPanel, 'Style', 'text', 'String', 'Sigma', 'Units', 'normalized', 'Position', [0.51, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', parametersPanel, 'Style', 'text', 'String', 'Energy', 'Units', 'normalized', 'Position', [0.58, 0.55, 0.05, 0.3]) ;
            uicontrol('Parent', parametersPanel, 'Style', 'text', 'String', 'Noise', 'Units', 'normalized', 'Position', [0.65, 0.55, 0.05, 0.3]) ;
            
            this.CTPopup = uicontrol('Parent', parametersPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]) ;
            this.curvePopup = uicontrol('Parent', parametersPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.15, 0.15, 0.1, 0.3]) ;
            
            this.angleEdit = uicontrol('Parent', parametersPanel, 'Style', 'edit', 'String', num2str(this.control.get('angle')), 'Units', 'normalized', 'Position', [0.3, 0.15, 0.05, 0.3]) ;
            this.spotsNbEdit = uicontrol('Parent', parametersPanel, 'Style', 'edit', 'String', num2str(this.control.get('spotsNb')), 'Units', 'normalized', 'Position', [0.37, 0.15, 0.05, 0.3]) ;
            this.spacingEdit = uicontrol('Parent', parametersPanel, 'Style', 'edit', 'String', num2str(this.control.get('spacing')), 'Units', 'normalized', 'Position', [0.44, 0.15, 0.05, 0.3]) ;
            this.sigmaEdit = uicontrol('Parent', parametersPanel, 'Style', 'edit', 'String', num2str(this.control.get('sigma')), 'Units', 'normalized', 'Position', [0.51, 0.15, 0.05, 0.3]);
            this.energyEdit = uicontrol('Parent', parametersPanel, 'Style', 'edit', 'String', num2str(this.control.get('energy')), 'Units', 'normalized', 'Position', [0.58, 0.15, 0.05, 0.3], 'Enable', 'off') ;
            this.noiseEdit = uicontrol('Parent', parametersPanel, 'Style', 'edit', 'String', num2str(this.control.get('sigmaNoise')), 'Units', 'normalized', 'Position', [0.65, 0.15, 0.05, 0.3]) ;
            
            uicontrol('Parent', parametersPanel, 'Style', 'pushbutton', 'String', 'Output folder', 'Units', 'normalized', 'Position', [0.72, 0.55, 0.1, 0.3], 'Callback', @this.folderCallback) ;
            this.folderEdit = uicontrol('Parent', parametersPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.72, 0.15, 0.1, 0.3]) ;
            
            this.goButton = uicontrol('Parent', parametersPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @this.goCallback) ;
            
            this.updatePopups() ;
            
            this.imPanel = uipanel('Parent', this.mainPanel, 'Title', 'CT', 'Position', [0 0 0.68 0.8]) ;
            this.mainPlot = [] ;
            this.imh = [];
            this.linesh = [];
            
            goPanel = uipanel('Parent', this.mainPanel, 'Title', 'Draw/Go', 'Position', [0.7 0 0.3 0.8]) ;
            
            uicontrol('Parent', goPanel, 'Style', 'pushbutton', 'String', 'Draw area', 'Units', 'normalized', 'Position', [0.1, 0.7, 0.8, 0.1], 'Callback', @this.drawCallback) ;
            uicontrol('Parent', goPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.1, 0.5, 0.8, 0.1], 'Callback', @this.simulationCallback) ;
            
            this.control.set('view', this);
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
        
        function update(this, varargin)
            switch varargin{1}
                case 'CTProjection'
                    if isempty(this.mainPlot)
                        this.mainPlot = subplot(1, 1, 1, 'Parent', this.imPanel);
                    else
                        subplot(this.mainPlot);
                    end
                    
                    im = this.control.get('CTProjection');
                    
                    if ~isempty(this.imh)
                        delete(this.imh)
                    end
                    this.imh = imagesc(im);
                    axis equal off
                    
                case 'lines'
                    if ~isempty(this.linesh)
                        for i=1:4
                            delete(this.linesh{i})
                        end
                    end

                    if isempty(this.mainPlot)
                        return
                    else
                        subplot(this.mainPlot);
                    end
                    
                    worldLimit1 = this.control.get('worldLimit1');
                    worldLimit2 = this.control.get('worldLimit2');
                    
                    y = [worldLimit1(1) worldLimit2(1)]
                    x = [worldLimit1(2) worldLimit2(2)]

                    this.linesh{1} = line([x(1) x(1)], [y(1), y(2)], 'Color', 'red');
                    this.linesh{2} = line([x(2) x(2)], [y(1), y(2)], 'Color', 'red');
                    this.linesh{3} = line([x(1) x(2)], [y(1), y(1)], 'Color', 'red');
                    this.linesh{4} = line([x(1) x(2)], [y(2), y(2)], 'Color', 'red');
            end
        end
    end
    
    methods (Access = private)
        function updatePopups(this)
            names = this.control.getCTNames() ;
            CTName = this.control.get('CTName') ;
            if ~isempty(CTName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, CTName)) ;
            else
                val = 1 ;
            end
            
            if isempty(val)
                val = 1 ;
            end
            
            set(this.CTPopup, 'String', names) ;
            set(this.CTPopup, 'Value', val) ;
       
            names = this.control.getCurvesNames() ;
            curveName = this.control.get('curveName') ;
            if ~isempty(curveName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, curveName)) ;
            else
                val = 1 ;
            end
            
            if isempty(val)
                val = 1 ;
            end
            set(this.curvePopup, 'String', names) ;
            set(this.curvePopup, 'Value', val) ;
        end
        
        function goCallback(this, source, event)
            this.control.set('angle', str2double(get(this.angleEdit, 'String'))) ;
%             this.control.set('name', get(this.nameEdit, 'String')) ;
            this.control.set('sigma', str2double(get(this.sigmaEdit, 'String'))) ;
            this.control.set('spacing', str2double(get(this.spacingEdit, 'String'))) ;
            this.control.set('spotsNb', str2double(get(this.spotsNbEdit, 'String'))) ;
            this.control.set('energy', str2double(get(this.energyEdit, 'String'))) ;
            this.control.set('sigmaNoise', str2double(get(this.noiseEdit, 'String'))) ;
            
            items = get(this.CTPopup, 'String') ;
            index_selected = get(this.CTPopup, 'Value') ;
            CTName = items{index_selected} ;
            this.control.set('CTName', CTName) ;
            
            items = get(this.curvePopup, 'String') ;
            index_selected = get(this.curvePopup, 'Value') ;
            curveName = items{index_selected} ;
            this.control.set('curveName', curveName) ;
            
            this.control.updateIm();
        end
        
        function folderCallback(this, source, event)
            path = this.control.get('path', 'PRGeneratorFolder');
            pathName = uigetdir(path);
            
            if pathName~=0
                this.control.set('path', 'PRGeneratorFolder', pathName);
            
                this.control.set('folder', pathName) ;
                
                this.folderEdit.set('String', this.control.get('folder'));
            end
        end
        
        function simulationCallback(this, source, event)
            this.control.simpleSimulation() ;
        end
        
        function drawCallback(this, source, event)
            [x, y] = ginput(2)
            
            if ~isempty(this.linesh)
                for i=1:4
                    delete(this.linesh{i})
                end
            end
            
            if isempty(this.mainPlot)
                return
            else
                subplot(this.mainPlot);
            end
            
            this.linesh{1} = line([x(1) x(1)], [y(1), y(2)], 'Color', 'red');
            this.linesh{2} = line([x(2) x(2)], [y(1), y(2)], 'Color', 'red');
            this.linesh{3} = line([x(1) x(2)], [y(1), y(1)], 'Color', 'red');
            this.linesh{4} = line([x(1) x(2)], [y(2), y(2)], 'Color', 'red');
            
            this.control.set('worldLimit1', [y(1) x(1)]);
            this.control.set('worldLimit2', [y(2) x(2)]);
        end
    end
end