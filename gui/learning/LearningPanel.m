classdef LearningPanel < handle   
    properties (Access = private)
        control ;
        
        regguiFig ;
        f ;
        mainPanel;

        tabGroup ;
        learningMLICDataTab ;
        
        pradioStatusView;
        
        learningMLICDataPanel;
    end
    
    methods (Access = public)
        function this = LearningPanel(control)
            this.control = control ;
            this.regguiFig = findobj('Name', 'regguiC') ;
            
            this.f = figure('Name', 'Proton Radiography Generator', 'MenuBar', 'None', 'NumberTitle', 'off', 'units', 'normalized', 'outerposition', [0 0 1 1]);
            
            %statusPanel = uipanel(this.f, 'Position', [0 0 1 0.04], 'BorderType', 'none') ;
            %this.pradioStatusView = PRadioStatusView(statusPanel, this.control) ;
            
            this.tabGroup = uitabgroup(this.f, 'Position', [0 0.04 1 0.96]) ;
            this.learningMLICDataTab = uitab(this.tabGroup, 'Title', 'MLIC', 'ButtonDownFcn', @this.learningMLICDataCallback) ;
            
            this.learningMLICDataPanel = LearningMLICDataPanel(this.learningMLICDataTab, this.control.get('learningMLICDataControl')) ;
            this.learningMLICDataPanel.setVisible('on') ;
        end
        
        function clearDisplay(this)
            % clearDisplay Set visibility of all panels to 'off'
            
            this.learningMLICDataPanel.setVisible('off');
        end
        
        function unclearDisplay(this)
            % unclearDisplay Set visibility of all panels to 'on'
            
            this.learningMLICDataPanel.setVisible('on');
        end
    end
    
    methods (Access = private)
        function learningMLICDataCallback(this, source, event)
            % learningMLICDataCallback Display the learningMLICData panel
            
            this.clearDisplay() ;
            this.learningMLICDataPanel.setVisible('on') ;
        end
    end
end
