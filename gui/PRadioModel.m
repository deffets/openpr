
classdef PRadioModel < handle
    % Generic Model for PR GUI
    
    properties (Access = private)
        regguiHObject ;
        progressControl ;
    end
    
    methods (Access = public)
        function this = PRadioModel(regguiHObject)
            % PRadioModel Creates the model
            %   PRadioModel(regguiHObject) where regguiHObject is the object handle of the Reggui interface
            this.regguiHObject = regguiHObject ;
            this.progressControl = [] ;
            
            this.startProgressControl() ;
        end
        
        function set(this, varargin)
            property = varargin{1};
            
            switch property
                case 'progressControl'
                    this.progressControl = varargin{2} ;
            end
        end
        
        function value = get(this, varargin)
            property = varargin{1};

            switch property
                case 'progressControl'
                    value = this.progressControl ;
                case 'version'
                    value = fileread(fullfile(getRegguiPath('version'), 'version.txt'));
                otherwise
                    error('Property no found');
            end
        end
    end
    
    methods (Access = private)
        function startProgressControl(this)
            
            handles = guidata(this.regguiHObject) ;

            if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                this.set('progressControl', handles.progressControl) ;
            end
            
        end
    end
end
