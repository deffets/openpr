
classdef HelpControl < GenericControl
    properties (Access = private)
    end
    
    methods (Access = public)
        function obj = HelpControl(model)
            obj@GenericControl(model) ;
        end
        
        function value = get(this, varargin)
            value = this.model.get(varargin{:});
        end
        
        function checkInstallation(this)
            this.model.checkInstallation();
        end
    end
end
