
classdef HelpModel < GenericModel
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function obj = HelpModel(regguiHObject)
            obj@GenericModel(regguiHObject);
        end
        
        function value = get(this, varargin)
            value = get@GenericModel(this, varargin{:});
        end
        
        function checkInstallation(this)
            handles = guidata(this.regguiHObject);
            
            instruction = 'handles = mainTest(handles);';
            
            handles.instructions{length(handles.instructions)+1} = instruction;
            handles = Execute_reggui_instructions(handles);
            guidata(this.regguiHObject, handles);
        end
    end
end
