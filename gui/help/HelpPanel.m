
classdef HelpPanel < MainPanel
    properties (Access = private)
        control;
    end
    
    methods (Access = public)
        function this = HelpPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Online help', 'Units', 'normalized', 'Position', [0.1 0.8 0.1 0.1], 'Callback', @this.helpCallback);
            uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Install third-party tools', 'Units', 'normalized', 'Position', [0.3 0.8 0.1 0.1], 'Callback', @this.installCallback);
            uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'PR Generator', 'Units', 'normalized', 'Position', [0.5 0.8 0.1 0.1], 'Callback', @this.PRGeneratorCallback);
            uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Check installation', 'Units', 'normalized', 'Position', [0.7 0.8 0.1 0.1], 'Callback', @this.checkInstallationCallback);
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private)
        function helpCallback(this, source, event)
            web(this.control.get('help', 'Home'), '-browser');
        end
        
        function installCallback(this, source, event)
            ExternalGUI();
        end
        
        function PRGeneratorCallback(this, source, event)
            LearningPanel(LearningControl(this.control.get('regguiHObject')));
        end
        
        function checkInstallationCallback(this, source, event)
            this.control.checkInstallation();
        end
    end
end
