
classdef GenericControl < handle
    % Generic controler for PR GUI
    %   See also GenericModel
    
    properties (Access = protected)
        model ; % Model inherited from GenericModel
    end
    
    methods (Access = public)
        function obj = GenericControl(model)
            % GenericControl Constructor
            %	GenericControl(model) where model must be inherited from
            %	GenericModel.
            
            obj.model = model ;
        end
        
        function CTNames = getCTNames(this)
            % getCTNames  Get names of images loaded in Reggui
            CTNames = this.model.getCTNames() ;
        end
        
        function referenceNames = getReferenceNames(this)
            % getReferenceNames  Get names of reference Bragg curves loaded in Reggui
            referenceNames = this.model.getReferenceNames() ;
        end
        
        function PRNames = getPRNames(this)
            % getPRNames  Get names of proton radiographs loaded in Reggui
            PRNames = this.model.getPRNames() ;
        end
        
        function PRNames = getVivaNames(this)
            % getVivaNames  Get names of Viva proton radiographs loaded in Reggui
            PRNames = this.model.getVivaNames() ;
        end
        
        function curvesNames = getCurvesNames(this)
            % getCurvesNames  Get names of calibration curves loaded in Reggui
            curvesNames = this.model.getCurvesNames() ;
        end
        
        function wetNames = getWETNames(this)
            % getWETNames  Get names of WET-maps loaded in Reggui
            wetNames = this.model.getWETNames() ;
        end
        
        function phantomNames = getPhantomNames(this)
            phantomNames = this.model.getPhantomNames() ;
        end
        
        function spectrumNames = getSpectrumNames(this)
            spectrumNames = this.model.getSpectrumNames() ;
        end
        
        function value = get(this, varargin)
            value = this.model.get(varargin{:}) ;
        end
    end
end
