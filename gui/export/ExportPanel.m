classdef ExportPanel < MainPanel
    properties (Access = private)
        control ;
        
    end
    
    methods (Access = public)
        function this = ExportPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
        end
    end
end
