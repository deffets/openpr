
classdef ExportControl < GenericControl
    properties (Access = private)
    end
    
    methods (Access = public)
        function obj = ExportControl(model)
            obj@GenericControl(model) ;
        end
    end
end
