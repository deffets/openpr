
classdef ExportModel < GenericModel
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function obj = ExportModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
        end
    end
end
