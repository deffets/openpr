
classdef MainPanel < handle
    % Panel object
    
    properties (Access = protected)
        mainPanel ; % uipanel handle
    end
    
    methods (Access = public)
        function obj = MainPanel(f)
            % MainPanel Creates a panel from a figure handle
            %   MainPanel(f) Creates a panel from a figure handle f
            
            obj.mainPanel = uipanel(f, 'BorderType', 'none') ;
        end
        
        function setVisible(this, visible)
            % setVisible set visibility of the panel
            %   setVisible(visible) Shows panel if visible is 'on'. Hide
            %   panel if visible is 'off'
            
            set(this.mainPanel, 'Visible', visible) ;
        end
    end
end
