
classdef RegistrationModel < GenericModel
    properties (SetObservable)
        CTName ;
        referenceName ;
        calibrationName ;
        PRName1 ;
        PRName2 ;
    end
    
    methods (Access = public)
        function obj = RegistrationModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
            
            obj.CTName = [] ;
            obj.referenceName = [] ;
            obj.calibrationName = [] ;
        end
        
        function registrationTypes = getRegistrationTypes(this)
            registrationTypes = {'6 DOF', '2 DOF'} ;
        end
    end
    
    methods (Access = private)
        
    end
end
