
classdef TransRegistrationPanel < MainPanel
    properties (Access = private)
        p1 ;
        p2 ;
        p3 ;
        
        control ;
        
        simuText ;
        PRText ;
        methodText ;
        
        vertMinusButton ;
        vertPlusButton ;
        horMinusButton ;
        horPlusButton ;
        goButton ;
        saveButton ;
        autoButton ;
        
        vertEdit ;
        horEdit ;
        simuNameEdit ;
        prNameEdit ;
        
        simuPopup ;
        PRPopup ;
        
        r1 ;
        r2 ;
    end
    
    methods (Access = public)
        function this = TransRegistrationPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.p1 = uipanel('Parent', this.mainPanel, 'Title', 'Parameters', 'Position', [0 0.9 1 0.09]) ;
            this.p2 = uipanel('Parent', this.mainPanel, 'Title', 'Registration', 'Position', [0 0.46 1 0.43]) ;
            this.p3 = uipanel('Parent', this.mainPanel, 'Title', 'Registration result', 'Position', [0 0 1 0.45]) ;
            
            v = ViewBraggPanel(this.p3, this.control) ;
            this.control.setViewBraggPanel(v) ;
            this.control.setPanel(this) ;
            
            this.simuText = uicontrol('Parent', this.p1, 'Style', 'text', 'String', 'Simulation', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]) ;
            this.PRText = uicontrol('Parent', this.p1, 'Style', 'text', 'String', 'PR', 'Units', 'normalized', 'Position', [0.15, 0.55, 0.1, 0.3]) ;
            this.methodText = uicontrol('Parent', this.p1, 'Style', 'text', 'String', 'Method', 'Units', 'normalized', 'Position', [0.3, 0.55, 0.2, 0.3]) ;
            
            
            this.simuPopup = uicontrol('Parent', this.p1, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]) ;
            this.PRPopup = uicontrol('Parent', this.p1, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.15, 0.15, 0.1, 0.3]) ;
            
            this.r1 = uicontrol('Parent', this.p1, 'Style', 'radiobutton', 'String', 'Cross correlation', 'Units', 'normalized', 'Position', [0.3, 0.15, 0.1, 0.3], 'Callback', @this.rbCallback, 'Tag', 'r1') ;
            this.r2 = uicontrol('Parent', this.p1, 'Style', 'radiobutton', 'String', 'Step by step', 'Units', 'normalized', 'Position', [0.4, 0.15, 0.1, 0.3], 'Callback', @this.rbCallback, 'Tag', 'r2') ;
            this.r1.set('Value', this.r1.get('Min')) ;
            this.r2.set('Value', this.r2.get('Max')) ;
                    
            this.goButton = uicontrol('Parent', this.p1, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @this.goCallback) ;
            
            this.vertMinusButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', '-', 'Units', 'normalized', 'Position', [0.6, 0.6, 0.04, 0.1], 'Callback', @this.transCallback, 'Tag', 'vertMinus') ;
            this.vertEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.65, 0.6, 0.04, 0.1], 'Callback', @this.transCallback, 'Tag', 'vertEdit') ;
            this.vertPlusButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', '+', 'Units', 'normalized', 'Position', [0.7, 0.6, 0.04, 0.1], 'Callback', @this.transCallback, 'Tag', 'vertPlus') ;
            
            this.horMinusButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', '-', 'Units', 'normalized', 'Position', [0.6, 0.45, 0.04, 0.1], 'Callback', @this.transCallback, 'Tag', 'horMinus') ;
            this.horEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.65, 0.45, 0.04, 0.1], 'Callback', @this.transCallback, 'Tag', 'horEdit') ;
            this.horPlusButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', '+', 'Units', 'normalized', 'Position', [0.7, 0.45, 0.04, 0.1], 'Callback', @this.transCallback, 'Tag', 'horPlus') ;
            
            uicontrol('Parent', this.p2, 'Style', 'text', 'String', 'Name (simu)', 'Units', 'normalized', 'Position', [0.6, 0.2, 0.075, 0.1]) ;
            uicontrol('Parent', this.p2, 'Style', 'text', 'String', 'Name (PR)', 'Units', 'normalized', 'Position', [0.7, 0.2, 0.075, 0.1]) ;
            this.simuNameEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.6, 0.1, 0.075, 0.1]) ;
            this.prNameEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.7, 0.1, 0.075, 0.1]) ;         
            this.saveButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'Units', 'normalized', 'Position', [0.8, 0.1, 0.05, 0.1], 'String', 'Save', 'Callback', @this.saveCallback) ;
            
            this.autoButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', 'AUTO', 'Units', 'normalized', 'Position', [0.6, 0.8, 0.14, 0.1], 'Callback', @this.autoCallback) ;
            
            this.updatePopups() ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
        
        function updateImages(this)
            [image1, image2, simulationImage, PRImage] = this.control.getImages() ;
            spacing1 = simulationImage.get('spacing') ;
            spacing2 = PRImage.get('spacing') ;

            RA = imref2d(size(simulationImage.get('data')), spacing1(2), spacing1(1));
            RA.XWorldLimits = simulationImage.get('XWorldLimits') ;
            RA.YWorldLimits = simulationImage.get('YWorldLimits') ;

            RB = imref2d(size(PRImage.get('data')), spacing2(2), spacing2(1));
            RB.XWorldLimits = PRImage.get('XWorldLimits') ;
            RB.YWorldLimits = PRImage.get('YWorldLimits') ;

            subplot(1, 2, 1, 'Parent',  this.p2) ;
            imh = imshowpair(simulationImage.get('data'), RA, PRImage.get('data'), RB) ;
            axis off ;
            drawnow ;
        end
    end
    
    methods (Access = private)
        function updatePopups(this)
            PRNames = this.control.getPRNames() ;
            
            if ~isempty(PRNames)
                this.simuPopup.set('String', PRNames) ;
                this.PRPopup.set('String', PRNames) ;
            end
        end
        
        function updateTransEdits(this)
            o = this.control.get('PROrigin') ;
            this.vertEdit.set('String', num2str(o(1))) ;
            this.horEdit.set('String', num2str(o(2))) ;
        end
        
        function goCallback(this, source, event)
            items = get(this.simuPopup, 'String') ;
            index_selected = get(this.simuPopup, 'Value') ;
            simuName = items{index_selected} ;          
            this.control.set('simulationName', simuName) ;
            
            items = get(this.PRPopup, 'String') ;
            index_selected = get(this.PRPopup, 'Value') ;
            PRName = items{index_selected} ;          
            this.control.set('PRName', PRName) ;
            
            if this.r2.get('Value')==this.r2.get('Max')
                this.control.set('method', 'xcorr') ;
            else
                this.control.set('method', 'step') ;
            end
    
            this.control.startNewRegistration() ;
            this.updateTransEdits() ;
        end
        
        function transCallback(this, source, event)
            origin = this.control.get('PROrigin') ;
            step = this.control.get('step') ;
            
            switch source.Tag
                case 'vertMinus'
                    origin(1) = origin(1) + step(1) ;
                case 'vertPlus'
                    origin(1) = origin(1) - step(1) ;
                case 'horMinus'
                    origin(2) = origin(2) - step(2) ;
                case 'horPlus'
                    origin(2) = origin(2) + step(2) ;
                case 'vertEdit'
                    origin(1) = str2double(this.vertEdit.get('String')) ;
                case 'horEdit'
                    origin(2) = str2double(this.horEdit.get('String')) ;
            end

            this.control.set('PROrigin', origin) ;
            this.updateTransEdits() ;
        end
        
        function saveCallback(this, source, event)
            this.control.set('newName1', this.simuNameEdit.get('String')) ;
            this.control.set('newName2', this.prNameEdit.get('String')) ;
            this.control.saveRegistration() ;
        end
        
        function autoCallback(this, source, event)
            this.control.autoRegistration() ;
            this.updateTransEdits() ;
        end
        
        function rbCallback(this, source, event)
            switch source.Tag
                case 'r1'
                    this.r1.set('Value', this.r1.get('Max'))
                    this.r2.set('Value', this.r2.get('Min'))
                case 'r2'
                    this.r1.set('Value', this.r1.get('Min'))
                    this.r2.set('Value', this.r2.get('Max'))
            end
        end
    end
end
