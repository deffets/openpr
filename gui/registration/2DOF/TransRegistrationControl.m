
classdef TransRegistrationControl < ViewBraggControl
    properties (Access = private)
        viewBraggPanel ;
        panel ;
    end
    
    methods (Access = public)
        function this = TransRegistrationControl(model)
            this@ViewBraggControl(model) ;
            
            addlistener(this.model, 'image2', 'PostSet', @this.imageListener);
        end
        
        function set(this, property, value)
            this.model.set(property, value) ;
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
        
        function startNewRegistration(this)
            this.model.startNewRegistration() ;
            this.viewBraggPanel.updateImages()
            this.panel.updateImages() ;
        end
        
        function setViewBraggPanel(this, viewBraggPanel)
            this.viewBraggPanel = viewBraggPanel ;
        end
        
        function setPanel(this, panel)
            this.panel = panel ;
        end
        
        function [image1, image2, simulationImage, PRImage] = getImages(this)
            [image1, image2, simulationImage, PRImage] = this.model.getImages() ;
        end
        
        function saveRegistration(this)
            this.model.saveRegistration() ;
        end
        
        function autoRegistration(this)
            this.model.autoRegistration() ;
        end
    end
    
    methods (Access = private)
        function imageListener(this, source, event)
            this.panel.updateImages() ;
            this.viewBraggPanel.updateImages() ;
        end       
    end
end
