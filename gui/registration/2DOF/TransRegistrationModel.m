
classdef TransRegistrationModel < ViewBraggModel
    properties (Access = private)
        simulationName ;
        PRName ;
        newName1 ;
        newName2 ;
        
        method ;
        step ;
        
        simulationImage ;
        PRImage ;
    end
    
    methods (Access = public)
        function obj = TransRegistrationModel(regguiHObject)
            obj@ViewBraggModel(regguiHObject) ;
            
            obj.simulationName = [] ;
            obj.PRName = [] ;
            obj.newName1 = [] ;
            obj.newName2 = [] ;
            obj.method = [] ;
        end
        
        function set(this, property, value)
            switch property
                case 'simulationName'
                    this.simulationName = value ;
                    this.PRName1 = value ;
                case 'PRName'
                    this.PRName = value ;
                    this.PRName2 = value ;
                case 'PROrigin'
                    this.updateImage(value) ;
                case 'newName1'
                    this.newName1 = value ;
                case 'newName2'
                    this.newName2 = value ;
                case 'method'
                    this.method = value ;
                otherwise
                    set@ViewBraggModel(this, property, value) ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'simulationName'
                    value = this.simulationName ;
                case 'PRName'
                    value = this.PRName ;
                case 'PROrigin'
                    value = this.braggImage2.get('origin') ;
                case 'step'
                    value = this.step ;
                case 'method'
                    value = this.method ;
                otherwise
                    value = get@ViewBraggModel(this, property) ;
            end
        end
        
        function startNewRegistration(this)
            this.image1ToDisplay = 1 ;
            this.image2ToDisplay = 1 ;
            this.featureName = 'Position of the maximum' ;
            
            handles = guidata(this.regguiHObject) ;
            
            this.braggImage1 = handles2BraggImage(this.simulationName, handles) ;
            this.braggImage2 = handles2BraggImage(this.PRName, handles) ;
            
            origin1 = this.braggImage1.get('origin') ;
            origin2 = this.braggImage2.get('origin') ;
            
            spacing1 = this.braggImage1.get('spacing') ;
            spacing2 = this.braggImage2.get('spacing') ;
            
            this.step = spacing1 ;
            
            im1 = maximumPosition(this.braggImage1.get('data'), this.braggImage1.get('depth')) ;
            im2 = maximumPosition(this.braggImage2.get('data'), this.braggImage2.get('depth')) ;
            
            this.simulationImage = Image2D(im1, origin1, spacing1) ;
            this.PRImage = Image2D(im2, origin2, spacing2) ;
            
            [worldLimit1, worldLimit2] = computeWorldLimits(this.simulationImage.get('XWorldLimits'), ...
                this.simulationImage.get('YWorldLimits'), this.PRImage.get('XWorldLimits'), ...
                this.PRImage.get('YWorldLimits'), spacing2) ;
            worldLimit2(1) = worldLimit2(1) - spacing2(1) ;
            worldLimit2(2) = worldLimit2(2) - spacing2(2) ;
%             
%             figure ;     
%             figure ;
%             r = rangeError(this.braggImage1.getData(worldLimit1, worldLimit2, spacing2), this.braggImage1.get('depth'), this.braggImage2.getData(worldLimit1, worldLimit2, spacing2), this.braggImage2.get('depth')) ;
%             imagesc(-r, [-2.5 2.5])
%             colormap jet
%             colorbar
%             axis equal off    
%             
%             figure ;
%             r = rangeError(this.braggImage1.getData(worldLimit1, worldLimit2, spacing2), this.braggImage1.get('depth'), this.braggImage2.getData(worldLimit1, worldLimit2, spacing2)-0.5+1*rand(size(this.braggImage2.getData(worldLimit1, worldLimit2, spacing2))), this.braggImage2.get('depth')) ;
% %             imagesc(r, [-4 4])
%             colorbar
%             axis equal off
%             figure ;
            
            this.image1 = Image2D(this.simulationImage.getData(worldLimit1, worldLimit2, spacing2), worldLimit1, spacing2) ;
            this.image2 = Image2D(this.PRImage.getData(worldLimit1, worldLimit2, spacing2), worldLimit1, spacing2) ;
        end
        

        function [image1, image2, simulationImage, PRImage] = getImages(this)
            image1 = this.image1 ;
            image2 = this.image2 ;
            simulationImage = this.simulationImage ;
            PRImage = this.PRImage ;
        end
        
        function saveRegistration(this)            
            handles = guidata(this.regguiHObject) ;
            instruction = ['handles = saveRegistration(' '''' this.simulationName '''' ', ' '''' this.PRName '''' ...
                ', [' num2str(this.braggImage2.get('origin')) '], ' '''' this.newName1 '''' ', ' '''' this.newName2 '''' ', handles) ;'] ;
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
        
        function autoRegistration(this)
            if strcmp(this.method, 'xcorr')
                newOrigin = registration2Dxcorr(this.braggImage1, this.braggImage2) ;
                this.updateImage(newOrigin) ;
            elseif strcmp(this.method, 'step')
                max_iter = 200 ;

                newOrigin_tmp = this.braggImage2.get('origin') ;

                if length(this.braggImage1.get('depth'))~=length(this.braggImage2.get('depth')) || sum(this.braggImage1.get('depth')~=this.braggImage2.get('depth'))
                    disp('Reinterpolating...')
                    data = this.braggImage1.get('data') ;
                    depth = this.braggImage2.get('depth') ;
                    depth1 = this.braggImage1.get('depth') ;

                    data2 = zeros(size(data, 1), size(data, 2), length(depth)) ;
                    for i=1 : size(data, 1)
                        for j=1 : size(data, 2)
                            data2(i, j, :) = interp1(depth1, squeeze(data(i, j, :)), depth) ;
                        end
                    end

                    b2 = BraggImage(data2, depth, this.braggImage1.get('origin'), this.braggImage1.get('spacing')) ;

                    disp('Reinterpolation done.')
                else
                    b2 = this.braggImage1 ;
                end

                for i=1 : max_iter ;
                    newOrigin = registration2DIter(b2, this.braggImage2, this.step, 10) ;
                    this.updateImage(newOrigin) ;

                    if newOrigin_tmp(1)==newOrigin(1) && newOrigin_tmp(2)==newOrigin(2)
                        break
                    end
                    newOrigin_tmp = newOrigin ;
                end
            else
                error('Incorrect value for registration method')
            end
        end
    end
    
    methods (Access = private)
        function updateImage(this, newOrigin)
            this.braggImage2.set('origin', newOrigin) ;
            this.PRImage.set('origin', newOrigin) ;
            this.image2.set('origin', newOrigin) ;
            
            spacing2 = this.braggImage2.get('spacing') ;
            
            [worldLimit1, worldLimit2] = computeWorldLimits(this.simulationImage.get('XWorldLimits'), ...
                this.simulationImage.get('YWorldLimits'), this.PRImage.get('XWorldLimits'), ...
                this.PRImage.get('YWorldLimits'), spacing2) ;
            worldLimit2(1) = worldLimit2(1) - spacing2(1) ;
            worldLimit2(2) = worldLimit2(2) - spacing2(2) ;
            
            this.image1 = Image2D(this.simulationImage.getData(worldLimit1, worldLimit2, spacing2), worldLimit1, spacing2) ;
            this.image2 = Image2D(this.PRImage.getData(worldLimit1, worldLimit2, spacing2), worldLimit1, spacing2) ;
        end
    end
end
