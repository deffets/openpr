
classdef AllDOFRegModel < GenericModel
    properties (SetObservable)
        wet1 ;
        wet2 ;
        wet3 ;
        
        wetReg1 ;
        wetReg2 ;
        wetReg3 ;
        
        correctionMatrix ;
    end
    
    properties (Access = protected)
        CTName ;
        CTNameForSaving ;
        referenceName ;
        calibrationName ;
        newName ;
        PRName270 ;
        PRName0 ;
        sigma ;
        yaw ;
        roll ;
        pitch ;
    end
    
    methods (Access = public)
        function this = AllDOFRegModel(regguiHthisect)
            this@GenericModel(regguiHthisect) ;
            
            this.CTName = [] ;
            this.referenceName = [] ;
            this.calibrationName = [] ;
            this.newName = [] ;
            this.PRName270 = [] ;
            this.PRName0 = [] ;
            this.sigma = 3 ;
            
            this.wet1 = [] ;
            this.wet2 = [] ;
            this.wet3 = [] ;

            this.wetReg1 = [] ;
            this.wetReg2 = [] ;
            this.wetReg3 = [] ;
            
            this.correctionMatrix = eye(4) ;
            this.yaw = 0 ;
            this.roll = 0 ;
            this.pitch = 0 ;
        end
        
        function set(this, property, value)
            switch property
                case 'CTName'
                    this.CTName = value ;
                case 'CTNameForSaving'
                    this.CTNameForSaving = value ;
                case 'referenceName'
                    this.referenceName = value ;
                case 'calibrationName'
                    this.calibrationName = value ;
                case 'newName'
                    this.newName = value ;
                case 'PRName270'
                    this.PRName270 = value ;
                case 'PRName0'
                    this.PRName0 = value ;
                case 'sigma'
                    this.sigma = value ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'CTName'
                    value = this.CTName ;
                case 'CTNameForSaving'
                    value = this.CTNameForSaving ;
                case 'referenceName'
                    value = this.referenceName ;
                case 'calibrationName'
                    value = this.calibrationName ;
                case 'newName'
                    value = this.newName ;
                case 'PRName270'
                    value = this.PRName270 ;
                case 'PRName0'
                    value = this.PRName0 ;
                case 'sigma'
                    value = this.sigma ;
                case 'wet1'
                    value = this.wet1 ;
                case 'wet2'
                    value = this.wet2 ;
                case 'wet3'
                    value = this.wet3 ;
                case 'wetReg1'
                    value = this.wetReg1 ;
                case 'wetReg2'
                    value = this.wetReg2 ;
                case 'wetReg3'
                    value = this.wetReg3 ;
                case 'correctionMatrix'
                    value = this.correctionMatrix ;
            end
        end
        
        function [yaw, pitch, roll, xTrans, yTrans, zTrans] = getTransfoParam(this)
            roll = this.roll ;
            pitch = this.pitch ;
            yaw = this.yaw ;
            
            trans = squeeze(this.correctionMatrix(4, 1:3)) ;
            
            xTrans = trans(1) ;
            yTrans = trans(3) ;
            zTrans = trans(2) ;
        end
        
        function saveRegistration(this)
            handles = guidata(this.regguiHObject) ;
            instruction = ['handles = applyTransform(' '''' this.CTNameForSaving '''' ', ' '[' ...
                num2str(this.correctionMatrix(1, 1:4)) ';' ...
                num2str(this.correctionMatrix(2, 1:4)) ';' ...
                num2str(this.correctionMatrix(3, 1:4)) ';' ...
                num2str(this.correctionMatrix(4, 1:4)) ']' ...
                ', ' '''' this.newName '''' ', handles) ;'] ;
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
        
        function startNewRegistration(this, fastMode)
            handles = guidata(this.regguiHObject) ;
            if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                handles.progressControl.set('runningMsg', '6 DOF Registration') ;
                handles.progressControl.set('progressMsg', 'Loading data') ;
                handles.progressControl.set('n', 0) ;
            end
            
            energy = 210 ;
            
            parPoolOK = 1 ;
            try
                p = parpool(4) ;
            catch
                parPoolOK = 0 ;
                warning('Parallel pool could not be launched')
            end
            
            if parPoolOK
                maxIter = 4 ;
            else
                maxIter = 3 ;
            end
            
            if strcmp(fastMode,'on')
                disp('Fast mode ON')
                maxIter = 3 ;
                maxFcnEval = 4 ;
            else
                maxFcnEval = 5 ;
            end
            
            
            handles = guidata(this.regguiHObject) ;
            
            hu = [];
            for i=1:length(handles.images.name)
                if(strcmp(handles.images.name{i}, this.CTName))
                    hu = handles.images.data{i};
                    huInfo = handles.images.info{i};
                end
            end
            if(isempty(hu))
                error('Input image not found in the current list')
            end

            calibration = [] ;
            for i=1:length(handles.mydata.name)
                if(strcmp(handles.mydata.name{i}, this.calibrationName))
                    calibration = handles.mydata.data{i};
                end
            end
            if(isempty(calibration))
                error('Calibration curve not found in the current list')
            end

            ref = [] ;
            for i=1:length(handles.mydata.name)
                if(strcmp(handles.mydata.name{i}, this.referenceName))
                    ref = handles.mydata.data{i} ;
                    depth = handles.mydata.info{i}.depth ;
                end
            end
            if(isempty(ref))        
                error('Reference IDD not found in the current list')
            end
            
            if(isempty(calibration))
                error('Calibration curve not found in the current list')
            end
            
            if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                handles.progressControl.set('progressMsg', 'Computing SPR') ;
            end
            if isfield(calibration, 'RS_materials') ;
                density = HU_to_density(hu, calibration.model) ;

                spr = density2spr_RS(density,  energy, calibration.RS_materials) ;
            else
                c = calibration.model.Stopping_Power' ;
                
                spr = interp1(c(1, :), c(2, :), hu, 'linear','extrap') ;
                spr(spr<0) = 0 ;
            end

%             density = HU_to_density(hu, calibration.model) ;
% 
%             disp('Computing SPR ...')
%             spr = density2spr_RS(density,  energy, calibration.RS_materials) ;

            spr = single(spr) ;
            sprSpacing = huInfo.Spacing ;
    
            anglez = 0 ; % 1.5 ;
            angley = 0 ; % 1.5 ;
            anglex = 0 ; % 1.5 ;
            xTrans = 0 ; % 5 ;
            yTrans = 0 ; % 5 ;
            zTrans = 0 ; % 5 ;

            disp(['Initial transfo: ' num2str([angley anglex anglez xTrans yTrans zTrans])])

            matz =  [cos(deg2rad(anglez)) -sin(deg2rad(anglez)) 0 0 ;
                     sin(deg2rad(anglez)) cos(deg2rad(anglez)) 0 0 ;
                     0 0 1 0 ;
                     0 0 0 1] ; % Yaw in matrix system ; roll in room system
            maty = [cos(deg2rad(angley)) 0 sin(deg2rad(angley)) 0 ;
                    0 1 0 0 ;
                    -sin(deg2rad(angley)) 0 cos(deg2rad(angley)) 0 ;
                    0 0 0 1] ; % Pitch in matrix system ; yaw in room system
            matx = [1 0 0 0
                    0 cos(deg2rad(anglex)) -sin(deg2rad(anglex)) 0 ;
                    0 sin(deg2rad(anglex)) cos(deg2rad(anglex)) 0 ;
                    0 0 0 1] ; % Roll in matrix system ; pitch in room system

            tMat = maty*matx*matz ;
            tMat(4, 1:3) = [xTrans yTrans zTrans] ;
            tform = invert(affine3d(tMat)) ;

            imR = imref3d(size(spr), [0 size(spr, 2)]-size(spr, 2)/2, [0 size(spr, 1)]-size(spr, 1)/2, [0 size(spr, 3)]-size(spr, 3)/2) ;
            spr2 = imwarp(spr, imR, tform, 'OutputView', imR, 'FillValues', 0) ;
%             spr2 = spr ;
            
            this.wet1 = spr2totalWET(spr2, sprSpacing, 0, 270) ;
            this.wet2 = spr2totalWET(spr2, sprSpacing, 0, 0) ;
            this.wetReg1 = spr2totalWET(spr2, sprSpacing, 0, 270) ;
            this.wetReg2 = spr2totalWET(spr2, sprSpacing, 0, 0) ;
            
            braggImage270 = handles2BraggImage(this.PRName270, handles) ;
            braggImage0 = handles2BraggImage(this.PRName0, handles) ;

            this.yaw = 0 ;
            this.pitch = 0 ;
            this.roll = 0 ;
            this.correctionMatrix = eye(4) ;
            spr3 = spr2 ;
    
            angle270 = 0 ;
            angle0 = 0 ;
            rollAngle = 0 ;

            tMat270 = eye(4) ;
            tMat0 = eye(4) ;
            tMatRoll = eye(4) ;
            transMat = eye(4) ;

            maxAngle270 = 2 ;
            maxAngle0 = 2 ;
            maxRollAngle = 2 ;
            maxTrans = 5 ;

            angle270Prev = 999 ;
            angle0Prev = 999 ;
            rollAnglePrev = 999 ;
            transPrev = 999 ;

            tic
            
            if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                handles.progressControl.set('progressMsg', 'Computing Initial Translations') ;
                handles.progressControl.set('n', 1/((maxIter*4+1)*2)) ;
            end
            
            preTransfo = tMat270*tMat0*tMatRoll ;
            transMat = submTransRegistrationComp(spr3, sprSpacing, braggImage270, braggImage0, ref, depth, this.sigma, maxFcnEval, maxTrans, transMat(4, 1:3), preTransfo) ;
            maxTrans = 2 ;

            this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat ;
            
            disp(['Predetermined: ' num2str([angle270 angle0 rollAngle transMat(4, 1) transMat(4, 2) transMat(4, 3)])])
            
            for i=1 : maxIter
                if parPoolOK && (isfield(handles, 'progressControl') && ~isempty(handles.progressControl))
                    handles.progressControl.set('progressMsg', ['Iteration ' num2str(i) '/' num2str(maxIter)]) ;

                end
                
                preTransfo = eye(4) ;
                postTransfo = tMat0*tMatRoll*transMat ;
                if parPoolOK
                    f(1) = parfeval(p, @braggRegistrationComp, 2, spr3, sprSpacing, braggImage270, ref, depth, this.sigma, 270, maxFcnEval, maxAngle270, angle270, preTransfo, postTransfo) ;
                elseif ~(strcmp(fastMode,'on') && abs(angle270-angle270Prev)<0.2)
                    angle270Prev = angle270 ;

                    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                        handles.progressControl.set('progressMsg', 'Computing Yaw') ;
                        handles.progressControl.set('n', i/(maxIter*4+1)) ;
                    end

                    [tMat270, angle270] = braggRegistrationComp(spr3, sprSpacing, braggImage270, ref, depth, this.sigma, 270, maxFcnEval, maxAngle270, angle270, preTransfo, postTransfo) ;

                    maxAngle270 = angle270 ;

                    this.yaw = angle270 ;
                    this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat ;
                end
                
                preTransfo = tMat270 ;
                postTransfo = tMatRoll*transMat ;
                if parPoolOK
                    f(2) = parfeval(p, @braggRegistrationComp, 2, spr3, sprSpacing, braggImage0, ref, depth, this.sigma, 0, maxFcnEval, maxAngle0, angle0, preTransfo, postTransfo) ;
                elseif ~(strcmp(fastMode,'on') && abs(angle0-angle0Prev)<0.2)
                    angle0Prev = angle0 ;
                
                    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                        handles.progressControl.set('progressMsg', 'Computing Pitch') ;
                        handles.progressControl.set('n', (i+1)/(maxIter*4+1)) ;
                    end

                    [tMat0, angle0] = braggRegistrationComp(spr3, sprSpacing, braggImage0, ref, depth, this.sigma, 0, maxFcnEval, maxAngle0, angle0, preTransfo, postTransfo) ;

                    maxAngle0 = angle0 ;

                    this.pitch = angle0 ;
                    this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat ;
                end
                
                preTransfo = tMat270*tMat0 ;
                postTransfo = transMat ;
                if parPoolOK
                    f(3) = parfeval(p, @braggRollIterComp, 2, spr3, sprSpacing, braggImage270, braggImage0, ref, depth, this.sigma, maxFcnEval, maxRollAngle, rollAngle, preTransfo, postTransfo) ;
                elseif ~(strcmp(fastMode,'on') && abs(rollAngle-rollAnglePrev)<0.2)
                    rollAnglePrev = rollAngle ;
                
                    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                        handles.progressControl.set('progressMsg', 'Computing Roll') ;
                        handles.progressControl.set('n', (i+2)/(maxIter*4+1)) ;
                    end

                    [tMatRoll, rollAngle] = braggRollIterComp(spr3, sprSpacing, braggImage270, braggImage0, ref, depth, this.sigma, maxFcnEval, maxRollAngle, rollAngle, preTransfo, postTransfo) ;

                    maxRollAngle = rollAngle ;

                    this.roll = rollAngle ;
                    this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat ;
                end
                
                preTransfo = tMat270*tMat0*tMatRoll ;
                if parPoolOK
                    f(4) = parfeval(p, @submTransRegistrationComp, 2, spr3, sprSpacing, braggImage270, braggImage0, ref, depth, this.sigma, maxFcnEval, maxTrans, transMat(4, 1:3), preTransfo) ;
                elseif ~(strcmp(fastMode,'on') && sum(abs(transMat(4, 1:3)-transPrev)<0.2))
                    transPrev = transMat(4, 1:3) ;
                
                    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                        handles.progressControl.set('progressMsg', 'Computing Translations') ;
                        handles.progressControl.set('n', (i+3)/(maxIter*4+1)) ;
                    end
                    
                    transMat = submTransRegistrationComp(spr3, sprSpacing, braggImage270, braggImage0, ref, depth, this.sigma, maxFcnEval, maxTrans, transMat(4, 1:3), preTransfo) ;

                    maxTrans = max(abs(transMat(4, 1:3))) ;

                    this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat ;
                end
                
                % Collect the results as they become available.
                if parPoolOK
                    for idx = 1:4
                      [completedIdx, value1, value2] = fetchNext(f);

                      switch completedIdx
                          case 1
                              tMat270 = value1 ;
                              angle270 = value2 ;
                              maxAngle270 = angle270 ;
                              this.yaw = angle270 ;
                              this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat ;
                          case 2
                              tMat0 = value1 ;
                              angle0 = value2 ;
                              maxAngle0 = angle0 ;
                              this.pitch = angle0 ;
                              this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat ;
                          case 3
                              tMatRoll = value1 ;
                              rollAngle = value2 ;
                              maxRollAngle = rollAngle ;
                              this.roll = rollAngle ;
                              this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat ;
                          case 4
                              transMat = value1 ;
                              maxTrans = max(abs(transMat(4, 1:3))) ;
                              this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat ;
                      end
                    end
                end

                if parPoolOK && (isfield(handles, 'progressControl') && ~isempty(handles.progressControl))
                    handles.progressControl.set('progressMsg', ['Iteration ' num2str(i) '/' num2str(maxIter) ' done']) ;
                    handles.progressControl.set('n', i/maxIter) ;
                end
                
                imR = imref3d(size(spr), [0 size(spr, 2)]-size(spr, 2)/2, [0 size(spr, 1)]-size(spr, 1)/2, [0 size(spr, 3)]-size(spr, 3)/2) ;
                sprTmp = imwarp(spr3, imR, tform, 'OutputView', imR, 'FillValues', 0) ;

                this.wetReg1 = spr2totalWET(sprTmp, sprSpacing, 0, 270) ;
                this.wetReg2 = spr2totalWET(sprTmp, sprSpacing, 0, 0) ;

                disp(['Determined: ' num2str([angle270 angle0 rollAngle transMat(4, 1) transMat(4, 2) transMat(4, 3)])])
                if (abs(angle270-angle270Prev)<0.1 && abs(angle0-angle0Prev)<0.1) && abs(rollAngle-rollAnglePrev)<0.1
                    break ;
                end
                        
            end
            
            toc
            
            if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                handles.progressControl.deleteProgress() ;
            end
            
            if parPoolOK
                delete(p) ;
            end
        end
    end    
end
