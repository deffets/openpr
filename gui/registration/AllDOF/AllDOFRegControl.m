
classdef AllDOFRegControl < GenericControl
    properties(Access = private)
        viewPanel ;
        savePanel ;
        
        yaw ;
        pitch ;
        roll ;
        xTrans ;
        yTrans ;
        zTrans ;
    end

    methods (Access = public)
        function this = AllDOFRegControl(model)
            this@GenericControl(model) ;
            
            addlistener(this.model, 'wet1', 'PostSet', @this.wetListener) ;
            addlistener(this.model, 'wet2', 'PostSet', @this.wetListener) ;
            addlistener(this.model, 'wet3', 'PostSet', @this.wetListener) ;
            
            addlistener(this.model, 'wetReg1', 'PostSet', @this.wetRegListener) ;
            addlistener(this.model, 'wetReg2', 'PostSet', @this.wetRegListener) ;
            addlistener(this.model, 'wetReg3', 'PostSet', @this.wetRegListener) ;
            
            addlistener(this.model, 'correctionMatrix', 'PostSet', @this.correctionMatrixListener) ;
        end
        
        function set(this, property, value)
            switch property
                case 'viewPanel'
                    this.viewPanel = value ;
                case 'savePanel'
                    this.savePanel = value ;
                otherwise
                    this.model.set(property, value) ;
            end
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
        
        function startNewRegistration(this, fastMode)
            this.model.startNewRegistration(fastMode) ;
        end
        
        function saveRegistration(this)
            this.model.saveRegistration() ;
        end
        
        function [yaw, pitch, roll, xTrans, yTrans, zTrans] = getTransfoParam(this)
            [yaw, pitch, roll, xTrans, yTrans, zTrans] = this.model.getTransfoParam() ;
        end
    end
    
    methods (Access = private)
        function wetListener(this, source, event)
            this.viewPanel.updateWetImages() ;
        end
        
        function wetRegListener(this, source, event)
            this.viewPanel.updateWetRegImages() ;
        end
        
        function correctionMatrixListener(this, source, event)
%             this.savePanel.updateTable() ;
            this.savePanel.updateTransfoParam() ;
        end
    end
end
