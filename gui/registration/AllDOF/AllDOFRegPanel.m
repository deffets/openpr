
classdef AllDOFRegPanel < MainPanel
    properties (Access = private)
        control ;
        
        selectionPanel ;
        viewPanel ;
        savePanel ;
    end
    
    methods (Access = public)
        function this = AllDOFRegPanel(f, control)
        	this@MainPanel(f) ;
            this.control = control ;
            
            p = uipanel('Parent', this.mainPanel, 'Position', [0 0.85 1 0.14], 'Title', 'Parameters') ;
            this.selectionPanel = AllDOFRegSelectionPanel(p, this.control) ;
            
            p = uipanel('Parent', this.mainPanel, 'Position', [0 0 0.66 0.84], 'Title', 'Registration') ;
            this.viewPanel = AllDOFRegViewPanel(p, this.control) ;
            
            p = uipanel('Parent', this.mainPanel, 'Position', [0.67 0 0.33 0.84], 'Title', 'Registration results') ;
            this.savePanel = AllDOFRegSavePanel(p, this.control) ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.selectionPanel.setVisible(visible) ;
                this.viewPanel.setVisible(visible) ;
                this.savePanel.setVisible(visible) ;
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private)
        
    end
end
