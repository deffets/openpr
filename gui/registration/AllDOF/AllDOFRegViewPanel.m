
classdef AllDOFRegViewPanel < MainPanel
    properties (Access = private)
        control ;
        
        s3 ;
        s4 ;
        
        h3 ;
        h4 ;
    end
    
    methods (Access = public)
        function this = AllDOFRegViewPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.control.set('viewPanel', this) ;
        end
        
        function updateWetImages(this)
            wet1 = this.control.get('wet1') ;
            wet2 = this.control.get('wet2') ;
            
            if ~isempty(wet1)
                subplot(2, 2, 1, 'Parent', this.mainPanel)
                imshow(mat2gray(wet1))
                axis equal off
            end
            
            if ~isempty(wet2)
                subplot(2, 2, 2, 'Parent', this.mainPanel)
                imshow(mat2gray(wet2))
                axis equal off
            end

        end
        
        function updateWetRegImages(this)
            wet1 = this.control.get('wetReg1') ;
            wet2 = this.control.get('wetReg2') ;
            
            if ~isempty(wet1)
                this.s3 = subplot(2, 2, 3, 'Parent', this.mainPanel) ;
%                 imshow(mat2gray(wet1))
                this.h3 = imshowpair(this.control.get('wet1'), wet1) ;
                set(this.h3, 'ButtonDownFcn', @this.mouseInSubplot3) ;
                axis equal off
            end
            
            if ~isempty(wet2)
                this.s4 = subplot(2, 2, 4, 'Parent', this.mainPanel) ;
%                 imshow(mat2gray(wet2))
                this.h4 = imshowpair(this.control.get('wet2'), wet2) ;
                set(this.h4, 'ButtonDownFcn', @this.mouseInSubplot4) ;
                axis equal off
            end

        end
    end
    
    
    methods (Access = private)
        function mouseInSubplot3(this, source, event)
            C = get (gca, 'CurrentPoint') ;
            pos1 = round(C(1, 1)-1) ;
            pos2 = round(C(1, 2)-1) ;

            subplot(this.s3) ;
            wetReg1 = this.control.get('wetReg1') ;
            wet1 = this.control.get('wet1') ;

            if pos1>size(wet1, 2)
                pos1 = size(wet1, 2) ;
            elseif pos1<1
                pos1 = 1 ;
            end
            if pos2>size(wet1, 1)
                pos2 = size(wet1, 2) ;
            elseif pos2<1
                pos2 = 1 ;
            end
            
            wet1(pos2:end, 1:pos1) = 0 ;
            wet1(1:pos2, pos1:end) = 0 ;

            wetReg1(1:pos2, 1:pos1) = 0 ;
            wetReg1(pos2:end, pos1:end) = 0 ;

            delete(this.h3)
            this.h3 = imshowpair(wet1, wetReg1) ;
     
            if ismember('control', get(gcf,'currentModifier'))
                set(gcf, 'WindowButtonMotionFcn', @this.mouseInSubplot3) ;
                return
            else
                set(gcf, 'WindowButtonMotionFcn', '') ;
            end

            set(this.h3, 'ButtonDownFcn', @this.mouseInSubplot3) ;
        end
        
        function mouseInSubplot4(this, source, event)
            C = get (gca, 'CurrentPoint') ;
            pos1 = round(C(1, 1)-1) ;
            pos2 = round(C(1, 2)-1) ;
            
            subplot(this.s4) ;
            wetReg1 = this.control.get('wetReg2') ;
            wet1 = this.control.get('wet2') ;
            
            if pos1>size(wet1, 2)
                pos1 = size(wet1, 2) ;
            elseif pos1<1
                pos1 = 1 ;
            end
            if pos2>size(wet1, 1)
                pos2 = size(wet1, 2) ;
            elseif pos2<1
                pos2 = 1 ;
            end
            
            wet1(pos2:end, 1:pos1) = 0 ;
            wet1(1:pos2, pos1:end) = 0 ;
            
            wetReg1(1:pos2, 1:pos1) = 0 ;
            wetReg1(pos2:end, pos1:end) = 0 ;
            
            this.h4 = imshowpair(wet1, wetReg1) ;
            
            if ismember('control', get(gcf,'currentModifier'))
                set(gcf, 'WindowButtonMotionFcn', @this.mouseInSubplot4) ;
                return
            else
                set(gcf, 'WindowButtonMotionFcn', '') ;
            end
            
            set(this.h4, 'ButtonDownFcn', @this.mouseInSubplot4) ;
        end
    end
end
