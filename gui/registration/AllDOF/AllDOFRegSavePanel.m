
classdef AllDOFRegSavePanel < MainPanel
    properties (Access = private)
        control ;
        
        p2 ;
        
        CTPopup ;
        newNameEdit ;
        saveButton ;
        
        yawEdit ;
        pitchEdit ;
        rollEdit ;
        xEdit ;
        yEdit ;
        zEdit ;
        
%         correctionTable ;
    end
    
    methods (Access = public)
        function this = AllDOFRegSavePanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.control.set('savePanel', this) ;
            
%             this.correctionTable = uitable('Parent', this.mainPanel, 'ColumnWidth', {50}) ;
%             this.updateTable() ;
            
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'CT', 'Units', 'normalized', 'Position', [0.01, 0.9, 0.3, 0.05]) ;
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.35, 0.9, 0.3, 0.05]) ;
            
            this.CTPopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.85, 0.3, 0.05]) ;
            this.newNameEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.35, 0.85, 0.3, 0.05]) ;         
            this.saveButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'Units', 'normalized', 'Position', [0.7, 0.85, 0.25, 0.05], 'String', 'Save', 'Callback', @this.saveCallback) ;
            
            this.p2 = uipanel('Parent', this.mainPanel, 'Position', [0.1 0.4 0.8 0.4]) ;
            uicontrol('Parent', this.p2, 'Style', 'text', 'String', 'Yaw: ', 'Units', 'normalized', 'Position', [0.01, 0.75, 0.2, 0.1]) ;
            uicontrol('Parent', this.p2, 'Style', 'text', 'String', 'Pitch: ', 'Units', 'normalized', 'Position', [0.01, 0.6, 0.2, 0.1]) ;
            uicontrol('Parent', this.p2, 'Style', 'text', 'String', 'Roll: ', 'Units', 'normalized', 'Position', [0.01, 0.45, 0.2, 0.1]) ;
            uicontrol('Parent', this.p2, 'Style', 'text', 'String', 'X: ', 'Units', 'normalized', 'Position', [0.01, 0.3, 0.2, 0.1]) ;
            uicontrol('Parent', this.p2, 'Style', 'text', 'String', 'Y: ', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.2, 0.1]) ;
            uicontrol('Parent', this.p2, 'Style', 'text', 'String', 'Z: ', 'Units', 'normalized', 'Position', [0.01, 0, 0.2, 0.1]) ;
            
            this.yawEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.25, 0.8, 0.2, 0.1]) ;
            this.pitchEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.25, 0.65, 0.2, 0.1]) ;
            this.rollEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.25, 0.5, 0.2, 0.1]) ;
            this.xEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.25, 0.35, 0.2, 0.1]) ;
            this.yEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.25, 0.2, 0.2, 0.1]) ;
            this.zEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.25, 0.05, 0.2, 0.1]) ;
            
            this.updatePopups() ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
        
%         function updateTable(this)
%             d = this.control.get('correctionMatrix') ;
%             this.correctionTable.set('Data', d) ;
%             this.correctionTable.Position(3) = this.correctionTable.Extent(3);
%             this.correctionTable.Position(4) = this.correctionTable.Extent(4);
%         end
        
        function updateTransfoParam(this)
            [yaw, pitch, roll, xTrans, yTrans, zTrans] = this.control.getTransfoParam() ;
            
            set(this.yawEdit, 'String', num2str(yaw)) ;
            set(this.pitchEdit, 'String', num2str(pitch)) ;
            set(this.rollEdit, 'String', num2str(roll)) ;
            set(this.xEdit, 'String', num2str(xTrans)) ;
            set(this.yEdit, 'String', num2str(yTrans)) ;
            set(this.zEdit, 'String', num2str(zTrans)) ;
        end
    end
    
    methods (Access = private)
        function updatePopups(this)
            CTNames = this.control.getCTNames() ;
            
            if ~isempty(CTNames)
                this.CTPopup.set('String', CTNames) ;
            end
        end
        
        function saveCallback(this, source, event)
            items = get(this.CTPopup, 'String') ;
            index_selected = get(this.CTPopup, 'Value') ;
            CTName = items{index_selected} ;          
            this.control.set('CTNameForSaving', CTName) ;
            
            this.control.set('newName', this.newNameEdit.get('String')) ;
            
            this.control.saveRegistration() ;
        end
    end
end
