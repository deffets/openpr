
classdef RegistrationPanel < MainPanel
    properties (Access = private)
        control ;

        transRegistrationPanel ;
        allDOFRegPanel ;
        DOF5RegPnl;
        
        tabGroup ;
        tab6DOF ;
        tab2DOF ;
    end
    
    methods (Access = public)
        function this = RegistrationPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.tabGroup = uitabgroup(this.mainPanel, 'Position', [0 0 1 1]) ;
            
            this.tab2DOF = uitab(this.tabGroup, 'Title', '2 DOF', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'tab2DOF') ;
            this.transRegistrationPanel = TransRegistrationPanel(this.tab2DOF, this.control.getTransRegistrationControl()) ;
            
            this.tab6DOF = uitab(this.tabGroup, 'Title', '6 DOF', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'tab6DOF') ;
            this.allDOFRegPanel = AllDOFRegPanel(this.tab6DOF, this.control.getAllDOFRegControl()) ;
            
            tab5DOF = uitab(this.tabGroup, 'Title', '5 DOF', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'tab5DOF') ;
            this.DOF5RegPnl = DOF5RegPanel(tab5DOF, this.control.get5DOFRegControl()) ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
            this.transRegistrationPanel.setVisible(visible) ;
            this.allDOFRegPanel.setVisible(visible) ;
        end
    end
    
    methods (Access = private)
        function panelCallback(this, source, event)
            switch source.Tag
                case 'tab2DOF'
                    this.transRegistrationPanel.setVisible('on') ;
                    this.allDOFRegPanel.setVisible('off') ;
                    this.DOF5RegPnl.setVisible('off') ;
                case 'tab6DOF'
                    this.transRegistrationPanel.setVisible('off') ;
                    this.allDOFRegPanel.setVisible('on') ;
                    this.DOF5RegPnl.setVisible('off') ;
                case 'tab5DOF'
                    this.transRegistrationPanel.setVisible('off') ;
                    this.allDOFRegPanel.setVisible('off') ;
                    this.DOF5RegPnl.setVisible('on') ;
            end
        end
    end
end
