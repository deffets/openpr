
classdef RegistrationControl < GenericControl
    properties (Access = private)
        registrationSelectionControl ;
        transRegistrationControl ;
        allDOFRegControl ;
        DOF5RegCtrl;
    end
    
    methods (Access = public)
        function this = RegistrationControl(model)
            this@GenericControl(model) ;
            
            transRegistrationModel = TransRegistrationModel(this.model.getRegguiHObject()) ;
            this.transRegistrationControl = TransRegistrationControl(transRegistrationModel) ;
            
            allDOFRegModel = AllDOFRegModel(this.model.getRegguiHObject()) ;
            this.allDOFRegControl = AllDOFRegControl(allDOFRegModel) ;
            
            allDOFRegModel = AllDOFRegModel(this.model.getRegguiHObject()) ;
            this.allDOFRegControl = AllDOFRegControl(allDOFRegModel) ;
            
            DOF5RegMdl = DOF5RegModel(this.model.getRegguiHObject()) ;
            this.DOF5RegCtrl = DOF5RegControl(DOF5RegMdl) ;
        end
        
        function registrationTypes = getRegistrationTypes(this)
            registrationTypes = this.model.getRegistrationTypes() ;
        end
        
        function control = getTransRegistrationControl(this)
            control = this.transRegistrationControl ;
        end
        
        function control = getAllDOFRegControl(this)
            control = this.allDOFRegControl ;
        end
        
        function control = get5DOFRegControl(this)
            control = this.DOF5RegCtrl;
        end
    end
end
