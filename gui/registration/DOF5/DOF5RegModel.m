
classdef DOF5RegModel < AllDOFRegModel
    properties (SetObservable)
    end
    
    properties (Access = private)
    end
    
    methods (Access = public)
        function this = DOF5RegModel(regguiHObject)
            this@AllDOFRegModel(regguiHObject) ;
        end
        
        function startNew5DOFRegistration(this, fastMode)
            maxIter = 7 ;
            costMethod = 'composed' ;

            handles = guidata(this.regguiHObject) ;
            if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                handles.progressControl.set('runningMsg', '5 DOF Registration') ;
                handles.progressControl.set('progressMsg', 'Loading data') ;
                handles.progressControl.set('n', 0) ;
            end
            
            energy = 210 ;
            
            hu = [];
            for i=1:length(handles.images.name)
                if(strcmp(handles.images.name{i}, this.CTName))
                    hu = handles.images.data{i};
                    huInfo = handles.images.info{i};
                end
            end
            if(isempty(hu))
                error('Input image not found in the current list')
            end

            calibration = [] ;
            for i=1:length(handles.mydata.name)
                if(strcmp(handles.mydata.name{i}, this.calibrationName))
                    calibration = handles.mydata.data{i};
                end
            end
            if(isempty(calibration))
                error('Calibration curve not found in the current list')
            end

            ref = [] ;
            for i=1:length(handles.mydata.name)
                if(strcmp(handles.mydata.name{i}, this.referenceName))
                    ref = handles.mydata.data{i} ;
                    depth = handles.mydata.info{i}.depth ;
                end
            end
            if(isempty(ref))        
                error('Reference IDD not found in the current list')
            end
            
            if(isempty(calibration))
                error('Calibration curve not found in the current list')
            end
            
            if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                handles.progressControl.set('progressMsg', 'Computing SPR') ;
            end
            if isfield(calibration, 'RS_materials') ;
                density = HU_to_density(hu, calibration.model) ;

                spr = density2spr_RS(density,  energy, calibration.RS_materials) ;
            else
                c = calibration.model.Stopping_Power' ;
                
                spr = interp1(c(1, :), c(2, :), hu, 'linear','extrap') ;
                spr(spr<0) = 0 ;
            end


            spr = single(spr) ;
            sprSpacing = huInfo.Spacing ;
            
            braggImage270 = handles2BraggImage(this.PRName270, handles) ;

            sigma = this.sigma;
            
            this.wet1 = spr2totalWET(spr, sprSpacing, 0, 270) ;
            this.wet2 = spr2totalWET(spr, sprSpacing, 0, 0) ;
            this.wetReg1 = spr2totalWET(spr, sprSpacing, 0, 270) ;
            this.wetReg2 = spr2totalWET(spr, sprSpacing, 0, 0) ;
            
            this.yaw = 0 ;
            this.pitch = 0 ;
            this.roll = 0 ;
            this.correctionMatrix = eye(4) ;

            
            angle270 = 0 ;
            angle0 = 0 ;
            rollAngle = 0 ;

            tMat270 = eye(4) ;
            tMat0 = eye(4) ;
            tMatRoll = eye(4) ;
            transMat = eye(4) ;

            maxAngle270 = 1 ;
            maxAngle0 = 1 ;
            maxRollAngle = 1 ;
            maxTrans = 2 ;

            angle270Prev = angle270 ;
            angle0Prev = angle0 ;
            rollAnglePrev = rollAngle ;
            transPrev = 0 ;


            preTransfo = tMat270*tMat0*tMatRoll ;
            transMat = submTransRegistrationComp(spr, sprSpacing, braggImage270, [], ref, depth, sigma, 5, maxTrans, transMat(4, 1:3), preTransfo, costMethod) ;

            this.correctionMatrix = transMat;
            
            disp(['Determined 0: ' num2str([angle270 angle0 rollAngle transMat(4, 1) transMat(4, 2) transMat(4, 3)])])

            for i=1 : maxIter
                    disp(['Iteration: ' num2str(i)])
                    
                    preTransfo = eye(4) ;
                    postTransfo = tMat0*tMatRoll*transMat ;
                    [tMat270, angle270] = braggRegistrationComp(spr, sprSpacing, braggImage270, ref, depth, sigma, 270, 7, maxAngle270, angle270, preTransfo, postTransfo, 270, costMethod) ;
                    maxAngle270 = angle270 ;
                    
                    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                        handles.progressControl.set('progressMsg', 'Computing Yaw') ;
                        handles.progressControl.set('n', i/(maxIter*4+1)) ;
                    end
                    this.yaw = angle270 ;
                    this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat;

                    preTransfo = tMat270 ;
                    postTransfo = tMatRoll*transMat ;
                    [tMat0, angle0] = braggRegistrationComp(spr, sprSpacing, braggImage270, ref, depth, sigma, 270, 7, maxAngle0, angle0, preTransfo, postTransfo, 0, costMethod) ;
                    maxAngle0 = angle0 ;
                    
                    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                        handles.progressControl.set('progressMsg', 'Computing Pitch') ;
                        handles.progressControl.set('n', (i+1)/(maxIter*4+1)) ;
                    end
                    this.pitch = angle0 ;
                    this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat;

                    preTransfo = tMat270*tMat0 ;
                    postTransfo = transMat ;
                    [tMatRoll, rollAngle] = braggRollIterComp(spr, sprSpacing, braggImage270, [], ref, depth, sigma, 7, maxRollAngle, rollAngle, preTransfo, postTransfo, costMethod) ;
                    maxRollAngle = rollAngle ;
                    
                    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                        handles.progressControl.set('progressMsg', 'Computing Roll') ;
                        handles.progressControl.set('n', (i+2)/(maxIter*4+1)) ;
                    end
                    this.roll = rollAngle ;
                    this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat;

                    preTransfo = tMat270*tMat0*tMatRoll ;
                    transMat = submTransRegistrationComp(spr, sprSpacing, braggImage270, [], ref, depth, sigma, 7, maxTrans, transMat(4, 1:3), preTransfo, costMethod) ;
                    maxTrans = abs(transPrev - max(abs(transMat(4, 1:3)))) ;
                    
                    if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                        handles.progressControl.set('progressMsg', 'Computing Translations') ;
                        handles.progressControl.set('n', (i+3)/(maxIter*4+1)) ;
                    end
                    this.correctionMatrix = tMat270*tMat0*tMatRoll*transMat;

                    disp(['Determined: ' num2str([angle270 angle0 rollAngle transMat(4, 1) transMat(4, 2) transMat(4, 3)])])
                    
                    if (abs(angle270-angle270Prev)<0.05 && abs(angle0-angle0Prev)<0.05) && abs(rollAngle-rollAnglePrev)<0.05
                        break
                    end

                    angle270Prev = angle270 ;
                    angle0Prev = angle0 ;
                    rollAnglePrev = rollAngle ;
                    transPrev = max(abs(transMat(4, 1:3))) ;
            end
            
            tform = affine3d(this.correctionMatrix) ;
            imR = imref3d(size(spr), [0 size(spr, 2)]-size(spr, 2)/2, [0 size(spr, 1)]-size(spr, 1)/2, [0 size(spr, 3)]-size(spr, 3)/2) ;
            spr = imwarp(spr, imR, tform, 'OutputView', imR, 'FillValues', 0) ;

            this.wetReg1 = spr2totalWET(spr, sprSpacing, 0, 270) ;
            this.wetReg2 = spr2totalWET(spr, sprSpacing, 0, 0) ;


            if isfield(handles, 'progressControl') && ~isempty(handles.progressControl)
                handles.progressControl.deleteProgress() ;
            end
        end
    end    
end
