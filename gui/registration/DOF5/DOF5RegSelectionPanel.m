
classdef DOF5RegSelectionPanel < MainPanel
    properties (Access = private)
        control ;
        
        CTText ;
        referenceText ;
        calibrationText ;
        sigmaText ;
        
        sigmaEdit ;
        
        CTPopup ;
        referencePopup ;
        curvePopup ;
        PR270Popup ;
        
        rFast ;
        rAccurate ;
        goButton ;
    end
    
    methods (Access = public)
        function this = DOF5RegSelectionPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            p = 0.15+0.15+0.075+0.15+0.075+0.15 ;
            this.CTText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'CT', 'Units', 'normalized', 'Position', [0.01, p, 0.1, 0.15]) ;
            this.referenceText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Reference', 'Units', 'normalized', 'Position', [0.15, p, 0.1, 0.15]) ;
            this.calibrationText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Calibration curve', 'Units', 'normalized', 'Position', [0.3, p, 0.1, 0.15]) ;
            this.sigmaText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Sigma', 'Units', 'normalized', 'Position', [0.45, p, 0.05, 0.15]) ;
            
            p = 0.15+0.15+0.075+0.15+0.075 ;
            this.sigmaEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', this.control.get('sigma'), 'Units', 'normalized', 'Position', [0.45, p, 0.05, 0.15]) ;
            
            this.CTPopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, p, 0.1, 0.15]) ;
            this.referencePopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.15, p, 0.1, 0.15]) ;
            this.curvePopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.3, p, 0.1, 0.15]) ;
            
            p = 0.15+0.15 ;
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'PR 270 deg.', 'Units', 'normalized', 'Position', [0.01, p, 0.1, 0.15]) ;
            
            this.PR270Popup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.15]) ;            
            
            bg = uibuttongroup('Parent', this.mainPanel, 'Title', 'Mode', 'Units', 'normalized', 'Position', [0.875-0.15, 0.1, 0.1, 0.8]);
            this.rFast = uicontrol('Parent', bg, 'Style', 'radiobutton', 'String', 'Fast', 'Units', 'normalized', 'Position', [0.1 0.5 0.8 0.4]);
            this.rAccurate = uicontrol('Parent', bg, 'Style', 'radiobutton', 'String', 'Accurate', 'Units', 'normalized', 'Position', [0.1 0.1 0.8 0.4]);


            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.4, 0.1, 0.2], 'Callback', @this.startRegistration) ;
            
            this.updatePopups() ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private) 
        function updatePopups(this)
            CTNames = this.control.getCTNames() ;
            referenceNames = this.control.getReferenceNames() ;
            curvesNames = this.control.getCurvesNames() ;
            PRNames = this.control.getPRNames() ;
            
            if ~isempty(CTNames)
                this.CTPopup.set('String', CTNames) ;
            end
            
            if ~isempty(referenceNames)
                this.referencePopup.set('String', referenceNames) ;
            end
            
            if ~isempty(curvesNames)
                this.curvePopup.set('String', curvesNames) ;
            end
            
            if ~isempty(PRNames)
                this.PR270Popup.set('String', PRNames) ;
%                 this.PR0Popup.set('String', PRNames) ;
            end
        end
        
        function startRegistration(this, source, event)
            items = get(this.CTPopup, 'String') ;
            index_selected = get(this.CTPopup, 'Value') ;
            CTName = items{index_selected} ;          
            this.control.set('CTName', CTName) ;
            
            items = get(this.referencePopup, 'String') ;
            index_selected = get(this.referencePopup, 'Value') ;
            referenceName = items{index_selected} ;
            this.control.set('referenceName', referenceName) ;
            
            items = get(this.curvePopup, 'String') ;
            index_selected = get(this.curvePopup, 'Value') ;
            curveName = items{index_selected} ;
            this.control.set('calibrationName', curveName) ;
            
            items = get(this.PR270Popup, 'String') ;
            index_selected = get(this.PR270Popup, 'Value') ;
            PRName = items{index_selected} ;          
            this.control.set('PRName270', PRName) ;
            
            sigma = this.sigmaEdit.get('String');
            this.control.set('sigma', str2double(sigma)) ;
            
    
            if (get(this.rAccurate, 'Value') == 1)
                fastMode = 'off' ;
            else
                fastMode = 'on' ;
            end
            
            this.control.startNew5DOFRegistration(fastMode) ;
        end
    end
end
