
classdef DOF5RegControl < AllDOFRegControl
    properties(Access = private)
    end

    methods (Access = public)
        function this = DOF5RegControl(model)
            this@AllDOFRegControl(model) ;
        end
        
        function set(this, property, value)
            set@AllDOFRegControl(this, property, value) ;
        end
        
        function value = get(this, property)
            value = get@AllDOFRegControl(this, property) ;
        end
        
        function startNew5DOFRegistration(this, fastMode)
            this.model.startNew5DOFRegistration(fastMode) ;
        end
    end
    
    methods (Access = private)
    end
end
