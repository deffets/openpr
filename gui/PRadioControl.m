
classdef PRadioControl < handle
    % Controler of the Proton Radiography interface
    %   See also PRadioView PRadioModel
    
    properties (Access = private)
        regguiHObject ;
        view ;
        model ;
        
        simulationControl ;
        simulationModel ;
        
        registrationModel ;
        registrationControl ;
        
        dataModel ;
        dataControl ;
        
        preprocessModel ;
        preprocessControl ;
        
        viewBraggModel ;
        viewBraggControl ;
        
        viewModel ;
        viewControl ;
        
        %!RELEASE
        optimizationControl ;
        %RELEASE
        
        exportControl ;
        progressControl ;
        helpControl;
        
        dataEditControl;
    end
    
    methods (Access = public)
        function obj = PRadioControl(regguiHObject)
            % GenericModel Creates a controler that can be passed to any
            % object inherited from PRadioView
            %   PRadioControl(regguiHObject) where regguiHObject is the object handle of the Reggui interface
            
            obj.regguiHObject = regguiHObject ;
          
            obj.model = PRadioModel(obj.regguiHObject) ;

            obj.dataModel = LoadModel(obj.regguiHObject) ;
            obj.dataControl = LoadControl(obj.dataModel, obj.regguiHObject) ;
            
            obj.preprocessModel = PreprocessModel(obj.regguiHObject) ;
            obj.preprocessControl = PreprocessControl(obj.preprocessModel, obj.regguiHObject) ;
            
            obj.registrationModel = RegistrationModel(obj.regguiHObject) ;
            obj.registrationControl = RegistrationControl(obj.registrationModel) ;
            
            obj.simulationModel = SimulationModel(obj.regguiHObject) ;
            obj.simulationControl = SimulationControl(obj.simulationModel) ;

            viewModel = ViewMainModel(obj.regguiHObject) ;
            obj.viewControl = ViewMainControl(viewModel) ;
            
            dataEditModel = DataEditModel(obj.regguiHObject) ;
            obj.dataEditControl = DataEditControl(dataEditModel) ;
            
            %!RELEASE
            optimizationModel = OptimizationModel(obj.regguiHObject) ;
            obj.optimizationControl = OptimizationControl(optimizationModel) ;

            exportModel = ExportModel(obj.regguiHObject) ;
            obj.exportControl = ExportControl(exportModel) ;
            %RELEASE
            
            helpModel = HelpModel(obj.regguiHObject);
            obj.helpControl = HelpControl(helpModel);
            
            obj.view = PRadioView(obj) ;
            obj.view.dataCallback() ;
        end

        function simulationCtrl = getSimulationControl(this)
            simulationCtrl = this.simulationControl ;
        end
        
        function registrationCtrl = getRegistrationControl(this)
            registrationCtrl = this.registrationControl ;
        end
        
        function viewCtrl = getViewControl(this)
            viewCtrl = this.viewControl ;
        end
        
        function dataCtrl = getDataControl(this)
            dataCtrl = this.dataControl ;
        end
        
        function preprocessCtrl = getPreprocessControl(this)
            preprocessCtrl = this.preprocessControl ;
        end
        
        %!RELEASE
        function optimizationControl = getOptimizationControl(this)
            optimizationControl = this.optimizationControl ;
        end
        
        function ctrl = getExportControl(this)
            ctrl = this.exportControl ;
        end
        %RELEASE
        
        function set(this, varargin)
            switch varargin{1}
                case 'n'
                    this.progressControl.set(n, varargin{2}) ;
            end            
        end
        
        function value = get(this, varargin)
            % get Get the value of a specific property
            %   get(property) Get the value of a specific property. See
            %   PRadioModel for all properties.
            %   See also PRadioModel
            
            property = varargin{1};

            switch property
                case 'simulationControl'
                    value = this.simulationControl;
                case 'registrationControl'
                    value = this.registrationControl;
                case 'viewControl'
                    value = this.viewControl;
                case 'dataControl'
                    value = this.dataControl;
                case 'preprocessControl'
                    value = this.preprocessControl;
                %!RELEASE
                case 'optimizationControl'
                    value = this.optimizationControl;
                case 'exportControl'
                    value = this.exportControl;
                %RELEASE
                case 'helpControl'
                    value = this.helpControl;
                case 'dataEditControl'
                    value = this.dataEditControl;
                otherwise
                    value = this.model.get(varargin{:}) ;
            end
        end
        
        function deleteProgress(this)
            this.progressControl.deleteProgress() ;
        end
    end
    
    methods (Access = private)
        
    end

end
