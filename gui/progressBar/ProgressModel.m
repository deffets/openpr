

classdef ProgressModel < handle
    properties (SetObservable)
        n ;
        runningMsg ;
        progressMsg ;
    end
    
    methods (Access = public)
        function this = ProgressModel()
            this.n = 0 ;
            this.runningMsg = [] ;
            this.progressMsg = [] ;
        end
        
        function set(this, property, value)
            switch property
                case 'n'
                    this.n = value ;
                case 'runningMsg'
                    this.runningMsg = value ;
                case 'progressMsg'
                    this.progressMsg = value ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'n'
                    value = this.n ;
                case 'runningMsg'
                    value = this.runningMsg ;
                case 'progressMsg'
                    value = this.progressMsg ;
            end
        end
        
        function deleteProgress(this)
            this.n = 0 ;
            this.runningMsg = [] ;
            this.progressMsg = [] ;
        end
    end
end
