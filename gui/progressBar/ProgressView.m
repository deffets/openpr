
classdef ProgressView < MainPanel
    properties (Access = private)
        control ;
        
        msg1 ;
        msg2 ;
        s1 ;
        hBar ;
        im ;
    end
    
    methods (Access = public)
        function this = ProgressView(f, control)
            this@MainPanel(f) ;
            
            this.control = control ;
            
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Running: ', 'HorizontalAlignment', 'left', 'Units', 'normalized', 'Position', [0.01, 0.5, 0.1, 0.49]) ;
            this.msg1 = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'HorizontalAlignment', 'left', 'Units', 'normalized', 'Position', [0.11, 0.5, 0.48, 0.49]) ;
            this.msg2 = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'HorizontalAlignment', 'left', 'Units', 'normalized', 'Position', [0.01, 0.01, 0.45, 0.49]) ;
            
            this.s1 = axes('Units', 'Normalized', 'Position', [0.6 0 0.5 1], 'Parent', this.mainPanel) ;
            axis off ;
            
            this.hBar = [] ;
            
            this.control.set('view', this) ;
        end
        
        function updateProgressImage(this)
            n = this.control.get('n') ;
            
            this.im = ones(1, 100, 3) ;
            this.im(1, 1:floor(n*100), [1 3]) = 0 ;
            
            subplot(this.s1) ;
            axis off ;
            if isempty(this.hBar)
                this.hBar = image(this.im, 'Parent', this.s1) ;
            end
            
            set(this.hBar, 'CData', this.im) ;
            drawnow ;
            
            runningMsg = this.control.get('runningMsg') ;
            this.msg1.set('String', runningMsg) ;
            
            progressMsg = this.control.get('progressMsg') ;
            this.msg2.set('String', progressMsg) ;
        end
        
        function delete(this)
        end
        
        function deleteImage(this)
            delete(this.hBar) ;
            this.hBar = [] ;
        end
    end
end
