
classdef ProgressControl < handle
    properties (SetObservable)
        model ;
        view ;
    end
    
    methods (Access = public)
        function this = ProgressControl(model)
            this.model = model ;
            this.view = [] ;
            
            addlistener(this.model, 'n', 'PostSet', @this.nListener) ;
            addlistener(this.model, 'runningMsg', 'PostSet', @this.nListener) ;
            addlistener(this.model, 'progressMsg', 'PostSet', @this.nListener) ;
        end
        
        function set(this, property, value)
            switch property
                case 'view'
                    this.view = value ;
                otherwise
                    this.model.set(property, value) ;
            end
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end

        function deleteProgress(this)
            this.model.deleteProgress() ;
            
            if ~isempty(this.view)
                this.view.deleteImage() ;
            end
        end
        
        function delete(this)
        end
    end
    
    methods (Access = private)
        function nListener(this, source, event)
            if ~isempty(this.view)
                this.view.updateProgressImage() ;
            end
        end
    end
end
