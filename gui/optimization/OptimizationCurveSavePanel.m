
classdef OptimizationCurveSavePanel < MainPanel
    properties (Access = private)
        control ;
        
        newNameEdit ;
        saveButton ;
    end
    
    methods (Access = public)
        function this = OptimizationCurveSavePanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.newNameEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.01, 0.4, 0.6, 0.2]) ;         
            this.saveButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Save', 'Units', 'normalized', 'Position', [0.68, 0.4, 0.3, 0.2], 'Callback', @this.saveCallback) ;
            
            this.control.set('curveSavePanel', this) ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
  
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private) 
        function saveCallback(this, source, event)
            this.control.set('newName', this.newNameEdit.get('String')) ;
            this.control.saveCurve() ;
        end
    end
end
