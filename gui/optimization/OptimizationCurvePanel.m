
classdef OptimizationCurvePanel < MainPanel
    properties (Access = private)
        control ;
        subplot1 ;
        
        curveSelectionTab ;
        curveSaveTab ;
        
        curveSelectionPanel ;
        curveSavePanel ;
        
        initialCurve ;
        currentCurve ;
        notOptimizedPoints ;
        optimizedPoints ;
        voxelsCurve ;
    end
    
    methods (Access = public)
        function this = OptimizationCurvePanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.control.set('viewPanel', this) ;
            
            this.initialCurve = [] ;
            this.currentCurve = [] ;
            this.notOptimizedPoints = [] ;
            this.optimizedPoints = [] ;
            this.subplot1 = [] ;
            
            p = uitabgroup('Parent', this.mainPanel, 'Position', [0 0 1 0.4]) ;
            this.curveSelectionTab = uitab(p, 'Title', 'Options', 'ButtonDownFcn', @this.optionsCallback) ;
            this.curveSelectionPanel = OptimizationCurveSelectionPanel(this.curveSelectionTab, this.control) ;
            
            this.curveSaveTab = uitab(p, 'Title', 'Save', 'ButtonDownFcn', @this.saveCallback) ;
            this.curveSavePanel = OptimizationCurveSavePanel(this.curveSaveTab, this.control) ;
        end
        
        function updateVoxelsCurve(this)
            if ~isempty(this.voxelsCurve)
                delete(this.voxelsCurve)
                this.voxelsCurve = [] ;
            end            
            
            curve = this.control.get('initialCurve') ;
            
            if isempty(curve)
                error('Calibration curve missing')
            end
                
            huRef = curve(1, :) ;

            if ~isempty(this.subplot1)
                spots270 = this.control.get('selectedSpots270') ;
                spots0 = this.control.get('selectedSpots0') ;
                
                N2 = zeros(size(spots270, 1)+size(spots0, 1), length(huRef)) ;
                
                i = 1 ;
                while i<=size(spots270, 1)
                    voxels = this.control.get('voxels', [spots270(i, 1), spots270(i, 2)], 270) ;
                    N = hu2weight(reshape(voxels, 1, length(voxels)), huRef) ;
                    N2(i, :) = log(N+1)*3/max(log(N)) ;
                    i = i+1 ;
                end
                
                for j=1:size(spots0, 1)
                    voxels = this.control.get('voxels', [spots0(j, 1), spots0(j, 2)], 0) ;
                    N = hu2weight(reshape(voxels, 1, length(voxels)), huRef) ;
                    N2(i+j, :) = log(N+1)*3/max(log(N)) ;
                end

                subplot(this.subplot1)
                hold on
                this.voxelsCurve = plot(repmat(huRef, size(N2, 1), 1)', N2', '--black') ;
                set(this.voxelsCurve, 'ButtonDownFcn', @this.mouseInSubplot1) ;
                hold off
            end
            
            
        end
        
        function updateInitialCurve(this)
            curve = this.control.get('initialCurve') ;
            
            if ~isempty(curve)
                this.subplot1 = subplot(2, 1, 1, 'Parent', this.mainPanel) ;
                plot(curve(1, :), curve(2, :), '-b') ;
                hold on
                this.initialCurve = plot(curve(1, :), curve(2, :), '*') ;
                set(this.initialCurve, 'ButtonDownFcn', @this.mouseInSubplot1) ;
                xlabel('HU')
                ylabel('SPR')
            end
        end
        
        function updateCurrentCurve(this)
             if ~isempty(this.currentCurve)
                delete(this.currentCurve)
             end
            
            curve = this.control.get('currentCurve') ;
            if ~isempty(curve) && ~isempty(this.subplot1)
                subplot(this.subplot1)
                hold on
                this.currentCurve = plot(curve(1, :), curve(2, :), 'red') ;
                set(this.currentCurve, 'ButtonDownFcn', @this.mouseInSubplot1) ;
                hold off
            end
        end
        
        function updateOptimizedPoints(this)
            if ~isempty(this.optimizedPoints)
                delete(this.optimizedPoints)
            end
            
            curve = this.control.get('optimizedPoints') ;
            
            if ~isempty(curve) && ~isempty(this.subplot1)
                subplot(this.subplot1)
                hold on
                this.optimizedPoints = plot(curve(1, :), curve(2, :), 'or') ;
                set(this.optimizedPoints, 'ButtonDownFcn', @this.mouseInSubplot1) ;
                hold off
            end
        end
        
        function updateNotOptimizedPoints(this)
            if ~isempty(this.notOptimizedPoints)
                delete(this.notOptimizedPoints)
            end
            
            curve = this.control.get('notOptimizedPoints') ;
            
            if ~isempty(curve) && ~isempty(this.subplot1)
                subplot(this.subplot1)
                hold on
                if (size(curve, 1)==1  && size(curve, 2)==2) || (size(curve, 2)==1  && size(curve, 1)==2)
                    this.notOptimizedPoints = plot(curve(1), curve(2), 'o', 'MarkerEdgeColor', 'black', 'MarkerSize', 8) ;
                else
                    this.notOptimizedPoints = plot(curve(1, :), curve(2, :), 'o', 'MarkerEdgeColor', 'black', 'MarkerSize', 8) ;
                end
                set(this.notOptimizedPoints, 'ButtonDownFcn', @this.mouseInSubplot1) ;
                hold off
            end
        end
    end
    
    methods (Access = private)
        function mouseInSubplot1(this, source, event)
            C = get (gca, 'CurrentPoint') ;
            pos1 = C(1, 1)-1 ;
            pos2 = C(1, 2)-1 ;
            
            this.control.set('calibrationPoint', pos1) ;
        end
        
        function optionsCallback(this, source, event)
            this.clearDisplay() ;
            this.curveSelectionPanel.setVisible('on') ;
        end
        
        function saveCallback(this, source, event)
            this.clearDisplay() ;
            this.curveSavePanel.setVisible('on') ;
        end
        
        function clearDisplay(this)
            this.curveSelectionPanel.setVisible('off') ;
            this.curveSavePanel.setVisible('off') ;
        end
    end
end
