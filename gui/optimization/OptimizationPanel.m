
classdef OptimizationPanel < MainPanel
    properties (Access = private)
        control ;
        
        selectionPanel ;
        viewPanel ;
        curvePanel ;
    end
    
    methods (Access = public)
        function this = OptimizationPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            p = uipanel('Parent', this.mainPanel, 'Position', [0 0.85 1 0.14], 'Title', 'Parameters') ;
            this.selectionPanel = OptimizationSelectionPanel(p, this.control) ;
            
            p = uipanel('Parent', this.mainPanel, 'Position', [0 0 0.6 0.84], 'Title', 'Beamlets') ;
            this.viewPanel = OptimizationWETPanel(p, this.control) ;
            
            p = uipanel('Parent', this.mainPanel, 'Position', [0.61 0 0.39 0.84], 'Title', 'Curve') ;
            this.curvePanel = OptimizationCurvePanel(p, this.control) ;
            
            this.control.set('viewPanel', this.viewPanel) ;
            this.control.set('curvePanel', this.curvePanel) ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.selectionPanel.setVisible(visible) ;
                
                set( gcf, 'toolbar', 'figure' )
            else
                set( gcf, 'toolbar', 'none' )
            end
            setVisible@MainPanel(this, visible) ;
        end
    end
end
