
classdef OptimizationControl < GenericControl
    properties (Access = private)
        viewPanel ;
        curvePanel ;
        curveSelectionPanel ;
        curveSavePanel ;
    end
    
    methods (Access = public)
        function this = OptimizationControl(model)
            this@GenericControl(model) ;
            
            addlistener(this.model, 'wet1', 'PostSet', @this.wetListener) ;
            addlistener(this.model, 'wet2', 'PostSet', @this.wetListener) ;
            addlistener(this.model, 'spots270', 'PostSet', @this.spotsListener) ;
            addlistener(this.model, 'spots0', 'PostSet', @this.spotsListener) ;
            addlistener(this.model, 'selectedSpots270', 'PostSet', @this.selectedSpotsListener) ;
            addlistener(this.model, 'selectedSpots0', 'PostSet', @this.selectedSpotsListener) ;
            addlistener(this.model, 'currentCurve', 'PostSet', @this.selectedSpotsListener) ;
            addlistener(this.model, 'notOptimizedPoints', 'PostSet', @this.selectedSpotsListener) ;
        end
        
        function value = get(this, property, value, view)
            if nargin<3
                value = this.model.get(property) ;
            else
                value = this.model.get(property, value, view) ;
            end
        end
        
        function set(this, property, value)
            switch property
                case 'viewPanel'
                    this.viewPanel = value ;
                case 'curvePanel'
                    this.curvePanel = value ;
                case 'curveSelectionPanel'
                    this.curveSelectionPanel = value ;
                case 'curveSavePanel'
                    this.curveSavePanel = value ;
                otherwise
                    this.model.set(property, value) ;
            end
        end
        
        function initWet(this)
            this.model.initWet() ;
            this.model.initSpots() ;
            this.model.initCurve() ;
            this.curvePanel.updateInitialCurve() ;
        end
        
        function startOptimization(this)
            this.model.startOptimization() ;
        end
        
        function saveCurve(this)
            this.model.saveCurve() ;
        end
    end
    
    methods (Access = private)
        function wetListener(this, source, event)
            this.viewPanel.updateWetImages() ;
        end
        
        function spotsListener(this, source, event)
            this.viewPanel.updateSpots() ;
        end
        
        function selectedSpotsListener(this, source, event)
            switch source.Name
                case 'selectedSpots270'
                    this.viewPanel.updateSelectedSpots() ;
                    this.curvePanel.updateVoxelsCurve() ;
                case 'selectedSpots0'
                    this.viewPanel.updateSelectedSpots() ;
                    this.curvePanel.updateVoxelsCurve() ;
                case 'currentCurve'
                    try
                        this.curvePanel.updateCurrentCurve() ;
    %                     this.curvePanel.updateVoxelsCurve() ;
                        this.curvePanel.updateOptimizedPoints() ;
                    catch
                    end
                case 'notOptimizedPoints'
                    this.curvePanel.updateNotOptimizedPoints() ;
            end
        end
    end
end
