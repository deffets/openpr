
classdef OptimizationCurveSelectionPanel < MainPanel
    properties (Access = private)
        control ;
        
        goButton ;
    end
    
    methods (Access = public)
        function this = OptimizationCurveSelectionPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
%             bg1 = uibuttongroup(this.mainPanel, 'Title', 'Selected Points Type', 'Units', 'Normalized', 'Position', [0.01 0.7 0.59 0.2]) ;
%             r1 = uicontrol(bg1, 'Style', 'radiobutton', 'String', 'Fixed', 'Units', 'Normalized', 'Position', [0 0 0.3 1]) ;
%             r2 = uicontrol(bg1, 'Style', 'radiobutton', 'String', 'To optimize', 'Units', 'Normalized', 'Position', [0.3 0 0.3 1]) ;
%             
%             bg2 = uibuttongroup(this.mainPanel, 'Title', 'Action', 'Units', 'Normalized', 'Position', [0.01 0.4 0.59 0.2]) ;
%             r3 = uicontrol(bg2, 'Style', 'radiobutton', 'String', 'Add', 'Units', 'Normalized', 'Position', [0 0 0.3 1]) ;
%             r4 = uicontrol(bg2, 'Style', 'radiobutton', 'String', 'Remove', 'Units', 'Normalized', 'Position', [0.3 0 0.3 1]) ;
            
            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.68, 0.4, 0.3, 0.2], 'Callback', @this.go) ;
            
            this.control.set('curveSelectionPanel', this) ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
  
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private) 
        
        
        function go(this, source, event)
            this.control.startOptimization() ;
        end
    end
end
