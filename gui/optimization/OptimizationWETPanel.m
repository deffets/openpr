
classdef OptimizationWETPanel < MainPanel
    properties (Access = private)
        control ;
        subplot1 ;
        subplot2 ;
        selectedSpotsH270 ;
        selectedSpotsH0 ;
        spots270H ;
        spots0H ;
    end
    
    methods (Access = public)
        function this = OptimizationWETPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.control.set('viewPanel', this) ;
            
            this.spots270H = [] ;
            this.spots0H = [] ;
        end
        
        function updateWetImages(this)
            wet1 = this.control.get('wet1') ;
            wet2 = this.control.get('wet2') ;
            
            if ~isempty(wet1)
                this.subplot1 = subplot(1, 2, 1, 'Parent', this.mainPanel) ;
                imh = imshow(mat2gray(wet1)) ;
                set(imh, 'ButtonDownFcn', @this.mouseInSubplot1) ;
                axis equal off
            end
            
            if ~isempty(wet2)
                this.subplot2 = subplot(1, 2, 2, 'Parent', this.mainPanel) ;
                imh = imshow(mat2gray(wet2)) ;
                set(imh, 'ButtonDownFcn', @this.mouseInSubplot2) ;
                axis equal off
            end
        end
        
        function updateSpots(this)
            spots270 = this.control.get('spots270') ;
            spots0 = this.control.get('spots0') ;
            
            if ~isempty(spots270)
                if ~isempty(this.spots270H)
                    delete(this.spots270H)
                end
                
                subplot(this.subplot1)
                
                hold on
                this.spots270H = plot(spots270(:, 2)+1, spots270(:, 1)+1, '.', 'MarkerEdgeColor', 'yellow') ;
                set(this.spots270H, 'ButtonDownFcn', @this.mouseInSubplot1) ;
                hold off
            end
            
            if ~isempty(spots0)
                if ~isempty(this.spots0H)
                    delete(this.spots0H)
                end
                
                subplot(this.subplot2)
                
                hold on
                this.spots0H = plot(spots0(:, 2)+1, spots0(:, 1)+1, '.', 'MarkerEdgeColor', 'yellow') ;
                set(this.spots0H, 'ButtonDownFcn', @this.mouseInSubplot2) ;
                hold off
            end
        end
        
        function updateSelectedSpots(this)
            spots270 = this.control.get('selectedSpots270') ;
            spots0 = this.control.get('selectedSpots0') ;
            delete(this.selectedSpotsH270) ;
            delete(this.selectedSpotsH0) ;
            
            if ~isempty(spots270)
                subplot(this.subplot1)
                
                hold on
                this.selectedSpotsH270 = plot(spots270(:, 2)+1, spots270(:, 1)+1, 'o', 'MarkerEdgeColor', 'blue', 'MarkerFaceColor', 'blue') ;
                set(this.selectedSpotsH270, 'ButtonDownFcn', @this.mouseInSubplot1) ;
                hold off
            end
            
            if ~isempty(spots0)
                subplot(this.subplot2)
                
                hold on
                this.selectedSpotsH0 = plot(spots0(:, 2)+1, spots0(:, 1)+1, 'o', 'MarkerEdgeColor', 'blue', 'MarkerFaceColor', 'blue') ;
                set(this.selectedSpotsH0, 'ButtonDownFcn', @this.mouseInSubplot2) ;
                hold off
            end
        end
    end
    
    methods (Access = private)
        function mouseInSubplot1(this, source, event)
            C = get (gca, 'CurrentPoint') ;
            pos1 = C(1, 1)-1 ;
            pos2 = C(1, 2)-1 ;
            
            this.control.set('PixelPosition270', [pos2, pos1]) ;
        end
        
        function mouseInSubplot2(this, source, event)
            C = get (gca, 'CurrentPoint') ;
            pos1 = C(1, 1)-1 ;
            pos2 = C(1, 2)-1 ;
            
            this.control.set('PixelPosition0', [pos2, pos1]) ;
        end
    end
end
