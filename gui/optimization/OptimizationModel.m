
classdef OptimizationModel < GenericModel
    properties (SetObservable)
        wet1 ;
        wet2 ;
        
        spots270 ;
        spots0 ;
        
        selectedSpots270 ;
        selectedSpots0 ;
        
        currentCurve ;
        optimizedPoints ;
        notOptimizedPoints ;
    end
    
    properties (Access = private)
        CTName ;
        referenceName ;
        calibrationName ;
        PRName270 ;
        PRName0 ;
        sigma ;
        braggImage270 ;
        braggImage0 ;
        ctImage ;
       
        initialCurve ;
        
        newName ;
    end
    
    methods (Access = public)
        function this = OptimizationModel(regguiHthisect)
            this@GenericModel(regguiHthisect) ;
            
            this.CTName = [] ;
            this.referenceName = [] ;
            this.calibrationName = [] ;
            this.PRName270 = [] ;
            this.PRName0 = [] ;
            this.sigma = 3 ;
            
            this.newName = [] ;
            
            this.wet1 = [] ;
            this.wet2 = [] ;

            this.spots270 = [] ;
            this.spots0 = [] ;
            this.selectedSpots270 = [] ;
            this.selectedSpots0 = [] ;
            
            this.initialCurve = [] ;
            this.currentCurve = [] ;
            this.optimizedPoints = [] ;
            this.notOptimizedPoints = [] ;
        end
        
        function set(this, property, value)
            switch property
                case 'CTName'
                    this.CTName = value ;
                case 'referenceName'
                    this.referenceName = value ;
                case 'calibrationName'
                    this.calibrationName = value ;
                case 'newName'
                    this.newName = value ;
                case 'PRName270'
                    this.PRName270 = value ;
                case 'PRName0'
                    this.PRName0 = value ;
                case 'sigma'
                    this.sigma = value ;
                case 'PixelPosition270'
                    this.newSelectedSpot(value(1), value(2), 270) ;
                case 'PixelPosition0'
                    this.newSelectedSpot(value(1), value(2), 0) ;
                case 'calibrationPoint'
                    this.newSelectedPoint(value) ;
            end
        end
        
        function value = get(this, property, value, view)
            switch property
                case 'CTName'
                    value = this.CTName ;
                case 'referenceName'
                    value = this.referenceName ;
                case 'calibrationName'
                    value = this.calibrationName ;
                case 'newName'
                    value = this.newName ;
                case 'PRName270'
                    value = this.PRName270 ;
                case 'PRName0'
                    value = this.PRName0 ;
                case 'sigma'
                    value = this.sigma ;
                case 'wet1'
                    value = this.wet1 ;
                case 'wet2'
                    value = this.wet2 ;
                case 'spots270'
                    value = this.spots270 ;
                case 'spots0'
                    value = this.spots0 ;
                case 'selectedSpots270'
                    value = this.selectedSpots270 ;
                case 'selectedSpots0'
                    value = this.selectedSpots0 ;
                case 'initialCurve'
                    value = this.initialCurve ;
                case 'currentCurve'
                    value = this.currentCurve ;
                case 'optimizedPoints'
                    value= this.optimizedPoints ;
                case 'notOptimizedPoints'
                    value = this.notOptimizedPoints ;
                case 'voxels'
                    value = this.getVoxels(value(1), value(2), view) ;
            end
        end
        
        function saveCurve(this)
            handles = guidata(this.regguiHObject) ;
            
            handles = saveCalibration(this.currentCurve, this.newName, handles) ;
            
            guidata(this.regguiHObject, handles) ;
        end
        
        function initWet(this)
            energy = 210 ;

            handles = guidata(this.regguiHObject) ;
            
            hu = [];
            for i=1:length(handles.images.name)
                if(strcmp(handles.images.name{i}, this.CTName))
                    hu = handles.images.data{i};
                    huInfo = handles.images.info{i};
                end
            end
            if(isempty(hu))
                error('Input image not found in the current list')
            end
            
            this.ctImage = Image3D(hu) ;

%             calibration = [] ;
%             for i=1:length(handles.mydata.name)
%                 if(strcmp(handles.mydata.name{i}, this.calibrationName))
%                     calibration = handles.mydata.data{i};
%                 end
%             end
%             if(isempty(calibration))
%                 error('Calibration curve not found in the current list')
%             end
% 
%             ref = [] ;
%             for i=1:length(handles.mydata.name)
%                 if(strcmp(handles.mydata.name{i}, this.referenceName))
%                     ref = handles.mydata.data{i} ;
%                     depth = handles.mydata.info{i}.depth ;
%                 end
%             end
%             if(isempty(ref))
%                 error('Calibration curve not found in the current list')
%             end
% 
%             density = HU_to_density(hu, calibration.model) ;
% 
%             disp('Computing SPR ...')
%             spr = density2spr_RS(density,  energy, calibration.RS_materials) ;
% 
%             spr = single(spr) ;
%             sprSpacing = huInfo.Spacing ;
%             
%             this.wet1 = flip(squeeze(spr2totalWET(spr, sprSpacing, 0, 270)), 1) ;
%             this.wet2 = flip(squeeze(spr2totalWET(spr, sprSpacing, 0, 0)), 1) ;

              XWorldLimits270 = this.ctImage.get('XWorldLimits', 270) ;
              YWorldLimits270 = this.ctImage.get('YWorldLimits', 270) ;
              XWorldLimits0 = this.ctImage.get('XWorldLimits', 0) ;
              YWorldLimits0 = this.ctImage.get('YWorldLimits', 0) ;
              
              data270 = this.ctImage.getData([YWorldLimits270(1) XWorldLimits270(1)], [YWorldLimits270(2) XWorldLimits270(2)]-1, [1 1], 270) ;
              data0 = this.ctImage.getData([YWorldLimits0(1) XWorldLimits0(1)], [YWorldLimits0(2) XWorldLimits0(2)]-1, [1 1], 0) ;
              
              this.wet1 = spr2totalWET(data270, this.ctImage.get('spacing'), 0, 270);
              this.wet2 = spr2totalWET(data0, this.ctImage.get('spacing'), 0, 0);
        end
        
        function initSpots(this)
            this.selectedSpots270 = [] ;
            this.selectedSpots0 = [] ;
            
            handles = guidata(this.regguiHObject) ;
            
            this.braggImage270 = handles2BraggImage(this.PRName270, handles) ;
            this.braggImage0 = handles2BraggImage(this.PRName0, handles) ;
            
            data270 = this.braggImage270.get('data') ;
            data0 = this.braggImage0.get('data') ;
            
            spacing270 = this.braggImage270.get('spacing') ;
            sapcing0 = this.braggImage0.get('spacing') ;
            
            origin270 = this.braggImage270.get('origin') ;
            origin0 = this.braggImage0.get('origin') ;

            r270_1 = repmat((0:(size(data270, 1)-1))', 1, size(data270, 2)) ;
            r270_2 = repmat(0:(size(data270, 2)-1), size(data270, 1), 1) ;
            r270(:, 1) = reshape(r270_1, numel(r270_1), 1)*spacing270(1) + origin270(1) ;
            r270(:, 2) = reshape(r270_2, numel(r270_2), 1)*spacing270(2) + origin270(2) ;
            r270(:, 3) = 0 ;
            
            this.spots270 = r270 ;
            
            r0_1 = repmat((0:(size(data0, 1)-1))', 1, size(data0, 2)) ;
            r0_2 = repmat(0:(size(data0, 2)-1), size(data0, 1), 1) ;
            r0(:, 1) = reshape(r0_1, numel(r0_1), 1)*sapcing0(1) + origin0(1) ;
            r0(:, 2) = reshape(r0_2, numel(r0_2), 1)*sapcing0(2) + origin0(2) ;
            r0(:, 3) = 0 ;
            
            this.spots0 = r0 ;
        end
        
        function initCurve(this, energy, huMin, huMax, step)
            this.initialCurve = [] ;
            this.currentCurve = [] ;
            this.optimizedPoints = [] ;
            this.notOptimizedPoints = [] ;
            
            if nargin<2
                energy = 210 ;
                huMin = -1100 ;
                huMax = 3100 ;
                step = 10 ;
            end
            
            handles = guidata(this.regguiHObject) ;
            
            calibration = [] ;
            for i=1:length(handles.mydata.name)
                if(strcmp(handles.mydata.name{i}, this.calibrationName))
                    calibration = handles.mydata.data{i};
                end
            end
            if(isempty(calibration))
                error('Calibration curve not found in the current list')
            end
            
            if isfield(calibration, 'RS_materials') ;
%                 hu = unique([-1100 -1000 -900 -550 -500 -450 0 20 40 60 200 500 700 1000 1400 2500 3000 3100 3200]) ;
                hu = unique([-1100 -1000 -992 -976 -480 -96 0 48 128 528 976 1488 1824 2224 3200]) ;
%                 hu = -1100 : 20 : 3200 ;

                density = HU_to_density(hu, calibration.model) ;

                this.initialCurve(1, 1:length(hu)) = hu ;
                this.initialCurve(2, 1:length(hu)) = density2spr_RS(density,  energy, calibration.RS_materials) ;
            else
                c = calibration.model.Stopping_Power' ;
                
%                 hu = unique([-1100 -1000 -900 0 20 40 60 200 500 700 1000 1500 2500 3000 3100 3200]) ;
                hu = unique([-1100 -1000 -992 -976 -480 -96 0 48 128 528 976 1488 1824 2224 3200]) ;
%                 hu = -1100 : 10 : 3200 ;
                this.initialCurve(1, 1:length(hu)) = hu ;
                
                c2 = interp1(c(1, :), c(2, :), hu, 'linear','extrap') ;
                c2(c2<0) = 0 ;
                this.initialCurve(2, 1:length(hu)) = c2 ;
            end
            
            rn = randn(1, length(hu)) ;
%             this.initialCurve(2, :) = this.initialCurve(2, :) + rn/(20*max(abs(rn))) ;
            this.currentCurve = this.initialCurve ;
        end
        
        function startOptimization(this)
%             initCurve = this.currentCurve ;
%             save('initCurve.mat', 'initCurve') ;
%             return
%             cCurve = this.initialCurve ;
%             cCurve(1, :)
%             cCurve(2, cCurve(1, :)==-500) = 0.15 ;
%             cCurve(2, cCurve(1, :)==3200) = 2.6 ;
%             cCurve(2, cCurve(1, :)==3100) = 2.5 ;
%             cCurve(2, cCurve(1, :)==3000) = 2.3 ;
%             cCurve(2, cCurve(1, :)==1400) = 1.6 ;
%             this.currentCurve = cCurve ;
%             return
            
            handles = guidata(this.regguiHObject) ;
            
            ref = [] ;
            for i=1:length(handles.mydata.name)
                if(strcmp(handles.mydata.name{i}, this.referenceName))
                    ref = handles.mydata.data{i} ;
                    depth = handles.mydata.info{i}.depth ;
                end
            end
            if(isempty(ref))
                error('Calibration curve not found in the current list')
            end
    
%             newSPR = optimizeSPR4(this.currentCurve, this.notOptimizedPoints, ...
%                 this.braggImage270, this.braggImage0, this.ctImage, ...
%                 this.selectedSpots270, this.selectedSpots0, depth, ref, this.sigma, this) ;

            newSPR = optimizeSPRSparse(this.currentCurve, this.notOptimizedPoints, ...
                this.braggImage270, this.braggImage0, this.ctImage, ...
                this.selectedSpots270, this.selectedSpots0, depth, ref, this.sigma, this) ;
            
            if numel(newSPR)==2
                newSPR = reshape(newSPR, 2, 1) ;
            end
            
            this.optimizedPoints = [this.optimizedPoints newSPR] ;
            this.notOptimizedPoints = [] ;
% 
%             for i=1:size(this.currentCurve, 2)
%                 if sum(this.currentCurve(1, i)==this.optimizedPoints(1, :))
%                     this.currentCurve(2, i) = this.optimizedPoints(2, this.currentCurve(1, i)==this.optimizedPoints(1, :)) ;
%                 end
%             end
            this.currentCurve = newSPR ;
        end
        
        function updateCurrentCurve(this, curve)
            this.currentCurve = curve ;
        end
    end
    
    methods (Access = private)
        function voxels = getVoxels(this, pos1, pos2, view)
            if view==270
                spacing = this.braggImage270.get('spacing') ;
                o = this.braggImage270.get('origin') ;
                XWorldLimits = this.braggImage270.get('XWorldLimits') ;
                YWorldLimits = this.braggImage270.get('YWorldLimits') ;
            elseif view==0
                spacing = this.braggImage0.get('spacing') ;
                o = this.braggImage0.get('origin') ;
                XWorldLimits = this.braggImage0.get('XWorldLimits') ;
                YWorldLimits = this.braggImage0.get('YWorldLimits') ;
            else
            end
            
            if (pos2<XWorldLimits(1) || pos2>XWorldLimits(2)) || (pos1<YWorldLimits(1) || pos1>YWorldLimits(2))
                error('Selected point is out of the PR area')
            end
            
            pos1 = round((pos1-o(1))/spacing(1))*spacing(1)+o(1) ;
            pos2 = round((pos2-o(2))/spacing(2))*spacing(2)+o(2) ;
            
            voxels = this.ctImage.getData([pos1 pos2], [pos1 pos2], this.ctImage.get('spacing'), view) ;
        end
        
        function newSelectedSpot(this, pos1, pos2, view)
            if view==270
                spacing = this.braggImage270.get('spacing') ;
                o = this.braggImage270.get('origin') ;
                XWorldLimits = this.braggImage270.get('XWorldLimits') ;
                YWorldLimits = this.braggImage270.get('YWorldLimits') ;
            elseif view==0
                spacing = this.braggImage0.get('spacing') ;
                o = this.braggImage0.get('origin') ;
                XWorldLimits = this.braggImage0.get('XWorldLimits') ;
                YWorldLimits = this.braggImage0.get('YWorldLimits') ;
            else
            end
            
            if (pos2<XWorldLimits(1) || pos2>XWorldLimits(2)) || (pos1<YWorldLimits(1) || pos1>YWorldLimits(2))
                error('Selected point is out of the PR area')
            end
            
            pos1 = round((pos1-o(1))/spacing(1))*spacing(1)+o(1) ;
            pos2 = round((pos2-o(2))/spacing(2))*spacing(2)+o(2) ;
            
            if view==270
                if ~isempty(this.selectedSpots270)
                    ind1 = (this.selectedSpots270(:, 1)==pos1) ;
                    ind2 = (this.selectedSpots270(:, 2)==pos2) ;
                    
                    inters = ind1 & ind2 ;
                    if sum(inters)
                        this.selectedSpots270(inters, :) = [] ;
                    else
                        this.selectedSpots270(size(this.selectedSpots270, 1)+1, :) = [pos1, pos2] ;
                    end
                else
                    this.selectedSpots270(size(this.selectedSpots270, 1)+1, :) = [pos1, pos2] ;
                end            
            elseif view==0
                if ~isempty(this.selectedSpots0)
                    ind1 = (this.selectedSpots0(:, 1)==pos1) ;
                    ind2 = (this.selectedSpots0(:, 2)==pos2) ;
                    
                    inters = ind1 & ind2 ;
                    if sum(inters)
                        this.selectedSpots0(inters, :) = [] ;
                    else
                        this.selectedSpots0(size(this.selectedSpots0, 1)+1, :) = [pos1, pos2] ;
                    end
                else
                    this.selectedSpots0(size(this.selectedSpots0, 1)+1, :) = [pos1, pos2] ;
                end  
            end
        end
        
        function newSelectedPoint(this, selectedHU)
            [m, ind] = min(abs(this.currentCurve(1, :)-selectedHU)) ;
            nearestHU = this.currentCurve(1, ind) ;
            
            if ~isempty(this.notOptimizedPoints) && ismember(nearestHU, this.notOptimizedPoints(1, :))
                this.notOptimizedPoints(:, squeeze(this.notOptimizedPoints(1, :))==nearestHU) = [] ;
            elseif ~isempty(this.optimizedPoints) && ismember(nearestHU, this.optimizedPoints(1, :))
                error('You cannot select an optimized point')
            else
                if isempty(this.notOptimizedPoints)
                    this.notOptimizedPoints = [nearestHU ; this.currentCurve(2, ind)];
                else
                    this.notOptimizedPoints(1, length(this.notOptimizedPoints(1, :))+1) = nearestHU ;
                    this.notOptimizedPoints(2, end) = this.currentCurve(2, ind) ;
                end
            end
        end
    end
end
