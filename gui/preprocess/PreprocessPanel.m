classdef PreprocessPanel < MainPanel
    properties (Access = private)
        control;
        
        tabGroup;
        
        %!RELEASE
        misrDataPanel;
        %RELEASE
        flipDataPanel;
        weightDataPanel;
        depthPanel;
        %!RELEASE
        splitVivaDataPanel;
        segmentVivaPanel;
        %RELEASE
    end
    
    methods (Access = public)
        function this = PreprocessPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            this.tabGroup = uitabgroup(this.mainPanel, 'Position', [0 0 1 1]);
            
            %!RELEASE
            misrTab = uitab(this.tabGroup, 'Title', 'MISR', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'MISR');
            this.misrDataPanel = MISRPanel(misrTab, this.control.getMISRControl());
            %RELEASE
            
            flipTab = uitab(this.tabGroup, 'Title', 'Flip', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'flip');
            this.flipDataPanel = FlipDataPanel(flipTab, this.control.getFlipDataControl());
            
            weightTab = uitab(this.tabGroup, 'Title', 'Weight', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'weight');
            this.weightDataPanel = WeightDataPanel(weightTab, this.control.getWeightDataControl());
            
            depthTab = uitab(this.tabGroup, 'Title', 'Depth tools', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'depth');
            this.depthPanel =  DepthPanel(depthTab, this.control.getDepthControl());
            
            %!RELEASE
            splitTab = uitab(this.tabGroup, 'Title', 'Split Viva data', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'split');
            this.splitVivaDataPanel =  SplitVivaPanel(splitTab, this.control.getSplitVivaControl());
            
            segmentTab = uitab(this.tabGroup, 'Title', 'Segment Viva data', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'segment');
            this.segmentVivaPanel =  SegmentVivaPanel(segmentTab, this.control.getSegmentVivaControl());
            %RELEASE
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
            %!RELEASE
            this.misrDataPanel.setVisible(visible);
            %RELEASE
            this.flipDataPanel.setVisible(visible);
            this.weightDataPanel.setVisible(visible);
            this.depthPanel.setVisible(visible);
            %!RELEASE
            this.splitVivaDataPanel.setVisible(visible);
            this.segmentVivaPanel.setVisible(visible);
            %RELEASE
        end
    end
    
    methods (Access = private)
        function panelCallback(this, source, event)
            switch source.Tag
                %!RELEASE
                case 'MISR'
                    this.setVisibleTab('off');
                    this.misrDataPanel.setVisible('on');
                %RELEASE
                case 'flip'
                    this.setVisibleTab('off');
                    this.flipDataPanel.setVisible('on');
                case 'weight'
                    this.setVisibleTab('off');
                    this.weightDataPanel.setVisible('on');
                case 'depth'
                    this.setVisibleTab('off');
                    this.depthPanel.setVisible('on');
                %!RELEASE
                case 'split'
                    this.setVisibleTab('off');
                    this.splitVivaDataPanel.setVisible('on');
                case 'segment'
                    this.setVisibleTab('off');
                    this.segmentVivaPanel.setVisible('on');
                %RELEASE
            end
        end
        
        function setVisibleTab(this, visible)
            %!RELEASE
            this.misrDataPanel.setVisible(visible);
            %RELEASE
            this.flipDataPanel.setVisible(visible);
            this.weightDataPanel.setVisible(visible);
            this.depthPanel.setVisible(visible);
            %!RELEASE
            this.splitVivaDataPanel.setVisible(visible);
            this.segmentVivaPanel.setVisible(visible);
            %RELEASE
        end
    end
end
