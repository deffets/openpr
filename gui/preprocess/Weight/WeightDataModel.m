
classdef WeightDataModel < GenericModel   
    properties (Access = private)
        PRName ;     
        referenceName ;
        
        transfo ;
    end
    
    methods (Access = public)
        function this = WeightDataModel(regguiHthisect)
            this@GenericModel(regguiHthisect) ;
            
            this.PRName = [] ;
            this.referenceName = [] ;
        end
        
        function set(this, property, value)
            switch property
                case 'PRName'
                    this.PRName = value ;
                case 'referenceName'
                    this.referenceName = value ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'PRName'
                    value = this.PRName ;
                case 'referenceName'
                    value = this.referenceName ;
            end
        end
        
        function weightPR(this)
            handles = guidata(this.regguiHObject) ;
            instruction = ['handles = weightPR(' '''' this.PRName '''' ', ' '''' this.referenceName '''' ',' ...
                '''' [this.PRName 'Weighted'] '''' ', handles) ;'] ;
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
    
    methods (Access = private)

    end
end
