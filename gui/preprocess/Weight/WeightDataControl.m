
classdef WeightDataControl < GenericControl
    properties(Access = private)
        savePanel ;
    end

    methods (Access = public)
        function this = WeightDataControl(model)
            this@GenericControl(model) ;
        end
        
        function set(this, property, value)
            switch property
                case 'nope'
                    
                otherwise
                    this.model.set(property, value) ;
            end
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
        
        function weightPR(this)
            this.model.weightPR() ;
        end
    end
end
