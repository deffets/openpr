
classdef WeightDataPanel < MainPanel
    properties (Access = private)
        control ;
        
        selectionPanel ;
    end
    
    methods (Access = public)
        function this = WeightDataPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            p1 = uipanel('Parent', this.mainPanel, 'Title', 'Parameters', 'Position', [0 0.87 1 0.12]) ;
            this.selectionPanel = WeightDataSelectionPanel(p1, this.control) ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
            this.selectionPanel.setVisible(visible) ;
        end
    end
end
