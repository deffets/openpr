
classdef MISRSelectionPanel < MainPanel
    properties (Access = private)       
        control;
        
        PRPopup;
        refPopup;
        WEPLPopup;
        
        sigmaEdit;
        lambdaEdit;
        wetEdit;
        
        useWEPLBox;
        
        goButton;
    end
    
    methods (Access = public)
        function this = MISRSelectionPanel(f, control)
            this@MainPanel(f);
            this.control = control;  
            
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'PR', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Reference', 'Units', 'normalized', 'Position', [0.15, 0.55, 0.1, 0.3]);
            this.useWEPLBox = uicontrol('Parent', this.mainPanel, 'Style', 'checkbox', 'String', 'Use WEPL map', 'Units', 'normalized', 'Position', [0.3, 0.55, 0.1, 0.3], 'Callback', @this.useWEPLCallback);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Sigma', 'Units', 'normalized', 'Position', [0.45, 0.55, 0.05, 0.3]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Lambda', 'Units', 'normalized', 'Position', [0.55, 0.55, 0.05, 0.3]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Name (WEPL)', 'Units', 'normalized', 'Position', [0.7, 0.55, 0.1, 0.3]);
             
            this.PRPopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]);
            this.refPopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.15, 0.15, 0.1, 0.3]);
            this.WEPLPopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.3, 0.15, 0.1, 0.3]);

            this.sigmaEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(this.control.get('sigma')), 'Units', 'normalized', 'Position', [0.45, 0.15, 0.05, 0.3]);
            this.lambdaEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(this.control.get('lambda')), 'Units', 'normalized', 'Position', [0.55, 0.15, 0.05, 0.3]);
            this.wetEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', this.control.get('newWetName'), 'Units', 'normalized', 'Position', [0.7, 0.15, 0.1, 0.3]);
            
            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @this.goCallback);
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups();
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private)
        function useWEPLCallback(this, source, event)
            WETNames = this.control.getWETNames();
            
            if ~isempty(WETNames)
                this.WEPLPopup.set('String', WETNames);
            end
            
            
            if this.useWEPLBox.Value
                this.WEPLPopup.set('Visible', 'on');
            else
                this.WEPLPopup.set('Visible', 'off');
            end
        end
        
        function updatePopups(this)
            PRNames = this.control.getPRNames();
            refNames = this.control.getReferenceNames();
            
            if ~isempty(PRNames) && ~isempty(refNames)
                this.PRPopup.set('String', PRNames);
                this.refPopup.set('String', refNames);
            end
            
            this.useWEPLCallback([], []);
        end
        
        function goCallback(this, source, event)
            items = get(this.PRPopup, 'String');
            index_selected = get(this.PRPopup, 'Value');
            simuName = items{index_selected};          
            this.control.set('PRName', simuName);
            
            items = get(this.refPopup, 'String');
            index_selected = get(this.refPopup, 'Value');
            refName = items{index_selected};          
            this.control.set('refName', refName);
            
            if this.useWEPLBox.Value
                items = get(this.WEPLPopup, 'String');
                index_selected = get(this.WEPLPopup, 'Value');
                WEPLName = items{index_selected};          
                this.control.set('WEPLName', WEPLName);
            else
                this.control.set('WEPLName', []);
            end
                
            this.control.set('newWetName', get(this.wetEdit, 'String'));
            this.control.set('sigma', str2double(get(this.sigmaEdit, 'String')));
            this.control.set('lambda', str2double(get(this.lambdaEdit, 'String')));
            
            this.control.startMISR();
        end
    end
end
