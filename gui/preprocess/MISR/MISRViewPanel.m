
classdef MISRViewPanel < MainPanel
    properties (Access = private)       
        control ;
        
        subplot1 ;
        subplot2 ;
    end
    
    methods (Access = public)
        function this = MISRViewPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
             
            this.control.set('viewPanel', this) ;
            this.subplot1 = [] ;
            this.subplot2 = [] ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
        end
        
        function updateImage1(this)
            image = this.control.get('image1') ;
            data = image.get('data') ;
            spacing = image.get('spacing') ;
            
            if isempty(this.subplot1)
                this.subplot1 = subplot(1, 2, 1, 'Parent', this.mainPanel) ;
            end
            
            subplot(this.subplot1)
            imshow(mat2gray(data), imref2d(size(data), spacing(2), spacing(1)))
            axis(this.subplot1, 'off');
            title(this.subplot1, 'PR')
            colormap(this.subplot1, 'gray');
            text(this.subplot1, 2, 5, ['Name: ' this.control.get('PRName')], 'Color', 'red')
            text(this.subplot1, 2, 15, ['Spacing: ' num2str(spacing)], 'Color', 'red')
        end
        
        function updateImage2(this)
            image = this.control.get('image2') ;
            data = image.get('data') ;
            
            if isempty(data)
                return
            end
            
            spacing = image.get('spacing') ;
            
            if isempty(this.subplot2)
                this.subplot2 = subplot(1, 2, 2, 'Parent', this.mainPanel) ;
            end
                        
            imagesc(this.subplot2, data)
            axis(this.subplot2, 'equal', 'off');
            title(this.subplot2, 'WET')
            colormap(this.subplot2, 'parula');
            colorbar(this.subplot2);
            text(this.subplot2, 2, 5, ['Name: ' this.control.get('image2Name')], 'Color', 'red')
            text(this.subplot2, 2, 15, ['Spacing: ' num2str(spacing)], 'Color', 'red')
        end
    end
end
