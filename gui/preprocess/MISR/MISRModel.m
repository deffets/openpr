
classdef MISRModel < GenericModel
    properties (SetObservable)
        image1;
        image2;
    end
    
    properties (Access = private)
        WEPLName;
        PRName;
        refName;
        newWetName;
        sigma;
        lambda;
        image2Name;
    end
    
    methods (Access = public)
        function this = MISRModel(regguiHObject)
            this@GenericModel(regguiHObject);
            
            this.sigma = 3;
            this.lambda = 0.5;
            
            this.PRName = [];
            this.refName = [];
            this.newWetName = [];
            this.WEPLName = [];
            
            this.image1 = [];
            this.image2 = [];
            
            this.image2Name = [];
        end
        
        function set(this, property, value)
            switch property
                case 'PRName'
                    this.PRName = value;
                case 'refName'
                    this.refName = value;
                case 'newWetName'
                    this.newWetName = value;
                case 'sigma'
                    this.sigma = value;
                case 'lambda'
                    this.lambda = value;
                case 'WEPLName'
                    this.WEPLName = value;
                otherwise
                    set@SimpleSimulationWETModel(this, property, value);
            end
        end
        
        function value = get(this, property)
            switch property
                case 'PRName'
                    value = this.PRName;
                case 'refName'
                    value = this.refName;
                case 'image1'
                    value = this.image1;
                case 'image2'
                    value = this.image2;
                case 'image2Name'
                    value = this.image2Name;
                case 'newWetName'
                    value = this.newWetName;
                case 'sigma'
                    value = this.sigma;
                case 'lambda'
                    value = this.lambda;
                case 'WEPLName'
                    value = this.WEPLName;
                otherwise
                    value = get@SimpleSimulationWETModel(this, property);
            end
        end
        
        function startMISR(this)
            handles = guidata(this.regguiHObject);
            
            braggImage1 = handles2BraggImage(this.PRName, handles);
            origin1 = braggImage1.get('origin');
            spacing1 = braggImage1.get('spacing');
            im1 = maximumPosition(braggImage1.get('data'), braggImage1.get('depth'));
            this.image1 = Image2D(im1, origin1, spacing1);
            
            handles = guidata(this.regguiHObject);
            instruction = ['handles = MISRInstruction(' '''' this.PRName '''' ', ' '''' this.refName '''' ', ' '''' this.WEPLName '''' ', ' num2str(this.sigma) ', '  num2str(this.lambda) ', ' '''' this.newWetName '''' ', handles);'];
            handles.instructions{length(handles.instructions)+1} = instruction;
            handles = Execute_reggui_instructions(handles);
            guidata(this.regguiHObject, handles);
            
            this.image2Name = handles.mydata.name{length(handles.mydata.name)};
            im2 = handles.mydata.data{length(handles.mydata.data)};
            this.image2 = Image2D(im2, [0 0], [1 1]);
        end
    end
end
