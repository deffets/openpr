
classdef MISRControl < SimpleSimulationWETControl
    properties(Access = private)
        viewPanel ;
        savePanel ;
%         actionPanel ;
    end

    methods (Access = public)
        function this = MISRControl(model)
            this@SimpleSimulationWETControl(model) ;
            
            addlistener(this.model, 'image1', 'PostSet', @this.image1Listener) ;
            addlistener(this.model, 'image2', 'PostSet', @this.image2Listener) ;
        end
        
        function set(this, property, value)
            switch property
                case 'viewPanel'
                    this.viewPanel = value ;
                case 'savePanel'
                    this.savePanel = value ;
                case 'actionPanel'
                    this.actionPanel = value ;
                otherwise
                    this.model.set(property, value) ;
            end
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
        
        function startMISR(this)
            this.model.startMISR() ;
        end
    end
    
    methods (Access = private)
        function image1Listener(this, source, event)
            this.viewPanel.updateImage1() ;
        end
        
        function image2Listener(this, source, event)
            this.viewPanel.updateImage2() ;
%             this.actionPanel.updatePopups() ;
        end
    end
end
