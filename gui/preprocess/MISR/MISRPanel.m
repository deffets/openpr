
classdef MISRPanel < MainPanel
    properties (Access = private)
        control ;
        
        selectionPanel ;
        viewPanel ;
        actionPanel ;
    end
    
    methods (Access = public)
        function this = MISRPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            p1 = uipanel('Parent', this.mainPanel, 'Title', 'Parameters', 'Position', [0 0.87 1 0.12]) ;
            this.selectionPanel = MISRSelectionPanel(p1, this.control) ;
            p2 = uipanel('Parent', this.mainPanel, 'Position', [0 0 1 0.85], 'Title', 'View') ;
            this.viewPanel = MISRViewPanel(p2, this.control) ;
%             p3 = uipanel('Parent', this.mainPanel, 'Position', [0.8 0 0.2 0.85], 'Title', 'Simulation From WET-Map') ;
%             this.actionPanel = MISRActionsPanel(p3, this.control) ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
            this.selectionPanel.setVisible(visible) ;
            this.viewPanel.setVisible(visible) ;
%             this.actionPanel.setVisible(visible) ;
        end
    end
    
    methods (Access = private)
    end
end
