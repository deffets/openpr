
classdef MISRActionsPanel < SimpleSimulationWETPanel    
    methods (Access = public)
        function this = MISRActionsPanel(f, control)
            this@SimpleSimulationWETPanel(f, control) ;
            
            this.wetText.set('Position', [0.05, 0.9, 0.3, 0.05]) ;
            this.wetPopup.set('Position', [0.4, 0.9, 0.5, 0.05]) ;
            
            this.referenceText.set('Position', [0.05, 0.8, 0.3, 0.05]) ;
            this.referencePopup.set('Position', [0.4, 0.8, 0.5, 0.05]) ;
                       
            this.sigmaText.set('Position', [0.05, 0.7, 0.3, 0.05]) ;
            this.sigmaEdit.set('Position', [0.4, 0.7, 0.5, 0.05]) ;
            
            this.nameText.set('Position', [0.05, 0.6, 0.3, 0.05]) ;
            this.nameEdit.set('Position', [0.4, 0.6, 0.5, 0.05]) ;

            this.goButton.set('Position', [0.1, 0.5, 0.8, 0.05]) ;
            
            this.control.set('actionPanel', this) ;
        end
        
        function setVisible(this, visible)
            setVisible@SimpleSimulationWETPanel(this, visible) ;
        end
    end
end
