
classdef SplitVivaModel < SplitVivaViewModel
    properties (Access = protected)
        PRName ;
    end
    
    methods (Access = public)
        function this = SplitVivaModel(regguiHObject)
            this@SplitVivaViewModel(regguiHObject) ;
            
            this.PRName = [] ;
        end
        
        function set(this, property, value)
            switch property
                case 'PRName'
                    this.PRName = value ;
                otherwise
                    set@SplitVivaViewModel(this, property, value) ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'PRName'
                    value = this.PRName ;
                otherwise
                    value = get@SplitVivaViewModel(this, property) ;
            end
        end
        
        function splitViva(this)
            handles = guidata(this.regguiHObject) ;
            
            instruction = ['handles = splitVivaPR(' '''' this.PRName '''' ', handles) ;'] ;
            
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
            
            this.set('splitName', this.PRName) ;
        end
    end
    
    methods (Access = private)

    end
end
