
classdef SplitVivaViewControl < GenericControl
    properties (Access = protected)
        viewPanel ;
    end
    
    methods (Access = public)
        function this = SplitVivaViewControl(model)
            this@GenericControl(model) ;
            
            addlistener(this.model, 'image', 'PostSet', @this.imageListener) ;
        end
        
        function set(this, property, value)
            switch property
                case 'viewPanel'
                    this.viewPanel = value ;
                otherwise
                    this.model.set(property, value) ;
            end
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
    end
    
    methods (Access = private)
        function imageListener(this, source, event)
            this.viewPanel.updateImage() ;
            try
                this.viewPanel.updateLines() ;
            catch
            end
        end
    end
end