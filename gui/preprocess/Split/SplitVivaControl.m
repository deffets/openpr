
classdef SplitVivaControl < SplitVivaViewControl
    properties(Access = private)
    end

    methods (Access = public)
        function this = SplitVivaControl(model)
            this@SplitVivaViewControl(model) ;
        end
        
        function set(this, property, value)
            set@SplitVivaViewControl(this, property, value) ;
        end
        
        function value = get(this, property)
            value = get@SplitVivaViewControl(this, property) ;
        end
        
        function splitViva(this)
            this.model.splitViva() ;
        end
    end
    
    methods (Access = private)
    end
end
