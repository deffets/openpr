
classdef SplitVivaViewModel < GenericModel
    properties (SetObservable) ;
        image ;
        splitData ;
    end
    
    properties (Access = protected)
        splitName ;
        energyInd ;
    end
    
    methods (Access = public)
        function this = SplitVivaViewModel(regguiHObject)
            this@GenericModel(regguiHObject) ;
            
            this.splitName = [] ;
            this.energyInd = 1 ;
        end
        
        function set(this, property, value)
            switch property
                case 'splitName'
                    this.splitName = value ;
                    this.updateData() ;
                case 'energyInd'
                    this.energyInd = value ;
                    this.updateImage() ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'splitName'
                    value = this.splitName ;
                case 'energyInd'
                    value = this.energyInd ;
                case 'image'
                    value = this.image ;
                case 'dataLength'
                    value = size(this.splitData, 3) ;
            end
        end
    end
    
    methods (Access = private)
        function updateData(this)
            handles = guidata(this.regguiHObject) ;
            
            for i=1 : length(handles.mydata.name)
                if strcmp(handles.mydata.name{i}, this.splitName)
                    this.splitData = handles.mydata.data{i} ;
                    break ;
                end
            end
            
            this.updateImage() ;
        end
        
        function updateImage(this)
            if isempty(this.splitData)
                return ;
            end
            
            this.image = this.splitData(:, :, round(this.energyInd)) ;
        end
    end
end
