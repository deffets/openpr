
classdef SplitVivaViewPanel < MainPanel
    properties (Access = protected)       
        control ;
        
        subplot1 ;
        slider ;
    end
    
    methods (Access = public)
        function this = SplitVivaViewPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
             
            this.control.set('viewPanel', this) ;
            
            this.subplot1 = subplot(2, 2, 1, 'Parent', this.mainPanel) ;
            axis off
            
            this.slider = uicontrol('Parent', this.mainPanel, 'Style', 'slider', 'Min', 0, 'Max', 1, 'Value', 1, 'SliderStep', [1 1], 'Units', 'Normalized', 'Position', [0.1 0.2 0.4 0.05], 'Callback', @this.sliderCallback) ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
        end
        
        function updateImage(this)
            data = this.control.get('image') ;
            
            R = imref2d(size(data), 0.127*2, 0.127*2) ;
            subplot(this.subplot1)
            mm = median(median(data)) ;
            data = data./(mm*8) ;
            imshow(data, R)
            colormap parula
            axis equal off
            
            dataL = this.control.get('dataLength') ;
            if dataL>1
                this.slider.set('Min', 1, 'Max', dataL, 'SliderStep', [1 1]/(dataL-1)) ;
            end

            ind = this.control.get('energyInd') ;
            this.slider.set('Value', ind) ;
        end
    end
    
    methods (Access = private)
        function sliderCallback(this, source, event)
            this.control.set('energyInd', this.slider.get('Value')) ;
        end
    end
end
