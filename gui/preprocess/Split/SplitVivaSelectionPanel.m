
classdef SplitVivaSelectionPanel < MainPanel
    properties (Access = protected)       
        control ;
        
        PRText ;
        
        PRPopup ;
        
        goButton ;
    end
    
    methods (Access = public)
        function this = SplitVivaSelectionPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;  
            
            this.PRText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Viva data', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]) ;
             
            this.PRPopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]) ;

            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @this.goCallback) ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private)
        function updatePopups(this)
            PRNames = this.control.getVivaNames() ;
            
            if ~isempty(PRNames)
                this.PRPopup.set('String', PRNames) ;
            end
        end
        
        function goCallback(this, source, event)
            items = get(this.PRPopup, 'String') ;
            index_selected = get(this.PRPopup, 'Value') ;
            simuName = items{index_selected} ;          
            this.control.set('PRName', simuName) ;
            
            disp('Splitting data...')
            this.control.splitViva() ;
        end
    end
end
