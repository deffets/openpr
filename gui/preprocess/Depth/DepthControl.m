
classdef DepthControl < GenericControl
    properties(Access = private)
        shiftDataControl;
        interpolateDataControl;
    end

    methods (Access = public)
        function this = DepthControl(model)
            this@GenericControl(model);
            
            this.shiftDataControl = ShiftDataControl(ShiftDataModel(this.model.getRegguiHObject()));
            this.interpolateDataControl = InterpolateDataControl(InterpolateDataModel(this.model.getRegguiHObject()));
        end
        
        function value = get(this, property)
            switch property
                case 'shiftDataControl'
                    value = this.shiftDataControl;
                case 'interpolateDataControl'
                    value = this.interpolateDataControl;
                otherwise
                    value = this.model.get(property);
            end
        end
    end
end
