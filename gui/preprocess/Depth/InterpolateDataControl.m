classdef InterpolateDataControl < GenericControl
    properties(Access = private)
    end

    methods (Access = public)
        function this = InterpolateDataControl(model)
            this@GenericControl(model);
        end
        
        function set(this, property, value)
            this.model.set(property, value);
        end
        
        function value = get(this, property)
            value = this.model.get(property);
        end
        
        function interpolatePR(this)
            this.model.interpolatePR();
        end
    end
end

