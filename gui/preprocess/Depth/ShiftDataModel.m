
classdef ShiftDataModel < GenericModel   
    properties (Access = private)
        PRName;     
        referenceName;
        shift_mm;
    end
    
    methods (Access = public)
        function this = ShiftDataModel(regguiHObject)
            this@GenericModel(regguiHObject);
            
            this.PRName = [];
        end
        
        function set(this, property, value)
            switch property
                case 'PRName'
                    this.PRName = value;
                case 'shift_mm'
                    this.shift_mm = value;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'PRName'
                    value = this.PRName;
                case 'shift_mm'
                    value = this.shift_mm;
            end
        end
        
        function shiftPR(this)
            handles = guidata(this.regguiHObject);
            instruction = ['handles = shiftPRInstr(' '''' this.PRName '''' ', ' num2str(this.shift_mm) ', ' ...
                '''' [this.PRName 'shifted'] '''' ', handles);'];
            handles.instructions{length(handles.instructions)+1} = instruction;
            handles = Execute_reggui_instructions(handles);
            guidata(this.regguiHObject, handles);
        end
    end
    
    methods (Access = private)

    end
end
