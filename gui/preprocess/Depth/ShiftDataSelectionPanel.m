
classdef ShiftDataSelectionPanel < MainPanel
    properties (Access = private)       
        control ;
        
        PRText1 ;
        nameText ;
        
        shiftEdit ;
        
        goButton ;

        PR1Popup ;
    end
    
    methods (Access = public)
        function this = ShiftDataSelectionPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;  
            
            this.PRText1 = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'PR to adjust', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]) ;
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Shift (mm)', 'Units', 'normalized', 'Position', [0.15, 0.55, 0.05, 0.3]) ;
            
            this.shiftEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(this.control.get('shift_mm')), 'Units', 'normalized', 'Position', [0.15, 0.15, 0.05, 0.3]) ;
            
            this.PR1Popup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]) ;

            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @this.goCallback) ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private)
        function updatePopups(this)
            PRNames = this.control.getPRNames() ;
            
            if ~isempty(PRNames)
                this.PR1Popup.set('String', PRNames) ;
            end
        end
        
        function goCallback(this, source, event)
            items = get(this.PR1Popup, 'String') ;
            index_selected = get(this.PR1Popup, 'Value') ;
            simuName = items{index_selected} ;          
            this.control.set('PRName', simuName) ;
            this.control.set('shift_mm', str2num(this.shiftEdit.get('String'))) ;
            
            this.control.shiftPR() ;
        end
    end
end
