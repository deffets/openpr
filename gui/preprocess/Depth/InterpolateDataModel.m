
classdef InterpolateDataModel < GenericModel   
    properties (Access = private)
        PRName;     
        referenceName;
    end
    
    methods (Access = public)
        function this = InterpolateDataModel(regguiHObject)
            this@GenericModel(regguiHObject);
            
            this.PRName = [];
            this.referenceName = [];
        end
        
        function set(this, property, value)
            switch property
                case 'PRName'
                    this.PRName = value;
                case 'referenceName'
                    this.referenceName = value;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'PRName'
                    value = this.PRName;
                case 'referenceName'
                    value = this.referenceName;
            end
        end
        
        function interpolatePR(this)
            handles = guidata(this.regguiHObject);
            instruction = ['handles = interpolatePRInstruction(' '''' this.PRName '''' ', ''' this.referenceName ''', ' ...
                '''' [this.PRName '_interpolated'] '''' ', handles);'];
            handles.instructions{length(handles.instructions)+1} = instruction;
            handles = Execute_reggui_instructions(handles);
            guidata(this.regguiHObject, handles);
        end
    end
    
    methods (Access = private)

    end
end
