
classdef DepthPanel < MainPanel
    properties (Access = private)
        control;
        
        shiftPanel;
        interpolatePanel;
    end
    
    methods (Access = public)
        function this = DepthPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            p1 = uipanel('Parent', this.mainPanel, 'Title', 'Shift', 'Position', [0 0.87 1 0.12]);
            this.shiftPanel = ShiftDataSelectionPanel(p1, this.control.get('shiftDataControl'));
            
            p2 = uipanel('Parent', this.mainPanel, 'Title', 'Re-interpolate', 'Position', [0 0.7 1 0.12]);
            this.interpolatePanel = InterpolateDataPanel(p2, this.control.get('interpolateDataControl'));
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
            this.shiftPanel.setVisible(visible);
            this.interpolatePanel.setVisible(visible);
        end
    end
end
