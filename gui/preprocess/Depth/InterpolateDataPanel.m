
classdef InterpolateDataPanel < MainPanel
    properties (Access = private)       
        control;
        
        PRText1;
        RefText1;
        
        nameText;
        
        goButton;

        PR1Popup;
        Ref1Popup;
    end
    
    methods (Access = public)
        function this = InterpolateDataPanel(f, control)
            this@MainPanel(f);
            this.control = control;  
            
            this.PRText1 = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'PR to adjust', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]);
            this.RefText1 = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Reference', 'Units', 'normalized', 'Position', [0.15, 0.55, 0.1, 0.3]);
                        
            this.PR1Popup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]);
            this.Ref1Popup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.15, 0.15, 0.1, 0.3]);

            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @this.goCallback);
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups();
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private)
        function updatePopups(this)
            PRNames = this.control.getPRNames();
            if ~isempty(PRNames)
                this.PR1Popup.set('String', PRNames);
            end
            
            referenceNames = this.control.getReferenceNames();
            if ~isempty(PRNames)
                this.Ref1Popup.set('String', referenceNames);
            end
        end
        
        function goCallback(this, source, event)
            items = get(this.PR1Popup, 'String');
            index_selected = get(this.PR1Popup, 'Value');
            PRName = items{index_selected};          
            this.control.set('PRName', PRName);
            
            items = get(this.Ref1Popup, 'String');
            index_selected = get(this.Ref1Popup, 'Value');
            referenceName = items{index_selected};          
            this.control.set('referenceName', referenceName);
            
            this.control.interpolatePR();
        end
    end
end
