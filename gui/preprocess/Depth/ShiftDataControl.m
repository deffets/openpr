
classdef ShiftDataControl < GenericControl
    properties(Access = private)
    end

    methods (Access = public)
        function this = ShiftDataControl(model)
            this@GenericControl(model);
        end
        
        function set(this, property, value)
            this.model.set(property, value);
        end
        
        function value = get(this, property)
            value = this.model.get(property);
        end
        
        function shiftPR(this)
            this.model.shiftPR();
        end
    end
end
