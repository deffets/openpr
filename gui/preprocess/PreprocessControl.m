
classdef PreprocessControl < GenericControl
    properties (Access = private)
        regguiHObject ;
        
        %!RELEASE
        misrControl ;
        %RELEASE
        flipdataControl ;
        weightDataControl ;
        depthControl ;
        %!RELEASE
        splitVivaControl ;
        segmentVivaControl ;
        %RELEASE
    end
    
    methods (Access = public)
        function obj = PreprocessControl(model, regguiHObject)
            obj@GenericControl(model) ;
            obj.regguiHObject = regguiHObject ;
            
            %!RELEASE
            misrModel = MISRModel(obj.model.getRegguiHObject()) ;
            obj.misrControl = MISRControl(misrModel) ;
            %RELEASE
            
            flipDataModel = FlipDataModel(obj.model.getRegguiHObject()) ;
            obj.flipdataControl = FlipDataControl(flipDataModel) ;
            
            weightDataModel = WeightDataModel(obj.model.getRegguiHObject()) ;
            obj.weightDataControl = WeightDataControl(weightDataModel) ;
            
            depthModel = GenericModel(obj.model.getRegguiHObject()) ;
            obj.depthControl = DepthControl(depthModel) ;
            
            %!RELEASE
            splitVivaModel = SplitVivaModel(obj.model.getRegguiHObject()) ;
            obj.splitVivaControl = SplitVivaControl(splitVivaModel) ;
            
            segmentVivaModel = SegmentVivaModel(obj.model.getRegguiHObject()) ;
            obj.segmentVivaControl = SegmentVivaControl(segmentVivaModel) ;
            %RELEASE
        end
        
        %!RELEASE
        function ctrl = getMISRControl(this)
            ctrl = this.misrControl ;
        end
        %RELEASE
        
        function ctrl = getFlipDataControl(this)
            ctrl = this.flipdataControl ;
        end
        
        function ctrl = getWeightDataControl(this)
            ctrl = this.weightDataControl ;
        end
        
        function ctrl = getDepthControl(this)
            ctrl = this.depthControl ;
        end
        
        %!RELEASE
        function ctrl = getSplitVivaControl(this)
            ctrl = this.splitVivaControl ;
        end
        
        function ctrl = getSegmentVivaControl(this)
            ctrl = this.segmentVivaControl ;
        end
        %RELEASE
    end
end
