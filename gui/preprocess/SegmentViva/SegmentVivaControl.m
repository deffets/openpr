
classdef SegmentVivaControl < SegmentVivaViewControl
    properties(Access = private)
    end

    methods (Access = public)
        function this = SegmentVivaControl(model)
            this@SegmentVivaViewControl(model) ;
        end
        
        function set(this, property, value)
            set@SplitVivaViewControl(this, property, value) ;
        end
        
        function value = get(this, property)
            value = get@SplitVivaViewControl(this, property) ;
        end
    end
    
    methods (Access = private)
    end
end
