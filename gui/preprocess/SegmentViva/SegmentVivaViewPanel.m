
classdef SegmentVivaViewPanel < SplitVivaViewPanel
    properties (Access = private)
        plus1 ;
        minus1 ;
        plus2 ;
        minus2 ;
        
        segment ;
        
        hLines ;
    end
    
    methods (Access = public)
        function this = SegmentVivaViewPanel(f, control)
            this@SplitVivaViewPanel(f, control) ;
            
            this.hLines = [] ;
            
            this.plus2 = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', '+', 'Units', 'normalized', 'Position', [0.7, 0.7, 0.05, 0.05], 'Callback', @this.transCallback, 'Tag', 'plus2') ;
            this.minus2 = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', '-', 'Units', 'normalized', 'Position', [0.6, 0.7, 0.05, 0.05], 'Callback', @this.transCallback, 'Tag', 'minus2') ;
            this.minus1 = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', '-', 'Units', 'normalized', 'Position', [0.65, 0.65, 0.05, 0.05], 'Callback', @this.transCallback, 'Tag', 'minus1') ;
            this.plus1 = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', '+', 'Units', 'normalized', 'Position', [0.65, 0.75, 0.05, 0.05], 'Callback', @this.transCallback, 'Tag', 'plus1') ;
            
            this.segment = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Segment', 'Units', 'normalized', 'Position', [0.6, 0.5, 0.15, 0.05], 'Callback', @this.segmentCallback, 'Tag', 'go') ;
        end
        
        function updateLines(this)
            if ~isempty(this.hLines)
                for i=1 : length(this.hLines)
                    try
                        delete(this.hLines(i)) ;
                    catch
                    end
                end
            end
            
            offset1 = this.control.get('offset1') ;
            offset2 = this.control.get('offset2') ;
            
            offx = this.control.get('offx') ;
            offy = this.control.get('offy') ;
            
            data = this.control.get('image') ;
            s = size(data) ;
            
            indh = 1 ;
            
            subplot(this.subplot1)
            axis on tight
            hold on
            for i=1 : length(offy)
                this.hLines(indh) = line([0 (s(2)-1)*(0.127*2)], [offset1+offy(i) offset1+offy(i)], 'Color', 'red') ;
                indh = indh+1 ;
            end

            for i=1 : length(offx)
                this.hLines(indh) = line([offset2+offx(i) offset2+offx(i)], [0 (s(1)-1)*(0.127*2)], 'Color', 'red') ;
                indh = indh+1 ;
            end
            
            this.hLines(indh) = line([0 (s(2)-1)*(0.127*2)], [offset1 offset1], 'Color', 'green') ;
            indh = indh+1 ;
            this.hLines(indh) = line([offset2 offset2], [0 (s(1)-1)*(0.127*2)], 'Color', 'green') ;
            indh = indh+1 ;
            
            hold off
        end
    end
    
    methods (Access = private)
        function transCallback(this, source, event)
            offset1 = this.control.get('offset1') ;
            offset2 = this.control.get('offset2') ;
            
            switch source.Tag
                case 'plus1'
                    this.control.set('offset1', offset1-1) ;
                case 'minus1'
                    this.control.set('offset1', offset1+1) ;
                case 'plus2'
                    this.control.set('offset2', offset2+1) ;
                case 'minus2'
                    this.control.set('offset2', offset2-1) ;
            end
        end
        
        function segmentCallback(this, source, event)
            this.control.segment() ;
        end
    end
end
