
classdef SegmentVivaViewControl < SplitVivaViewControl
    properties (Access = private)
    end
    
    methods (Access = public)
        function this = SegmentVivaViewControl(model)
            this@SplitVivaViewControl(model) ;
            
            addlistener(this.model, 'offset1', 'PostSet', @this.offsetListener) ;
            addlistener(this.model, 'offset2', 'PostSet', @this.offsetListener) ;
        end
        
        function segment(this)
            this.model.segment() ;
        end
    end
    
    methods (Access = private)
        function offsetListener(this, source, event)
            this.viewPanel.updateLines() ;
        end
    end
end