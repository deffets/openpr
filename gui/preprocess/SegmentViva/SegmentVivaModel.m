
classdef SegmentVivaModel < SegmentVivaViewModel
    properties (Access = private)
        DISO ;
    end
    
    methods (Access = public)
        function this = SegmentVivaModel(regguiHthisect)
            this@SegmentVivaViewModel(regguiHthisect) ;
            
            this.DISO = 100 ;
        end
        
        function set(this, property, value)
            switch property
                case 'PRName'
                    this.set('splitName', value) ;
                case 'DISO'
                    this.DISO = value ;
                otherwise
                    set@SegmentVivaViewModel(this, property, value) ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'PRName'
                    value = this.get('splitName') ;
                case 'DISO'
                    value = this.DISO ;
                otherwise
                    value = get@SegmentVivaViewModel(this, property) ;
            end
        end
    end
    
    methods (Access = private)

    end
end
