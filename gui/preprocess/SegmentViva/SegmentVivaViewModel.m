
classdef SegmentVivaViewModel < SplitVivaModel
    properties (SetObservable) ;
        offset1 ;
        offset2 ;
    end
    
    properties (Access = private)
    end
    
    methods (Access = public)
        function this = SegmentVivaViewModel(regguiHObject)
            this@SplitVivaModel(regguiHObject) ;
            
            this.offset1 = 0 ;
            this.offset2 = 0 ;
            
            addlistener(this, 'splitData', 'PostSet', @this.imageListener) ;
            this.imageListener([], []) ;
        end
        
        function set(this, property, value)
            switch property
                case 'offset1'
                    this.offset1 = value ;
                case 'offset2'
                    this.offset2 = value ;
                otherwise
                    set@SplitVivaViewModel(this, property, value) ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'offset1'
                    value = this.offset1 ;
                case 'offset2'
                    value = this.offset2 ;
                case 'offy'
                    value = this.computeOffY() ;
                case 'offx'
                    value = this.computeOffX() ;
                otherwise
                    value = get@SplitVivaViewModel(this, property) ;
            end
        end
        
        function segment(this)
            offx = this.computeOffX() ;
            offy = this.computeOffY() ;
            
            data = this.get('image') ;
            
            indx = unique(round((this.offset2+offx)/(0.127*2) + 1)) ;
            indy = unique(round((this.offset1+offy)/(0.127*2) + 1)) ;
            
            indx(indx>size(data, 2)) = [] ;
            indy(indy>size(data, 1)) = [] ;
            
            spacing = [20, 20] ;
            newName = [this.splitName '_segmented'] ;
            
            handles = guidata(this.regguiHObject) ;
            
            instruction = ['handles = segmentViva(' '''' this.splitName '''' ',[' num2str(indx) '], [' num2str(indy) '], [' num2str(spacing(1)) ' ' num2str(spacing(2)) '], ' '''' newName '''' ', handles) ;'] ;
            
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
    
    methods (Access = private)
        function imageListener(this, source, event)
            this.offset1 = size(this.image, 1)*0.127 ;
            this.offset2 = size(this.image, 2)*0.127 ;
        end
        
        function offy = computeOffY(this)
            data = this.get('image') ;
            s = size(data) ;
            
            spacing = 20 ;
            sad = 1859.1 ;
            d = this.get('DISO') ;

            ind = 1 ;
            for i=0 : -spacing : -s(1)*0.127*2
                offy(ind) = -spacing*ind*(sad+d)/sad ;
                ind = ind+1 ;
            end
            
            offy((this.offset1+offy)<=0) = [] ;
            
            ind2 = 1 ;
            for i=spacing : spacing : s(1)*0.127*2
                offy(ind) = spacing*ind2*(sad+d)/sad ;
                ind = ind +1 ;
                ind2 = ind2 + 1 ;
            end
            
            offy((this.offset1+offy)>=s(1)*0.127*2) = [] ;
        end
        
        function offx = computeOffX(this)
            data = this.get('image') ;
            s = size(data) ;
            
            spacing = 20 ;
            sad = 2234.8 ;
            d = this.get('DISO') ;
            
%             sad = sad + 300 ;
            
            ind = 1 ;
            for i=0 : -spacing : -s(2)*0.127*2
                offx(ind) = -spacing*ind*(sad+d)/sad ;
                ind = ind+1 ;
            end
            
            offx((this.offset2+offx)<=0) = [] ;
            
            ind2 = 1 ;
            for i=spacing : spacing : s(2)*0.127*2
                offx(ind) = spacing*ind2*(sad+d)/sad ;
                ind = ind +1 ;
                ind2 = ind2 + 1 ;
            end
            
            offx((this.offset2+offx)>=s(2)*0.127*2) = [] ;
        end
    end
end
