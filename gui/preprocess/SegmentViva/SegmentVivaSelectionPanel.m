
classdef SegmentVivaSelectionPanel < SplitVivaSelectionPanel
    properties (Access = private)
        DISOText ;
        DISOEdit ;
    end
    
    methods (Access = public)
        function this = SegmentVivaSelectionPanel(f, control)
            this@SplitVivaSelectionPanel(f, control) ;
            
            set(this.goButton, 'Callback', @this.goCallback) ;
            
            this.DISOText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Distance to iso', 'Units', 'normalized', 'Position', [0.15, 0.55, 0.1, 0.3]) ;
            
            DISO = this.control.get('DISO') ;
            this.DISOEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(DISO), 'Units', 'normalized', 'Position', [0.15, 0.15, 0.1, 0.3]) ;
        end
    end
    
    methods (Access = private)
        function updateDISO(this)
            DISO = this.control.get('DISO') ;
            this.DISOEdit.set('String', num2str(DISO)) ;
        end
        
        function goCallback(this, source, event)
            items = get(this.PRPopup, 'String') ;
            index_selected = get(this.PRPopup, 'Value') ;
            simuName = items{index_selected} ;          
            this.control.set('PRName', simuName) ;
            this.control.set('DISO', str2double(this.DISOEdit.get('String'))) ;
        end
    end
end
