
classdef SegmentVivaPanel < MainPanel
    properties (Access = private)
        control ;
        
        selectionPanel ;
        viewPanel ;
    end
    
    methods (Access = public)
        function this = SegmentVivaPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            p1 = uipanel('Parent', this.mainPanel, 'Title', 'Data', 'Position', [0 0.87 1 0.12]) ;
            this.selectionPanel = SegmentVivaSelectionPanel(p1, this.control) ;
            p2 = uipanel('Parent', this.mainPanel, 'Position', [0 0 1 0.85], 'Title', 'View') ;
            this.viewPanel = SegmentVivaViewPanel(p2, this.control) ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible) ;
            this.selectionPanel.setVisible(visible) ;
            this.viewPanel.setVisible(visible) ;   
        end
    end
    
    methods (Access = private)
    end
end
