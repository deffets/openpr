
classdef PreprocessModel < GenericModel
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function obj = PreprocessModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
        end
    end
end
