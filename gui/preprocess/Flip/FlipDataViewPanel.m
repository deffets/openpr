
classdef FlipDataViewPanel < MainPanel
    properties (Access = private)       
        control ;
        
        subplot1 ;
        subplot2 ;
    end
    
    methods (Access = public)
        function this = FlipDataViewPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
             
            this.control.set('viewPanel', this) ;
            this.subplot1 = [] ;
            this.subplot2 = [] ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
        end
        
        function updateImage1(this)
            image = this.control.get('image1') ;
            data = image.get('data') ;
            spacing = image.get('spacing') ;
            
            if isempty(this.subplot1)
                this.subplot1 = subplot(1, 2, 1, 'Parent', this.mainPanel) ;
            end
            
            subplot(this.subplot1)
            imagesc(data)
            imshow(mat2gray(data), imref2d(size(data), spacing(2), spacing(1)))
            axis off
        end
        
        function updateImage2(this)
            image = this.control.get('image2') ;
            data = image.get('data') ;
            spacing = image.get('spacing') ;
            
            if isempty(this.subplot2)
                this.subplot2 = subplot(1, 2, 2, 'Parent', this.mainPanel) ;
            end
            
            subplot(this.subplot2)
            imshow(mat2gray(data), imref2d(size(data), spacing(2), spacing(1)))
            axis off
        end
    end
end
