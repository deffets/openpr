
classdef FlipDataControl < GenericControl
    properties(Access = private)
        viewPanel ;
        savePanel ;
    end

    methods (Access = public)
        function this = FlipDataControl(model)
            this@GenericControl(model) ;
            
            addlistener(this.model, 'image1', 'PostSet', @this.image1Listener) ;
            addlistener(this.model, 'image2', 'PostSet', @this.image2Listener) ;
        end
        
        function set(this, property, value)
            switch property
                case 'viewPanel'
                    this.viewPanel = value ;
                case 'savePanel'
                    this.savePanel = value ;
                otherwise
                    this.model.set(property, value) ;
            end
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
        
        function initImages(this)
            this.model.initImages() ;
        end
        
        function saveTransformation(this)
            this.model.saveTransformation() ;
        end
    end
    
    methods (Access = private)
        function image1Listener(this, source, event)
            this.viewPanel.updateImage1() ;
        end
        
        function image2Listener(this, source, event)
            this.viewPanel.updateImage2() ;
        end
    end
end
