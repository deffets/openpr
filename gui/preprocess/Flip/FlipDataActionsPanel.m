
classdef FlipDataActionsPanel < MainPanel
    properties (Access = private)       
        control ;
        
    end
    
    methods (Access = public)
        function this = FlipDataActionsPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;  

            flip1Button = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Flip vertically', 'Units', 'normalized', 'Position', [0.1, 0.9, 0.8, 0.05], 'Callback', @this.flip1Callback) ;
            flip2Button = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Flip horizontally', 'Units', 'normalized', 'Position', [0.1, 0.8, 0.8, 0.05], 'Callback', @this.flip2Callback) ;
            rotateButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Rotate', 'Units', 'normalized', 'Position', [0.1, 0.7, 0.8, 0.05], 'Callback', @this.rotateCallback) ;
            
            saveButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Save', 'Units', 'normalized', 'Position', [0.1, 0.5, 0.8, 0.05], 'Callback', @this.saveCallback) ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private)
        function flip1Callback(this, source, event)
            this.control.set('action', 'flip1')
        end
        
        function flip2Callback(this, source, event)
            this.control.set('action', 'flip2')
        end
        
        function rotateCallback(this, source, event)
            this.control.set('action', 'rot90')
        end
        
        function saveCallback(this, source, event)
            this.control.saveTransformation() ;
        end
    end
end
