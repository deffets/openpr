
classdef FlipDataSelectionPanel < MainPanel
    properties (Access = private)       
        control ;
        
        PRText1 ;
        PRText2 ;
        
        goButton ;

        PR1Popup ;
        PR2Popup ;
    end
    
    methods (Access = public)
        function this = FlipDataSelectionPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;  
            
            this.PRText1 = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'PR to adjust', 'Units', 'normalized', 'Position', [0.01, 0.55, 0.1, 0.3]) ;
            this.PRText2 = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Fixed PR', 'Units', 'normalized', 'Position', [0.15, 0.55, 0.1, 0.3]) ;
             
            this.PR1Popup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.15, 0.1, 0.3]) ;
            this.PR2Popup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.15, 0.15, 0.1, 0.3]) ;

            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.35, 0.1, 0.4], 'Callback', @this.goCallback) ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private)
        function updatePopups(this)
            PRNames = this.control.getPRNames() ;
            
            if ~isempty(PRNames)
                this.PR1Popup.set('String', PRNames) ;
                this.PR2Popup.set('String', PRNames) ;
            end
        end
        
        function goCallback(this, source, event)
            items = get(this.PR1Popup, 'String') ;
            index_selected = get(this.PR1Popup, 'Value') ;
            simuName = items{index_selected} ;          
            this.control.set('PRName1', simuName) ;
            
            items = get(this.PR2Popup, 'String') ;
            index_selected = get(this.PR2Popup, 'Value') ;
            PRName = items{index_selected} ;          
            this.control.set('PRName2', PRName) ;
            
            this.control.initImages() ;
        end
    end
end
