
classdef FlipDataModel < GenericModel
    properties (SetObservable)
        image1 ;
        image2 ;
    end
    
    properties (Access = private)
        PRName1 ;
        PRName2 ;
        
        transfo ;
    end
    
    methods (Access = public)
        function this = FlipDataModel(regguiHthisect)
            this@GenericModel(regguiHthisect) ;
            
            this.PRName1 = [] ;
            this.PRName2 = [] ;
            
            this.image1 = [] ;
            this.image2 = [] ;
        end
        
        function set(this, property, value)
            switch property
                case 'PRName1'
                    this.PRName1 = value ;
                case 'PRName2'
                    this.PRName2 = value ;
                case 'action'
                    this.addAction(value) ;
            end
        end
        
        function value = get(this, property)
            switch property
                case 'PRName1'
                    value = this.PRName1 ;
                case 'PRName2'
                    value = this.PRName2 ;
                case 'image1'
                    value = this.image1 ;
                case 'image2'
                    value = this.image2 ;
            end
        end
        
        function initImages(this)
            this.transfo = [] ;
            
            handles = guidata(this.regguiHObject) ;
            
            braggImage1 = handles2BraggImage(this.PRName1, handles) ;
            origin1 = braggImage1.get('origin') ;
            spacing1 = braggImage1.get('spacing') ;
            im1 = maximumPosition(braggImage1.get('data'), braggImage1.get('depth')) ;
            this.image1 = Image2D(im1, origin1, spacing1) ;
            
            braggImage2 = handles2BraggImage(this.PRName2, handles) ;
            origin2 = braggImage2.get('origin') ;
            spacing2 = braggImage2.get('spacing') ;
            im2 = maximumPosition(braggImage2.get('data'), braggImage2.get('depth')) ;
            this.image2 = Image2D(im2, origin2, spacing2) ;
        end
        
        function saveTransformation(this)
            handles = guidata(this.regguiHObject) ;
            instruction = ['handles = applyFlipTransform(' '''' this.PRName1 '''' ', ' '[' ...
                num2str(this.transfo) '], handles) ;'] ;
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
    
    methods (Access = private)
        function addAction(this, value)
            im1 = this.image1.get('data') ;
            origin1 = this.image1.get('origin') ;
            spacing1 = this.image1.get('spacing') ;
            
            switch value
                case 'flip1'
                    im1 = flip(im1, 1) ;
                    this.transfo(length(this.transfo)+1) = 1 ;
                case 'flip2'
                    im1 = flip(im1, 2) ;
                    this.transfo(length(this.transfo)+1) = 2 ;
                case 'rot90'
                    im1 = rot90(im1) ;
                    this.transfo(length(this.transfo)+1) = 90 ;
            end
            
            this.image1 = Image2D(im1, origin1, spacing1) ;
        end
    end
end
