
classdef PRadioView < handle
    % View of the Proton Radiography interface
    %   See also PRadioControl PRadioModel
    
    properties (Access = private)
        control ;
        
        regguiFig ;
        f ;
        mainPanel ,

        tabGroup ;
        dataTab ;
        preprocessTab ;
        simulationTab ;
        registrationTab ;
        viewTab ;
        %!RELEASE
        optimisationTab ;
        exportTab ;
        %RELEASE
        dockTab ;
        closeTab ;
        helpTab;
        dataEditTab;
        
        dataPanel ;
        preprocessPanel ;
        registrationPanel ;
        simulationPanel ;
        %!RELEASE
        optimizationPanel ;
        %RELEASE
        viewPanel ;
        %!RELEASE
%         exportPanel ;
        %RELEASE
        helpPanel;
        dataEditPanel;
        
        pradioStatusView ;
    end
    
    methods (Access = public)
        function obj = PRadioView(control)
            % PRadioView Creates the View of the PR interface
            %   PRadioView(control) where control is any object inherited
            %   from PRadioControl
            
            obj.control = control ;
            obj.regguiFig = findobj('Name', 'regguiC') ;
            
%             obj.f = figure('Name', 'Proton radiography interface', 'MenuBar', 'None', 'NumberTitle', 'off') ;
            obj.f = uipanel(obj.regguiFig, 'BorderType', 'none', 'Tag', 'PR Interface') ;
            
            obj.initTab() ;
        end
        
        function initTab(obj)
            obj.tabGroup = uitabgroup(obj.f, 'Position', [0 0.04 1 0.96]) ;
            
            statusPanel = uipanel(obj.f, 'Position', [0 0 1 0.04], 'BorderType', 'none') ;
            obj.pradioStatusView = PRadioStatusView(statusPanel, obj.control) ;
            
            obj.dataTab = uitab(obj.tabGroup, 'Title', 'Load', 'ButtonDownFcn', @obj.dataCallback) ;
            obj.simulationTab = uitab(obj.tabGroup, 'Title', 'Simulation', 'ButtonDownFcn', @obj.simulationCallback) ;
            obj.preprocessTab = uitab(obj.tabGroup, 'Title', 'Pre-process', 'ButtonDownFcn', @obj.preprocessCallback) ;
            obj.registrationTab = uitab(obj.tabGroup, 'Title', 'Registration', 'ButtonDownFcn', @obj.registrationCallback) ;
            obj.viewTab = uitab(obj.tabGroup, 'Title', 'View (Compare)', 'ButtonDownFcn', @obj.viewCallback) ;
            obj.dataEditTab = uitab(obj.tabGroup, 'Title', 'Data', 'ButtonDownFcn', @obj.dataEditCallback) ;
            
            %!RELEASE
%             obj.optimisationTab = uitab(obj.tabGroup, 'Title', 'Optimization', 'ButtonDownFcn', @obj.optimizationCallback) ;
%             obj.exportTab = uitab(obj.tabGroup, 'Title', 'Export', 'ButtonDownFcn', @obj.exportCallback) ;
            %RELEASE
            obj.dockTab = uitab(obj.tabGroup, 'Title', 'Undock', 'ButtonDownFcn', @obj.dockCallback) ;
            obj.closeTab = uitab(obj.tabGroup, 'Title', 'Close', 'ButtonDownFcn', @obj.closeCallback) ;
            obj.helpTab = uitab(obj.tabGroup, 'Title', 'Help', 'ButtonDownFcn', @obj.helpCallback) ;

            obj.dataPanel = LoadPanel(obj.dataTab, obj.control.getDataControl()) ;
            obj.dataPanel.setVisible('on') ;
            
            obj.simulationPanel = SimulationPanel(obj.simulationTab, obj.control.getSimulationControl()) ;
            obj.simulationPanel.setVisible('off') ;
            
            obj.preprocessPanel = PreprocessPanel(obj.preprocessTab, obj.control.getPreprocessControl()) ;
            obj.preprocessPanel.setVisible('off') ;
            
            obj.registrationPanel = RegistrationPanel(obj.registrationTab, obj.control.getRegistrationControl()) ;
            obj.registrationPanel.setVisible('off') ;

            obj.viewPanel = ViewMainPanel(obj.viewTab, obj.control.getViewControl()) ;
            obj.viewPanel.setVisible('off') ;
            
            obj.dataEditPanel = DataEditPanel(obj.dataEditTab, obj.control.get('dataEditControl')) ;
            obj.dataEditPanel.setVisible('off') ;
            
            %!RELEASE
%             obj.optimizationPanel = OptimizationPanel(obj.optimisationTab, obj.control.getOptimizationControl()) ;
%             obj.optimizationPanel.setVisible('off') ;
            
%             obj.exportPanel = ExportPanel(obj.exportTab, obj.control.getExportControl()) ;
%             obj.exportPanel.setVisible('off') ;
            %RELEASE
            
            obj.helpPanel = HelpPanel(obj.helpTab, obj.control.get('helpControl')) ;
            obj.helpPanel.setVisible('off') ;
        end
        
        function dataCallback(this, source, event)
            % dataCallback Display the data panel
            
            this.clearDisplay() ;
            this.dataPanel.setVisible('on') ;
        end
        
        function preprocessCallback(this, source, event)
            % preprocessCallback Display the preprocess panel
            
            this.clearDisplay() ;
            this.preprocessPanel.setVisible('on') ;
        end
        
        function registrationCallback(this, source, event)
            % registrationCallback Display the registration panel
            
            this.clearDisplay() ;
            this.registrationPanel.setVisible('on') ;
        end
        
        function simulationCallback(this, source, event)
            % simulationCallback Display the simulation panel
            
            this.clearDisplay() ;
            this.simulationPanel.setVisible('on') ;
        end
        
        %!RELEASE
        function optimizationCallback(this, source, event)
            % optimizationCallback Display the optimization panel
            
            this.clearDisplay() ;
            this.optimizationPanel.setVisible('on') ;
        end
        %RELEASE
        
        function viewCallback(this, source, event)
            % viewCallback Display the view panel
            
            this.clearDisplay() ;
            this.viewPanel.setVisible('on') ;
        end
        
        function dataEditCallback(this, source, event)
            % dataCallback Display the data panel

            this.clearDisplay() ;
            this.dataEditPanel.setVisible('on') ;
        end
        
        %!RELEASE
%         function exportCallback(this, source, event)
%             % exportCallback Display the export panel
%             
%             this.clearDisplay() ;
%             this.exportPanel.setVisible('on') ;
%         end
        %RELEASE
        
        function dockCallback(this, source, event)
            % dockCallback Dock/undock the PR interface in/from the Reggui
            % interface
            
            this.unclearDisplay() ;
            
            if strcmp(this.dockTab.get('Title'), 'Undock')
                und = 1 ;

                newF = [];
                %!RELEASE
                newF = figure('Name', 'Proton Radiography Interface', 'MenuBar', 'None', 'NumberTitle', 'off', 'units', 'normalized', 'outerposition', [0 0 1 1]);
                %RELEASE
                if isempty(newF)
                    newF = figure('Name', 'Proton Radiography Interface - open', 'MenuBar', 'None', 'NumberTitle', 'off', 'units', 'normalized', 'outerposition', [0 0 1 1]);
                end

                newF = uipanel(newF, 'BorderType', 'none', 'Tag', 'PR Interface fig') ;
            else
                und = 0 ;
                newF = uipanel(this.regguiFig, 'BorderType', 'none', 'Tag', 'PR Interface') ;
            end
                
            this.closeCallback([], []) ;
            this.f = newF ;
            this.initTab() ;

            if und
                this.dockTab.set('Title', 'Dock') ;
            else
                this.dockTab.set('Title', 'Undock') ;
            end
            set(this.regguiFig, 'toolbar', 'none')
        end
        
        function closeCallback(this, source, event)
            % closeCallback Close the PR interface
            
            this.unclearDisplay();
            set(this.regguiFig, 'toolbar', 'none');
            
            if strcmp(get(this.f, 'Tag'), 'PR Interface fig')
                delete(get(this.f, 'Parent')) ;
            else
                delete(this.f) ;
            end
        end
        
        function helpCallback(this, source, event)
            this.clearDisplay() ;
            this.helpPanel.setVisible('on') ;
        end
        
        function clearDisplay(this)
            % clearDisplay Set visibility of all panels to 'off'
            
            this.dataPanel.setVisible('off') ;
            this.preprocessPanel.setVisible('off') ;
            this.registrationPanel.setVisible('off') ;
            this.simulationPanel.setVisible('off') ;
            %!RELEASE
%             this.optimizationPanel.setVisible('off') ;
            %RELEASE
            this.viewPanel.setVisible('off') ;
            this.dataEditPanel.setVisible('off') ;
            %!RELEASE
%             this.exportPanel.setVisible('off') ;
            %RELEASE
            this.helpPanel.setVisible('off');
        end
        
        function unclearDisplay(this)
            % unclearDisplay Set visibility of all panels to 'on'
            
            this.dataPanel.setVisible('on') ;
            this.preprocessPanel.setVisible('on') ;
            this.registrationPanel.setVisible('on') ;
            this.simulationPanel.setVisible('on') ;
            %!RELEASE
%             this.optimizationPanel.setVisible('on') ;
            %RELEASE
            this.viewPanel.setVisible('on') ;
            this.dataEditPanel.setVisible('on') ;
            %!RELEASE
%             this.exportPanel.setVisible('on') ;
            %RELEASE
            this.helpPanel.setVisible('on');
        end
    end
end
