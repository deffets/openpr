
classdef PRadioStatusView < MainPanel
    properties (Access = private)
        control ;
        
        p2 ;
        msg1 ;
        progressView ;
    end
    
    methods (Access = public)
        function this = PRadioStatusView(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            c = fix(clock) ;
            this.msg1 = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', ['Proton Radiography Interface - Opened ' num2str(c(1)) '/' num2str(c(2)) '/' num2str(c(3)) ' ' num2str(c(4)) ':' num2str(c(5))], 'Units', 'normalized', 'Position', [0.11, 0.5, 0.2, 0.49]) ;
        
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', ['Version: ' this.control.get('version')], 'Units', 'normalized', 'Position', [0.11, 0.01, 0.2, 0.49]) ;
            
            %!RELEASE
            p1 = uipanel('Parent', this.mainPanel, 'Units', 'Normalized', 'Position', [0, 0, 0.1, 1], 'BorderType', 'None') ;
            ax1 = axes('Parent', p1, 'Units', 'Normalized', 'Position', [0 0 0.33 1]) ;
            imshow(fullfile('icons', 'iba_logo.jpg'), 'Parent', ax1)
            ax2 = axes('Parent', p1, 'Units', 'Normalized', 'Position', [0.33 0 0.33 1]) ;
            imshow(fullfile('icons', 'ucl_logo.jpg'), 'Parent', ax2)
            %RELEASE
            
            this.p2 = uipanel('Parent', this.mainPanel, 'Units', 'Normalized', 'Position', [0.6 0 0.4 1]) ;
            this.progressView = ProgressView(this.p2, this.control.get('progressControl')) ;
        end
        
        function delete(this)
        end
    end
end
