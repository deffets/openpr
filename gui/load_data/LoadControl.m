
classdef LoadControl < GenericControl
    properties (Access = private)
        regguiHObject ;
        
        dataControl ;
        %!RELEASE
        dataFPControl ;
        %RELEASE
        dataPlanControl;
    end
    
    methods (Access = public)
        function obj = LoadControl(model, regguiHObject)
            obj@GenericControl(model) ;
            obj.regguiHObject = regguiHObject ;
            
            dataModel = DataModel(obj.model.getRegguiHObject()) ;
            obj.dataControl = DataControl(dataModel) ;
            
            %!RELEASE
            dataFPModel = DataFPModel(obj.model.getRegguiHObject()) ;
            obj.dataFPControl = DataFPControl(dataFPModel) ;
            %RELEASE
            
            dataPlanModel = DataPlanModel(obj.model.getRegguiHObject());
            obj.dataPlanControl = DataControl(dataPlanModel);
        end
        
        function ctrl = getDataControl(this)
            ctrl = this.dataControl ;
        end
        
        %!RELEASE
        function ctrl = getDataFPControl(this)
            ctrl = this.dataFPControl ;
        end
        %RELEASE
        
        function ctrl = getDataPlanControl(this)
            ctrl = this.dataPlanControl();
        end
    end
end
