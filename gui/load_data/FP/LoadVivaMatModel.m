
classdef LoadVivaMatModel < LoadVivaSeqModel    
    methods (Access = public)
        function this = LoadVivaMatModel(regguiHObject)
            this@LoadVivaSeqModel(regguiHObject) ;
        end
        
        function importMat(this)
            handles = guidata(this.regguiHObject) ;
            instruction = ['handles = loadMatViva(' '''' this.PRFile '''' ', ' '''' this.PREnergyFile '''' ', [' num2str(this.spacing) '], [' num2str(this.spotsNb) '], ' '''' this.PRName '''' ', handles) ;'] ;
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
    
    methods (Access = private)
        
    end
end
