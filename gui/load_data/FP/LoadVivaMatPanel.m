
classdef LoadVivaMatPanel < LoadVivaSeqPanel    
    methods (Access = public)
        function this = LoadVivaMatPanel(f, control)
            this@LoadVivaSeqPanel(f, control) ;
            
            this.goButton1.set('Callback', @this.goCallback) ;
          
            this.update('all') ;
        end
    end
    
    methods (Access = private)        
        function goCallback(this, source, event)
            this.control.set('PRFile', get(this.PRFileEdit, 'String')) ;
            this.control.set('PREnergyFile', get(this.PREnergyFileEdit, 'String')) ;
            this.control.set('spotsNb', [str2num(get(this.spotsNbEdit1, 'String')), str2num(get(this.spotsNbEdit2, 'String'))]) ;
            this.control.set('spacing', [str2num(get(this.spacingEdit1, 'String')), str2num(get(this.spacingEdit2, 'String'))]) ;
            this.control.set('PRName', get(this.PRNameEdit, 'String')) ;
            
            this.control.importMat() ;
        end
    end
end
