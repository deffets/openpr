
classdef LoadVivaSeqModel < GenericModel
    properties (SetObservable)
        PRFile ;
        PREnergyFile ;
        spacing ;
        spotsNb ;
        PRName ;
    end
    
    methods (Access = public)
        function obj = LoadVivaSeqModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
            
            obj.PRFile = [] ;
            obj.PREnergyFile = [] ;

            obj.spacing = [20 20] ;
            obj.spotsNb = [20 20] ;
            
            obj.PRName = [] ;
        end
        
        function value = get(this, varargin)
            switch varargin{1}
                case 'PRFile'
                    value = this.PRFile ;
                case 'PREnergyFile'
                    value = this.PREnergyFile ;
                case 'spacing'
                    value = this.spacing ;
                case 'spotsNb'
                    value = this.spotsNb ;
                case 'PRName'
                    value = this.PRName ;
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end
        
        function set(this, varargin)
            value = varargin{2};
            
            switch varargin{1}
                case 'PRFile'
                    this.PRFile = value ;
                case 'PREnergyFile'
                    this.PREnergyFile = value ;
                case 'spacing'
                    this.spacing = value ;
                case 'spotsNb'
                    this.spotsNb = value ;
                case 'PRName'
                    this.PRName = value ;
                otherwise
                    set@GenericModel(this, varargin{:});
            end
        end
        
        function importPR(this)
            handles = guidata(this.regguiHObject) ;
            instruction = ['handles = loadPRFP(' '''' this.PRFile '''' ', ' '''' this.PREnergyFile '''' ', [' num2str(this.spacing) '], [' num2str(this.spotsNb) '], ' '''' this.PRName '''' ', handles) ;'] ;
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
    
    methods (Access = private)
        
    end
end
