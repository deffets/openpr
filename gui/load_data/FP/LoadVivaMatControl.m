
classdef LoadVivaMatControl < LoadVivaSeqControl    
    methods (Access = public)
        function this = LoadVivaMatControl(model)
            this@LoadVivaSeqControl(model) ;
        end
        
        function importMat(this)
            this.model.importMat() ;
        end
    end    
end
