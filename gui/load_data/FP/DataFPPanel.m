
classdef DataFPPanel < MainPanel
    properties (Access = private)
        control ;
        
        loadVivaSeqPanel ;
        loadVivaMatPanel ;
    end
    
    methods (Access = public)
        function this = DataFPPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            % Load Viva Seq file
            loadVivaSeqP = uipanel('Parent', this.mainPanel, 'Title', 'Load Viva file', 'Position', [0 0.825 1 0.15]) ;
            this.loadVivaSeqPanel = LoadVivaSeqPanel(loadVivaSeqP, this.control.get('loadVivaSeqControl')) ;
            
            % Load Viva Mat file
            loadVivaMatP = uipanel('Parent', this.mainPanel, 'Title', 'Load Mat file (MC2 simulation)', 'Position', [0 0.65 1 0.15]) ;
            this.loadVivaMatPanel = LoadVivaMatPanel(loadVivaMatP, this.control.get('loadVivaMatControl')) ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
            
            this.loadVivaSeqPanel.setVisible(visible) ;
            this.loadVivaMatPanel.setVisible(visible) ;
        end
    end
end
