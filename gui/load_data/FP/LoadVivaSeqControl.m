
classdef LoadVivaSeqControl < GenericControl    
    methods (Access = public)
        function obj = LoadVivaSeqControl(model)
            obj@GenericControl(model) ;
        end
        
        function value = get(this, varargin)
            value = this.model.get(varargin{:}) ;
        end
        
        function set(this, varargin)
            this.model.set(varargin{:}) ;
        end
        
        function importPR(this)
            this.model.importPR() ;
        end
    end
    
    methods (Access = private)
        
    end
    
end
