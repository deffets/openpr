
classdef LoadVivaSeqPanel < MainPanel
    properties (Access = protected)
        loadPRPanel ;
        
        PRFileText ;
        PREnergyFileText ;
        spotsNbText ;
        spacingText ;
        PRNameText ;
        
        PRFileButton ;    
        PREnergyFileButton ,
        goButton1 ;
       
        PRFileEdit ;
        PREnergyFileEdit ;
        spotsNbEdit1 ;
        spotsNbEdit2 ;
        spacingEdit1 ;
        spacingEdit2 ;
        
        PRNameEdit ;

        control ;
    end
    
    methods (Access = public)
        function this = LoadVivaSeqPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
          
            this.PRFileText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Viva file', 'Units', 'normalized', 'Position', [0.01, 0.7, 0.1, 0.2]) ;
            this.PREnergyFileText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Energy file', 'Units', 'normalized', 'Position', [0.15, 0.7, 0.1, 0.2]) ;
            this.spotsNbText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Spots per frame', 'Units', 'normalized', 'Position', [0.3, 0.7, 0.1, 0.2]) ;
            this.spacingText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Spacing', 'Units', 'normalized', 'Position', [0.45, 0.7, 0.1, 0.2]) ;
            this.PRNameText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.6, 0.7, 0.1, 0.2]) ;
            
            this.PRFileButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Open', 'Units', 'normalized', 'Position', [0.01, 0.4, 0.1, 0.2], 'Callback', @this.PRFileCallback) ;
            this.PREnergyFileButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Open', 'Units', 'normalized', 'Position', [0.15, 0.4, 0.1, 0.2], 'Callback', @this.PREnergyFileCallback) ;
            this.goButton1 = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.89, 0.3, 0.1, 0.4], 'Callback', @this.go1Callback) ;
          
            this.update('all') ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
        end
        
        function update(this, property)
            switch property
                case 'all'
                    this.update('PRFile') ;
                    this.update('PREnergyFile') ;
                    this.update('spotsNb') ;
                    this.update('spacing') ;
                    this.update('PRName') ;
                case 'PRFile'
                    PRFile = this.control.get('PRFile') ;
                    this.PRFileEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', PRFile, 'Units', 'normalized', 'Position', [0.01, 0.1, 0.1, 0.2]) ;
                case 'PREnergyFile'
                    PREnergyFile = this.control.get('PREnergyFile') ;
                    this.PREnergyFileEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', PREnergyFile, 'Units', 'normalized', 'Position', [0.15, 0.1, 0.1, 0.2]) ;
                case 'spotsNb'
                    spotsNb = this.control.get('spotsNb') ;
                    this.spotsNbEdit1 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(spotsNb(1)), 'Units', 'normalized', 'Position', [0.3, 0.4, 0.025, 0.2]) ;
                    this.spotsNbEdit2 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(spotsNb(2)), 'Units', 'normalized', 'Position', [0.35, 0.4, 0.025, 0.2]) ;
                case 'spacing'
                    spacing = this.control.get('spacing') ;
                    this.spacingEdit1 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(spacing(1)), 'Units', 'normalized', 'Position', [0.45, 0.4, 0.025, 0.2]) ;
                    this.spacingEdit2 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', num2str(spacing(2)), 'Units', 'normalized', 'Position', [0.5, 0.4, 0.025, 0.2]) ;
                case 'PRName'
                    PRName = this.control.get('PRName') ;
                    this.PRNameEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'String', PRName, 'Units', 'normalized', 'Position', [0.6, 0.4, 0.1, 0.2]) ;
            end
        end
    end
    
    methods (Access = private)
        function PRFileCallback(this, source, event)
            [fileName, pathName] = uigetfile('*.*') ;
            this.control.set('PRFile', fullfile(pathName, fileName)) ;
            this.update('PRFile') ;
        end
        
        function PREnergyFileCallback(this, source, event)
            [fileName, pathName] = uigetfile('*.*') ;
            this.control.set('PREnergyFile', fullfile(pathName, fileName)) ;
            this.update('PREnergyFile') ;
        end
        
        function go1Callback(this, source, event)
            this.control.set('PRFile', get(this.PRFileEdit, 'String')) ;
            this.control.set('PREnergyFile', get(this.PREnergyFileEdit, 'String')) ;
            this.control.set('spotsNb', [str2num(get(this.spotsNbEdit1, 'String')), str2num(get(this.spotsNbEdit2, 'String'))]) ;
            this.control.set('spacing', [str2num(get(this.spacingEdit1, 'String')), str2num(get(this.spacingEdit2, 'String'))]) ;
            this.control.set('PRName', get(this.PRNameEdit, 'String')) ;
            
            this.control.importPR() ;
        end
    end
    
    
end
