
classdef DataFPControl < GenericControl
    properties (Access = private)
        loadVivaSeqControl ;
        loadVivaMatControl ;
    end
    
    methods (Access = public)
        function this = DataFPControl(model)
            this@GenericControl(model) ;
            
            loadVivaSeqModel = LoadVivaSeqModel(this.model.getRegguiHObject()) ;
            this.loadVivaSeqControl = LoadVivaSeqControl(loadVivaSeqModel) ;
            
            loadVivaMatModel = LoadVivaMatModel(this.model.getRegguiHObject()) ;
            this.loadVivaMatControl = LoadVivaMatControl(loadVivaMatModel) ;
        end
        
        function value = get(this, property)
            switch property
                case 'loadVivaSeqControl'
                    value = this.loadVivaSeqControl ;
                case 'loadVivaMatControl'
                    value = this.loadVivaMatControl ;
            end
        end
    end    
end
