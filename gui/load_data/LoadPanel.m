classdef LoadPanel < MainPanel
    properties (Access = private)
        control ;
        
        tabGroup ;
        
        dataPanel ;
        %!RELEASE
        dataFPPanel ;
        %RELEASE
        
        dataPlanPanel;
    end
    
    methods (Access = public)
        function this = LoadPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.tabGroup = uitabgroup(this.mainPanel, 'Position', [0 0 1 1]) ;
            
            dataTab = uitab(this.tabGroup, 'Title', 'General', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'data') ;
            this.dataPanel = DataPanel(dataTab, this.control.getDataControl()) ;
            
            %!RELEASE
            dataFPTab = uitab(this.tabGroup, 'Title', 'Flat Panel', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'dataFP') ;
            this.dataFPPanel = DataFPPanel(dataFPTab, this.control.getDataFPControl()) ;
            %RELEASE
            
            dataPlanTab = uitab(this.tabGroup, 'Title', 'Plan', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'dataPlan') ;
            this.dataPlanPanel = DataPlanPanel(dataPlanTab, this.control.getDataControl()) ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
            this.dataPanel.setVisible(visible) ;
            %!RELEASE
            this.dataFPPanel.setVisible(visible) ;
            %RELEASE
            this.dataPlanPanel.setVisible(visible) ;
        end
    end
    
    methods (Access = private)
        function panelCallback(this, source, event)
            switch source.Tag
                case 'data'
                    this.dataPanel.setVisible('on') ;
                    this.dataPlanPanel.setVisible('off') ;
                    %!RELEASE
                    this.dataFPPanel.setVisible('off') ;
                
                case 'dataFP'
                    this.dataPanel.setVisible('off') ;
                    this.dataPlanPanel.setVisible('off') ;
                    this.dataFPPanel.setVisible('on') ;
                %RELEASE
                case 'dataPlan'
                    this.dataPanel.setVisible('off') ;
                    this.dataFPPanel.setVisible('off') ;
                    this.dataPlanPanel.setVisible('on') ;
            end
        end
    end
end
