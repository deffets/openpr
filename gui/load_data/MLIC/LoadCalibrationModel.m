
classdef LoadCalibrationModel < GenericModel
    properties (Access = private)
        calibrationFile ;
        materialsFile ;
        calibrationName ;
    end
    
    methods (Access = public)
        function this = LoadCalibrationModel(regguiHObject)
            this@GenericModel(regguiHObject) ;
            
            this.calibrationFile = [] ;
            this.materialsFile = [] ;
            this.calibrationName = [] ;
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'calibrationFile'
                    value = this.calibrationFile ;
                case 'materialsFile'
                    value = this.materialsFile ;
                case 'calibrationName'
                    value = this.calibrationName ;
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end

        function set(this, varargin)
            value = varargin{2};
            
            switch varargin{1}
                case 'calibrationFile'
                    this.calibrationFile = value ;
                case 'materialsFile'
                    this.materialsFile = value ;
                case 'calibrationName'
                    this.calibrationName = value ;
                otherwise
                    set@GenericModel(this, varargin{:});
            end
        end

        function importCurve(this)
            handles = guidata(this.regguiHObject) ;
            instruction = ['handles = loadCalibration(' '''' this.calibrationFile '''' ', ' '''' this.materialsFile '''' ', ' '''' this.calibrationName '''' ', handles) ;'] ;
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
end
