
classdef LoadMLICPRPanel < MainPanel
    properties (Access = private)
        control ;
        
        fileEdit ;
        calibrationFileEdit ;
        spotsNbEdit1 ;
        spotsNbEdit2 ;
        spacingEdit1 ;
        spacingEdit2 ;
        energyEdit ;
        nameEdit ;
        
        fileButton ;
        calibrationFileButton ;
        goButton ;
    end
    
    methods (Access = public)
        function this = LoadMLICPRPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            % File
            this.fileButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Open', 'Units', 'normalized', 'Position', [0.01, 0.4, 0.1, 0.2], 'Callback', @this.fileCallback) ;
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'PR file', 'Units', 'normalized', 'Position', [0.01, 0.7, 0.1, 0.2]) ;
            this.fileEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.01, 0.1, 0.1, 0.2]) ;
            
            % Calibration file
            this.calibrationFileButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Open', 'Units', 'normalized', 'Position', [0.15, 0.4, 0.1, 0.2], 'Callback', @this.calibrationFileCallback) ;
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Calibration file', 'Units', 'normalized', 'Position', [0.15, 0.7, 0.1, 0.2]) ;
            this.calibrationFileEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.15, 0.1, 0.1, 0.2]) ;
            
            % SpotsNb
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Spots per frame', 'Units', 'normalized', 'Position', [0.3, 0.7, 0.1, 0.2]) ;
            this.spotsNbEdit1 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.3, 0.4, 0.025, 0.2]) ;
            this.spotsNbEdit2 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.35, 0.4, 0.025, 0.2]) ;
            
            % spacing
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Spacing', 'Units', 'normalized', 'Position', [0.45, 0.7, 0.1, 0.2]) ;
            this.spacingEdit1 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.45, 0.4, 0.025, 0.2]) ;
            this.spacingEdit2 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.5, 0.4 0.025, 0.2]) ;
            
            % energy
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Energy', 'Units', 'normalized', 'Position', [0.6, 0.7, 0.1, 0.2]) ;
            this.energyEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.6, 0.4, 0.1, 0.2]) ;
            
            % Name
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.75, 0.7, 0.1, 0.2]) ;
            this.nameEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.75, 0.4, 0.1, 0.2]) ;
            
            % Go
            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.89, 0.3, 0.1, 0.4], 'Callback', @this.goCallback) ;
            
            uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', '?', 'Units', 'normalized', 'Position', [0.97, 0.8, 0.03, 0.2], 'Callback', @this.helpCallback) ;
            
            % Update edit uicontrol
            this.updateEditControl('all') ;
        end
    end
    
    methods (Access = private)
        function updateFile(this)
            fileName = this.control.get('fileName') ;
            set(this.fileEdit, 'String', fileName) ;
        end
        
        function updateEditControl(this, controlName)
            switch controlName
                case 'all'
                    this.updateEditControl('PRFile') ;
                    this.updateEditControl('PRCalibrationFile') ;
                    this.updateEditControl('spotsNb') ;
                    this.updateEditControl('spacing') ;
                    this.updateEditControl('PRName') ;
                    this.updateEditControl('energy') ;
                case 'PRFile'
                    PRFile = this.control.get('PRFile') ;
                    set(this.fileEdit, 'String', PRFile) ;
                case 'PRCalibrationFile'
                    PRCalibrationFile = this.control.get('PRCalibrationFile') ;
                    set(this.calibrationFileEdit, 'String', PRCalibrationFile) ;
                case 'spotsNb'
                    spotsNb = this.control.get('spotsNb') ;
                    set(this.spotsNbEdit1, 'String', num2str(spotsNb(1))) ;
                    set(this.spotsNbEdit2, 'String', num2str(spotsNb(2))) ;
                case 'spacing'
                    spacing = this.control.get('spacing') ;
                    set(this.spacingEdit1, 'String', num2str(spacing(1))) ;
                    set(this.spacingEdit2, 'String', num2str(spacing(2))) ;
                case 'PRName'
                    PRName = this.control.get('PRName') ;
                    set(this.nameEdit, 'String', PRName) ;
                case 'energy'
                    energy = this.control.get('energy') ;
                    set(this.energyEdit, 'String', energy) ;
            end
        end
        
        function fileCallback(this, source, event)
            path = this.control.get('path', 'PRFile');
            [fileName, pathName] = uigetfile(fullfile(path, '*.*'));
            
            if fileName~=0
                this.control.set('path', 'PRFile', pathName);
            
                this.control.set('PRFile', fullfile(pathName, fileName)) ;
                this.updateEditControl('PRFile') ;
            end
        end
        
        function calibrationFileCallback(this, source, event)
            path = this.control.get('path', 'PRCalibrationFile');
            [fileName, pathName] = uigetfile(fullfile(path, '*.*'));
            
            if fileName~=0
                this.control.set('path', 'PRCalibrationFile', pathName);
            
                this.control.set('PRCalibrationFile', fullfile(pathName, fileName)) ;
                this.updateEditControl('PRCalibrationFile') ;
            end
        end
        
        function goCallback(this, source, event)
            this.control.set('PRFile', get(this.fileEdit, 'String')) ;
            this.control.set('PRCalibrationFile', get(this.calibrationFileEdit, 'String')) ;
            this.control.set('spotsNb', [str2double(get(this.spotsNbEdit1, 'String')), str2double(get(this.spotsNbEdit2, 'String'))]) ;
            this.control.set('spacing', [str2double(get(this.spacingEdit1, 'String')), str2double(get(this.spacingEdit2, 'String'))]) ;
            this.control.set('PRName', get(this.nameEdit, 'String')) ;
            
            this.control.importPR() ;
        end
        
        function helpCallback(this, source, event)
            web(this.control.get('help', 'Load_PR'), '-browser');
        end
    end
end
