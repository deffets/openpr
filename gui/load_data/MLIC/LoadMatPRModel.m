
classdef LoadMatPRModel < GenericModel
    properties (Access = private)
        fileName;
        spacing;
        depthOrigin;
        depthSpacing;
        name;
    end
    
    methods (Access = public)
        function this = LoadMatPRModel(regguiHthisect)
            this@GenericModel(regguiHthisect);
            
            this.fileName = [];
            this.name = [];
            this.spacing = [5, 5];
            this.depthOrigin = 0;
            this.depthSpacing = 1;
        end
        
        function set(this, varargin)
            value = varargin{2};
            
            switch varargin{1}
                case 'fileName'
                    this.fileName = value;
                case 'name'
                    this.name = value;
                case 'spacing'
                    this.spacing = value;
                case 'depthOrigin'
                    this.depthOrigin = value;
                case 'depthSpacing'
                    this.depthSpacing = value;
                otherwise
                    set@GenericModel(this, varargin{:});
            end
        end
        
        function value = get(this, varargin)
            switch varargin{1}
                case 'fileName'
                    value = this.fileName;
                case 'name'
                    value = this.name;
                case 'spacing'
                    value = this.spacing;
                case 'depthOrigin'
                    value = this.depthOrigin;
                case 'depthSpacing'
                    value = this.depthSpacing;
                otherwise
                    value = get@GenericModel(this, varargin{:});    
            end
        end
        
        function importMatPR(this)
            handles = guidata(this.regguiHObject);
                        
            instruction = ['handles = loadMatPR(' '''' this.fileName '''' ', [' num2str(this.spacing) '], ' num2str(this.depthOrigin) ', ' num2str(this.depthSpacing) ', ' '''' this.name '''' ', ' 'handles);'];
            handles.instructions{length(handles.instructions)+1} = instruction;
            handles = Execute_reggui_instructions(handles);
            guidata(this.regguiHObject, handles);
        end
    end
end