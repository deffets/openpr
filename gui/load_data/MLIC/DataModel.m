
classdef DataModel < GenericModel
    properties (SetObservable);
    end
    
    methods (Access = public)
        function obj = DataModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
        end
    end
end
