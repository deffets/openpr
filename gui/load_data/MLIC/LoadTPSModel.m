
classdef LoadTPSModel < GenericModel
    properties (Access = private)
        fileName ;
        name ;
        spotsNb ;
        spacing ;
        direction ;
        depthOrigin;
        depthStep;
    end 
    
    methods (Access = public)
        function obj = LoadTPSModel(regguiHObject)
            obj@GenericModel(regguiHObject) ;
            
            obj.fileName = [] ;
            obj.name = [] ;
            obj.spotsNb = [9, 9] ;
            obj.spacing = [5, 5] ;
            obj.depthOrigin = 0;
            obj.depthStep = 1;
            obj.direction = ['X', 'Y'] ;
        end
        
        function set(this, varargin)
            value = varargin{2};
            
            switch varargin{1}
                case 'fileName'
                    this.fileName = value ;
                case 'name'
                    this.name = value ;
                case 'spotsNb'
                    this.spotsNb = value ;
                case 'spacing'
                    this.spacing = value ;
                case 'direction'
                    this.direction = value ;
                case 'depthOrigin'
                    this.depthOrigin = value;
                case 'depthStep'
                    this.depthStep = value;
                otherwise
                    set@GenericModel(this, varargin{:});
            end
        end
        
        function value = get(this, varargin)
            switch varargin{1}
                case 'fileName'
                    value = this.fileName ;
                case 'name'
                    value = this.name ;
                case 'spotsNb'
                    value = this.spotsNb ;
                case 'spacing'
                    value = this.spacing ;
                case 'direction'
                    value = this.direction ;
                case 'depthOrigin'
                    value = this.depthOrigin;
                case 'depthStep'
                    value = this.depthStep;
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end
        
        function importTPSPR(this)
            handles = guidata(this.regguiHObject) ;
            
            [pathstr, fileN, ext] = fileparts(this.fileName) ;
            instruction = ['handles = loadRSSimulation(' '''' pathstr '''' ', ' '''' [fileN ext] '''' ', ' num2str(this.spotsNb(1)) ', ' num2str(this.spotsNb(2)) ', ' '''' this.direction(1) '''' ', ' '''' this.direction(2) '''' ', [' num2str(this.spacing) '], ' num2str(this.depthOrigin) ', ' num2str(this.depthStep) ', ' '''' this.name '''' ', ' 'handles) ;'] ;
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
end