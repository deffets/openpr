
classdef LoadTPSPanel < MainPanel
    properties (Access = private)
        control ;
        
        fileText ;
        spotsNbText ;
        spacingText ;
        PRNameText ;
        
        fileEdit ;
        spotsNbEdit1 ;
        spotsNbEdit2 ;
        spacingEdit1 ;
        spacingEdit2 ;
        directionEdit1 ;
        directionEdit2 ;
        depthOriginEdit;
        depthStepEdit;
        nameEdit ;
        
        fileButton ;
        goButton ;
    end
    
    methods (Access = public)
        function obj = LoadTPSPanel(f, control)
            obj@MainPanel(f) ;
            obj.control = control ;
            
            obj.fileText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'TPS file', 'Units', 'normalized', 'Position', [0.01, 0.7, 0.1, 0.2]) ;
            obj.spotsNbText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Spots per frame', 'Units', 'normalized', 'Position', [0.15, 0.7, 0.1, 0.2]) ;
            obj.spacingText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Spacing', 'Units', 'normalized', 'Position', [0.3, 0.7, 0.1, 0.2]) ;
            obj.PRNameText = uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.75, 0.7, 0.1, 0.2]) ;
            
            obj.fileButton = uicontrol('Parent', obj.mainPanel, 'Style', 'pushbutton', 'String', 'Open', 'Units', 'normalized', 'Position', [0.01, 0.4, 0.1, 0.2], 'Callback', @obj.fileCallback) ;
            
            obj.fileEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.01, 0.1, 0.1, 0.2]) ;
            
            obj.spotsNbEdit1 = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.15, 0.4, 0.025, 0.2]) ;
            obj.spotsNbEdit2 = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.2, 0.4, 0.025, 0.2]) ;
            
            obj.spacingEdit1 = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.3, 0.4, 0.025, 0.2]) ;
            obj.spacingEdit2 = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.35, 0.4 0.025, 0.2]) ;
            
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Depth orig.', 'Units', 'normalized', 'Position', [0.45, 0.7, 0.045, 0.2]) ;
            obj.depthOriginEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.45, 0.4, 0.025, 0.2]) ;
            uicontrol('Parent', obj.mainPanel, 'Style', 'text', 'String', 'Depth res.', 'Units', 'normalized', 'Position', [0.5, 0.7, 0.045, 0.2]) ;
            obj.depthStepEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.5, 0.4 0.025, 0.2]) ;
            
            obj.directionEdit1 = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.60, 0.4, 0.025, 0.2]) ;
            obj.directionEdit2 = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.65, 0.4 0.025, 0.2]) ;
            
            obj.nameEdit = uicontrol('Parent', obj.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.75, 0.4, 0.1, 0.2]) ;
            
            obj.goButton = uicontrol('Parent', obj.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.89, 0.3, 0.1, 0.4], 'Callback', @obj.goCallback) ;
            
            uicontrol('Parent', obj.mainPanel, 'Style', 'pushbutton', 'String', '?', 'Units', 'normalized', 'Position', [0.97, 0.8, 0.03, 0.2], 'Callback', @obj.helpCallback) ;
            
            obj.updateSoptsNbEdit() ;
            obj.updateSpacingEdit() ;
            obj.updatedepthEdit();
            obj.updateDirectionEdit() ;
        end
    end
    
    methods (Access = private)
        function updateFile(this)
            fileName = this.control.get('fileName') ;
            set(this.fileEdit, 'String', fileName) ;
        end
        
        function updateSoptsNbEdit(this)
            spotsNb = this.control.get('spotsNb') ;
            set(this.spotsNbEdit1, 'String', num2str(spotsNb(1))) ;
            set(this.spotsNbEdit2, 'String', num2str(spotsNb(2))) ;
        end
        
        function updateSpacingEdit(this)
             spacing = this.control.get('spacing') ;
             set(this.spacingEdit1, 'String', num2str(spacing(1))) ;
             set(this.spacingEdit2, 'String', num2str(spacing(2))) ;
        end
        
        function updatedepthEdit(this)
            depthOrigin = this.control.get('depthOrigin') ;
            depthStep = this.control.get('depthStep') ;
            set(this.depthOriginEdit, 'String', num2str(depthOrigin)) ;
            set(this.depthStepEdit, 'String', num2str(depthStep)) ;
        end
        
        function updateDirectionEdit(this)
             direction = this.control.get('direction') ;
             set(this.directionEdit1, 'String', direction(1)) ;
             set(this.directionEdit2, 'String', direction(2)) ;
        end
        
        function fileCallback(this, source, event)
            path = this.control.get('path', 'TPSFile');
            [fileName, pathName] = uigetfile(fullfile(path, '*.*'));
            
            if fileName~=0
                this.control.set('path', 'TPSFile', pathName);
                this.control.set('fileName', fullfile(pathName, fileName)) ;
                this.updateFile() ;
            end
        end
        
        function goCallback(this, source, event)
            this.control.set('fileName', get(this.fileEdit, 'String')) ;
            this.control.set('spotsNb', [str2double(get(this.spotsNbEdit1, 'String')), str2double(get(this.spotsNbEdit2, 'String'))]) ;
            this.control.set('spacing', [str2double(get(this.spacingEdit1, 'String')), str2double(get(this.spacingEdit2, 'String'))]) ;
            this.control.set('depthOrigin', str2double(get(this.depthOriginEdit, 'String'))) ;
            this.control.set('depthStep', str2double(get(this.depthStepEdit, 'String'))) ;
            this.control.set('direction', [get(this.directionEdit1, 'String'), get(this.directionEdit2, 'String')]) ;
            this.control.set('name', get(this.nameEdit, 'String')) ;
            
            this.control.importTPSPR() ;
        end
        
        function helpCallback(this, source, event)
            web(this.control.get('help', 'Load_RS_Simulation'), '-browser');
        end
    end
end
