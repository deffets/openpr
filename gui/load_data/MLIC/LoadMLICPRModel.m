
classdef LoadMLICPRModel < GenericModel
    properties (SetObservable)
        PRFile ;
        PRCalibrationFile ;
        spacing ;
        spotsNb ;
        energy ;
        PRName ;
    end
    
    methods (Access = public)
        function this = LoadMLICPRModel(regguiHObject)
            this@GenericModel(regguiHObject) ;
            
            this.PRFile = [] ;
            this.PRCalibrationFile = [] ;
            this.spacing = [5 5] ;
            this.spotsNb = [9 9] ;
            this.energy = 210 ;
            this.PRName = [] ;
        end
        
        function value = get(this, varargin)
            switch varargin{1}
                case 'PRFile'
                    value = this.PRFile ;
                case 'PRCalibrationFile'
                    value = this.PRCalibrationFile ;
                case 'spacing'
                    value = this.spacing ;
                case 'spotsNb'
                    value = this.spotsNb ;
               	case 'energy'
                    value = this.energy ;
                case 'PRName'
                    value = this.PRName ;
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end
        
        function set(this, varargin)
            value = varargin{2};
            
            switch varargin{1}
                case 'PRFile'
                    this.PRFile = value ;
                case 'PRCalibrationFile'
                    this.PRCalibrationFile = value ;
                case 'spacing'
                    this.spacing = value ;
                case 'spotsNb'
                    this.spotsNb = value ;
                case 'energy'
                    this.energy = value ;
                case 'PRName'
                    this.PRName = value ;
                otherwise
                    set@GenericModel(this, varargin{:});
            end
        end
        
        function importPR(this)
            handles = guidata(this.regguiHObject) ;
            instruction = ['handles = loadPR(' '''' this.PRFile '''' ', ' '''' this.PRCalibrationFile '''' ', [], [' num2str(this.spacing) '], [' num2str(this.spotsNb) '], ' '''' this.PRName '''' ', handles) ;'] ;
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
end
