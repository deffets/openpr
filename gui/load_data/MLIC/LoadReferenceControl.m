
classdef LoadReferenceControl < GenericControl
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function this = LoadReferenceControl(model)
            this@GenericControl(model) ;
        end
        
        function set(this, varargin)
            this.model.set(varargin{:}) ;
        end
        
        function value = get(this, varargin)
        	value = this.model.get(varargin{:}) ;
        end
        
        function importReference(this)
            this.model.importReference() ;
        end
    end
end
