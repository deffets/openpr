
classdef LoadCalibrationPanel < MainPanel
    properties (Access = private)
        control ;

        fileButton ;
        fileEdit ;
        
        materialsFileButton ;
        materialsFileEdit ;
        
        nameEdit ;
        
        goButton ;
    end
    
    methods (Access = public)
        function this = LoadCalibrationPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;

            % Calibration file
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'CT calibration file', 'Units', 'normalized', 'Position', [0.01, 0.7, 0.1, 0.2]) ;
            this.fileButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Open', 'Units', 'normalized', 'Position', [0.01, 0.4, 0.1, 0.2], 'Callback', @this.fileCallback) ;
            this.fileEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.01, 0.1, 0.1, 0.2]) ;
            
            % Materials file
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Materials file', 'Units', 'normalized', 'Position', [0.15, 0.7, 0.1, 0.2]) ;
            this.materialsFileButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Open', 'Units', 'normalized', 'Position', [0.15, 0.4, 0.1, 0.2], 'Callback', @this.materialsFileCallback) ;
            this.materialsFileEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.15, 0.1, 0.1, 0.2]) ;
           
            % Name
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.75, 0.7, 0.1, 0.2]) ;
            this.nameEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.75, 0.4, 0.1, 0.2]) ;
            
            % Go
            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.89, 0.3, 0.1, 0.4], 'Callback', @this.goCallback) ;
            
            uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', '?', 'Units', 'normalized', 'Position', [0.97, 0.8, 0.03, 0.2], 'Callback', @this.helpCallback) ;
            
            % Update edit uicontrol
            this.updateEditControl('all') ;
        end
    end
    
    methods (Access = private)
        function updateFile(this)
            fileName = this.control.get('fileName') ;
            set(this.fileEdit, 'String', fileName) ;
        end
        
        function updateEditControl(this, controlName)
            switch controlName
                case 'all'
                    this.updateEditControl('calibrationFile') ;
                    this.updateEditControl('materialsFile') ;
                    this.updateEditControl('calibrationName') ;
                case 'calibrationFile'
                    calibrationFile = this.control.get('calibrationFile') ;
                    set(this.fileEdit, 'String', calibrationFile) ;
                case 'materialsFile'
                    materialsFile = this.control.get('materialsFile') ;
                    set(this.materialsFileEdit, 'String', materialsFile) ;
                case 'calibrationName'
                    calibrationName = this.control.get('calibrationName') ;
                    set(this.nameEdit, 'String', calibrationName) ;
            end
        end
        
        function fileCallback(this, source, event)
            path = this.control.get('path', 'calibrationCurveFile');
            [fileName, pathName] = uigetfile(fullfile(path, '*.*'));
            
            if fileName~=0
                this.control.set('path', 'calibrationCurveFile', pathName);
            
                this.control.set('calibrationFile', fullfile(pathName, fileName)) ;
                this.updateEditControl('calibrationFile') ;
            end
        end
        
        function materialsFileCallback(this, source, event)
            path = this.control.get('path', 'materialsFile');
            [fileName, pathName] = uigetfile(fullfile(path, '*.*'));
            
            if fileName~=0
                this.control.set('path', 'materialsFile', pathName);
            
                this.control.set('materialsFile', fullfile(pathName, fileName)) ;
                this.updateEditControl('materialsFile') ;
            end
        end
        
        function goCallback(this, source, event)
            this.control.set('calibrationFile', get(this.fileEdit, 'String')) ;
            this.control.set('materialsFile', get(this.materialsFileEdit, 'String')) ;
            this.control.set('calibrationName', get(this.nameEdit, 'String')) ;
            
            this.control.importCurve() ;
        end
        
        function helpCallback(this, source, event)
            web(this.control.get('help', 'Load_Calibration_Curve'), '-browser');
        end
    end
end
