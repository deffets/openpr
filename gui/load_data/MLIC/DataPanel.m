
classdef DataPanel < MainPanel
    properties (Access = private)
        loadMLICPRPanel ;
        loadReferencePanel ;
        loadCalibrationPanel ;
        loadTPSPanel ;
        loadMatPRPanel ;
        
        control ;
    end
    
    methods (Access = public)
        function this = DataPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            % Load PR
            loadMLICPRP = uipanel('Parent', this.mainPanel, 'Title', 'Load PR', 'Position', [0 0.825 1 0.15]) ;
            this.loadMLICPRPanel = LoadMLICPRPanel(loadMLICPRP, this.control.get('loadMLICPRControl')) ;

            
            % Load reference
            loadReferenceP = uipanel('Parent', this.mainPanel, 'Title', 'Load reference', 'Position', [0 0.65 1 0.15]) ;
            this.loadReferencePanel = LoadReferencePanel(loadReferenceP, this.control.get('loadReferenceControl')) ;
            
            
            % Load calibration curve
            loadCalibrationP = uipanel('Parent', this.mainPanel, 'Title', 'Load calibration curve', 'Position', [0 0.475 1 0.15]) ;
            this.loadCalibrationPanel = LoadCalibrationPanel(loadCalibrationP, this.control.get('loadCalibrationControl')) ;
            
            
            % Load TPS data
            loadTPSP = uipanel('Parent', this.mainPanel, 'Position', [0 0.275 1 0.15], 'Title', 'Load RayStation simulation') ;
            this.loadTPSPanel = LoadTPSPanel(loadTPSP, this.control.get('loadTPSControl')) ;
            
            
            % Load Mat file
            loadMatPRP = uipanel('Parent', this.mainPanel, 'Position', [0 0.1 1 0.15], 'Title', 'Load .mat PR') ;
            this.loadMatPRPanel = LoadMatPRPanel(loadMatPRP, this.control.get('loadMatPRControl')) ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
            
            this.loadMLICPRPanel.setVisible(visible) ;
            this.loadReferencePanel.setVisible(visible) ;
            this.loadCalibrationPanel.setVisible(visible) ;
            this.loadTPSPanel.setVisible(visible) ;
            this.loadMatPRPanel.setVisible(visible) ;
        end
    end
end
