
classdef LoadCalibrationControl < GenericControl
    properties (Access = private)
    end
    
    methods (Access = public)
        function this = LoadCalibrationControl(model)
            this@GenericControl(model) ;
        end
        
        function value = get(this, varargin)
            value = this.model.get(varargin{:});
        end
        
        function set(this, varargin)
            this.model.set(varargin{:});
        end
        
        function importCurve(this)
            this.model.importCurve() ;
        end
    end   
end
