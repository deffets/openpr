
classdef LoadTPSControl < GenericControl
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function obj = LoadTPSControl(model)
            obj@GenericControl(model) ;
        end
        
        function setFileName(this, fileName)
            this.model.setFileName(fileName) ;
        end
        
        function set(this, varargin)
            this.model.set(varargin{:}) ;
        end
        
        function value = get(this, varargin)
        	value = this.model.get(varargin{:}) ;
        end
        
        function importTPSPR(this)
            this.model.importTPSPR() ;
        end
    end
end
