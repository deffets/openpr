
classdef DataControl < GenericControl
    properties (Access = private)
        loadMLICPRControl ;
        loadReferenceControl ;
        loadCalibrationControl ;
        loadTPSControl ;
        loadMatPRControl ;
    end
    
    methods (Access = public)
        function obj = DataControl(model)
            obj@GenericControl(model) ;
            
            loadMLICPRModel = LoadMLICPRModel(obj.model.getRegguiHObject()) ;
            obj.loadMLICPRControl = LoadMLICPRControl(loadMLICPRModel) ;
            
            loadReferenceModel = LoadReferenceModel(obj.model.getRegguiHObject()) ;
            obj.loadReferenceControl = LoadReferenceControl(loadReferenceModel) ;
            
            loadCalibrationModel = LoadCalibrationModel(obj.model.getRegguiHObject()) ;
            obj.loadCalibrationControl = LoadCalibrationControl(loadCalibrationModel) ;
            
            loadTPSModel = LoadTPSModel(obj.model.getRegguiHObject()) ;
            obj.loadTPSControl = LoadTPSControl(loadTPSModel) ;
            
            loadMatPRModel = LoadMatPRModel(obj.model.getRegguiHObject()) ;
            obj.loadMatPRControl = LoadMatPRControl(loadMatPRModel) ;
        end
        
        function value = get(this, property)
            switch property
                case 'loadMLICPRControl'
                    value = this.loadMLICPRControl ;
                case 'loadReferenceControl'
                    value = this.loadReferenceControl ;
                case 'loadCalibrationControl'
                    value = this.loadCalibrationControl ;
                case 'loadTPSControl'
                    value = this.loadTPSControl ;
                case 'loadMatPRControl'
                    value = this.loadMatPRControl ;
                otherwise
                    value = this.model.get(property) ;
            end
        end
        
        function set(this, property, value)
            this.model.set(property, value)
        end
    end   
end
