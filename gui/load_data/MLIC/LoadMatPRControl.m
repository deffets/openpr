
classdef LoadMatPRControl < GenericControl
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function this = LoadMatPRControl(model)
            this@GenericControl(model);
        end
        
        function setFileName(this, fileName)
            this.model.setFileName(fileName);
        end
        
        function set(this, varargin)
            this.model.set(varargin{:});
        end
        
        function value = get(this, varargin)
        	value = this.model.get(varargin{:});
        end
        
        function importMatPR(this)
            this.model.importMatPR();
        end
    end
end
