
classdef LoadMatPRPanel < MainPanel
    properties (Access = private)
        control;
        
        fileText;
        spacingText;
        PRNameText;
        
        fileButton;
        
        fileEdit;
        spacingEdit1;
        spacingEdit2;
        depthEdit1;
        depthEdit2;
        nameEdit;
        
        goButton;
    end
    
    methods (Access = public)
        function this = LoadMatPRPanel(f, control)
            this@MainPanel(f);
            this.control = control;

            this.fileText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Mat file', 'Units', 'normalized', 'Position', [0.01, 0.7, 0.1, 0.2]);
            this.spacingText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Spacing', 'Units', 'normalized', 'Position', [0.15, 0.7, 0.075, 0.2]);
            this.PRNameText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.75, 0.7, 0.1, 0.2]);

            this.fileButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Open', 'Units', 'normalized', 'Position', [0.01, 0.4, 0.1, 0.2], 'Callback', @this.fileCallback);

            this.fileEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.01, 0.1, 0.1, 0.2]);

            this.spacingEdit1 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.15, 0.1, 0.025, 0.2]);
            this.spacingEdit2 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.2, 0.1 0.025, 0.2]);
            
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Depth', 'Units', 'normalized', 'Position', [0.3, 0.7, 0.075, 0.2]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Offset', 'Units', 'normalized', 'Position', [0.3, 0.4, 0.025, 0.2]);
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Sampling', 'Units', 'normalized', 'Position', [0.35, 0.4, 0.05, 0.2]);
            this.depthEdit1 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.3, 0.1, 0.025, 0.2]);
            this.depthEdit2 = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.35, 0.1 0.025, 0.2]);

            this.nameEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.75, 0.4, 0.1, 0.2]);

            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.89, 0.3, 0.1, 0.4], 'Callback', @this.goCallback);

            uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', '?', 'Units', 'normalized', 'Position', [0.97, 0.8, 0.03, 0.2], 'Callback', @this.helpCallback);
            
            this.updateSpacingEdit();
        end
    end
    
    methods (Access = private)
        function updateFile(this)
            fileName = this.control.get('fileName');
            set(this.fileEdit, 'String', fileName);
        end
        
        function updateSpacingEdit(this)
             spacing = this.control.get('spacing');
             set(this.spacingEdit1, 'String', num2str(spacing(1)));
             set(this.spacingEdit2, 'String', num2str(spacing(2)));
             
             origin = this.control.get('depthOrigin');
             set(this.depthEdit1, 'String', num2str(origin));
             
             depthSpacing = this.control.get('depthSpacing');
             set(this.depthEdit2, 'String', num2str(depthSpacing));
        end
        
        function fileCallback(this, source, event)
            path = this.control.get('path', 'MatPR');
            [fileName, pathName] = uigetfile(fullfile(path, '*.*'));
            
            if fileName~=0
                this.control.set('path', 'MatPR', pathName);
            
                this.control.set('fileName', fullfile(pathName, fileName));
                this.updateFile();
            end
        end
        
        function goCallback(this, source, event)
            this.control.set('fileName', get(this.fileEdit, 'String'));
            this.control.set('spacing', [str2double(get(this.spacingEdit1, 'String')), str2double(get(this.spacingEdit2, 'String'))]);
            this.control.set('depthOrigin', str2double(get(this.depthEdit1, 'String')));
            this.control.set('depthSpacing', str2double(get(this.depthEdit2, 'String')));
            this.control.set('name', get(this.nameEdit, 'String'));
            
            this.control.importMatPR();
        end
        
        function helpCallback(this, source, event)
            web(this.control.get('help', 'Load_Mat_PR'), '-browser');
        end
    end
end
