
classdef PlanEditControl < GenericControl
    properties (Access = private)
        planEditPanel;
    end
    
    methods (Access = public)
        function this = PlanEditControl(model)
            this@GenericControl(model) ;            
        end
        
        function set(this, property, value)
            switch property
                case 'planEditPanel'
                    this.planEditPanel = value;
                otherwise
                    this.model.set(property, value);
            end
        end
        
        function value = get(this, property)
            value = this.model.get(property);
        end
        
        function loadPlan(this, planName)
            this.model.loadPlan(planName);
        end
        
        function clearPlan(this)
            this.model.clearPlan();
        end
        
        function save(this)
            this.model.save();
        end
    end
    
    methods (Access = private)
    end
end
