
classdef DataPlanPanel < MainPanel
    properties (Access = private)
        control;
        planEditPanel;
    end
    
    methods (Access = public)
        function this = DataPlanPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            p1 = uipanel('Parent', this.mainPanel, 'Title', 'Plan parameters', 'Position', [0 0 0.5 1]);
            planCtrl = PlanEditControl(PlanEditModel(this.control.get('regguiHObject')));
            this.planEditPanel = PlanEditPanel(p1, planCtrl);
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
            this.planEditPanel.setVisible(visible);
        end        
    end
end
