
classdef PlanEditModel < GenericModel    
    properties (SetObservable)
        types;
        x;
        y;
        MUs;
        names;
    end
    
    properties (Access = private)
        planData;
        planInfo;
        newName;
    end
    
    methods (Access = public)
        function this = PlanEditModel(regguiHObject)
            this@GenericModel(regguiHObject);
            
            this.types = [];
            this.x = [];
            this.y = [];
            this.MUs = [];
            this.names = [];
            this.newName = [];
            this.planData = [];
            this.planInfo = [];
        end
        
        function set(this, varargin)
            property = varargin{1};
            value = varargin{2};
            
            switch property
                case 'types'
                    this.types = value;
                case 'x'
                    this.x = value;
                case 'y'
                    this.y = value;
                case 'MUs'
                    this.MUs = value ;
                case 'names'
                    this.names = value;
                case 'newName'
                    this.newName = value;
                otherwise
                    set@GenericModel(this, varargin{:});
            end
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'types'
                    value = this.types;
                case 'x'
                    value = this.x;
                case 'y'
                    value = this.y;
                case 'MUs'
                    value = this.MUs;
                case 'names'
                    value = this.names;
                case 'newName'
                    value = this.newName;
                case 'repeatNb'
                    value = this.repeatNb;
                case 'name'
                    value = this.name;
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end
        
        function loadPlan(this, planName)
            handles = guidata(this.get('regguiHObject'));
            
            this.planData = [];
            this.planInfo = [];
            handles.plans.name
            for i=1:length(handles.plans.name)
                if(strcmp(handles.plans.name{i}, planName))
                    this.planData = handles.plans.data{i};
                    this.planInfo = handles.plans.info{i};
                    break ;
                end
            end
            if(isempty(this.planData))
                error('Plan not found in the current list')
            end

            this.names = [];
            this.types = [];
            this.x = [];
            this.y = [];
            this.MUs = [];
            
            ind = 1;
            for i=1 : length(this.planData)
                this.types{ind} = 'Beam';
                name = char([this.planData{i}.name(:)]');
                this.names{ind} = name;
                this.MUs(ind) = this.planData{i}.final_weight;
                this.x(ind) = NaN;
                this.y(ind) = NaN;
                
                ind = ind+1;
                                
                for j=1 : length(this.planData{i}.spots)
                    this.types{ind} = 'Layer';
                    this.names{ind} = num2str(this.planData{i}.spots(j).energy);
                    this.MUs(ind) = NaN;
                    this.x(ind) = NaN;
                    this.y(ind) = NaN;

                    ind = ind+1;
                    
                    for k=1 : length(this.planData{i}.spots(j).weight)
                        this.types{ind} = 'Spot';
                        this.names{ind} = '';
                        this.MUs(ind) = this.planData{i}.spots(j).weight(k);
                        this.x(ind) = this.planData{i}.spots(j).xy(k, 1);
                        this.y(ind) = this.planData{i}.spots(j).xy(k, 2);
                        ind = ind+1;
                    end
                end
            end
            size(this.names)
            size(this.MUs)
        end
        
        function clearPlan(this)
            this.types = {};
            this.x = [];
            this.y = [];
            this.MUs = [];
            this.names = [];
            this.newName = [];
        end
        
        function save(this)
            handles = guidata(this.regguiHObject) ;
            
            handles = saveDetector(this.detectorDimensions, this.detectorPosition, this.materials, this.lengths, this.actors, this.repeatNb, this.name, handles);
            
            guidata(this.regguiHObject, handles) ;
        end
    end
end
