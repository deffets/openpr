
classdef PlanEditPanel < MainPanel
    properties (Access = private)
        control;
        
        dimTab;
        
        nameEdit;
        
        planPopup;
        
        updateButton;
        clearButton;
        saveButton;
    end
    
    methods (Access = public)
        function this = PlanEditPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            cNames = {'Name/Energy', 'Type', 'x', 'y', 'MU', 'Remove', 'Add'};
            names = this.control.get('names');
            types = this.control.get('types');
            x = this.control.get('x');
            y = this.control.get('y');
            mu = this.control.get('MUs');
            
            if isempty(types)
                types = {'None'};
                x = {0};
                y = {0};
                mu = {0};
                names = {'None'};
            else
                if ~iscell(types)
                    types = {types};
                    names = {names};
                end
                
                x = num2cell(x);
                y = num2cell(y);
                mu = num2cell(mu);
            end
            
            addCell = logical(zeros(1, length(types)));
            removeCell = logical(zeros(1, length(types)));

            removeCell = num2cell(removeCell);
            addCell = num2cell(addCell);
            
            tableData = {names{:} ; types{:} ; x{:} ; y{:} ; mu{:} ; removeCell{:} ; addCell{:}}';
                
            rNames = num2cell(1:length(types))';
            this.dimTab = uitable('Parent', this.mainPanel, 'Data', tableData, 'ColumnName', ...
                cNames, 'RowName', rNames, 'Units', 'Normalized', 'Position', [0.01, 0.2, 0.9, 0.7], 'ColumnEditable', [true true true true true true true], 'CellEditCallback', @this.tableCallback);
            
            this.planPopup = uicontrol('Parent', this.mainPanel, 'String', this.control.get('planNames'), 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.95, 0.1, 0.025]) ;
            
            this.updateButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Load', 'Units', 'normalized', 'Position', [0.15, 0.95, 0.1, 0.025], 'Callback', @this.loadCallback);
            this.clearButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Clear', 'Units', 'normalized', 'Position', [0.3, 0.95, 0.1, 0.025], 'Callback', @this.clearCallback);
            
            uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Name', 'Units', 'normalized', 'Position', [0.01, 0.1, 0.05, 0.025]);
            this.nameEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.1, 0.1, 0.1, 0.025]);
            this.saveButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Save', 'Units', 'normalized', 'Position', [0.25, 0.1, 0.1, 0.025], 'Callback', @this.goCallback);
            
            
            this.control.set('planEditPanel', this);
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePopups() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = private)
        function updatePopups(this)
            planNames = this.control.get('planNames') ;
            
            if ~isempty(planNames)
                this.planPopup.set('String', planNames) ;
            end
        end
        
        
        function fillTable(this)
            names = this.control.get('names');
            types = this.control.get('types');
            x = this.control.get('x');
            y = this.control.get('y');
            mu = this.control.get('MUs');
            
            if isempty(types)
                types = {'None'};
                x = {0};
                y = {0};
                mu = {0};
                names = {'None'};
            else
                if ~iscell(types)
                    types = {types};
                    names = {names};
                end
                
                x = num2cell(x);
                y = num2cell(y);
                mu = num2cell(mu);
            end
            
            addCell = logical(zeros(1, length(types)));
            removeCell = logical(zeros(1, length(types)));

            removeCell = num2cell(removeCell);
            addCell = num2cell(addCell);
            
            tableData = {names{:} ; types{:} ; x{:} ; y{:} ; mu{:} ; removeCell{:} ; addCell{:}}';

            rNames = num2cell(1:size(tableData, 1))';
            this.dimTab.set('RowName', rNames, 'Data', tableData, 'ColumnEditable', [true true true true true true true], 'CellEditCallback', @this.tableCallback);
        end
        
        function tableCallback(this, source, event)
            r = event.Indices(1);
            c = event.Indices(2);
            if c<6
                return
            end
            
            tableData = this.dimTab.get('Data');
            names = {tableData{1:end, 1}};
            types = {tableData{1:end, 2}};
            x = {tableData{1:end, 3}};
            y = {tableData{1:end, 4}};
            mu = {tableData{1:end, 5}};
            
            if c==6 && length(types)>1
                tableData(r, :) = [];
            end
            
            if c==7
                names = {names{1:r} 'None' names{r+1:end}};
                types = {types{1:r} 'None' types{r+1:end}};
                x = {x{1:r} 0  x{r+1:end}};
                y = {y{1:r} 0 y{r+1:end}};
                mu = {mu{1:r} 0 mu{r+1:end}};
                    
                addCell = logical(zeros(1, length(types)));
                removeCell = logical(zeros(1, length(types)));

                removeCell = num2cell(removeCell);
                addCell = num2cell(addCell);
                
                tableData = {names{:} ; types{:} ; x{:} ; y{:} ; mu{:} ; removeCell{:} ; addCell{:}}';
            end

            rNames = num2cell(1:size(tableData, 1))';
            this.dimTab.set('RowName', rNames, 'Data', tableData, 'ColumnEditable', [true true true true true true true], 'CellEditCallback', @this.tableCallback);
        end
        
        function loadCallback(this, source, event)
            items = get(this.planPopup, 'String');
            index_selected = get(this.planPopup, 'Value');
            planName = items{index_selected};
            
            this.control.loadPlan(planName);
            
            this.fillTable();
        end
        
        function clearCallback(this, source, event)
            
        end
        
        function updateCallback(this, source, event)
            tableData = this.dimTab.get('Data');
            names = {tableData{1:end, 1}};
            types = {tableData{1:end, 2}};
            x = {tableData{1:end, 3}};
            y = {tableData{1:end, 4}};
            mu = {tableData{1:end, 5}};
            
            names = cell2mat(names);
            types = cell2mat(types);
            x = cell2mat(x);
            y = cell2mat(y);
            mu = cell2mat(mu);
            
            this.control.set('names', names);
            this.control.set('types', types);
            this.control.set('x', x);
            this.control.set('y', y);
            this.control.set('MUs', mu);
        end
    
        function goCallback(this, source, event)
            this.updateCallback([], []);
            
            this.control.set('name', this.nameEdit.get('String'));

            this.control.save() ;
        end
    end
end
