
classdef ViewMultipleBraggWETControl < ViewMultipleBraggControl
    properties (Access = private)
        viewWETImagesPanel;
        ViewMultipleWETDataPanel;
    end
    
    methods (Access = public)
        function this = ViewMultipleBraggWETControl(model)
            this@ViewMultipleBraggControl(model) ;
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'viewWETImagesPanel'
                    value = this.viewWETImagesPanel ;
                case 'ViewMultipleWETDataPanel'
                    value = this.ViewMultipleWETDataPanel;
                case 'CData'
                    value = this.viewWETImagesPanel.get('CData');
                case 'XData'
                    value = this.viewWETImagesPanel.get('XData');
                case 'YData'
                    value = this.viewWETImagesPanel.get('YData');
                case 'CLim'
                    value = this.viewWETImagesPanel.get('CLim');
                otherwise
                    value = get@ViewMultipleBraggControl(this, varargin{:});
            end
        end
        
        function value = set(this, property, value)        
            switch property
                case 'viewWETImagesPanel'
                    this.viewWETImagesPanel = value ;
                case 'ViewMultipleWETDataPanel'
                    this.ViewMultipleWETDataPanel = value;
                case 'wetNames'
                    this.model.set(property, value) ;
                    this.viewWETImagesPanel.updatePopups() ;
                otherwise
                    set@ViewMultipleBraggControl(this, property, value);    
            end
        end
    end
    
    methods (Access = protected)
        function positionListener(this, source, event)
            positionListener@ViewMultipleBraggControl(this, source, event);
            
            this.viewWETImagesPanel.updatePixel();
            this.ViewMultipleWETDataPanel.updateTable();
        end
    end
end
