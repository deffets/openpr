
classdef ViewPRWETPanel < MainPanel
    properties (Access = private)
        control ;
    
        viewSelectionPanel ;
        wiewWETSelectionPanel;
    end
    
    methods (Access = public)
        function this = ViewPRWETPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            p0 = uipanel(this.mainPanel, 'Position', [0 0.5 0.1 0.5], 'Title', 'WET');
            this.wiewWETSelectionPanel = ViewSelectionWETPanel(p0, this.control);
            
            p1 = uipanel(this.mainPanel, 'Position', [0 0 0.1 0.5], 'Title', 'PR');
            this.viewSelectionPanel = ViewSelectionPanel(p1, this.control);
            
            p2 = uipanel(this.mainPanel, 'Position', [0.11 0 0.5 0.5], 'Title', 'PR');
            vp = ViewMultipleBraggImagesPanel(p2, this.control);
            this.control.set('viewMultipleBraggImagesPanel', vp);
            
            p4 = uipanel(this.mainPanel, 'Position', [0.11 0.5 0.5 0.5], 'Title', 'WET');
            vp2 = ViewMultipleBraggImagesWETPanel(p4, this.control);
            this.control.set('viewWETImagesPanel', vp2);
            
            p3 = uipanel(this.mainPanel, 'Position', [0.61 0 0.39 0.5], 'Title', 'PR');
            ViewMultipleBraggCurvesPanel(p3, this.control);
            
            p5 = uipanel(this.mainPanel, 'Position', [0.61 0.5 0.39 0.5], 'Title', 'WET');
            ViewMultipleWETDataPanel(p5, this.control);
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')                
                set( gcf, 'toolbar', 'figure' )
                this.wiewWETSelectionPanel.setVisible('on') ;
                this.viewSelectionPanel.setVisible('on') ;
            else
                set( gcf, 'toolbar', 'none' )
            end
            
            setVisible@MainPanel(this, visible) ;
        end
    end
    
    methods (Access = private)        
 
    end
end
