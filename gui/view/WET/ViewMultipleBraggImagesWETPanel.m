
classdef ViewMultipleBraggImagesWETPanel < ViewMultipleBraggImagesPanel
    properties (Access = private)
    end
    
    methods (Access = public)
        function this = ViewMultipleBraggImagesWETPanel(f, control)
            this@ViewMultipleBraggImagesPanel(f, control);
        end
        
        function updatePopups(this)
            WETNames = this.control.get('wetNames') ;
            wetFeatureNames = this.control.get('wetFeatureNames') ;
            
            if ~isempty(WETNames)
                if isempty(this.PRPopup1)
                    this.PRPopup1 = uicontrol('Parent', this.p1, 'Style', 'popup', 'String', WETNames, 'Value', 1, 'Units', 'normalized', 'Position', [0.01, 0.4, 0.2, 0.3]) ;
                    this.PRPopup2 = uicontrol('Parent', this.p1, 'Style', 'popup', 'String', WETNames, 'Value', 1, 'Units', 'normalized', 'Position', [0.25, 0.4, 0.2, 0.3]) ;
                    this.PRPopup3 = uicontrol('Parent', this.p1, 'Style', 'popup', 'String', wetFeatureNames, 'Value', 1, 'Units', 'normalized', 'Position', [0.5, 0.4, 0.2, 0.3]) ;
                    this.goButton = uicontrol('Parent', this.p1, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.75, 0.3, 0.2, 0.4], 'Callback', @this.goCallback) ;
                else
                    this.PRPopup1.set('String', WETNames, 'Value', 1) ;
                    this.PRPopup2.set('String', WETNames, 'Value', 1) ;
                    this.PRPopup3.set('Value', 1) ;
                    this.goButton.set('Visible', 'on') ;
                end
            end
        end
        
        function updateImages(this)
            images = this.control.get('wetImagesToDisplay') ;
            image1 = images{1} ;
            image2 = images{2} ;
            
            if isempty(images)
                return
            end
            
            if isempty(this.mainPlot)
                this.mainPlot = subplot(1, 1, 1, 'Parent',  this.p2) ;
            else
                s1 = subplot(this.mainPlot) ;
            end
            
            spacing1 = image1.get('spacing') ;
            
            RA = imref2d(size(image1.get('data')), spacing1(2), spacing1(1));
            RA.XWorldLimits = image1.get('XWorldLimits')-spacing1(2)/2 ;
            RA.YWorldLimits = image1.get('YWorldLimits')-spacing1(1)/2 ;

            if ~isempty(this.imh)
                delete(this.imh)
            end
            
            if ~isempty(this.cb)
                delete(this.cb)
            end
                
            if ~isempty(image2)
                spacing2 = image2.get('spacing') ;

                RB = imref2d(size(image2.get('data')), spacing2(2), spacing2(1));
                RB.XWorldLimits = image2.get('XWorldLimits')-spacing1(2)/2 ;
                RB.YWorldLimits = image2.get('YWorldLimits')-spacing1(1)/2 ;

                this.imh = imshowpair(image1.get('data'), RA, image2.get('data'), RB) ;
                
                colorbar('off')
            else
                d = image1.get('data') ;
                
                imTemp = imshow(d, RA) ;
                xd = imTemp.XData;
                yd = imTemp.YData;
                
                offX = (xd(end)-xd(1)+1)/size(d, 2);
                offY = (yd(end)-yd(1)+1)/size(d, 1);
                delete(imTemp);
                
%                 if max(d(:))>10
                    if min(d(:))<0
                        this.imh = imagesc(d, [-5 5]) ;
                    else
                        this.imh = imagesc(d, [0 5]) ;
                    end

                    set(this.imh, 'XData', xd, 'YData', yd);
                    set(this.mainPlot, 'xlim', [xd(1)-offX xd(end)+offX], 'ylim', [yd(1)-offY yd(end)+offY]);

                    colormap(this.mainPlot, 'parula')

                    if min(d(:))<0
                        tickLabels = [-5 -2.5 0 2.5 5] ;
                    else
                        tickLabels = [0 2.5 5] ;
                    end

                    this.cb = colorbar('Parent', this.p2);
                    set(this.cb, 'YTick', tickLabels, 'TickLabels', tickLabels, 'TicksMode', 'manual', 'TickLabelsMode', 'manual');
%                 else
%                     this.imh = imagesc(d) ;
%                 
% 
%                     set(this.imh, 'XData', xd, 'YData', yd);
%                     set(this.mainPlot, 'xlim', [xd(1)-offX xd(end)+offX], 'ylim', [yd(1)-offY yd(end)+offY]);
% 
%                     colormap(this.mainPlot, 'parula')
% 
%                     d = d(:) ;
%                     tickLabels = (0:0.1:1)*(max(d)-min(d))+min(d);
% 
%                     this.cb = colorbar('Parent', this.p2);
%                     set(this.cb, 'YTick', tickLabels, 'TickLabels', tickLabels, 'TicksMode', 'manual', 'TickLabelsMode', 'manual');
%                 end
            end
            
            set(this.imh, 'ButtonDownFcn', @this.mouseInSubplot1);
            axis equal off ;
            drawnow ;
        end
    end
    
    methods (Access = protected)
        function goCallback(this, source, event)
            featureNames = this.control.get('wetFeatureNames') ;
            featureInd = this.PRPopup3.get('Value') ;
            wetFeatureName = featureNames{featureInd} ;
            
            this.control.set('wetImage1Ind', this.PRPopup1.get('Value')) ;
            this.control.set('wetImage2Ind', this.PRPopup2.get('Value')) ;
            
            this.control.set('wetFeatureName', wetFeatureName) ;
            this.updateImages() ;
        end
        
        function exportCallback(this, source, event)
            images = this.control.get('wetImagesToDisplay');
            image1 = images{1};
            image2 = images{2};
            
            if isempty(images)
                return
            end
            
            if ~isempty(image2)
                warndlg('This will only export the WEPL map of the first selected data.');
            end
            
            data = image1.get('data');
            [file, path] = uiputfile({'*.mat'});

            if file
                save(fullfile(path, file), 'data');
            end
        end
        
        function roiCallback(this, source, event)
            roiModel = RoiUncertaintyModel(this.control.get('regguiHObject'));
            roiControl = RoiUncertaintyControl(roiModel, this.control);
            f = figure('Name', 'ROI interface', 'NumberTitle', 'off', 'units', 'normalized', 'outerposition', [0 0 1 1]);
            RoiUncertaintyPanel(f, roiControl);
        end
    end
end
