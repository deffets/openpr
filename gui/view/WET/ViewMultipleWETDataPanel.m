
classdef ViewMultipleWETDataPanel < MainPanel
    properties (Access = private)
        control ;
        dimTab;
    end
    
    methods (Access = public)
        function this = ViewMultipleWETDataPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.control.set('ViewMultipleWETDataPanel', this) ;
            
            this.dimTab = uitable('Parent', this.mainPanel, 'ColumnName', ...
                'None', 'RowName', 'None', 'Units', 'Normalized', 'Position', [0, 0, 1, 1], 'ColumnEditable', false);
            this.updateTable();
        end
        
        function updateTable(this)            
            WETNames = this.control.get('wetNames') ;
            WETData = this.control.get('wetData') ;

            this.dimTab.set('ColumnName', WETNames, 'RowName', WETNames);
            
            this.dimTab.set('Data', WETData);
        end
    end
    
    methods (Access = private)    
    end
end
