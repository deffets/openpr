
classdef ViewSelectionWETPanel < ViewSelectionPanel
    properties (Access = private)
    end
    
    methods (Access = public)
        function this = ViewSelectionWETPanel(f, control)
            this@ViewSelectionPanel(f, control) ;
            this.updateList() ;
        end
    end
    
    methods (Access = protected) 
        function updateList(this)
            names = this.control.getWETNames() ;
            this.PRList.set('String', names) ;
        end
        
        function goCallback(this, source, event)
            names = this.PRList.get('String') ;
            value = this.PRList.get('Value') ;
            
            wetNames = names(value) ;

            this.control.set('wetNames', wetNames) ;
        end
    end
end
