
classdef ViewMultipleBraggWETModel < ViewMultipleBraggModel
    properties (Access = private)
        wetImagesToDisplay;
        wetImages;
        wetNames;
        wetFeatureName;
        wetFeatureNames;
        wetImage1Ind;
        wetImage2Ind;
    end
    
    methods (Access = public)
        function this = ViewMultipleBraggWETModel(model)
            this@ViewMultipleBraggModel(model) ;
            
            this.wetNames = [];
            this.wetImages = [];
            this.wetFeatureName = [] ;
            this.wetFeatureNames = {'Superimpose', 'Difference', 'Absolute difference', 'Relative difference', 'Relative absolute difference'};
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'wetNames'
                    value = this.wetNames;
                case 'wetImagesToDisplay'
                    this.updateWETImages() ;
                    value = this.wetImagesToDisplay ;
                case 'wetFeatureName'
                    value = this.wetFeatureName;
                case 'wetFeatureNames'
                    value = this.wetFeatureNames;
                case 'wetData'
                    value = [];
                    
                    if isempty(this.wetNames) || isempty(this.wetFeatureName)
                        return
                    end
                    
                    handles = guidata(this.regguiHObject) ;
                    
                    wetData = zeros(length(this.wetNames), 1);
                    for i=1 : length(this.wetNames)
                        wTempi = handles2Image2D(this.wetNames{i}, handles);
                        
                        try
                            wetData(i) = squeeze(wTempi.getData(this.pixelPosition)) ;
                        catch
                            return;
                        end
                    end
                    
                    wetDatai = repmat(wetData, 1, length(this.wetNames));
                    wetDataj = repmat(wetData', length(this.wetNames), 1);
                    
                    value = zeros(length(this.wetNames));
                    
                    switch this.wetFeatureName
                        case 'Superimpose'
                            % Nothing to do
                        case 'Difference'
                            value = wetDatai-wetDataj ;
                        case 'Absolute difference'
                            value = abs(wetDatai-wetDataj) ;
                        case 'Relative difference'
                            value = (wetDatai-wetDataj)./wetDataj*100 ;
                            value(abs(wetDatai)<10) = 0;
                        case 'Relative absolute difference'
                            value = abs(wetDatai-wetDataj)./abs(wetDataj)*100 ;
                            value(abs(wetDatai)<10) = 0;
                    end

                    value(logical(eye(length(this.wetNames)))) = wetData;
                otherwise
                    value = get@ViewMultipleBraggModel(this, varargin{:});
            end
        end
        
        function set(this, property, value)
            switch property                 
                case 'wetNames'
                    this.wetNames = value;
                case 'wetImage1Ind'
                    this.wetImage1Ind = value;
                case 'wetImage2Ind'
                    this.wetImage2Ind = value;
                case 'wetFeatureName'
                    this.wetFeatureName = value;
                otherwise
                    set@ViewMultipleBraggModel(this, property, value);
            end
        end
        
        function export3DCallback(this, name)
            [imA, imB] = this.getImages();
            
            if isempty(imA)
                return;
            end
            if ~isempty(imB)
                warndlg('Feature not yet implemented for 2 images. This will only export first image.');
            end
            
            handles = guidata(this.regguiHObject);
            handles = add2DImage2Images(imA, 270, name, handles);
            
            guidata(this.regguiHObject, handles);
        end
    end
    
    methods (Access = private)
        function [imA, imB] = getImages(this)
            handles = guidata(this.regguiHObject) ;
            
            for i=1 : length(this.wetNames)
                wTemp{i} = handles2Image2D(this.wetNames{i}, handles) ;
            end
            
            this.wetImages = wTemp ;
            
            wImage1 = handles2Image2D(this.wetNames{this.wetImage1Ind}, handles) ;   
            wImage2 = handles2Image2D(this.wetNames{this.wetImage2Ind}, handles) ;
            
            origin1 = wImage1.get('origin') ;
            origin2 = wImage2.get('origin') ;
            spacing1 = wImage1.get('spacing') ;
            spacing2 = wImage2.get('spacing') ;

            [worldLimit1, worldLimit2] = computeWorldLimits(wImage1.get('XWorldLimits'), ...
                wImage1.get('YWorldLimits'), wImage2.get('XWorldLimits'), ...
                wImage2.get('YWorldLimits'), spacing2) ;
            worldLimit2(1) = worldLimit2(1) - spacing2(1) ;
            worldLimit2(2) = worldLimit2(2) - spacing2(2) ;

            im1 = wImage1.getData(worldLimit1, worldLimit2, spacing2) ;
            im2 = wImage2.getData(worldLimit1, worldLimit2, spacing2) ;

            origin1 = worldLimit1 ;
            origin2 = worldLimit1 ;
            spacing1 = spacing2 ;

            switch this.wetFeatureName
                case 'Superimpose'
                    % Nothing to do
                case 'Difference'
                    im1 = im1-im2 ;
                    im2 = [] ;
                case 'Absolute difference'
                    im1 = abs(im1-im2) ;
                    im2 = [] ;
                case 'Relative difference'
                    im1 = (im1-im2)./im1*100 ;
                    im1(abs(im1-im2)<10) = 0;
                    im2 = [] ;
                case 'Relative absolute difference'
                    im1 = abs(im1-im2)./abs(im1)*100 ;
                    im1(abs(im1-im2)<10) = 0;
                    im2 = [] ;
            end
            
            imA = Image2D(im1, origin1, spacing1);
            
            if ~isempty(im2)
                imB = Image2D(im2, origin2, spacing2);
            else
                imB = [];
            end
        end
        
        function updateWETImages(this)
            [imA, imB] = this.getImages();
            
            im1 = imA.get('data');
            origin1 = imA.get('origin');
            spacing1 = imA.get('spacing');
            
            if ~isempty(imB)
                im2 = imB.get('data');
                origin2 = imB.get('origin');
                spacing2 = imB.get('spacing');
            else
                im2 = [];
            end
                
            this.wetImagesToDisplay{1} = Image2D(im1, origin1, spacing1) ;
            if isempty(im2)
                this.wetImagesToDisplay{2} = im2 ;
            else
                this.wetImagesToDisplay{2} = Image2D(im2, origin2, spacing2) ;
            end
        end
    end
end
