
classdef ViewBraggPanel < MainPanel
    properties (Access = private)
        control ;
        subplot1 ;
        subplot2 ;
        h1 ;
        h2 ;
        h3 ;
        h4 ;
        imh;
    end
    
    methods (Access = public)
        function obj = ViewBraggPanel(f, control)
            obj@MainPanel(f) ;
            obj.control = control ;
            
            obj.imh = [];

            try
                obj.updateImages() ;
                obj.updateCurves() ;
            catch
                
            end
            
            uicontrol('Parent', obj.mainPanel, 'Style', 'pushbutton', 'String', 'Decompose', 'Units', 'normalized', 'Position', [0.05, 0.8, 0.1, 0.1], 'Callback', @obj.decomposeCallback) ;
        end
        
        function updateImages(this)   
            [image1, image2] = this.control.getImages() ;
            
            if ~isempty(image1)
                spacing1 = image1.get('spacing') ;
                
                RA = imref2d(size(image1.get('data')), spacing1(2), spacing1(1));
                RA.XWorldLimits = image1.get('XWorldLimits') ;
                RA.YWorldLimits = image1.get('YWorldLimits') ;
            end
            
            if ~isempty(image2)
                spacing2 = image2.get('spacing') ;
                
                RB = imref2d(size(image2.get('data')), spacing2(2), spacing2(1));
                RB.XWorldLimits = image2.get('XWorldLimits') ;
                RB.YWorldLimits = image2.get('YWorldLimits') ;
            end

            if ~isempty(image1) && ~isempty(image2)
                this.subplot1 = subplot(1, 2, 1, 'Parent',  this.mainPanel) ;
                if ~isempty(this.imh)
                    delete(this.imh)
                end
                
                this.imh = imshowpair(image1.get('data'), RA, image2.get('data'), RB) ;
                set(this.imh, 'ButtonDownFcn', @this.mouseInSubplot1);
                axis equal off ;
                drawnow ;
            elseif ~isempty(image1)
                this.subplot1 = subplot(1, 2, 1, 'Parent',  this.mainPanel) ;
                if ~isempty(this.imh)
                    delete(this.imh)
                end
                
                this.imh = imshow(mat2gray(image1.get('data')), RA) ;
                set(this.imh, 'ButtonDownFcn', @this.mouseInSubplot1);
                axis equal off ;
                drawnow ;
            elseif ~isempty(image2)
                this.subplot1 = subplot(1, 2, 1, 'Parent',  this.mainPanel) ;
                if ~isempty(this.imh)
                    delete(this.imh)
                end
                
                this.imh = imshow(mat2gray(image2.get('data')), RB) ;
                set(this.imh, 'ButtonDownFcn', @this.mouseInSubplot1);
                axis equal off ;
                drawnow ;
            end
        end
    end
    
    methods (Access = private)
        function mouseInSubplot1(this, source, event)
            C = get (gca, 'CurrentPoint') ;
            pos1 = C(1, 1) ;
            pos2 = C(1, 2) ;
            
%             C_init = C ;
%             for k=1:3
%                 C(1, 1) = C_init(1, 1)+(k-1)*45 ;
%                 for i=1:9
%                     for j=1:9
%                         if mod(i, 2)==0
%                             pos1 = C(1, 1)+(j-1)*5 ;
%                             pos2 = C(1, 2)+(9-i)*5 ;
%                         else
%                             pos1 = C(1, 1)+(9-j)*5 ;
%                             pos2 = C(1, 2)+(9-i)*5 ;
%                         end

                        this.control.setPixelPosition(pos2, pos1) ;
                        this.updateCurves() ;
%                         pause(0.1)
%                     end
%                 end
%             end
        end
        
        function decomposeCallback(this, source, event)
            curves = this.control.get('decomposedCurves') ;
            [depth, ycurve1, xcurve2, ycurve2] = this.control.getCurves() ;
            
            try
                delete(this.h1)
                delete(this.h2)
                delete(this.h3)
                delete(this.h4)
            catch
                
            end
            
            [xcurve1, ycurve1, xcurve2, ycurve2] = this.control.getCurves() ;
            this.subplot2 = subplot(1, 2, 2, 'Parent',  this.mainPanel) ;
            legendText = {} ;
            if ~isempty(ycurve1)
                plot(squeeze(xcurve1), squeeze(ycurve1)./mean(squeeze(ycurve1)), 'color', 'blue')
                legendText{length(legendText)+1} = this.control.get('simulationName') ;
            end
            if ~isempty(ycurve2)
                hold on
                plot(squeeze(xcurve2), squeeze(ycurve2)./mean(squeeze(ycurve2)), 'color', 'red')
                legendText{length(legendText)+1} = this.control.get('PRName') ;
            end
            
            plot(sum(curves, 1)/mean(sum(curves, 1)), '--black')
            legendText{length(legendText)+1} = 'Recomposed curve' ;
            
            hold off
            try
                legend(legendText)
            catch
            end
            
            [pos1, pos2] = this.control.getPixelPosition() ;
            
            [image1, image2] = this.control.getImages() ;
            
            spacing1 = image1.get('spacing') ;
            spacing2 = image2.get('spacing') ;
            
            pixelScaling(1) = min([spacing1(1) ; spacing2(1)]) ;
            pixelScaling(2) = min([spacing1(2) ; spacing2(2)]) ;
            
            subplot(this.subplot1)
            hold on
            this.h1 = line ([pos2 pos2], [pos1 pos1+pixelScaling(1)], 'color', 'r', 'LineWidth', 2) ;
            this.h2 = line ([pos2 pos2+pixelScaling(2)], [pos1 pos1], 'color', 'r', 'LineWidth', 2) ;
            this.h3 = line ([pos2 pos2]+pixelScaling(2), [pos1 pos1+pixelScaling(1)], 'color', 'r', 'LineWidth', 2) ;
            this.h4 = line ([pos2 pos2+pixelScaling(2)], [pos1 pos1]+pixelScaling(1), 'color', 'r', 'LineWidth', 2) ;
            hold off
            
            subplot(this.subplot2) ;
            for i=1 : size(curves, 1)
                hold on
                plot(depth, curves(i, :)/mean(ycurve2))
            end
            
            hold off
        end
        
        function updateCurves(this)
            try
                delete(this.h1)
                delete(this.h2)
                delete(this.h3)
                delete(this.h4)
            catch
                
            end
            
            [xcurve1, ycurve1, xcurve2, ycurve2] = this.control.getCurves() ;
            this.subplot2 = subplot(1, 2, 2, 'Parent',  this.mainPanel) ;
            legendText = {} ;
            if ~isempty(ycurve1)
                plot(squeeze(xcurve1), squeeze(ycurve1), 'color', 'blue')
                legendText{length(legendText)+1} = this.control.get('simulationName') ;
            end
            if ~isempty(ycurve1)
                hold on
                plot(squeeze(xcurve2), squeeze(ycurve2), 'color', 'red')
                legendText{length(legendText)+1} = this.control.get('PRName') ;
            end
            hold off
            try
                legend(legendText)
            catch
            end
            
            [pos1, pos2] = this.control.getPixelPosition() ;
            
            [image1, image2] = this.control.getImages() ;
            
            spacing1 = image1.get('spacing') ;
            spacing2 = image2.get('spacing') ;
            
            pixelScaling(1) = min([spacing1(1) ; spacing2(1)]) ;
            pixelScaling(2) = min([spacing1(2) ; spacing2(2)]) ;
            
            subplot(this.subplot1)
            hold on
            this.h1 = line ([pos2 pos2], [pos1 pos1+pixelScaling(1)], 'color', 'r', 'LineWidth', 2) ;
            this.h2 = line ([pos2 pos2+pixelScaling(2)], [pos1 pos1], 'color', 'r', 'LineWidth', 2) ;
            this.h3 = line ([pos2 pos2]+pixelScaling(2), [pos1 pos1+pixelScaling(1)], 'color', 'r', 'LineWidth', 2) ;
            this.h4 = line ([pos2 pos2+pixelScaling(2)], [pos1 pos1]+pixelScaling(1), 'color', 'r', 'LineWidth', 2) ;
            hold off
        end
    end
end
