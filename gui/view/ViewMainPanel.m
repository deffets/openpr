
classdef ViewMainPanel < MainPanel
    properties (Access = private)
        control ;

        prViewPanel ;
        wetViewPanel ;
        
        tabGroup ;
        tabPR ;
        tabWET ;
    end
    
    methods (Access = public)
        function this = ViewMainPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.tabGroup = uitabgroup(this.mainPanel, 'Position', [0 0 1 1]) ;
            
            this.tabPR = uitab(this.tabGroup, 'Title', 'PR', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'tabPR') ;
            this.prViewPanel = ViewPanel(this.tabPR, this.control.get('prViewControl')) ;
            
            this.tabWET = uitab(this.tabGroup, 'Title', 'WET', 'ButtonDownFcn', @this.panelCallback, 'Tag', 'tabWET') ;
            this.wetViewPanel = ViewPRWETPanel(this.tabWET, this.control.get('wetViewControl')) ;
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
            this.prViewPanel.setVisible(visible) ;
            this.wetViewPanel.setVisible(visible) ;
        end
    end
    
    methods (Access = private)
        function panelCallback(this, source, event)
            switch source.Tag
                case 'tabPR'
                    this.prViewPanel.setVisible('on') ;
                    this.wetViewPanel.setVisible('off') ;
                case 'tabWET'
                    this.prViewPanel.setVisible('off') ;
                    this.wetViewPanel.setVisible('on') ;
            end
        end
    end
end
