
classdef SaveCurveControl < ViewMultipleBraggControl
    properties (Access = private)
        saveCurvePanel;
    end
    
    methods (Access = public)
        function this = SaveCurveControl(model)
            this@ViewMultipleBraggControl(model);
            
            addlistener(this.model, 'reference', 'PostSet', @this.updateCurve);
            addlistener(this.model, 'PRNames', 'PostSet', @this.updatePopups);
        end
        
        function value = get(this, varargin)
            value = get@ViewMultipleBraggControl(this, varargin{:});
        end
        
        function set(this, property, value)
            switch property
                case 'saveCurvePanel'
                    this.saveCurvePanel = value;
                otherwise
                    set@ViewMultipleBraggControl(this, property, value);
            end
        end
        
        function saveCurve(this)
            this.model.saveCurve();
        end
    end
    
    methods (Access = protected)
        function updateCurve(this, source, event)
            this.saveCurvePanel.updateCurve();
        end
        
        function updatePopups(this, source, event)
            this.saveCurvePanel.updatePopups();
        end
    end
end
