
classdef ViewModel < GenericModel
    properties (Access = protected)
        image1ToDisplay ;
        image2ToDisplay ;
        
        PRName1 ;
        PRName2 ;
        featureName ;
    end
    
    methods (Access = public)
        function this = ViewModel(regguiHObject)
            this@GenericModel(regguiHObject) ;
            
            this.image1ToDisplay = 1 ;
            this.image2ToDisplay = 1 ;
            this.PRName1 = [] ;
            this.PRName2 = [] ;
            this.featureName = 'Position of the maximum' ;
        end
        
        function value = get(this, property)
            switch property
                case 'PRName1'
                    value = this.PRName1 ;
                case 'PRName2'
                    value = this.PRName2 ;
                case 'featureName'
                    value = this.featureName ;
                case 'featuresNames'
                    value = {'Position of the maximum', 'STD error', 'Range error'} ;
                case 'imagesToDisplay'
                    value = [this.image1ToDisplay, this.image2ToDisplay] ;
                otherwise
                    error('Property not found')
            end
        end
        
        function set(this, property, value)
            switch property
                case 'PRName1'
                    this.PRName1 = value ;
                case 'PRName2'
                    this.PRName2 = value ;
                case 'featureName'
                    this.featureName = value ;
                case 'imagesToDisplay'
                    this.image1ToDisplay = value(1) ;
                    this.image2ToDisplay = value(2) ;
            end
        end
    end
end
