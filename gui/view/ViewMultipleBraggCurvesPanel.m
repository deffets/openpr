
classdef ViewMultipleBraggCurvesPanel < MainPanel
    properties (Access = private)
        control;
        
        mainPlot;
        
        curvesH;
    end
    
    methods (Access = public)
        function this = ViewMultipleBraggCurvesPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            this.control.set('viewMultipleBraggCurvesPanel', this);
            
            uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Save', 'Units', 'normalized', 'Position', [0.01, 0.01, 0.1, 0.1], 'Callback', @this.exportCallback);
            
            this.mainPlot = [];
            this.curvesH = [];
        end
        
        function updateCurves(this)
            if ~isempty(this.curvesH)
                for i=1 : length(this.curvesH)
                    delete(this.curvesH{i})
                end
            end
            
            curves = this.control.get('curves');
            
            xcurves = curves{1};
            ycurves = curves{2};
            
            if isempty(xcurves)
                return
            end
            
            if isempty(this.mainPlot)
                this.mainPlot = subplot(1, 1, 1, 'Parent',  this.mainPanel);
            else
                subplot(this.mainPlot)
            end
            
            cc = lines(18);
            hold on
            for i=1 : length(xcurves)
            	tmp_curvesH{i} = plot(xcurves{i}, ycurves{i}, 'color', cc(i,:));
            end
            hold off
            
            legend(this.control.get('PRNames'))
            
            this.curvesH = tmp_curvesH;
        end
    end
    
    methods (Access = private)
        function exportCallback(this, source, event)
            curves = this.control.get('curves');
            PRNames = this.control.get('PRNames');
            
            depths = curves{1};
            idds = curves{2};
            
            if isempty(depths)
                return
            end
            
            for i=1 : length(depths)
                data{i}.PRName = PRNames{i};
                data{i}.depth = depths{i};
                data{i}.idd = idds{i};
            end
            
            [file, path] = uiputfile({'*.mat'});

            if file
                save(fullfile(path, file), 'data');
            end
        end
    end
end
