
classdef ViewMultipleBraggImagesPanel < MainPanel
    properties (Access = protected)
        control;
        
        p1;
        p2;
        mainPlot;
        pixelH;
        imh;
        cb;
        
        PRPopup1;
        PRPopup2;
        PRPopup3;
        
        warningText;
        
        goButton;
    end
    
    methods (Access = public)
        function this = ViewMultipleBraggImagesPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
%             this.control.set('viewMultipleBraggImagesPanel', this);
            
            this.p1 = uipanel('Parent', this.mainPanel, 'Units', 'normalized', 'Position', [0, 0.9, 1, 0.1], 'BorderType', 'none');
            this.p2 = uipanel('Parent', this.mainPanel, 'Units', 'normalized', 'Position', [0, 0.05, 1, 0.7], 'BorderType', 'none');
            p3 = uipanel('Parent', this.mainPanel, 'Units', 'normalized', 'Position', [0, 0.8, 1, 0.1], 'BorderType', 'none');
            this.warningText = uicontrol('Parent', p3, 'Style', 'text', 'String', '', 'Units', 'normalized', 'Position', [0.01, 0.01, 0.98, 0.98]);
            p4 = uipanel('Parent', this.mainPanel, 'Units', 'normalized', 'Position', [0, 0.01, 1, 0.05], 'BorderType', 'none');
            uicontrol('Parent', p4, 'Style', 'pushbutton', 'String', 'Save', 'Units', 'normalized', 'Position', [0.01, 0.01, 0.1, 0.9], 'Callback', @this.exportCallback);
            uicontrol('Parent', p4, 'Style', 'pushbutton', 'String', 'Define ROI', 'Units', 'normalized', 'Position', [0.15, 0.01, 0.1, 0.9], 'Callback', @this.roiCallback);
            uicontrol('Parent', p4, 'Style', 'pushbutton', 'String', 'Export to Reggui as 3D', 'Units', 'normalized', 'Position', [0.3, 0.01, 0.2, 0.9], 'Callback', @this.export3DCallback);
            
            this.PRPopup1 = [];
            this.PRPopup2 = [];
            this.PRPopup3 = [];
            
            this.mainPlot = [];
            this.pixelH = [];
            this.imh = [];
            this.cb = [];
        end
        
        function updatePopups(this)
            PRNames = this.control.get('PRNames');
            featureNames = this.control.get('featureNames');
            
            if ~isempty(PRNames)
                if isempty(this.PRPopup1)
                    this.PRPopup1 = uicontrol('Parent', this.p1, 'Style', 'popup', 'String', PRNames, 'Value', 1, 'Units', 'normalized', 'Position', [0.01, 0.4, 0.2, 0.3]);
                    this.PRPopup2 = uicontrol('Parent', this.p1, 'Style', 'popup', 'String', PRNames, 'Value', 1, 'Units', 'normalized', 'Position', [0.25, 0.4, 0.2, 0.3]);
                    this.PRPopup3 = uicontrol('Parent', this.p1, 'Style', 'popup', 'String', featureNames, 'Value', 1, 'Units', 'normalized', 'Position', [0.5, 0.4, 0.2, 0.3]);
                    this.goButton = uicontrol('Parent', this.p1, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.75, 0.3, 0.2, 0.4], 'Callback', @this.goCallback);
                else
                    this.PRPopup1.set('String', PRNames, 'Value', 1);
                    this.PRPopup2.set('String', PRNames, 'Value', 1);
                    this.PRPopup3.set('Value', 1);
                    this.goButton.set('Visible', 'on');
                end
            end
        end
        
        function updateImages(this)
            images = this.control.get('imagesToDisplay');
            image1 = images{1};
            image2 = images{2};
            
            if isempty(images)
                return
            end
            
            if isempty(this.mainPlot)
                this.mainPlot = subplot(1, 1, 1, 'Parent',  this.p2);
            else
                s1 = subplot(this.mainPlot);
            end
            
            spacing1 = image1.get('spacing');
            
            RA = imref2d(size(image1.get('data')), spacing1(2), spacing1(1));
            RA.XWorldLimits = image1.get('XWorldLimits')-spacing1(2)/2;
            RA.YWorldLimits = image1.get('YWorldLimits')-spacing1(1)/2;

            if ~isempty(this.imh)
                delete(this.imh)
            end
            
            if ~isempty(this.cb)
                delete(this.cb)
            end
                
            if ~isempty(image2)
                spacing2 = image2.get('spacing');

                RB = imref2d(size(image2.get('data')), spacing2(2), spacing2(1));
                RB.XWorldLimits = image2.get('XWorldLimits')-spacing1(2)/2;
                RB.YWorldLimits = image2.get('YWorldLimits')-spacing1(1)/2;

                this.imh = imshowpair(image1.get('data'), RA, image2.get('data'), RB);
                
                colorbar('off')
            else
                d = image1.get('data');
                
                imTemp = imshow(d, RA);
                xd = imTemp.XData;
                yd = imTemp.YData;
                
                offX = (xd(end)-xd(1)+1)/size(d, 2);
                offY = (yd(end)-yd(1)+1)/size(d, 1);
                delete(imTemp);
                
                this.imh = imagesc(d);
                set(this.imh, 'XData', xd, 'YData', yd);
                set(this.mainPlot, 'xlim', [xd(1)-offX xd(end)+offX], 'ylim', [yd(1)-offY yd(end)+offY]);
                
                colormap(this.mainPlot, 'parula')
                
                d = d(:);
                tickLabels = (0:0.1:1)*(max(d)-min(d))+min(d);

                this.cb = colorbar('Parent', this.p2);
                
                set(this.cb, 'YTick', tickLabels, 'TickLabels', tickLabels, 'TicksMode', 'manual', 'TickLabelsMode', 'manual');
            end
            
            set(this.imh, 'ButtonDownFcn', @this.mouseInSubplot1);
            axis equal off;
            drawnow;
            
            dataInfo = this.control.get('dataInfo', 'spacing');
            for i=1 : length(dataInfo)
                a = dataInfo{i};
                
                sp1(i) = a(1);
                sp2(i) = a(2);
            end
            
            if numel(unique(sp1))>1 || numel(unique(sp2))>1
                this.warningText.set('String', 'Warning: The pixel size of the selected data differs => The Bragg curves on the right panel might not be those at the selected location but the closest ones. To have matching pixel size, consider the 2 DOF registration tool.');
                this.warningText.set('ForegroundColor', [1 0 0]);
            else
                this.warningText.set('String', 'Selected data have the same pixel size');
                this.warningText.set('ForegroundColor', [0.3 0.6 0.1]);
            end
        end
        
        function updatePixel(this)
            if ~isempty(this.pixelH)
                delete(this.pixelH)
            end
            
            pos = this.control.get('pixelPosition');
            
            subplot(this.mainPlot)
            hold on
            this.pixelH = plot(pos(2), pos(1), '.r', 'MarkerSize', 10);
            hold off
        end
        
        function value = get(this, property)
            switch property
                case 'CData'
                    value = this.imh.CData;
                case 'XData'
                    value = this.imh.XData;
                case 'YData'
                    value = this.imh.YData;
                case 'CLim'
                    value = this.imh.Parent.CLim;
            end
        end
    end
    
    methods (Access = protected)        
        function goCallback(this, source, event)
            featureNames = this.control.get('featureNames');
            featureInd = this.PRPopup3.get('Value');
            featureName = featureNames{featureInd};
            
            this.control.set('image1Ind', this.PRPopup1.get('Value'));
            this.control.set('image2Ind', this.PRPopup2.get('Value'));
            
            this.control.set('featureName', featureName);
            this.updateImages();
        end
        
        function mouseInSubplot1(this, source, event)
            C = get (gca, 'CurrentPoint');
            pos1 = C(1, 1);
            pos2 = C(1, 2);
            
            this.control.set('pixelPosition', [pos2, pos1]);
        end
        
        function exportCallback(this, source, event)
            images = this.control.get('imagesToDisplay');
            image1 = images{1};
            image2 = images{2};
            
            if isempty(images)
                return
            end
            
            if ~isempty(image2)
                error('Cannot import several images')
            end
            
            data = image1.get('data');
            [file, path] = uiputfile({'*.mat'});

            if file
                save(fullfile(path, file), 'data');
            end
        end
        
        function roiCallback(this, source, event)
            roiModel = RoiModel(this.control.get('regguiHObject'));
            roiControl = RoiControl(roiModel, this.control);
            f = figure('Name', 'ROI interface', 'NumberTitle', 'off', 'units', 'normalized', 'outerposition', [0 0 1 1]);
            RoiPanel(f, roiControl);
        end
        
        function export3DCallback(this, source, event)
            prompt = {'Enter name for resulting data'};
            title = 'Export 2D image to openREGGUI as 3D data';
            definput = {'3D data name'};
            answer = inputdlg(prompt, title, [1 40], definput);
            
            if isempty(answer)
                return;
            end
            
            this.control.export3DCallback(answer{1});
        end
    end
end
