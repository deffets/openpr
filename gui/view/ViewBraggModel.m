
classdef ViewBraggModel < ViewModel
    properties (SetObservable)
        image1;
        image2;
    end
     
    properties (Access = protected)       
        braggImage1;
        braggImage2;
        
        xcurve1;
        ycurve1;
        xcurve2;
        ycurve2;
        
        pos1;
        pos2;
    end
    
    methods (Access = public)
        function obj = ViewBraggModel(regguiHObject)
            obj@ViewModel(regguiHObject);
            
            obj.xcurve1 = [];
            obj.ycurve1 = [];
            obj.xcurve2 = [];
            obj.ycurve2 = [];
        end
        
        function value = get(this, property)
            switch property
                case 'decomposedCurves'
                    value = this.decomposeCurve();
                otherwise
                    warning('No such property')
            end
        end
        
        function updateImages(this)
            handles = guidata(this.regguiHObject);
            
            if this.image1ToDisplay
                this.braggImage1 = handles2BraggImage(this.PRName1, handles);
                spacing1 = this.braggImage1.get('spacing');
                origin1 = this.braggImage1.get('origin');
                
                switch this.featureName
                    case 'Position of the maximum'
                        im1 = maximumPosition(this.braggImage1.get('data'), this.braggImage1.get('depth'));
                    case 'STD error'
                        this.braggImage2 = handles2BraggImage(this.PRName2, handles);
                        im1 = stdError(this.braggImage1.get('data'), this.braggImage1.get('depth'), this.braggImage2.get('data'), this.braggImage2.get('depth'));
                    case 'Range error'
                        this.braggImage2 = handles2BraggImage(this.PRName2, handles);
                        im1 = rangeError(this.braggImage1.get('data'), this.braggImage1.get('depth'), this.braggImage2.get('data'), this.braggImage2.get('depth'));
                end
                
                this.image1 = Image2D(im1, origin1, spacing1);
            else
                this.image1 = [];
            end
            
            if this.image2ToDisplay
                this.braggImage2 = handles2BraggImage(this.PRName2, handles);
                spacing2 = this.braggImage2.get('spacing');
                origin2 = this.braggImage2.get('origin');
                
                switch this.featureName
                    case 'Position of the maximum'
                        im2 = maximumPosition(this.braggImage2.get('data'), this.braggImage2.get('depth'));
                    case 'STD error'
                        this.braggImage1 = handles2BraggImage(this.PRName1, handles);
                        im2 = stdError(this.braggImage2.get('data'), this.braggImage2.get('depth'), this.braggImage1.get('data'), this.braggImage1.get('depth'));
                    case 'Range error'
                        this.braggImage1 = handles2BraggImage(this.PRName1, handles);
                        im2 = rangeError(this.braggImage2.get('data'), this.braggImage2.get('depth'), this.braggImage1.get('data'), this.braggImage1.get('depth'), 0.5, 25);
                end
                
                this.image2 = Image2D(im2, origin2, spacing2);
            else
                this.image2 = [];
            end
        end
        
        function updateCurves(this)
            if this.image1ToDisplay
                curve1 = squeeze(this.braggImage1.getData([this.pos1 this.pos2]));
                this.xcurve1 = this.braggImage1.get('depth');
                this.ycurve1 = curve1(~isnan(curve1))/mean(curve1(~isnan(curve1)));
            else
                this.xcurve1 = [];
                this.ycurve1 = [];
            end
            
            if this.image2ToDisplay
                curve2 = squeeze(this.braggImage2.getData([this.pos1 this.pos2]));
                this.xcurve2 = this.braggImage2.get('depth');
                this.ycurve2 = curve2(~isnan(curve2))/mean(curve2(~isnan(curve2)));
            else
                this.xcurve2 = [];
                this.ycurve2 = [];
            end
        end
        
        function setPixelPosition(this, pos1, pos2)
            spacing1 = this.braggImage1.get('spacing');
            spacing2 = this.braggImage2.get('spacing');
            
            pixelScaling(1) = max([spacing1(1); spacing2(1)]);
            pixelScaling(2) = max([spacing1(2); spacing2(2)]);
            
            if ~isempty(this.image2)
                o = this.image2.get('origin');
            else
                o = this.image1.get('origin');
            end

            this.pos1 = (floor((pos1-mod(o(1), pixelScaling(1)))/pixelScaling(1)))*pixelScaling(1) + mod(o(1), pixelScaling(1));
            this.pos2 = (floor((pos2-mod(o(2), pixelScaling(2)))/pixelScaling(2)))*pixelScaling(2) + mod(o(2), pixelScaling(2));
        end
        
        function [image1, image2] = getImages(this)
            image1 = this.image1;
            image2 = this.image2;
        end
        
        function [xcurve1, ycurve1, xcurve2, ycurve2] = getCurves(this)
            this.updateCurves();
            
            xcurve1 = this.xcurve1;
            ycurve1 = this.ycurve1;
            xcurve2 = this.xcurve2;
            ycurve2 = this.ycurve2;
        end
        
        function [pos1, pos2] = getPixelPosition(this)
            pos1 = this.pos1;
            pos2 = this.pos2;
        end
    end
    
    methods (Access = private)
        function curves = decomposeCurve(this)
            handles = guidata(this.regguiHObject);
            
            % Ref
            referenceName = 'ref270';
            ref = [];
            for i=1:length(handles.mydata.name)
                if(strcmp(handles.mydata.name{i}, referenceName))
                    ref = handles.mydata.data{i};
                    depth = handles.mydata.info{i}.depth;
                end
            end
            if(isempty(ref))
                error('Reference curve not found in the current list')
            end
            
            bTable = braggTable(ref, depth, 1, 200);
            
            [xcurve1, ycurve1, xcurve2, ycurve2] = this.getCurves();

            a = decomposeCurve(bTable.curves, ycurve2);
            a = a(:);
            
            curves = repmat(a, 1, size(bTable.curves, 2)).*bTable.curves;            
        end
    end
end
