
classdef ViewMainControl < GenericControl
    properties (Access = private)
        prViewControl;
        wetViewControl;
    end
    
    methods (Access = public)
        function this = ViewMainControl(model)
            this@GenericControl(model) ;
            
            prViewModel = SaveCurveModel(this.model.getRegguiHObject()) ;
            this.prViewControl = SaveCurveControl(prViewModel) ;
            
            wetViewModel = ViewMultipleBraggWETModel(this.model.getRegguiHObject()) ;
            this.wetViewControl = ViewMultipleBraggWETControl(wetViewModel) ;
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'prViewControl'
                    value = this.prViewControl ;
                case 'wetViewControl'
                    value = this.wetViewControl ;
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end
    end
end
