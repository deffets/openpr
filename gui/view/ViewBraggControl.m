
classdef ViewBraggControl < GenericViewControl
    properties (Access = private)
        
    end
    
    methods (Access = public)
        function obj = ViewBraggControl(model)
            obj@GenericViewControl(model) ;
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
        
        function [image1, image2] = getImages(this)
            [image1, image2] = this.model.getImages() ;
        end
        
        function [xcurve1, ycurve1, xcurve2, ycurve2] = getCurves(this)
            [xcurve1, ycurve1, xcurve2, ycurve2] = this.model.getCurves() ;
        end
        
        function updateImages(this)
            this.model.updateImages() ;
        end
        
        function [pos1, pos2] = getPixelPosition(this)
            [pos1, pos2] = this.model.getPixelPosition() ;
        end
        
        function setPixelPosition(this, pos1, pos2)
            this.model.setPixelPosition(pos1, pos2) ;
        end
        
        function pixelScaling = getPixelScaling(this)
            pixelScaling = this.model.getPixelScaling() ;
        end
    end
    
    methods (Access = private)
        
    end
end
