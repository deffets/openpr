
classdef ViewSelectionPanel < MainPanel
    properties (Access = protected)
        control ;
        
        PRList ;
        goButton ;
    end
    
    methods (Access = public)
        function this = ViewSelectionPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.PRList = uicontrol('Parent', this.mainPanel, 'Style', 'listbox', 'Max', 10, 'Units', 'normalized', 'Position', [0, 0.1, 1, 0.9]) ;
%             this.PRList = uitable('Parent', this.mainPanel, 'ColumnName', {'Data', 'Display'}, 'ColumnFormat', {'char', 'logical'}, 'ColumnEditable', [false, true], 'RowName', [], 'Units', 'normalized', 'Position', [0, 0, 1, 1]);
            
            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0, 0.025, 1, 0.05], 'Callback', @this.goCallback) ;

            this.updateList() ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updateList() ;
            end
            
            setVisible@MainPanel(this, visible)
        end
    end
    
    methods (Access = protected) 
        function updateList(this)
            names = this.control.getPRNames() ;
            this.PRList.set('String', names) ;
            
%             for i=1 : length(names)
%                 d{i, 1} = names{i} ;
%                 d{i, 2} = false ;
%             end
% 
%             this.PRList.set('Data', d) ;
        end
        
        function goCallback(this, source, event)
            names = this.PRList.get('String') ;
            value = this.PRList.get('Value') ;
            
            PRNames = names(value) ;

            this.control.set('PRNames', PRNames) ;
        end
    end
end
