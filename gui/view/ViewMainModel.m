
classdef ViewMainModel < GenericModel
    properties (Access = protected)
    end
    
    methods (Access = public)
        function this = ViewMainModel(regguiHObject)
            this@GenericModel(regguiHObject) ;
        end
    end
end