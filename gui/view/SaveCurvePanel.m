
classdef SaveCurvePanel < MainPanel
    properties (Access = private)
        control;
        
        refNameEdit;
        PRPopup1;
        goButton;
        
        cPlot;
    end
    
    methods (Access = public)
        function this = SaveCurvePanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            this.cPlot = [];
            this.control.set('saveCurvePanel', this);
        end
        
        function updatePopups(this)
            PRNames = this.control.get('PRNames');
            
            if ~isempty(PRNames)
                if isempty(this.PRPopup1)
                    this.mainPanel.set('Title', 'Save curve as reference');
                    this.PRPopup1 = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'String', PRNames, 'Value', 1, 'Units', 'normalized', 'Position', [0.01, 0.8, 0.2, 0.1], 'Callback', @this.popupCallback);
                    uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Curve name', 'Units', 'normalized', 'Position', [0.7, 0.8, 0.3, 0.1]) ;
                    this.refNameEdit = uicontrol('Parent', this.mainPanel, 'Style', 'edit', 'Units', 'normalized', 'Position', [0.7, 0.5, 0.3, 0.2]) ;
                    this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'Save', 'Units', 'normalized', 'Position', [0.7, 0.1, 0.3, 0.2], 'Callback', @this.goCallback);
                    
                    this.popupCallback([], []);
                else
                    this.PRPopup1.set('String', PRNames, 'Value', 1);
                    this.goButton.set('Visible', 'on');
                end
            end
        end
        
        function updateCurve(this)
            if isempty(this.cPlot)
                this.cPlot = subplot(1, 1, 1, 'Parent',  uipanel('Parent', this.mainPanel, 'Units', 'normalized', 'Position', [0.01 0.1, 0.6, 0.6], 'BorderType', 'none')) ;
            end
            
            subplot(this.cPlot)
            ref = this.control.get('reference');
            plot(ref(1, :), ref(2, :));
        end
    end
    
    methods (Access = private)
        function popupCallback(this, source, event)
            this.control.set('prInd', this.PRPopup1.get('Value'));
        end
        
        function goCallback(this, source, event)
            this.control.set('curveName', this.refNameEdit.get('String')) ;
            this.control.saveCurve() ;
        end
    end
end
