
classdef ViewControl < GenericViewControl
    properties (Access = private)
        viewBraggControl ;
    end
    
    methods (Access = public)
        function obj = ViewControl(model, viewBraggControl)
            obj@GenericViewControl(model) ;
            
            obj.viewBraggControl = viewBraggControl ;
        end
        
        function viewBraggControl = getViewBraggControl(this)
            viewBraggControl = this.viewBraggControl ;
        end
        
        function value = get(this, property)
%             value = this.model.get(property) ;
            value = this.viewBraggControl.get(property) ;
        end
        
        function set(this, property, value)
%             this.model.set(property, value) ;
            this.viewBraggControl.set(property, value) ;
        end
    end
end
