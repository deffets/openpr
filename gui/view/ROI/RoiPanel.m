
classdef RoiPanel < MainPanel
    properties (Access = private)
        control;
        
        mainPlot;
        imh;
        maskH;
        maskHNames;
        polyROIPlot;
        
        activeName;
        
        polyX;
        polyY;
        lines;
        points;
        circle;
        polyROI;
        ballRadius;
        
        xData;
        yData;
        pixelSize;
        
        selectedText;
        nameEdit;
        namesList;
        maskButton;
        polygonButton;
        ballButton;
        saveButton;
        mask3DButton;
        
        maskColors;
    end
    
    properties (Access = protected)
        p2;
    end
    
    methods (Access = public)
        function this = RoiPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            p1 = uipanel('Parent', this.mainPanel, 'Units', 'normalized', 'Position', [0.2, 0, 0.6, 1], 'BorderType', 'none');
            this.p2 = uipanel('Parent', this.mainPanel, 'Units', 'normalized', 'Position', [0.8, 0, 0.2, 1]);
            p3 = uipanel('Parent', this.mainPanel, 'Units', 'normalized', 'Position', [0, 0, 0.2, 1]);
            
            uicontrol('Parent', this.p2, 'Style', 'text', 'String', 'Currently selected:', 'Units', 'normalized', 'Position', [0.0, 0.9, 0.45, 0.05]);
            this.selectedText = uicontrol('Parent', this.p2, 'Style', 'text', 'Units', 'normalized', 'Position', [0.55, 0.9, 0.45, 0.05]);
            
            this.nameEdit = uicontrol('Parent', this.p2, 'Style', 'edit', 'String', 'new_name', 'Units', 'normalized', 'Position', [0.0, 0.7, 1, 0.05]);
            this.polygonButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', 'Create new', 'Units', 'normalized', 'Position', [0.0, 0.65, 0.45, 0.05], 'Callback', @this.newCallback);
            uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', 'Create new from selection', 'Units', 'normalized', 'Position', [0.55, 0.65, 0.45, 0.05], 'Callback', @this.newFromSelectedCallback);
           
            this.polygonButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', 'Polygon', 'Units', 'normalized', 'Position', [0.3, 0.5, 0.4, 0.05], 'Callback', @this.polygonCallback);
            this.ballButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', 'Ball', 'Units', 'normalized', 'Position', [0.3, 0.4, 0.4, 0.05], 'Callback', @this.ballCallback);
            this.saveButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', 'Save to file', 'Units', 'normalized', 'Position', [0.3, 0.3, 0.4, 0.05], 'Callback', @this.saveCallback);
            this.mask3DButton = uicontrol('Parent', this.p2, 'Style', 'pushbutton', 'String', 'Export to Reggui as 3D contour', 'Units', 'normalized', 'Position', [0.3, 0.2, 0.4, 0.05], 'Callback', @this.mask3DCallback);
            
            this.namesList = uitable('Parent', p3, 'Units', 'normalized', 'Position', [0.0, 0, 1, 1], 'RowName', [], 'ColumnName', {'Contour label                     ', '', 'Visible'}, 'ColumnWidth', 'auto', 'CellEditCallback', @this.selectedCellCallback, 'CellSelectionCallback', @this.selectedCellCallback);
            
            
            this.mainPlot = subplot(1, 1, 1, 'Parent',  p1);

            d = this.control.get('CData');
            if isempty(d)
                error('No data given to the ROI tool.');
            end
            
            this.xData = this.control.get('XData');
            this.yData = this.control.get('YData');
            this.pixelSize = [this.yData(2)-this.yData(1) this.xData(2)-this.xData(1)]./[size(d, 1)-1, size(d, 2)-1];
            this.imh = image(d, 'Parent', this.mainPlot, 'XData', this.xData, 'YData', this.yData);
            this.imh.Parent.set('CLim', this.control.get('CLim'));
            
            xd = this.imh.XData;
            yd = this.imh.YData;

            offX = (xd(end)-xd(1)+1)/size(d, 2);
            offY = (yd(end)-yd(1)+1)/size(d, 1);
            delete(this.imh);

            this.imh = imagesc(d);
            set(this.imh, 'XData', xd, 'YData', yd);
            set(this.mainPlot, 'xlim', [xd(1)-offX xd(end)+offX], 'ylim', [yd(1)-offY yd(end)+offY]);
                
            axis equal off
            
            this.control.set('roiPanel', this);
            
            this.polyX = [];
            this.polyY = [];
            this.lines = {};
            this.points = {};
            this.circle = [];
            
            this.updateTable();
        end
        
        function setVisible(this, visible)
            setVisible@MainPanel(this, visible)
        end
        
        function close(this)
            close(this.mainPanel.get('Parent'));
        end
    end
    
    methods (Access = protected)
        function updateTable(this)
            if isempty(this.maskColors)
                rng(0)
                this.maskColors = round(prism*255);
            end
            
            dInit = this.namesList.get('Data');
            
            maskNames = this.control.get('maskNames');
            if ~iscell(maskNames)
                if strcmp(maskNames, 'None')
                    return;
                else
                    maskNames = {maskNames};
                end
            end
            
%             d = cell(length(maskNames), 3);
            
            for i=size(dInit, 1)+1 : length(maskNames)
                dInit{i, 1} = maskNames{i};
                hex(:,2:7) = reshape(sprintf('%02X', this.maskColors(i, :).'), 6, []).'; 
                hex(:,1) = '#';
                dInit{i, 2} = strcat(['<html><body bgcolor="' hex '" text="' hex '">'], '----');
                dInit{i, 3} = false;
            end
            
            this.namesList.set('Data', dInit);
            
            this.namesList.set('ColumnEditable', [false, false, true])
        end
        
        function selectedCellCallback(this, source, event)
            if length(event.Indices)>1
                switch event.Indices(2)
                    case 1
                        d = this.namesList.get('Data');
                        name = d{event.Indices(1), 1};
                        this.selectedText.set('String', name);
                        if ~isempty(name)
                            this.setNewActiveContour(name, true);
                        end
                    case 3
                        try
                            d = this.namesList.get('Data');
                            name = d{event.Indices(1), 1};
                            color = this.maskColors(event.Indices(1), :);

                            if event.NewData
                                this.showContour(name, color);
                            else
                                this.unshowContour(name);
                            end
                        catch
                        end
                end
            end
        end
        
        function newCallback(this, source, event)
            name = this.control.checkName(this.nameEdit.get('String'));
            this.nameEdit.set('String', name);
            this.updateHandles(true);
            this.updateTable();
            this.setNewActiveContour(name, false);
        end
        
        function polygonCallback(this, source, event)
            this.createPolygon(true);
        end
        
        function saveCallback(this, source, event)
%             data = this.computeMaskFromActivePolygon();
            im = this.control.get('ROI', this.activeName);
            data = im.get('data');
            
            if isempty(data)
                return
            end
            
            [file, path] = uiputfile({'*.mat'});

            if file
                save(fullfile(path, file), 'data');
            end
        end
        
        function newFromSelectedCallback(this, source, event)
            oldName = this.activeName;
            
            name = this.control.checkName(this.nameEdit.get('String'));
            this.nameEdit.set('String', name);
                        
            this.createPolygonFromMask(oldName);
            
            this.activeName = name;
            this.updateHandles(true);
            
            this.deleteGraphical('all');
            this.updateTable();
        end
        
        function mask3DCallback(this, source, event)
            this.control.exportAs3DMask(this.activeName, 270);
        end
        
        function setNewActiveContour(this, name, existing)
            this.activeName = name;
            this.deleteGraphical('all');
            
            if existing
                this.createPolygonFromMask(name);
            else
                this.createPolygon(true);
            end
        end
        
        function createPolygonFromMask(this, name)
            im = this.control.get('ROI', name);
            
            origin = im.get('origin');
            spacing = im.get('spacing');
            TFin = single(im.get('data'));
            
            if isempty(TFin)
                return;
            end
            
            TFin(TFin~=0) = 1;
            
            [X, Y] = meshgrid(this.xData(1):this.pixelSize(1):this.xData(2), this.yData(1):this.pixelSize(2):this.yData(2));
            
            [X2, Y2] = meshgrid(origin(2):spacing(2):(origin(2)+spacing(2)*(size(TFin, 2)-1)), origin(1):spacing(1):(origin(1)+spacing(1)*(size(TFin, 1)-1)));
            TFin = logical(interp2(X2, Y2, TFin, X, Y, 'linear', 0));
                
            X = X(TFin);
            X = X(:);
            Y = Y(TFin);
            Y = Y(:);
            
            [X, XInd] = sort(X, 'ascend');
            Y = Y(XInd);
            
            [~, XFirstInds] = unique(X, 'first');
            [~, XLastInds] = unique(X, 'last');
            
            pgon = [];
            for i=1 : length(XFirstInds)
                currentX = X(XFirstInds(i));
                
                Y_currentX = Y(XFirstInds(i):XLastInds(i));
                Y_currentX = sort(Y_currentX, 'descend');
                
                x = [currentX-this.pixelSize(2)/2 currentX+this.pixelSize(2)/2 currentX+this.pixelSize(2)/2 currentX-this.pixelSize(2)/2];
                
                firstY = Y_currentX(1);
                currentY = firstY;
                
                for j=1 : length(Y_currentX)
                    if Y_currentX(j) ~= currentY-this.pixelSize(1)
                        y = [firstY+this.pixelSize(1)/2 firstY+this.pixelSize(1)/2 currentY-this.pixelSize(1)/2 currentY-this.pixelSize(1)/2];
                    
                        pgon2 = polyshape(x, y);
                        
                        if isempty(pgon)
                        	pgon = pgon2;
                        else
                            pgon = simplify(union(pgon, pgon2));
                        end
                        
                        firstY = Y_currentX(j);
                    end
                    
                    currentY = Y_currentX(j);
                end
                
                y = [firstY+this.pixelSize(1)/2 firstY+this.pixelSize(1)/2 currentY-this.pixelSize(1)/2 currentY-this.pixelSize(1)/2];
                
                pgon2 = polyshape(x, y);

                if isempty(pgon)
                    pgon = pgon2;
                else
                    pgon = simplify(union(pgon, pgon2));
                end
                
                if ~mod(i, 20)
                    this.polyROI = simplify(pgon);
                    this.updatePolyROIPlot();
                    pause(0.1)
                end
            end
            
            this.polyROI = simplify(pgon);
            this.updatePolyROIPlot();
        end
        
        function TFin = computeMaskFromActivePolygon(this)
            if ~isempty(this.polyROI)
                [X1, Y1] = meshgrid(this.xData(1):this.pixelSize(2):this.xData(2), this.yData(1):this.pixelSize(1):this.yData(2));
                X = X1(:);
                Y = Y1(:);
                TFin = isinterior(this.polyROI, X, Y);
                TFin = reshape(TFin, size(X1));
            else
                TFin = [];
            end
        end
        
        function createPolygon(this, enable)
            if enable
                this.polyX = [];
                this.polyY = [];
                set(this.imh, 'ButtonDownFcn', @this.mouseInSubplot1);
                
                for i=1:length(this.maskH)
                    this.maskH{i}.set('ButtonDownFcn', @this.mouseInSubplot1);
                end
            else                
                set(this.imh, 'ButtonDownFcn', '');
                for i=1:length(this.maskH)
                    this.maskH{i}.set('ButtonDownFcn', '');
                end
                
                
                if ~isempty(this.polyROI)
                    [X, Y] = meshgrid(this.xData(1):this.pixelSize(2):this.xData(2), this.yData(1):this.pixelSize(1):this.yData(2));

                    X = X(:);
                    Y = Y(:);

                    TFin = isinterior(this.polyROI, X, Y);

                    X = X(TFin);
                    Y = Y(TFin);

                    x1 = [X(1)-this.pixelSize(2)/2 X(1)-this.pixelSize(2)/2 X(1)+this.pixelSize(2)/2 X(1)+this.pixelSize(2)/2];
                    y1 = [Y(1)-this.pixelSize(1)/2 Y(1)+this.pixelSize(1)/2 Y(1)+this.pixelSize(1)/2 Y(1)-this.pixelSize(1)/2];

                    pgon = polyshape(x1, y1);

                    for i=2 : 1 : length(X)
                        x1 = [X(i)-this.pixelSize(2)/2 X(i)-this.pixelSize(2)/2 X(i)+this.pixelSize(2)/2 X(i)+this.pixelSize(2)/2];
                        y1 = [Y(i)-this.pixelSize(1)/2 Y(i)+this.pixelSize(1)/2 Y(i)+this.pixelSize(1)/2 Y(i)-this.pixelSize(1)/2];

                        pgon = simplify(union(pgon, polyshape(x1, y1))); 
                    end
                end

                this.polyROI = simplify(pgon);
            
                this.updateHandles(false);
                
                this.updatePolyROIPlot();
                this.deleteGraphical('lines');
                this.deleteGraphical('points');
            end
        end

        function showContour(this, name, color)
            this.unshowContour(name);
            
            im = this.control.get('ROI', name);
            
            origin = im.get('origin');
            spacing = im.get('spacing');
            TFin = single(im.get('data'));
            
            TFin(TFin~=0) = 1;
            
            [X, Y] = meshgrid(this.xData(1):this.pixelSize(2):this.xData(2), this.yData(1):this.pixelSize(1):this.yData(2));
            
            [X2, Y2] = meshgrid(origin(2):spacing(2):(origin(2)+spacing(2)*(size(TFin, 2)-1)), origin(1):spacing(1):(origin(1)+spacing(1)*(size(TFin, 1)-1)));
            TFin = logical(interp2(X2, Y2, TFin, X, Y, 'linear', 0));
                
            c = cat(3, color(1)*ones(size(TFin)), color(2)*ones(size(TFin)), color(3)*ones(size(TFin)));
            
            
            subplot(this.mainPlot)
            hold on
            this.maskH{length(this.maskH)+1} = imshow(c, 'XData', this.xData, 'YData', this.yData);
            this.maskHNames{length(this.maskHNames)+1} = name;
            set(this.maskH{length(this.maskH)}, 'alphadata', 0.4*TFin);
            hold off
        end
        
        function unshowContour(this, name)
            for i=1:length(this.maskHNames)
                if strcmp(this.maskHNames{i}, name)
                    delete(this.maskH{i});
                    this.maskH(i) = [];
                    this.maskHNames(i) = [];
                    break;
                end
            end
        end
        
        function deleteGraphical(this, property)
            switch property
                case 'all'
                    this.deleteGraphical('lines');
                    this.deleteGraphical('ROIPlot');
                    this.deleteGraphical('points');
                    
                    this.polyX = [];
                    this.polyY = [];
                    this.polyROI = [];
                case 'lines'
                     for i=length(this.lines) : -1 : 1 
                        delete(this.lines{i})
                    end
                    this.lines = {};
                case 'ROIPlot'
                    if ~isempty(this.polyROIPlot)
                        delete(this.polyROIPlot);
                    end
                case  'points'
                    for i=length(this.points) : -1 : 1 
                        delete(this.points{i})
                    end
                    this.points = {};
            end
            
        end
        
        function updatePolyROIPlot(this)
            this.deleteGraphical('ROIPlot');
            
            subplot(this.mainPlot)
            hold on
            this.polyROIPlot = plot(this.polyROI, 'EdgeColor', this.getActiveColor()/255, 'FaceColor', 'none');
            hold off
        end
        
        function color = getActiveColor(this)
        	color = [255 0 0];
            
            dInit = this.namesList.get('Data');
            for i=1 : size(dInit, 1)
                if strcmp(dInit(i, 1), this.activeName)
                    color = this.maskColors(i, :);
                    break;
                end
            end
        end
        
        function updateHandles(this, new)
            disp('Upadting handles');
            
            TFin = this.computeMaskFromActivePolygon();
            
            origin = [this.yData(1), this.xData(1)];
            spacing = this.pixelSize;
            
            if new
                this.control.saveROI(Image2D(TFin, origin, spacing), this.activeName);
            else
                this.control.updateROI(Image2D(TFin, origin, spacing), this.activeName);
            end
        end
        
        function ballCallback(this, source, event)            
            if strcmp(this.ballButton.get('String'), 'Save and exit ball')
                this.updateHandles(false);
                this.createPolygon(false);
                if ~isempty(this.circle)
                    delete(this.circle);
                end
                this.ballButton.set('String', 'Ball');
                set(gcf, 'WindowButtonMotionFcn', '') ;
            else
                if ~isempty(this.polyROI)
                    set(gcf, 'WindowButtonMotionFcn', @this.ballMouse, 'Interruptible', 'off') ;
                    this.ballButton.set('String', 'Save and exit ball');
                end
            end
        end
        
        function ballMouse(this, source, event)
            if ~isempty(this.circle)
                delete(this.circle);
            end
            
            C = get (gca, 'CurrentPoint');
            pos1 = C(1, 1);
            pos2 = C(1, 2);
            
            if pos1<this.xData(1) || pos1>this.xData(2)
                return
            end
            if pos2<this.yData(1) || pos2>this.yData(2)
                return
            end
            
            if ~ismember('control', get(gcf,'currentModifier'))
                vertexid = nearestvertex(this.polyROI, pos1, pos2);
                xC = this.polyROI.Vertices(vertexid,1);
                yC = this.polyROI.Vertices(vertexid,2);

                dist = sqrt((xC-pos1)^2 + (yC-pos2)^2);

                radius1 = min(50*this.pixelSize(1)/dist, dist);
                radius2 = min(50*this.pixelSize(2)/dist, dist);
                this.ballRadius = min(radius1, radius2);
            end
            
            t = 0.05:0.5:2*pi;
            x1 = cos(t)*this.ballRadius;
            y1 = sin(t)*this.ballRadius;
            pgon = polyshape(x1+pos1, y1+pos2);
            
            subplot(this.mainPlot)
            hold on
            this.circle = plot(pgon, 'EdgeColor', this.getActiveColor()/255, 'FaceColor', 'none'); %rectangle('Position', [pos1-this.pixelSize(1) pos2-this.pixelSize(2) 2*this.pixelSize(1) 2*this.pixelSize(2)],'Curvature', [1 1], 'EdgeColor', 'red');
            hold off
            
            if ismember('control', get(gcf,'currentModifier'))
                if isinterior(this.polyROI, pos1, pos2)
                    this.polyROI = simplify(union(this.polyROI, pgon));
                else
                    this.polyROI = subtract(this.polyROI, pgon);
                end
                
                this.polyROI = setPolygonOnEdge(this, this.polyROI);
                
                this.updatePolyROIPlot();
            end
        end
        
        function pgon = setPolygonOnEdge(this, pgon)
            x = pgon.Vertices(:, 1);
            y = pgon.Vertices(:, 2);
            
            [x2, y2] = this.setPointOnEdge(x, y);
            
            pgon = polyshape(x2(:), y2(:));
        end
        
        function [x2, y2] = setPointOnEdge(this, x, y)
            pos1 = x(:);
            pos2 = y(:);
            
            off1 = mod(this.xData(1), this.pixelSize(2));
            off2 = mod(this.yData(1), this.pixelSize(1));
            
            off1 = off1(:);
            off2 = off2(:);
                        
            m1 = mod(pos1-off1+this.pixelSize(2)/2, this.pixelSize(2));
            m2 = mod(pos2-off2+this.pixelSize(1)/2, this.pixelSize(1));
            
            m1 = m1(:);
            m2 = m2(:);
            
            offs1 = zeros(size(pos1));
            offs2 = zeros(size(pos2));
            
            offs1(m1<=this.pixelSize(2)/2) = -m1(m1<=this.pixelSize(2)/2);
            offs1(m1>this.pixelSize(2)/2) = this.pixelSize(2)-m1(m1>this.pixelSize(2)/2);
            
            offs2(m2<=this.pixelSize(1)/2) = -m2(m2<=this.pixelSize(1)/2);
            offs2(m2>this.pixelSize(1)/2) = this.pixelSize(1)-m2(m2>this.pixelSize(1)/2);

            x2 = pos1+offs1;
            y2 = pos2+offs2;
        end
        
        function mouseInSubplot1(this, source, event)
            C = get (gca, 'CurrentPoint');
            pos1 = C(1, 1)
            pos2 = C(1, 2)
            
            [pos1, pos2] = setPointOnEdge(this, pos1, pos2);
            
            if ~isempty(this.polyX)
                if pos1==this.polyX(1) && pos2==this.polyY(1)
                    this.polyX(end+1) = pos1;
                    this.polyY(end+1) = pos2;
                    
                    pgon = polyshape(this.polyX, this.polyY);

                    if ~isempty(this.polyROI)
                        this.polyROI = simplify(union(this.polyROI, pgon));
                    else
                        this.polyROI = pgon;
                    end
                    
                    this.createPolygon(false);
                    return
                end
                
%                 if length(this.polyX)>1
%                     nanP = isnan(polyshape([this.polyX pos1], [this.polyY pos2]).Vertices);
%                     if sum(nanP(:))
%                         return
%                     end
%                 end
                
                this.polyX(end+1) = pos1;
                this.polyY(end+1) = pos2;
                
                if length(this.polyX)>2
                    pgon = polyshape(this.polyX, this.polyY);

                    if ~isempty(this.polyROI)
                        this.polyROI = simplify(union(this.polyROI, pgon));
                    else
                        this.polyROI = pgon;
                    end
                end

                subplot(this.mainPlot)
                hold on
                this.lines{length(this.lines)+1} = line(this.polyX(end-1:end), this.polyY(end-1:end), 'Color', this.getActiveColor()/255, 'ButtonDownFcn', @this.mouseInSubplot1);
                hold off
            else
                this.polyX(1) = pos1;
                this.polyY(1) = pos2;
            end
            
            subplot(this.mainPlot)
            hold on
            if isempty(this.points)
                this.points{1} = plot(this.polyX(end), this.polyY(end), '.', 'color', this.getActiveColor()/255, 'ButtonDownFcn', @this.mouseInSubplot1);
            else
                this.points{length(this.points)+1} = plot(this.polyX(end), this.polyY(end), '.', 'color', this.getActiveColor()/255, 'ButtonDownFcn', @this.mouseInSubplot1);
            end
            hold off
        end
    end
end