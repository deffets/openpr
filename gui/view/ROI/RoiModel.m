
classdef RoiModel < GenericModel
    properties (Access = private)
        initialViewControl;
        initialized;
        
        CLim;
        CData;
        XData;
        YData;
    end
    
    methods (Access = public)
        function this = RoiModel(regguiHObject)
            this@GenericModel(regguiHObject);
            
            this.initialized = false;
        end
        
        function initialize(this, CData, XData, YData, CLim)
            this.CData = CData;
            this.XData = XData;
            this.YData = YData;
            this.CLim = CLim;
            
            this.initialized = true;
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'XData'
                    value = this.XData;
                case 'YData'
                    value = this.YData;
                case 'CData'
                    value = this.CData;
                case 'CLim'
                    value = this.CLim;
                case 'ROI'
                    roiName = varargin{2};
                    value = handles2Image2D(roiName, guidata(this.regguiHObject));
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end
        
        function myImageName = checkName(this, name)
            handles = guidata(this.regguiHObject);
             
            myImageName = check_existing_names(name, handles.mydata.name);
            myImageName = check_existing_names(myImageName, handles.images.name);
        end
        
        function updateROI(this, image2D, name)
            handles = guidata(this.regguiHObject);
            handles = image2D2handles(image2D, 'mask', name, handles, true);
            guidata(this.regguiHObject, handles);
        end
        
        function saveROI(this, image2D, name)
            handles = guidata(this.regguiHObject);
            handles = image2D2handles(image2D, 'mask', name, handles, false);
            guidata(this.regguiHObject, handles);
        end
        
        function exportAs3DMask(this, name, gantryAngle)
            handles = guidata(this.regguiHObject);
            handles = add2DImage2Images(name, gantryAngle, name, handles);
            guidata(this.regguiHObject, handles);
        end
    end
end