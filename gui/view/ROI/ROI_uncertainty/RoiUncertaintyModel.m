
classdef RoiUncertaintyModel < RoiModel
    properties (Access = private)
    end
    
    methods (Access = public)
        function this = RoiUncertaintyModel(regguiHObject)
            this@RoiModel(regguiHObject);
        end
        
        function createMaskFromUncertaintyRule(this, mmUncertainty, stdUncertainty, wet1Name, wet2Name, outputName)
            handles = guidata(this.regguiHObject);
            
            wImage1 = handles2Image2D(wet1Name, handles);   
            wImage2 = handles2Image2D(wet2Name, handles);
            
            spacing2 = wImage2.get('spacing');

            [worldLimit1, worldLimit2] = computeWorldLimits(wImage1.get('XWorldLimits'), ...
                wImage1.get('YWorldLimits'), wImage2.get('XWorldLimits'), ...
                wImage2.get('YWorldLimits'), spacing2);
            worldLimit2(1) = worldLimit2(1) - spacing2(1);
            worldLimit2(2) = worldLimit2(2) - spacing2(2);

            im1 = wImage1.getData(worldLimit1, worldLimit2, spacing2);
            im2 = wImage2.getData(worldLimit1, worldLimit2, spacing2);
            
            wetError = im1-im2;
            
            mask = false(size(wetError));
            mask(abs(wetError)<(mmUncertainty+im1*0.01*stdUncertainty)) = true;
            
            this.saveROI(Image2D(mask, worldLimit1, spacing2), outputName);
        end
    end
end