
classdef RoiUncertaintyControl < RoiControl
    properties (Access = private)
    end
    
    methods (Access = public)
        function this = RoiUncertaintyControl(model, initialViewControl)
            this@RoiControl(model, initialViewControl);
        end
        
        function createMaskFromUncertaintyRule(this, mmUncertainty, stdUncertainty, wet1Name, wet2Name, outputName)
            this.model.createMaskFromUncertaintyRule(mmUncertainty, stdUncertainty, wet1Name, wet2Name, outputName);
        end
    end
end
