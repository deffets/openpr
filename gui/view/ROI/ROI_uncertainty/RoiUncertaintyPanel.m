
classdef RoiUncertaintyPanel < RoiPanel
    properties (Access = private)
        control;
        
        mmEdit;
        stdEdit;
        nameEdit;
        
        wet1Popup;
        wet2Popup;
    end
    
    methods (Access = public)
        function this = RoiUncertaintyPanel(f, control)
            this@RoiPanel(f, control);
            this.control = control;
            
            pos = this.p2.get('Position');
            pos(4) = 0.6;
            this.p2.set('Position', pos)
            
            pos(2) = pos(4)+0.05;
            pos(4) = 1-pos(2);
            p4 = uipanel('Parent', this.mainPanel, 'Units', 'normalized', 'Position', pos);

            uicontrol('Parent', p4, 'Style', 'text', 'String', 'Create mask from uncertainty rule', 'Units', 'normalized', 'Position', [0, 0.9, 1, 0.05]);
            this.mmEdit = uicontrol('Parent', p4, 'Style', 'edit', 'String', '0.6', 'Units', 'normalized', 'Position', [0, 0.8, 0.2, 0.05]);
            uicontrol('Parent', p4, 'Style', 'text', 'String', 'mm ±', 'Units', 'normalized', 'Position', [0.2, 0.8, 0.2, 0.05]);
            this.stdEdit = uicontrol('Parent', p4, 'Style', 'edit', 'String', '2.4', 'Units', 'normalized', 'Position', [0.4, 0.8, 0.2, 0.05]);
            uicontrol('Parent', p4, 'Style', 'text', 'String', '% (1.5 STD)', 'Units', 'normalized', 'Position', [0.6, 0.8, 0.4, 0.05]);
            
            uicontrol('Parent', p4, 'Style', 'text', 'String', 'Reference WEPL', 'Units', 'normalized', 'Position', [0, 0.7, 0.45, 0.05]);
            uicontrol('Parent', p4, 'Style', 'text', 'String', 'WEPL 2', 'Units', 'normalized', 'Position', [0.55, 0.7, 0.45, 0.05]);
            this.wet1Popup = uicontrol('Parent', p4, 'Style', 'popupmenu', 'String', this.control.get('WETNames'), 'Units', 'normalized', 'Position', [0, 0.65, 0.45, 0.05]);
            this.wet2Popup = uicontrol('Parent', p4, 'Style', 'popupmenu', 'String', this.control.get('WETNames'), 'Units', 'normalized', 'Position', [0.55, 0.65, 0.45, 0.05]);
            
            this.nameEdit = uicontrol('Parent', p4, 'Style', 'edit', 'String', 'new_name', 'Units', 'normalized', 'Position', [0, 0.4, 0.45, 0.1]);
            uicontrol('Parent', p4, 'Style', 'pushbutton', 'String', 'Create mask', 'Units', 'normalized', 'Position', [0.55, 0.4, 0.45, 0.1], 'Callback', @this.createMaskCallback);
        end
    end
    
    methods (Access = private)
        function createMaskCallback(this, source, event)
            mmUncertainty = str2double(this.mmEdit.get('String'));
            stdUncertainty = str2double(this.stdEdit.get('String'));
            
            wet1NameInd = this.wet1Popup.get('Value');
            wet1Names = this.wet1Popup.get('String');
            wet1Name = wet1Names{wet1NameInd};
            
            wet2NameInd = this.wet2Popup.get('Value');
            wet2Names = this.wet2Popup.get('String');
            wet2Name = wet2Names{wet2NameInd};
            
            outputName = this.nameEdit.get('String');
            
            this.control.createMaskFromUncertaintyRule(mmUncertainty, stdUncertainty, wet1Name, wet2Name, outputName);
            this.updateTable();
        end
    end
end
