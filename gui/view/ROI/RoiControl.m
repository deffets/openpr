
classdef RoiControl < GenericViewControl
    properties (Access = private)
        initialViewControl;
        roiPanel;
        errorAtCreation;
    end
    
    methods (Access = public)
        function this = RoiControl(model, initialViewControl)
            this@GenericViewControl(model);
            
            this.initialViewControl = initialViewControl;
            
            this.errorAtCreation = false;
            
            try
                model.initialize(this.initialViewControl.get('CData'), this.initialViewControl.get('XData'), this.initialViewControl.get('YData'), this.initialViewControl.get('CLim'));
            catch
                this.errorAtCreation = true;
            end
        end
        
        function value = get(this, varargin)
            value = this.model.get(varargin{:});
        end
        
        function set(this, property, value)
            switch property
                case 'roiPanel'
                    this.roiPanel = value;
                    
                    if this.errorAtCreation
                        this.roiPanel.close();
                    end
                otherwise
                    this.model.set(property, value);
            end
        end
        
        function myImageName = checkName(this, name)
            myImageName = this.model.checkName(name);
        end
        
        function updateROI(this, image2D, name)
            this.model.updateROI(image2D, name);
        end
        
        function saveROI(this, image2D, name)
            this.model.saveROI(image2D, name);
        end
        
        function exportAs3DMask(this, name, gantryAngle)
            this.model.exportAs3DMask(name, gantryAngle);
        end
    end
end
