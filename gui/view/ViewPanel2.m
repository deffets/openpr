
classdef ViewPanel < MainPanel
    properties (Access = private)
        control ;
        selectionPanel ;
        selectionTitle ;
        PRText1 ;
        PRText2 ;
        featureText ;
        goButton ;
        PRPopup1 ;
        PRPopup2 ;
        featurePopup ;
        p2 ;
    end
    
    methods (Access = public)
        function this = ViewPanel(f, control)
            this@MainPanel(f) ;
            this.control = control ;
            
            this.selectionPanel = uipanel('Parent', this.mainPanel, 'Position', [0 0.85 1 0.125]) ;
            selectionTitlePanel = uipanel('Parent', this.mainPanel, 'Position', [0, 0.945, 0.1, 0.03]) ;
            this.p2 = uipanel(this.mainPanel, 'Position', [0.11 0 0.9 0.825]) ;
            this.selectionTitle = uicontrol('Parent', selectionTitlePanel, 'Style', 'text', 'String', 'Parameters', 'Units', 'normalized', 'Position', [0, 0, 1, 1], 'BackgroundColor', [.84 .84 .84], 'FontWeight', 'bold') ;
            p3 = uipanel(this.mainPanel, 'Position', [0 0 0.1 0.825]) ;
            ViewSelectionPanel(p3, this.control) ;
            
            
            this.featureText = uicontrol('Parent', this.mainPanel, 'Style', 'text', 'String', 'Feature', 'Units', 'normalized', 'Position', [0.3, 0.9, 0.1, 0.025]) ;

            val = this.control.get('imagesToDisplay') ;
            image1ToDisplay = val(1) ;
            this.PRText1 = uicontrol('Parent', this.mainPanel, 'Style', 'checkbox', 'String', 'PR 1', 'Value', image1ToDisplay, 'Units', 'normalized', 'Position', [0.01, 0.9, 0.1, 0.025]) ; 
            
            val = this.control.get('imagesToDisplay') ;
            image2ToDisplay = val(2) ;
            this.PRText2 = uicontrol('Parent', this.mainPanel, 'Style', 'checkbox', 'String', 'PR 2', 'Value', image2ToDisplay, 'Units', 'normalized', 'Position', [0.15, 0.9, 0.1, 0.025]) ;

            this.PRPopup1 = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.01, 0.875, 0.1, 0.025]) ;
            this.PRPopup2 = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.15, 0.875, 0.1, 0.025]) ;
            this.featurePopup = uicontrol('Parent', this.mainPanel, 'Style', 'popup', 'Units', 'normalized', 'Position', [0.3, 0.875, 0.1, 0.025]);

            this.updatePRPopup1() ;
            this.updatePRPopup2() ;
            this.updateFeaturesPopup() ;
            this.displayBraggPanel() ;
            
            this.goButton = uicontrol('Parent', this.mainPanel, 'Style', 'pushbutton', 'String', 'GO', 'Units', 'normalized', 'Position', [0.875, 0.875, 0.1, 0.05], 'Callback', @this.goCallback) ;
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')
                this.updatePRPopup1() ;
                this.updatePRPopup2() ;
                
                set( gcf, 'toolbar', 'figure' )
            else
                set( gcf, 'toolbar', 'none' )
            end
            
            setVisible@MainPanel(this, visible)
        end
        
        function updatePRPopup1(this)
            names = this.control.getPRNames() ;
            PRName1 = this.control.get('PRName1') ;
            if ~isempty(PRName1)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, PRName1)) ;
            else
                val = 1 ;
            end
            this.PRPopup1.set('String', names, 'Value', val) ;
        end
        
        function updatePRPopup2(this)
            names = this.control.getPRNames() ;
            PRName2 = this.control.get('PRName2') ;
            if ~isempty(PRName2)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, PRName2)) ;
            else
                val = 1 ;
            end
            this.PRPopup2.set('String', names, 'Value', val(1)) ;
        end
        
        function updateFeaturesPopup(this)
            names = this.control.get('featuresNames') ;
            featureName = this.control.get('featureName') ;
            if ~isempty(featureName)
                ind = 1 : length(names) ;
                val = ind(strcmp(names, featureName)) ;
            else
                val = 1 ;
            end
            this.featurePopup.set('String', names, 'Value', val) ;
        end
    end
    
    methods (Access = private)        
        function goCallback(this, source, event)
            items = get(this.PRPopup1, 'String') ;
            index_selected = get(this.PRPopup1, 'Value') ;
            PRName1 = items{index_selected} ;
            this.control.set('PRName1', PRName1) ;
            
            items = get(this.PRPopup2, 'String') ;
            index_selected = get(this.PRPopup2, 'Value') ;
            PRName2 = items{index_selected} ;
            this.control.set('PRName2', PRName2) ;
            
            items = get(this.featurePopup, 'String') ;
            index_selected = get(this.featurePopup, 'Value') ;
            featureName = items{index_selected} ;
            this.control.set('featureName', featureName) ;
            
            this.control.set('imagesToDisplay', [get(this.PRText1, 'Value'), get(this.PRText2, 'Value')]) ;
            
            viewBraggControl = this.control.getViewBraggControl() ;
            viewBraggControl.updateImages() ;
            this.displayBraggPanel() ;
        end
        
        function displayBraggPanel(this)
            viewBraggControl = this.control.getViewBraggControl() ;
            ViewBraggPanel(this.p2, viewBraggControl) ;
        end
    end
end
