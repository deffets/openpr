
classdef GenericViewControl < GenericControl
    properties (Access = private)
        regguiHObject ;
    end
    
    methods (Access = public)
        function obj = GenericViewControl(model)
            obj@GenericControl(model) ;
            obj.regguiHObject = obj.model.getRegguiHObject() ;
        end
        
        function value = get(this, property)
            value = this.model.get(property) ;
        end
        
        function set(this, property, value)
            this.model.set(property, value) ;
        end
    end
end
