

classdef SaveCurveModel < ViewMultipleBraggModel
    properties (SetObservable)
        curveName;
        prInd;
        reference;
    end
    
    methods (Access = public)
        function this = SaveCurveModel(regguiHObject)
            this@ViewMultipleBraggModel(regguiHObject);
            
            this.prInd = [];
            this.curveName = [];
            this.reference = [];
            
            addlistener(this, 'curves', 'PostSet', @this.updateCurves);
            addlistener(this, 'prInd', 'PostSet', @this.updateCurves);
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'curveName'
                    value = this.curveName;
                case 'prInd'
                    value = this.prInd;
                    this.updateCurves();
                case 'reference'
                    value = this.reference;
                otherwise
                    value = get@ViewMultipleBraggModel(this, varargin{:});
            end
        end
        
        function set(this, property, value)
            switch property
                case 'prInd'
                    this.prInd = value;
                case 'curveName'
                    this.curveName = value;
                otherwise
                    set@ViewMultipleBraggModel(this, property, value);
            end
        end
        
        function saveCurve(this)
            handles = guidata(this.regguiHObject) ;
                        
            instruction = ['handles = saveReference(' '''' this.PRNames{this.prInd} '''' ', [' num2str(this.pixelPosition) '], ' '''' this.curveName '''' ', ' 'handles) ;'] ;
            handles.instructions{length(handles.instructions)+1} = instruction ;
            handles = Execute_reggui_instructions(handles) ;
            guidata(this.regguiHObject, handles) ;
        end
    end
    
    methods (Access = private)
        function updateCurves(this, source, event)
            curve = this.get('curves');
            if ~isempty(curve)
                reference(1, :) = curve{1}{this.prInd};
                reference(2, :) = curve{2}{this.prInd};

                this.reference = reference;
            end
        end
    end
end
