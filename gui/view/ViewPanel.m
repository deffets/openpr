
classdef ViewPanel < MainPanel
    properties (Access = private)
        control;
    
        viewSelectionPanel;
    end
    
    methods (Access = public)
        function this = ViewPanel(f, control)
            this@MainPanel(f);
            this.control = control;
            
            p1 = uipanel(this.mainPanel, 'Position', [0 0 0.1 1]);
            this.viewSelectionPanel = ViewSelectionPanel(p1, this.control);
            
            p2 = uipanel(this.mainPanel, 'Position', [0.11 0 0.43 1]);
            vp = ViewMultipleBraggImagesPanel(p2, this.control);
            this.control.set('viewMultipleBraggImagesPanel', vp);
            
            p3 = uipanel(this.mainPanel, 'Position', [0.55 0.4 0.45 0.5]);
            ViewMultipleBraggCurvesPanel(p3, this.control);
            
            p4 = uipanel(this.mainPanel, 'Position', [0.55 0.1 0.45 0.2]);
            SaveCurvePanel(p4, this.control);
        end
        
        function setVisible(this, visible)
            if strcmp(visible, 'on')                
                set( gcf, 'toolbar', 'figure' )
                this.viewSelectionPanel.setVisible('on');
            else
                set( gcf, 'toolbar', 'none' )
            end
            
            setVisible@MainPanel(this, visible);
        end
    end
    
    methods (Access = private)        
 
    end
end
