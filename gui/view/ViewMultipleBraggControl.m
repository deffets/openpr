
classdef ViewMultipleBraggControl < GenericViewControl
    properties (Access = private)
        viewMultipleBraggImagesPanel;
        viewMultipleBraggCurvesPanel;
    end
    
    methods (Access = public)
        function this = ViewMultipleBraggControl(model)
            this@GenericViewControl(model);

            addlistener(this.model, 'pixelPosition', 'PostSet', @this.positionListener);
            this.viewMultipleBraggImagesPanel = [];
            this.viewMultipleBraggCurvesPanel = [];
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'CData'
                    value = this.viewMultipleBraggImagesPanel.get('CData');
                case 'XData'
                    value = this.viewMultipleBraggImagesPanel.get('XData');
                case 'YData'
                    value = this.viewMultipleBraggImagesPanel.get('YData');
                case 'CLim'
                    value = this.viewMultipleBraggImagesPanel.get('CLim');
                otherwise
                    value = this.model.get(varargin{:});
            end
        end
        
        function set(this, property, value)
            switch property
                case 'viewMultipleBraggImagesPanel'
                    this.viewMultipleBraggImagesPanel = value;
                case 'viewMultipleBraggCurvesPanel'
                    this.viewMultipleBraggCurvesPanel = value;                    
                case 'PRNames'
                    this.model.set(property, value);
                    this.viewMultipleBraggImagesPanel.updatePopups();
                otherwise
                    this.model.set(property, value);
            end
        end
        
        function dataInit(this)
            this.model.updateImages();
        end
        
        function export3DCallback(this, name)
            this.model.export3DCallback(name);
        end
    end
    
    methods (Access = protected)
        function positionListener(this, source, event)
            if ~isempty(this.viewMultipleBraggImagesPanel)
                this.viewMultipleBraggImagesPanel.updatePixel();
            end
            
            if ~isempty(this.viewMultipleBraggCurvesPanel)
                this.viewMultipleBraggCurvesPanel.updateCurves();
            end
        end
    end
end
