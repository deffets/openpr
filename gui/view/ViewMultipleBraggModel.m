
classdef ViewMultipleBraggModel < GenericModel
    properties (SetObservable)
        curves;
        images;
        pixelPosition;

        PRNames;
        
        braggImages;

        image1Ind;
        image2Ind;
        
        featureName;
        featureNames;
    end
    
    properties (Access = private)
        xcurves;
        ycurves;
    end
    
    methods (Access = public)
        function this = ViewMultipleBraggModel(regguiHObject)
            this@GenericModel(regguiHObject);
            
            this.curves = [];
            this.xcurves = [];
            this.ycurves = [];

            this.pixelPosition = [];
            
            this.image1Ind = 1;
            this.image2Ind = 1;
            
            this.featureName = [];
            this.featureNames = {'Position of the maximum', 'STD error', 'Range error', 'Squared error'};
        end
        
        function value = get(this, varargin)
            property = varargin{1};
            
            switch property
                case 'PRNames'
                    value = this.PRNames;
                case 'imagesToDisplay'
                    this.updateImages();
                    value = this.images;
                case 'curves'
                    value = this.curves;
                case 'pixelPosition'
                    value = this.pixelPosition;
                    this.updateCurves();
                case 'featureName'
                    value = this.featureName;
                case 'featureNames'
                    value = this.featureNames;
                case 'dataInfo'
                    value = this.getDataInfo(varargin{2});
                otherwise
                    value = get@GenericModel(this, varargin{:});
            end
        end
        
        function set(this, property, value)
            switch property
                case 'PRNames'
                    this.PRNames = value;
                case 'pixelPosition'
                    this.pixelPosition = value;
                case 'image1Ind'
                    this.image1Ind = value;
                case 'image2Ind'
                    this.image2Ind = value;
                case 'featureName'
                    this.featureName = value;
                case 'featureNames'
                    this.featureNames = value;
            end
        end
        
        function export3DCallback(this, name)
            error('Not yet implemented for this panel.');
        end
    end
    
    methods (Access = private)
        function updateCurves(this)
            handles = guidata(this.regguiHObject);
            l = length(this.PRNames);
            
            for i=1 : l
                this.braggImages{i} = handles2BraggImage(this.PRNames{i}, handles);
                
                try
                    curve = squeeze(this.braggImages{i}.getData(this.pixelPosition));
                    xcurve = this.braggImages{i}.get('depth');
                    ycurve = curve/nanmean(curve);
                catch
                    xcurve = [];
                    ycurve = [];
                end
                
                tmp_xcurves{i} = xcurve;
                tmp_ycurves{i} = ycurve;
            end
            
            this.xcurves = tmp_xcurves;
            this.ycurves = tmp_ycurves;
            
            value{1} = this.xcurves;
            value{2} = this.ycurves;
            this.curves = value;
        end
        
        function updateImages(this)
            handles = guidata(this.regguiHObject);
            
            for i=1 : length(this.PRNames)
                bTemp{i} = handles2BraggImage(this.PRNames{i}, handles);
            end
            
            this.braggImages = bTemp;
            
            braggImage1 = handles2BraggImage(this.PRNames{this.image1Ind}, handles);   
            braggImage2 = handles2BraggImage(this.PRNames{this.image2Ind}, handles);
            
            origin1 = braggImage1.get('origin');
            origin2 = braggImage2.get('origin');
            spacing1 = braggImage1.get('spacing');
            spacing2 = braggImage2.get('spacing');
            
            if strcmp(this.featureName, 'Position of the maximum')
                im1 = maximumPosition(braggImage1.get('data'), braggImage1.get('depth'));
                im2 = maximumPosition(braggImage2.get('data'), braggImage2.get('depth'));
            else
                [worldLimit1, worldLimit2] = computeWorldLimits(braggImage1.get('XWorldLimits'), ...
                    braggImage1.get('YWorldLimits'), braggImage2.get('XWorldLimits'), ...
                    braggImage2.get('YWorldLimits'), spacing2);
                worldLimit2(1) = worldLimit2(1) - spacing2(1);
                worldLimit2(2) = worldLimit2(2) - spacing2(2);

                braggImage1Data = braggImage1.getData(worldLimit1, worldLimit2, spacing2);
                braggImage2Data = braggImage2.getData(worldLimit1, worldLimit2, spacing2);
                
                origin1 = worldLimit1;
                origin2 = worldLimit1;
                spacing1 = spacing2;

                switch this.featureName
                    case 'STD error'
                        im1 = stdError(braggImage1Data, braggImage1.get('depth'), braggImage2Data, braggImage2.get('depth'));
                        im2 = [];
                    case 'Range error'
                        im1 = rangeError(braggImage1Data, braggImage1.get('depth'), braggImage2Data, braggImage2.get('depth'), 0.1, 20);
                        im2 = [];
                    case 'Squared error'
                        im1 = squareError(braggImage1Data, braggImage1.get('depth'), braggImage2Data, braggImage2.get('depth'));
                        im2 = [];
                end
            end
                
            this.images{1} = Image2D(im1, origin1, spacing1);
            if isempty(im2)
                this.images{2} = im2;
            else
                this.images{2} = Image2D(im2, origin2, spacing2);
            end
        end
        
        function dataInfo = getDataInfo(this, infoName)
            for i=1 : length(this.braggImages)
                dataInfo{i} = this.braggImages{i}.get(infoName);
            end
        end
    end
end
