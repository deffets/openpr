<div align="center">
  <h1>openPR</h1>
  <p>This plugin analyses the integrated depth dose curves recorded by the Giraffe multi-layer ionisation chamber in order to construct a proton radiograph, i.e. a 2D-map of the measured proton range. This plugin was developped by Sylvain Deffet during his Ph.D. thesis "Proton radiography to reduce range uncertainty in proton therapy" at the UCLouvain.</p>
</div>

<hr />

## Official repository
This Gitlab repository is unmaintained and unused. Here is the official location of the [source code](https://openreggui.org/git/open/REGGUI).

## License
APACHE 2.0 © openREGGUI