
% This funnction generates md files compatible with gitlab policy from Matlab html documentation files

function publish_to_git(gitFolder)
    list = dir_rec(fullfile(getRegguiPath('help'), 'html'));

    for i=1 : length(list)
        [pathstr,  name, ext] = fileparts(list(i).name);

        if strcmp(ext, '.html')
            newname = [fullfile(gitFolder, name), '.md'];
            str = fileread(list(i).name);
            C = strsplit(str, '</style></head><body><div class="content">');
            str = C{2};
            C = strsplit(str, '<p class="footer">');
            str = C{1};
            
            str = strrep(str, '<a href="', '<a href="openPR/html/');
            
            for i=1 : 99
                str = strrep(str, ['<a href="openPR/html/#' num2str(i)], '<a href="');
            end
            
            str = strrep(str, '.html', '');
            
            str = strrep(str, '<!--introduction-->', '');
            str = strrep(str, '<!--/introduction-->', '');
            
            fid = fopen(newname,'wt');
            fprintf(fid, str);
            fclose(fid);
        end
    end
end