
% This script generates Matlab html documentation from mdoc files

options = struct('format', 'html', 'outputDir', fullfile(getRegguiPath('help'), 'html'));

list = dir_rec(getRegguiPath('openREGGUI-pr'));
            
for i=1 : length(list)
    [pathstr,  name, ext] = fileparts(list(i).name);
    
    if strcmp(ext, '.mdoc')
        newname = [fullfile(getRegguiPath('help'), 'html', name), '.m'];
        copyfile(list(i).name, newname);
        publish(newname, options);
        delete(newname);
    end
end
