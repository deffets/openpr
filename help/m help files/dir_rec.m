
function list = dir_rec(path)

    list = dir(path);
    list2 = [];
    
    for i=1 : length(list)
        if list(i).isdir && ~strcmp(list(i).name, '..') && ~strcmp(list(i).name, '.');
            
            list2 = [list2; dir_rec(fullfile(path, list(i).name))];
        end
    end
    
    for j=1 : length(list)
        list(j).name = fullfile(path, list(j).name);
    end
    
    list = [list; list2];
end

