
function outLink = readLink(property)
    outLink = [];

    helpPath = getRegguiPath('help');
    
    helpFile = fullfile(helpPath, 'help_links.txt');
    
    f = fopen(helpFile, 'r');
    
    lineT = fgetl(f);
    ind = 1;
    while ischar(lineT)
        ind1 = strfind(lineT, ' ');
        
        pathName = lineT(1:ind1-1);
        
        if strcmp(property, pathName)
        	outLink = lineT(ind1+1:end);
            break;
        end
        
        lineT = fgetl(f);
        ind = ind+1;
    end
end
